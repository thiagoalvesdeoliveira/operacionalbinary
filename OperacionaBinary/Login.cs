﻿using MongoDbCRUD.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OperacionaBinary
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        string usuarioID;

        public Login(string userID)
        {
            InitializeComponent();

            usuarioID = userID;


        }

        private LoginUserCollection db = new LoginUserCollection();

        private UsuarioCollection dbUsuario = new UsuarioCollection();

        private OperacionalCollection dbOperacional = new OperacionalCollection();

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            var user = dbUsuario.GetAllUsuario();

            var master = false;

            var senha = user.Where(c => c.Senha == txtSenha.Text).ToList();

            var email = user.Where(c => c.Email == txtEmail.Text).ToList();

            var userId = user.Find(c => c.Email == txtEmail.Text);

            //if (userId.EmailIq.Contains("thiagoadoq@gmail.com"))
            //{
            //    master = true;
            //}

            if (senha.Count > 0 && email.Count > 0)
            {
                if (txtAut.Text == userId.Autenticacao.ToString())
                {
                    var op = dbOperacional.GetAllOperacional().ToList();
                    var operacionalUser = op.Find(c => c.UsuarioId == userId.Id.ToString());

                    if (operacionalUser == null)
                    {
                        CadastroOperacional operacional = new CadastroOperacional(userId.Id.ToString(), false, master);
                        operacional.Show();
                        this.Hide();

                    }
                    else
                    {
                        Menu mainMenu = new Menu(userId.Id.ToString());
                        mainMenu.Show();
                        this.Hide();
                    }
                }
                else
                {

                    MessageBox.Show("Autenticação não valida", "Validador De Autenticação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("Email ou senha invalidos", "Validador De Autenticação", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            CadastroUsuario cadastroUsuario = new CadastroUsuario();

            cadastroUsuario.Show();
            this.Hide();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            
        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
