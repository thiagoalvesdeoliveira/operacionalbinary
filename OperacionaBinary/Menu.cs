﻿using MongoDbCRUD.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OperacionaBinary
{
    public partial class Menu : Form
    {
        string usuarioId = "";

        private OperacionalCollection db = new OperacionalCollection();
        private UsuarioCollection dbUser = new UsuarioCollection();


        public Menu()
        {
            InitializeComponent();
        }

        public Menu(string userId)
        {
           
            usuarioId = userId;
            InitializeComponent();
        }


        private void Menu_Load(object sender, EventArgs e)
        {
            txtUserId.Text = usuarioId;

            var usuario = dbUser.GetUsuarioById(usuarioId);

            if(usuario.Autenticacao == 122378 || usuario.Autenticacao == 101010101)
            {
                menuCotacao.Visible = true;
                munuPesquisa.Visible = true;
                menuAutenticacao.Visible = true;
                subNenuOTC.Visible = true;                
                nemuCadastrar.Visible = true;
            }
            else
            {
                menuCotacao.Visible = false;
                munuPesquisa.Visible = false;
                menuAutenticacao.Visible = false;
                subNenuOTC.Visible = false;                
                nemuCadastrar.Visible = false;
            }

            //var operacional = db.GetAllOperacional().ToList();

            ////ToDO: Alterar valor fixo da semana
            //var op = operacional.Find(c => c.UsuarioId.ToString() == usuarioId /*&& c.DiaSemana == 1*/); 

            //if (operacional.Count > 0)
            //{
            //    //ToDo:Ver como vai ser passado valor da banca real e nivel da operação
            //    var banca = 173.18;
            //    var nivel = 1;
            //    txtEntrada.Text = op.ValorEntrada.ToString();
            //    txtStopLoss.Text = op.StopLoss;
            //    txtStopWin.Text = op.StopWin;
            //    txtValorBReal.Text = "100.00";
            //    txtSemana.Text = op.DiaSemana.ToString();
            //    txtNivel.Text = nivel.ToString();



            //    var saldoStopLoss = Convert.ToDecimal( banca) - Convert.ToDecimal(op.StopLoss.Replace(".", ","));
            //    txtSaldoStopLoss.Text = saldoStopLoss.ToString();

            //    var saldoStopWin = Convert.ToDecimal(banca) - Convert.ToDecimal(op.StopWin.Replace(".", ","));
            //    txtSaldoStopWin.Text = saldoStopWin.ToString();

            //};
        }      

        private void cadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var useId = txtUserId.Text;

            var usuario = dbUser.GetUsuarioById(useId);

            CadastroOperacional operacional = new CadastroOperacional(usuarioId, true,false);
            operacional.Show();
            this.Hide();
            txtUserId.Text = usuarioId;

        }

        private void alteraçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var useId = txtUserId.Text;

            usuarioId = useId;

            this.Hide();
            OperacionalSemanal operacionalSemanal = new OperacionalSemanal(usuarioId, false);
            operacionalSemanal.Show();
            this.Hide();


        }

        private void tENDENCIAToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void bynariaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var useId = txtUserId.Text;

            usuarioId = useId;

            ExecultarRobo execultarRobo = new ExecultarRobo(usuarioId);
            execultarRobo.Show();
            this.Hide();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AlteracaoOperacional02 alteracaoOperacional = new AlteracaoOperacional02(txtUserId.Text,1);
            this.Hide();
            alteracaoOperacional.Show();
          
        }

        private void adicionarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AutUsuario autUsuario = new AutUsuario();
            autUsuario.Show();
        }

        private void menuAutenticacao_Click(object sender, EventArgs e)
        {

        }
    }
}
