﻿using MongoDB.Driver.Core.WireProtocol.Messages.Encoders;
using MongoDbCRUD.Models;
using OperacionaBinary.Context;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OperacionaBinary
{
    public partial class AlteracaoOperacional : Form
    {
        public AlteracaoOperacional()
        {
            InitializeComponent();
        }

        string usuarioID = "";
        int codigoSemana = 0;
        int tecnicaTrayder = 0;
        string moedaOpeacional = "";
        bool mDigital = false;
        bool mBynary = false;
        bool motcBynary = false;
        bool motcDigital = false;

        private OperacionalCollection dbOperacional = new OperacionalCollection();

        private UsuarioCollection dbUser = new UsuarioCollection();



        public AlteracaoOperacional(string userID, int codSemana, int tecSemana, string moeda, bool digital, bool binary, bool otcBinary, bool otcDigital)
        {
            InitializeComponent();

            usuarioID = userID;
            codigoSemana = codSemana;
            tecnicaTrayder = tecSemana;
            moedaOpeacional = moeda;

            mDigital = digital;
            mBynary = binary;
            motcBynary = otcBinary;
            motcDigital = otcDigital;


        }

        private void AlteracaoOperacional_Load(object sender, EventArgs e)
        {
            try
            {
                var usuario = dbUser.GetUsuarioById(usuarioID);

                var operacional = dbOperacional.GetAllOperacional();
                var paridade = "";
                var diretorio = "";


                if (usuario.Autenticacao == 122378)
                {
                    txtTraiderId.Visible = true;
                    txtTopTrayders.Visible = true;
                    txtValorMinimos.Visible = true;
                    lblValorMInimo.Visible = true;
                    lblTopTrayder.Visible = true;

                }
                else
                {
                    txtTraiderId.Visible = false;
                    txtTopTrayders.Visible = false;
                    txtValorMinimos.Visible = false;
                    lblValorMInimo.Visible = false;
                    lblTopTrayder.Visible = false;

                }

                if (mDigital)
                {

                    paridade = "Digital";
                }
                else if (mBynary)
                {

                    paridade = "Binary ";
                }
                else if (motcBynary)
                {

                    paridade = "OTC-Binary";
                }
                else if (motcDigital)
                {

                    paridade = "OTC-Digital";
                }


                if (codigoSemana == 1)
                    rd01.Checked = true;

                if (codigoSemana == 2)
                    rd02.Checked = true;

                if (codigoSemana == 3)
                    rd03.Checked = true;

                if (codigoSemana == 4)
                    rd04.Checked = true;

                if (codigoSemana == 5)
                    rd05.Checked = true;




                var operacinalSemanalDIgitalOTC = operacional.Find(c => c.UsuarioId == usuarioID && c.DiaSemana == codigoSemana && c.TrayderId == tecnicaTrayder && c.DiretorioDigital != "" && c.DiretorioDigital.Contains("OtcDigital"));

                var operacinalSemanalBynariOTC = operacional.Find(c => c.UsuarioId == usuarioID && c.DiaSemana == codigoSemana && c.TrayderId == tecnicaTrayder && c.DiretorioBinario != "" && c.DiretorioBinario.Contains("OtcBynaria"));

                if (paridade.Contains("Digital"))
                {
                    var operacinalSemanalDIgital = operacional.Find(c => c.UsuarioId == usuarioID && c.DiaSemana == codigoSemana && c.TrayderId == tecnicaTrayder && c.DiretorioDigital != "");
                    if (operacinalSemanalDIgital != null)
                    {

                        txtUserId.Text = usuarioID;
                        //txtDigital.Text = operacinalSemanalDIgital.DiretorioDigital;
                        //txtBinaria.Text = operacinalSemanalDIgital.DiretorioBinario;
                        txtEntrada.Text = operacinalSemanalDIgital.ValorEntrada.ToString();
                        txtLoss.Text = operacinalSemanalDIgital.StopLoss;
                        txtWin.Text = operacinalSemanalDIgital.StopWin;
                        txtContta.Text = operacinalSemanalDIgital.Conta.ToString();
                        txtValorMinimos.Text = operacinalSemanalDIgital.ValorMinimo.ToString();
                        txtTopTrayders.Text = operacinalSemanalDIgital.TopTryder;
                        rdDigital.Checked = mDigital;
                        rdBynary.Checked = mBynary;
                        rdotcBynary.Checked = motcBynary; ;
                        rdotcDigital.Checked = motcDigital;
                    }

                }
                else if (paridade.Contains("Binary"))
                {
                    var operacinalSemanalBynari = operacional.Find(c => c.UsuarioId == usuarioID && c.DiaSemana == codigoSemana && c.TrayderId == tecnicaTrayder && c.DiretorioBinario != "");

                    if (operacinalSemanalBynari  != null)
                    {


                        txtUserId.Text = usuarioID;
                        //txtDigital.Text = operacinalSemanalBynari.DiretorioDigital;
                        //txtBinaria.Text = operacinalSemanalBynari.DiretorioBinario;
                        txtEntrada.Text = operacinalSemanalBynari.ValorEntrada.ToString();
                        txtLoss.Text = operacinalSemanalBynari.StopLoss;
                        txtWin.Text = operacinalSemanalBynari.StopWin;
                        txtContta.Text = operacinalSemanalBynari.Conta.ToString();
                        txtValorMinimos.Text = operacinalSemanalBynari.ValorMinimo.ToString();
                        //txtTopTrayders.Text = operacinalSemanalBynari.TopTryder;
                        rdDigital.Checked = mDigital;
                        rdBynary.Checked = mBynary;
                        rdotcBynary.Checked = motcBynary; ;
                        rdotcDigital.Checked = motcDigital;
                    }

                }

                if (paridade.Contains("OTC-Binary"))
                {
                    txtUserId.Text = usuarioID;
                    //txtDigital.Text = operacinalSemanalBynari.DiretorioDigital;
                    //txtBinaria.Text = operacinalSemanalBynari.DiretorioBinario;
                    txtEntrada.Text = operacinalSemanalBynariOTC.ValorEntrada.ToString();
                    txtLoss.Text = operacinalSemanalBynariOTC.StopLoss;
                    txtWin.Text = operacinalSemanalBynariOTC.StopWin;
                    txtContta.Text = operacinalSemanalBynariOTC.Conta.ToString();
                    txtValorMinimos.Text = operacinalSemanalBynariOTC.ValorMinimo.ToString();
                    //txtTopTrayders.Text = operacinalSemanalBynari.TopTryder;
                    rdDigital.Checked = mDigital;
                    rdBynary.Checked = mBynary;
                    rdotcBynary.Checked = motcBynary; ;
                    rdotcDigital.Checked = motcDigital;
                }

                if (paridade.Contains("OTC-Digital"))
                {

                    txtUserId.Text = usuarioID;
                    //txtDigital.Text = operacinalSemanalBynari.DiretorioDigital;
                    //txtBinaria.Text = operacinalSemanalBynari.DiretorioBinario;
                    txtEntrada.Text = operacinalSemanalDIgitalOTC.ValorEntrada.ToString();
                    txtLoss.Text = operacinalSemanalDIgitalOTC.StopLoss;
                    txtWin.Text = operacinalSemanalDIgitalOTC.StopWin;
                    txtContta.Text = operacinalSemanalDIgitalOTC.Conta.ToString();
                    txtValorMinimos.Text = operacinalSemanalDIgitalOTC.ValorMinimo.ToString();
                    //txtTopTrayders.Text = operacinalSemanalBynari.TopTryder;
                    rdDigital.Checked = mDigital;
                    rdBynary.Checked = mBynary;
                    rdotcBynary.Checked = motcBynary; ;
                    rdotcDigital.Checked = motcDigital;

                }

                if (txtEntrada.Text == "")
                {
                    MessageBox.Show("Não Foi Possivel Localizar Operacional!", "Operacional", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                    
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Não Foi Possivel Localizar Operacional!", "Operacional", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }

        }

        private void btnNovoO_Click(object sender, EventArgs e)
        {
            OperacionalSemanal operacionalSemanal = new OperacionalSemanal(usuarioID);
            operacionalSemanal.Show();
            this.Hide();
            btnAlterar.Enabled = false;
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            var diaSemana = 0;
            var operacional = dbOperacional.GetAllOperacional();
            var operacionalUser = operacional.FindAll(c => c.UsuarioId == usuarioID && c.DiaSemana == codigoSemana && c.TrayderId == tecnicaTrayder).ToList();

            var operacionalDigital = "";
            var operacionalBynari = "";

            var operacinalSemanalDIgital = operacionalUser.Where(c => c.DiretorioDigital != "").FirstOrDefault();
            var operacinalSemanalBynari = operacionalUser.Where(c => c.DiretorioBinario != "").FirstOrDefault();
            //var operacinalSemanalBynariOTC = operacionalUser.Where(c => c.DiretorioBinario.Contains("OtcBynaria")).FirstOrDefault();
            //var operacinalSemanalDIgitalOTC = operacionalUser.Where(c => c.DiretorioDigital.Contains("OtcDigital")).FirstOrDefault();
            //var operacionalOTCBynaria = operacinalSemanalBynariOTC.DiretorioBinario;
            //var operacionalOTCDigital = operacinalSemanalDIgitalOTC.DiretorioDigital;

            if (operacinalSemanalDIgital != null)
            {
                operacionalDigital = operacinalSemanalDIgital.DiretorioDigital;
            }
            if (operacinalSemanalBynari != null)
            {
                operacionalBynari = operacinalSemanalBynari.DiretorioBinario;
            }

            var usuarioDTO = dbUser.GetUsuarioById(usuarioID);


            if (rd01.Checked)
            {
                diaSemana = 1;
            }
            else if (rd02.Checked)
            {
                diaSemana = 2;
            }
            else if (rd03.Checked)
            {
                diaSemana = 3;
            }
            else if (rd04.Checked)
            {
                diaSemana = 4;
            }
            else if (rd05.Checked)
            {
                diaSemana = 5;
            }

            if (rdBynary.Checked)
            {
                CadastroOperacional operacional1 = new CadastroOperacional();

                var operacionalAdd = new OperacionalBynari()
                {
                    DiretorioDigital = operacinalSemanalDIgital.DiretorioBinario,
                    ValorEntrada = Convert.ToInt32(txtValorEntrada.Text.Replace(".00", "")),
                    StopLoss = txtStopLoss.Text,
                    StopWin = txtStopWin.Text,
                    DataCadastro = operacinalSemanalDIgital.DataCadastro,
                    UsuarioId = usuarioDTO.Id.ToString(),
                    DiaSemana = diaSemana,
                    Conta = txtConta.Text,
                    TopTryder = txtTop.Text,
                    ValorMinimo = Convert.ToInt32(txtValorMinimo.Text.Replace(".00", "")),
                    TrayderId = operacinalSemanalDIgital.TrayderId
                };
                             
               
                operacional1.Alterar("", "", operacionalBynari, operacionalDigital, operacionalAdd.Conta, operacionalAdd.ValorEntrada, operacionalAdd.StopLoss, operacionalAdd.StopWin, operacionalAdd.TrayderId, operacionalAdd.ValorMinimo, operacionalAdd.TopTryder, mDigital, mBynary, motcBynary, motcDigital, usuarioDTO.Senha_IQ, usuarioDTO.Email_IQ);               
                dbOperacional.UpdateContact(operacionalAdd.Id.ToString(), operacionalAdd);

                MessageBox.Show("Operaciol Alterado com sucesso!", "Operacional", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

            }
            else
            {
                CadastroOperacional operacional1 = new CadastroOperacional();            

                var operacionalAdd = new OperacionalBynari
                {
                    DiretorioDigital = operacinalSemanalDIgital.DiretorioDigital,                    
                    ValorEntrada = Convert.ToInt32(txtValorEntrada.Text.Replace(".00", "")),
                    StopLoss = txtStopLoss.Text,
                    StopWin = txtStopWin.Text,
                    DataCadastro =operacinalSemanalDIgital.DataCadastro,
                    UsuarioId = usuarioDTO.Id.ToString(),
                    DiaSemana = diaSemana,
                    Conta = txtConta.Text,
                    TopTryder = txtTop.Text,
                    ValorMinimo = Convert.ToInt32(txtValorMinimo.Text.Replace(".00", "")),
                    TrayderId = operacinalSemanalDIgital.TrayderId
                };

                operacional1.Alterar("", "", operacionalBynari, operacionalDigital, operacionalAdd.Conta, operacionalAdd.ValorEntrada, operacionalAdd.StopLoss, operacionalAdd.StopWin, operacionalAdd.TrayderId, operacionalAdd.ValorMinimo, operacionalAdd.TopTryder, mDigital, mBynary, motcBynary, motcDigital, usuarioDTO.Senha_IQ, usuarioDTO.Email_IQ);                
                dbOperacional.UpdateContact(operacionalAdd.Id.ToString(), operacionalAdd);
                MessageBox.Show("Operaciol Alterado com sucesso!", "Operacional", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            txtUserId.Text = usuarioID;
            Menu mainMenu = new Menu(usuarioID);
            mainMenu.Show();
        }

        private void txtEntrada_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtStopLoss.Focus();
            }
        }

        private void txtLoss_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtStopWin.Focus();
            }
        }

        private void txtWin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtContta.Focus();
            }
        }
        private void txtContta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtValorMinimos.Focus();
            }
        }
        private void txtValorMinimos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtTraiderId.Focus();
            }
        }
    }
}
