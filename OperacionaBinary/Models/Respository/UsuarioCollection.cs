﻿using MongoDB.Driver;
using MongoDB.Bson;
using MongoDbCRUD.Models.Respository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MongoDbCRUD.Models
{
    // this class is our Product Collection (Product Table)
    public class UsuarioCollection
    {
       
        internal MongoDBRespository _repo = new MongoDBRespository();
        public IMongoCollection<Usuario> Collection;

        public UsuarioCollection()
        {
            // here we were created new Collection with Products name
            this.Collection = _repo.db.GetCollection<Usuario>("Usuarios");
        }

        public void InsertContact(Usuario contact)
        {
            this.Collection.InsertOneAsync(contact);
        }
       
        public List<Usuario> GetAllUsuario()
        {
            var query = this.Collection
                .Find(new BsonDocument())
                .ToListAsync();
            return query.Result;
        }
       
        public Usuario GetUsuarioById(string id)
        {
            var product = this.Collection.Find(
                    new BsonDocument { { "_id", new ObjectId(id) } })
                    .FirstAsync()
                    .Result;
            return product;
        }
       
        public void UpdateContact(string id, Usuario contact)
        {
            contact.Id = new ObjectId(id);

            var filter = Builders<Usuario>
                .Filter
                .Eq(s => s.Id, contact.Id);
            this.Collection.ReplaceOneAsync(filter, contact);

        }

        public void DeleteContact(string usuarioId)
        {
            Usuario contact = new Usuario();
            contact.Id = new ObjectId(usuarioId);
            var filter = Builders<Usuario>.Filter.Eq(s => s.Id, contact.Id);
            this.Collection.DeleteOneAsync(filter);

        }
    }
}