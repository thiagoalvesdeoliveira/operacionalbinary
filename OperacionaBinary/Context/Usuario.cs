﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MongoDbCRUD.Models
{
    public class Usuario
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [Required]
        [BsonElement("Nome")]
        public string Nome { get; set; }

        [Required]
        [BsonElement("Cpf")]
        public string Cpf { get; set; }

        [Required]
        [BsonElement("Email")]
        public string Email { get; set; }

        [Required]
        [BsonElement("Senha")]
        public string Senha { get; set; }

       
        [BsonElement("SenhaConfirme")]
        public string SenhaConfirme { get; set; }

        [Required]
        [BsonElement("Email_IQ")]
        public string Email_IQ { get; set; }

        [Required]
        [BsonElement("Senha_IQ")]
        public string Senha_IQ { get; set; }

       
        [BsonElement("SenhaConfirme_IQ")]
        public string SenhaConfirme_IQ { get; set; }

        
        [BsonElement("Conta")]
        public string Conta { get; set; }

        [Required]
        [BsonElement("DataCadastro")]
        public DateTime DataCadastro { get; set; }

        
        [BsonElement("Autenticacao")]
        public int Autenticacao { get; set; }
    }
}