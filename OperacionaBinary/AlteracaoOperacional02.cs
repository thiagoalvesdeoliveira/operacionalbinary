﻿using MongoDB.Driver.Core.WireProtocol.Messages.Encoders;
using MongoDbCRUD.Models;
using OperacionaBinary.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OperacionaBinary
{
    public partial class AlteracaoOperacional02 : Form
    {
        private UsuarioCollection dbUsuario = new UsuarioCollection();
        private OperacionalCollection dbOperacional = new OperacionalCollection();

        public AlteracaoOperacional02()
        {
            InitializeComponent();
        }

        string usuarioId = "";
        int diaSemana = 1;
        public AlteracaoOperacional02(string userID, int diasema)
        {
            InitializeComponent();
            usuarioId = userID;
            diaSemana = diasema;

        }

        private void btnNovo_Click(object sender, EventArgs e)
        {


        }

        private void DeleteOperacional_Load(object sender, EventArgs e)
        {
            var listaUsuario = dbUsuario.GetAllUsuario().ToList();
            var user = listaUsuario.FirstOrDefault(c => c.Id.ToString() == usuarioId);
            var listaOperacional = dbOperacional.GetAllOperacional().ToList();

            foreach (var item in listaUsuario)
            {
                cmUser.Items.Add(item.Nome);
            }

            cmUser.Text = user.Nome;
            txtUserNome.Text = user.Nome;

            //dataGriOperacional.DataSource = listaOperacional.Where(c => c.UsuarioId == usuarioId).ToList();
            //dataGriOperacional.Columns["ID"].HeaderText = "OperacionalID"; // altera cabeçalho da coluna
            ////dataGriOperacional.Columns["Diretorio"].HeaderText = "OperacionalID"; // altera cabeçalho da coluna
            if (cmUser.Text != "")
            {
                var userDTO = listaUsuario.Find(c => c.Nome == cmUser.Text);

                foreach (var itemO in listaOperacional.FindAll(c => c.UsuarioId == userDTO.Id.ToString() && c.DiaSemana == diaSemana))
                {
                    if (itemO.DiretorioDigital != "")
                    {
                        cmOperacional.Items.Add(itemO.DiretorioDigital);
                        txtDiretorioDigital.Text = itemO.DiretorioDigital;
                        txtValorEntrada.Text = itemO.ValorEntrada.ToString();
                        txtStopLoss.Text = itemO.StopLoss;
                        txtStopWin.Text = itemO.StopWin;
                        txtSemana.Text = itemO.DiaSemana.ToString();
                        txtTrayderID.Text = itemO.TrayderId.ToString();
                        txtUserID.Text = itemO.UsuarioId.ToString();
                        txtOperacionalID.Text = itemO.Id.ToString();
                        cmOperacional.Items.Remove(itemO);
                        txtDiaSemana.Text = diaSemana.ToString();

                    }
                    if (itemO.DiretorioBinario != "")
                    {
                        cmOperacional.Items.Add(itemO.DiretorioBinario);
                        txtDiretorioBinaria.Text = itemO.DiretorioDigital;
                        txtValorEntrada.Text = itemO.ValorEntrada.ToString();
                        txtStopLoss.Text = itemO.StopLoss;
                        txtStopWin.Text = itemO.StopWin;
                        txtSemana.Text = itemO.DiaSemana.ToString();
                        txtTrayderID.Text = itemO.TrayderId.ToString();
                        txtUserID.Text = itemO.UsuarioId.ToString();
                        txtOperacionalID.Text = itemO.Id.ToString();
                        cmOperacional.Items.Remove(itemO);
                        txtDiaSemana.Text = diaSemana.ToString();
                    }
                }

                if (!listaOperacional.Any(c => c.UsuarioId.ToString() == userDTO.Id.ToString()))
                {
                    MessageBox.Show("Não Contem Operacional Para Esse Usuario");

                }
            }
            else
            {
                MessageBox.Show("Antes De Fazer A Pesquisa Precisa Selecionar Um Usúario");
            }

        }

        private void cmOperacional_SelectedIndexChanged(object sender, EventArgs e)
        {
            var listaOperacional = dbOperacional.GetAllOperacional().ToList();

            var operacionalUserBinari = listaOperacional.Where(c => c.DiretorioBinario.Contains($"{cmOperacional.Text}")).FirstOrDefault();
            var operacionalUserDigital = listaOperacional.Where(c => c.DiretorioDigital.Contains($"{cmOperacional.Text}")).FirstOrDefault();

            if (operacionalUserBinari != null)
            {
                txtDiretorioBinaria.Text = operacionalUserBinari.DiretorioBinario;
                txtOperacionalID.Text = operacionalUserBinari.Id.ToString();


            }
            else
            {
                txtDiretorioDigital.Text = operacionalUserDigital.DiretorioDigital;
                txtOperacionalID.Text = operacionalUserDigital.Id.ToString();

            }

        }


        private void dataGriOperacional_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var listaOperacional = dbOperacional.GetAllOperacional().ToList();
            var listaUsuario = dbUsuario.GetAllUsuario().ToList();
            var userDTO = listaUsuario.Find(c => c.Nome == cmUser.Text);

            foreach (var itemO in listaOperacional.FindAll(c => c.UsuarioId == userDTO.Id.ToString()))
            {
                if (itemO.DiretorioDigital != "")
                {
                    cmOperacional.Items.Add(itemO.DiretorioDigital);
                    txtDiretorioDigital.Text = itemO.DiretorioDigital;
                    txtValorEntrada.Text = itemO.ValorEntrada.ToString();
                    txtStopLoss.Text = itemO.StopLoss;
                    txtStopWin.Text = itemO.StopWin;
                    txtSemana.Text = itemO.DiaSemana.ToString();
                    txtTrayderID.Text = itemO.TrayderId.ToString();
                    txtUserID.Text = itemO.UsuarioId.ToString();
                }
                else if (itemO.DiretorioBinario != "")
                {
                    cmOperacional.Items.Add(itemO.DiretorioBinario);
                    txtDiretorioBinaria.Text = itemO.DiretorioDigital;
                    txtValorEntrada.Text = itemO.ValorEntrada.ToString();
                    txtStopLoss.Text = itemO.StopLoss;
                    txtStopWin.Text = itemO.StopWin;
                    txtSemana.Text = itemO.DiaSemana.ToString();
                    txtTrayderID.Text = itemO.TrayderId.ToString();
                    txtUserID.Text = itemO.UsuarioId.ToString();

                }
            }
        }

        public void limpaCampos()
        {
            txtDiretorioDigital.Clear();
            txtValorEntrada.Clear();
            txtStopLoss.Clear();
            txtStopWin.Clear();
            txtSemana.Clear();
            txtTrayderID.Clear();
            chDigital.Checked = false;
            chBinaria.Checked = false;

        }

        private void dataGriOperacional_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            var listaOperacional = dbOperacional.GetAllOperacional().ToList();
            var listaUsuario = dbUsuario.GetAllUsuario().ToList();
            var userDTO = listaUsuario.Find(c => c.Nome == cmUser.Text);

            foreach (var itemO in listaOperacional.FindAll(c => c.UsuarioId == usuarioId))
            {
                if (itemO.DiretorioDigital != "")
                {
                    cmOperacional.Items.Add(itemO.DiretorioDigital);
                    txtDiretorioDigital.Text = itemO.DiretorioDigital;
                    txtValorEntrada.Text = itemO.ValorEntrada.ToString();
                    txtStopLoss.Text = itemO.StopLoss;
                    txtStopWin.Text = itemO.StopWin;
                    txtSemana.Text = itemO.DiaSemana.ToString();
                    txtTrayderID.Text = itemO.TrayderId.ToString();
                    txtUserID.Text = itemO.UsuarioId.ToString();
                }
                else if (itemO.DiretorioBinario != "")
                {
                    cmOperacional.Items.Add(itemO.DiretorioBinario);
                    txtDiretorioBinaria.Text = itemO.DiretorioDigital;
                    txtValorEntrada.Text = itemO.ValorEntrada.ToString();
                    txtStopLoss.Text = itemO.StopLoss;
                    txtStopWin.Text = itemO.StopWin;
                    txtSemana.Text = itemO.DiaSemana.ToString();
                    txtTrayderID.Text = itemO.TrayderId.ToString();
                    txtUserID.Text = itemO.UsuarioId.ToString();

                }

            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var listaOperacional = dbOperacional.GetAllOperacional().ToList();
            var operacionalUserDigital = listaOperacional.FirstOrDefault(c => c.DiretorioBinario.Contains(cmOperacional.Text) && c.UsuarioId == usuarioId);
            var operacionalUserBynaria = listaOperacional.FirstOrDefault(c => c.DiretorioDigital.Contains(cmOperacional.Text) && c.UsuarioId == usuarioId);

            if (operacionalUserDigital != null)
            {
                txtDiretorioDigital.Text = operacionalUserDigital.DiretorioDigital;
                txtDiretorioBinaria.Text = operacionalUserDigital.DiretorioBinario;
                txtValorEntrada.Text = operacionalUserDigital.ValorEntrada.ToString();
                txtStopLoss.Text = operacionalUserDigital.StopLoss;
                txtStopWin.Text = operacionalUserDigital.StopWin;
                txtSemana.Text = operacionalUserDigital.DiaSemana.ToString();
                txtTrayderID.Text = operacionalUserDigital.TrayderId.ToString();
                txtUserID.Text = operacionalUserDigital.UsuarioId.ToString();
                txtOperacionalID.Text = operacionalUserDigital.Id.ToString();
            }

            if (operacionalUserBynaria != null)
            {
                txtDiretorioDigital.Text = operacionalUserBynaria.DiretorioDigital;
                txtDiretorioBinaria.Text = operacionalUserBynaria.DiretorioBinario;
                txtValorEntrada.Text = operacionalUserBynaria.ValorEntrada.ToString();
                txtStopLoss.Text = operacionalUserBynaria.StopLoss;
                txtStopWin.Text = operacionalUserBynaria.StopWin;
                txtSemana.Text = operacionalUserBynaria.DiaSemana.ToString();
                txtTrayderID.Text = operacionalUserBynaria.TrayderId.ToString();
                txtUserID.Text = operacionalUserBynaria.UsuarioId.ToString();
                txtOperacionalID.Text = operacionalUserBynaria.Id.ToString();
            }

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            var listaUsuario = dbUsuario.GetAllUsuario().ToList();
            var userDTO = listaUsuario.Find(c => c.Nome == cmUser.Text);


            var operacionalUserBinari = dbOperacional.GetOperacionalById(txtOperacionalID.Text.ToString());
            var operacionalUserDigital = dbOperacional.GetOperacionalById(txtOperacionalID.Text.ToString());

            var listaOperacional = dbOperacional.GetAllOperacional();
            var listaUser = listaOperacional.Where(c => c.UsuarioId == usuarioId).ToList();

            foreach (var item in listaUser)
            {
                if (item.DiretorioDigital.Contains("RoboDigital"))
                {
                    var operacionalAdd = new OperacionalBynari()
                    {
                        DiretorioDigital = item.DiretorioDigital,
                        DiretorioBinario = item.DiretorioBinario,
                        ValorEntrada = Convert.ToInt32(txtValorEntrada.Text.Replace(".00", "")),
                        StopLoss = txtStopLoss.Text,
                        StopWin = txtStopWin.Text,
                        DataCadastro = item.DataCadastro,
                        UsuarioId = userDTO.Id.ToString(),
                        DiaSemana = Convert.ToInt32(txtSemana.Text),
                        Conta = item.Conta,
                        ValorMinimo = 0,
                        TrayderId = item.TrayderId,
                        TopTryder = "0",
                    };

                    chDigital.Checked = true;
                    chBinaria.Checked = false;
                    CadastroOperacional cadastroOperacional = new CadastroOperacional();

                    cadastroOperacional.Alterar("", "", operacionalAdd.DiretorioBinario, operacionalAdd.DiretorioDigital, operacionalAdd.Conta, operacionalAdd.ValorEntrada, operacionalAdd.StopLoss, operacionalAdd.StopWin, operacionalAdd.TrayderId, operacionalAdd.ValorMinimo, operacionalAdd.TopTryder, chDigital.Checked, chBinaria.Checked, false, false, userDTO.Senha_IQ, userDTO.Email_IQ);
                    dbOperacional.UpdateContact(item.Id.ToString(), operacionalAdd);
                    MessageBox.Show($"Operaciol={item.DiretorioBinario}-{item.DiretorioDigital} Alterado com sucesso!", "Operacional", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    var operacionalAdd = new OperacionalBynari()
                    {
                        DiretorioDigital = item.DiretorioDigital,
                        DiretorioBinario = item.DiretorioBinario,
                        ValorEntrada = Convert.ToInt32(txtValorEntrada.Text.Replace(".00", "")),
                        StopLoss = txtStopLoss.Text,
                        StopWin = txtStopWin.Text,
                        DataCadastro = item.DataCadastro,
                        UsuarioId = userDTO.Id.ToString(),
                        DiaSemana = Convert.ToInt32(txtSemana.Text),
                        Conta = item.Conta,
                        ValorMinimo = 0,
                        TrayderId = item.TrayderId,
                        TopTryder = "0",
                    };

                    chBinaria.Checked = true;
                    chDigital.Checked = false;
                    CadastroOperacional cadastroOperacional = new CadastroOperacional();

                    cadastroOperacional.Alterar("", "", operacionalAdd.DiretorioBinario, operacionalAdd.DiretorioDigital, operacionalAdd.Conta, operacionalAdd.ValorEntrada, operacionalAdd.StopLoss, operacionalAdd.StopWin, operacionalAdd.TrayderId, operacionalAdd.ValorMinimo, operacionalAdd.TopTryder, chDigital.Checked, chBinaria.Checked, false, false, userDTO.Senha_IQ, userDTO.Email_IQ);
                    dbOperacional.UpdateContact(item.Id.ToString(), operacionalAdd);
                    MessageBox.Show($"Operaciol={item.DiretorioBinario}-{item.DiretorioDigital} Alterado com sucesso!", "Operacional", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                }
            }

            limpaCampos();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            limpaCampos();
            cmOperacional.Items.Clear();
        }

        private void cmOperacional_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            GetOperacionalUser();

        }

        private void GetOperacionalUser()
        {

            var diaSemana = Convert.ToInt32(txtDiaSemana.Text);
            var listaOperacional = dbOperacional.GetAllOperacional().ToList();
            var operacionalUserDigital = listaOperacional.FirstOrDefault(c => c.DiretorioBinario.Contains(cmOperacional.Text) && c.UsuarioId == usuarioId);
            var operacionalUserBynaria = listaOperacional.FirstOrDefault(c => c.DiretorioDigital.Contains(cmOperacional.Text) && c.UsuarioId == usuarioId);

            if (operacionalUserDigital != null)
            {
                txtDiretorioDigital.Text = operacionalUserDigital.DiretorioDigital;
                txtDiretorioBinaria.Text = operacionalUserDigital.DiretorioBinario;
                txtValorEntrada.Text = operacionalUserDigital.ValorEntrada.ToString();
                txtStopLoss.Text = operacionalUserDigital.StopLoss;
                txtStopWin.Text = operacionalUserDigital.StopWin;
                txtSemana.Text = operacionalUserDigital.DiaSemana.ToString();
                txtTrayderID.Text = operacionalUserDigital.TrayderId.ToString();
                txtUserID.Text = operacionalUserDigital.UsuarioId.ToString();
                txtOperacionalID.Text = operacionalUserDigital.Id.ToString();
            }

            if (operacionalUserBynaria != null)
            {
                txtDiretorioDigital.Text = operacionalUserBynaria.DiretorioDigital;
                txtDiretorioBinaria.Text = operacionalUserBynaria.DiretorioBinario;
                txtValorEntrada.Text = operacionalUserBynaria.ValorEntrada.ToString();
                txtStopLoss.Text = operacionalUserBynaria.StopLoss;
                txtStopWin.Text = operacionalUserBynaria.StopWin;
                txtSemana.Text = operacionalUserBynaria.DiaSemana.ToString();
                txtTrayderID.Text = operacionalUserBynaria.TrayderId.ToString();
                txtUserID.Text = operacionalUserBynaria.UsuarioId.ToString();
                txtOperacionalID.Text = operacionalUserBynaria.Id.ToString();
            }

            if (operacionalUserDigital == null && operacionalUserBynaria == null)
            {
                MessageBox.Show("Não Contem Operacional Para Esse Dia Da Semana!");

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Menu mainMenu = new Menu(usuarioId);
            this.Hide();
            mainMenu.Show();
        }


        private void btbPsquisa_Click(object sender, EventArgs e)
        {
            var listaOperacional = dbOperacional.GetAllOperacional().ToList();
            var listaUsuario = dbUsuario.GetAllUsuario().ToList();
            var userDTO = listaUsuario.Find(c => c.Nome == cmUser.Text);


            foreach (var itemO in listaOperacional.FindAll(c => c.UsuarioId == usuarioId))
            {
                if (itemO.DiretorioDigital != "")
                {
                    cmOperacional.Items.Add(itemO.DiretorioDigital);
                    txtDiretorioDigital.Text = itemO.DiretorioDigital;
                    txtValorEntrada.Text = itemO.ValorEntrada.ToString();
                    txtStopLoss.Text = itemO.StopLoss;
                    txtStopWin.Text = itemO.StopWin;
                    txtSemana.Text = itemO.DiaSemana.ToString();
                    txtTrayderID.Text = itemO.TrayderId.ToString();
                    txtUserID.Text = itemO.UsuarioId.ToString();
                }
                else if (itemO.DiretorioBinario != "")
                {
                    cmOperacional.Items.Add(itemO.DiretorioBinario);
                    txtDiretorioBinaria.Text = itemO.DiretorioDigital;
                    txtValorEntrada.Text = itemO.ValorEntrada.ToString();
                    txtStopLoss.Text = itemO.StopLoss;
                    txtStopWin.Text = itemO.StopWin;
                    txtSemana.Text = itemO.DiaSemana.ToString();
                    txtTrayderID.Text = itemO.TrayderId.ToString();
                    txtUserID.Text = itemO.UsuarioId.ToString();

                }

            }

        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            AlteracaoOperacional02 op = new AlteracaoOperacional02(usuarioId, Convert.ToInt32(txtDiaSemana.Text));
            this.Hide();
            op.Show();
        }

        private void btnNovo_Click_1(object sender, EventArgs e)
        {
            try
            {
                dbOperacional.DeleteContact(txtOperacionalID.Text.ToString());

                MessageBox.Show("Operacional Deletado Com SUcesso");
            }
            catch (Exception)
            {
                MessageBox.Show("Não foi Possivel Deletar Operacional");

            }

        }

        private void button6_Click(object sender, EventArgs e)
        {
            var listaUsuario = dbUsuario.GetAllUsuario().ToList();
            var userDTO = listaUsuario.Find(c => c.Nome == txtUserNome.Text);
            var operacionalUserBinari = dbOperacional.GetOperacionalById(txtOperacionalId01.Text.ToString());
            var operacionalUserDigital = dbOperacional.GetOperacionalById(txtOperacionalId01.Text.ToString());
            var listaOperacional = dbOperacional.GetAllOperacional();
            var listaUser = listaOperacional.Where(c => c.UsuarioId == usuarioId).ToList();


            if (operacionalUserDigital.DiretorioDigital.Contains("RoboDigital"))
            {
                var operacionalAdd = new OperacionalBynari()
                {
                    DiretorioDigital = operacionalUserDigital.DiretorioDigital,
                    DiretorioBinario = operacionalUserDigital.DiretorioBinario,
                    ValorEntrada = Convert.ToInt32(txtValorEntarda01.Text.Replace(".00", "")),
                    StopLoss = txtStopLoss01.Text,
                    StopWin = txtStopWin01.Text,
                    DataCadastro = operacionalUserDigital.DataCadastro,
                    UsuarioId = userDTO.Id.ToString(),
                    DiaSemana = Convert.ToInt32(txtDiaSemanaCad.Text),
                    Conta = operacionalUserDigital.Conta,
                    ValorMinimo = 0,
                    TrayderId = operacionalUserDigital.TrayderId,
                    TopTryder = "0",
                };

                chDigital01.Checked = true;
                chBynaria01.Checked = false;
                CadastroOperacional cadastroOperacional = new CadastroOperacional();

                cadastroOperacional.Alterar("", "", operacionalAdd.DiretorioBinario, operacionalAdd.DiretorioDigital, operacionalAdd.Conta, operacionalAdd.ValorEntrada, operacionalAdd.StopLoss, operacionalAdd.StopWin, operacionalAdd.TrayderId, operacionalAdd.ValorMinimo, operacionalAdd.TopTryder, chDigital01.Checked, chBynaria01.Checked, false, false, userDTO.Senha_IQ, userDTO.Email_IQ);
                dbOperacional.UpdateContact(operacionalUserDigital.Id.ToString(), operacionalAdd);

                MessageBox.Show($"Operaciol={operacionalUserDigital.DiretorioBinario}-{operacionalUserDigital.DiretorioDigital} Alterado com sucesso!", "Operacional", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            else
            {
                var operacionalAdd = new OperacionalBynari()
                {
                    DiretorioDigital = operacionalUserBinari.DiretorioDigital,
                    DiretorioBinario = operacionalUserBinari.DiretorioBinario,
                    ValorEntrada = Convert.ToInt32(txtValorEntarda01.Text.Replace(".00", "")),
                    StopLoss = txtStopLoss01.Text,
                    StopWin = txtStopWin01.Text,
                    DataCadastro = operacionalUserBinari.DataCadastro,
                    UsuarioId = userDTO.Id.ToString(),
                    DiaSemana = Convert.ToInt32(txtDiaSemanaCad.Text),
                    Conta = operacionalUserBinari.Conta,
                    ValorMinimo = 0,
                    TrayderId = operacionalUserBinari.TrayderId,
                    TopTryder = "0",
                };

                chBynaria01.Checked = true;
                chDigital01.Checked = false;
                CadastroOperacional cadastroOperacional = new CadastroOperacional();

                cadastroOperacional.Alterar("", "", operacionalAdd.DiretorioBinario, operacionalAdd.DiretorioDigital, operacionalAdd.Conta, operacionalAdd.ValorEntrada, operacionalAdd.StopLoss, operacionalAdd.StopWin, operacionalAdd.TrayderId, operacionalAdd.ValorMinimo, operacionalAdd.TopTryder, chDigital01.Checked, chBynaria01.Checked, false, false, userDTO.Senha_IQ, userDTO.Email_IQ);
                dbOperacional.UpdateContact(operacionalUserBinari.Id.ToString(), operacionalAdd);

                MessageBox.Show($"Operaciol={operacionalUserBinari.DiretorioBinario}-{operacionalUserBinari.DiretorioDigital} Alterado com sucesso!", "Operacional", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

            }

            limpaCampos();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var listaUsuario = dbUsuario.GetAllUsuario().ToList();
            var user = listaUsuario.FirstOrDefault(c => c.Id.ToString() == usuarioId);
            var listaOperacional = dbOperacional.GetAllOperacional().ToList();

            cmUser.Text = user.Nome;

            if (cmUser.Text != "")
            {
                var userDTO = listaUsuario.Find(c => c.Nome == cmUser.Text);

                foreach (var itemO in listaOperacional.FindAll(c => c.UsuarioId == userDTO.Id.ToString() && c.DiaSemana.ToString() == txtDiaSemana01.Text))
                {
                    if (itemO.DiretorioDigital != "")
                    {
                        cmOperacinal01.Items.Add(itemO.DiretorioDigital);
                        txtDiretorioDigtla01.Text = itemO.DiretorioDigital;
                        txtValorEntarda01.Text = itemO.ValorEntrada.ToString();
                        txtStopLoss01.Text = itemO.StopLoss;
                        txtStopWin01.Text = itemO.StopWin;
                        txtDiaSemanaCad.Text = itemO.DiaSemana.ToString();
                        txtTrayderID01.Text = itemO.TrayderId.ToString();
                        txtUser01.Text = itemO.UsuarioId.ToString();
                        txtOperacionalId01.Text = itemO.Id.ToString();
                        txtDiaSemana.Text = diaSemana.ToString();
                        chBynaria01.Checked = false;
                        chDigital01.Checked = true;

                    }
                    if (itemO.DiretorioBinario != "")
                    {
                        cmOperacinal01.Items.Add(itemO.DiretorioBinario);
                        txtDiretorioBynaria01.Text = itemO.DiretorioBinario;
                        txtValorEntarda01.Text = itemO.ValorEntrada.ToString();
                        txtStopLoss01.Text = itemO.StopLoss;
                        txtStopWin01.Text = itemO.StopWin;
                        txtDiaSemanaCad.Text = itemO.DiaSemana.ToString();
                        txtTrayderID01.Text = itemO.TrayderId.ToString();
                        txtUser01.Text = itemO.UsuarioId.ToString();
                        txtOperacionalId01.Text = itemO.Id.ToString();
                        txtDiaSemana.Text = diaSemana.ToString();
                        chBynaria01.Checked = true;
                        chDigital01.Checked = false;
                    }
                }

                if (!listaOperacional.Any(c => c.UsuarioId.ToString() == userDTO.Id.ToString()))
                {
                    MessageBox.Show("Não Contem Operacional Para Esse Usuario");
                }
            }
            else
            {
                MessageBox.Show("Usúario não contem Operacional");
            }
        }

        private void cmOperacinal01_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetOperacionalUserIndividual();
        }

        private void GetOperacionalUserIndividual()
        {
            var diaSemana = Convert.ToInt32(txtDiaSemana.Text);
            var listaOperacional = dbOperacional.GetAllOperacional().ToList();
            var operacionalUserDigital = listaOperacional.FirstOrDefault(c => c.DiretorioDigital.Contains(cmOperacinal01.Text) && c.UsuarioId == usuarioId);
            var operacionalUserBynaria = listaOperacional.FirstOrDefault(c => c.DiretorioBinario.Contains(cmOperacinal01.Text) && c.UsuarioId == usuarioId);

            if (operacionalUserDigital != null)
            {
                cmOperacinal01.Items.Add(operacionalUserDigital.DiretorioDigital);
                txtDiretorioDigtla01.Text = operacionalUserDigital.DiretorioDigital;
                txtValorEntarda01.Text = operacionalUserDigital.ValorEntrada.ToString();
                txtStopLoss01.Text = operacionalUserDigital.StopLoss;
                txtStopWin01.Text = operacionalUserDigital.StopWin;
                txtDiaSemanaCad.Text = operacionalUserDigital.DiaSemana.ToString();
                txtTrayderID01.Text = operacionalUserDigital.TrayderId.ToString();
                txtUser01.Text = operacionalUserDigital.UsuarioId.ToString();
                txtOperacionalId01.Text = operacionalUserDigital.Id.ToString();
                txtDiaSemana.Text = diaSemana.ToString();
                chBynaria01.Checked = false;
                chDigital01.Checked = true;
            }

            if (operacionalUserBynaria != null)
            {
                cmOperacinal01.Items.Add(operacionalUserBynaria.DiretorioDigital);
                txtDiretorioDigtla01.Text = operacionalUserBynaria.DiretorioDigital;
                txtValorEntarda01.Text = operacionalUserBynaria.ValorEntrada.ToString();
                txtStopLoss01.Text = operacionalUserBynaria.StopLoss;
                txtStopWin01.Text = operacionalUserBynaria.StopWin;
                txtDiaSemanaCad.Text = operacionalUserBynaria.DiaSemana.ToString();
                txtTrayderID01.Text = operacionalUserBynaria.TrayderId.ToString();
                txtUser01.Text = operacionalUserBynaria.UsuarioId.ToString();
                txtOperacionalId01.Text = operacionalUserBynaria.Id.ToString();
                txtDiaSemana.Text = diaSemana.ToString();
                chBynaria01.Checked = false;
                chDigital01.Checked = true;
            }

            if (operacionalUserDigital == null && operacionalUserBynaria == null)
            {
                MessageBox.Show("Não Contem Operacional Para Esse Dia Da Semana!");

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Menu mainMenu = new Menu(usuarioId);
            this.Hide();
            mainMenu.Show();
        }
    }
}
