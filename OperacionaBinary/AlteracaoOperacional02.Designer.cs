﻿namespace OperacionaBinary
{
    partial class AlteracaoOperacional02
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cmOperacional = new System.Windows.Forms.ComboBox();
            this.txtDiaSemana = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.chBinaria = new System.Windows.Forms.CheckBox();
            this.chDigital = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtOperacionalID = new System.Windows.Forms.TextBox();
            this.txtTrayderID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSemana = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDiretorioDigital = new System.Windows.Forms.TextBox();
            this.txtStopLoss = new System.Windows.Forms.TextBox();
            this.txtValorEntrada = new System.Windows.Forms.TextBox();
            this.txtStopWin = new System.Windows.Forms.TextBox();
            this.txtDiretorioBinaria = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.cmUser = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtUserNome = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.cmOperacinal01 = new System.Windows.Forms.ComboBox();
            this.txtDiaSemana01 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.chBynaria01 = new System.Windows.Forms.CheckBox();
            this.chDigital01 = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtOperacionalId01 = new System.Windows.Forms.TextBox();
            this.txtTrayderID01 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDiaSemanaCad = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtUser01 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtDiretorioDigtla01 = new System.Windows.Forms.TextBox();
            this.txtStopLoss01 = new System.Windows.Forms.TextBox();
            this.txtValorEntarda01 = new System.Windows.Forms.TextBox();
            this.txtStopWin01 = new System.Windows.Forms.TextBox();
            this.txtDiretorioBynaria01 = new System.Windows.Forms.TextBox();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.cmOperacional);
            this.tabPage2.Controls.Add(this.txtDiaSemana);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.btnDelete);
            this.tabPage2.Controls.Add(this.cmUser);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1181, 583);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "OPERACIONAL";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button2.Location = new System.Drawing.Point(866, 50);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 31);
            this.button2.TabIndex = 111;
            this.button2.Text = "Buscar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(791, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 20);
            this.label4.TabIndex = 110;
            this.label4.Text = "Dia Semana";
            // 
            // cmOperacional
            // 
            this.cmOperacional.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmOperacional.FormattingEnabled = true;
            this.cmOperacional.Location = new System.Drawing.Point(136, 50);
            this.cmOperacional.Name = "cmOperacional";
            this.cmOperacional.Size = new System.Drawing.Size(639, 32);
            this.cmOperacional.TabIndex = 90;
            this.cmOperacional.SelectedIndexChanged += new System.EventHandler(this.cmOperacional_SelectedIndexChanged_1);
            // 
            // txtDiaSemana
            // 
            this.txtDiaSemana.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaSemana.Location = new System.Drawing.Point(795, 52);
            this.txtDiaSemana.Name = "txtDiaSemana";
            this.txtDiaSemana.Size = new System.Drawing.Size(65, 29);
            this.txtDiaSemana.TabIndex = 109;
            this.txtDiaSemana.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 20);
            this.label1.TabIndex = 91;
            this.label1.Text = "OPERACIONAL";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.chBinaria);
            this.panel1.Controls.Add(this.chDigital);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtOperacionalID);
            this.panel1.Controls.Add(this.txtTrayderID);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtSemana);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtUserID);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txtDiretorioDigital);
            this.panel1.Controls.Add(this.txtStopLoss);
            this.panel1.Controls.Add(this.txtValorEntrada);
            this.panel1.Controls.Add(this.txtStopWin);
            this.panel1.Controls.Add(this.txtDiretorioBinaria);
            this.panel1.Location = new System.Drawing.Point(10, 88);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(926, 439);
            this.panel1.TabIndex = 92;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button1.Location = new System.Drawing.Point(0, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 32);
            this.button1.TabIndex = 108;
            this.button1.Text = "<Voltar Menu";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chBinaria
            // 
            this.chBinaria.AutoSize = true;
            this.chBinaria.Enabled = false;
            this.chBinaria.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chBinaria.Location = new System.Drawing.Point(799, 404);
            this.chBinaria.Name = "chBinaria";
            this.chBinaria.Size = new System.Drawing.Size(91, 28);
            this.chBinaria.TabIndex = 107;
            this.chBinaria.Text = "Bynaria";
            this.chBinaria.UseVisualStyleBackColor = true;
            // 
            // chDigital
            // 
            this.chDigital.AutoSize = true;
            this.chDigital.Enabled = false;
            this.chDigital.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chDigital.Location = new System.Drawing.Point(678, 404);
            this.chDigital.Name = "chDigital";
            this.chDigital.Size = new System.Drawing.Size(79, 28);
            this.chDigital.TabIndex = 106;
            this.chDigital.Text = "Digital";
            this.chDigital.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(115, 343);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 20);
            this.label5.TabIndex = 103;
            this.label5.Text = "OperacionalID";
            this.label5.Visible = false;
            // 
            // txtOperacionalID
            // 
            this.txtOperacionalID.Enabled = false;
            this.txtOperacionalID.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOperacionalID.Location = new System.Drawing.Point(244, 337);
            this.txtOperacionalID.Name = "txtOperacionalID";
            this.txtOperacionalID.Size = new System.Drawing.Size(594, 29);
            this.txtOperacionalID.TabIndex = 101;
            this.txtOperacionalID.Visible = false;
            // 
            // txtTrayderID
            // 
            this.txtTrayderID.Enabled = false;
            this.txtTrayderID.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrayderID.Location = new System.Drawing.Point(600, 206);
            this.txtTrayderID.Name = "txtTrayderID";
            this.txtTrayderID.Size = new System.Drawing.Size(225, 29);
            this.txtTrayderID.TabIndex = 100;
            this.txtTrayderID.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(670, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 20);
            this.label3.TabIndex = 99;
            this.label3.Text = "Dia Semana";
            // 
            // txtSemana
            // 
            this.txtSemana.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSemana.Location = new System.Drawing.Point(773, 29);
            this.txtSemana.Name = "txtSemana";
            this.txtSemana.Size = new System.Drawing.Size(65, 29);
            this.txtSemana.TabIndex = 98;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(144, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 20);
            this.label2.TabIndex = 97;
            this.label2.Text = "UsúarioID";
            // 
            // txtUserID
            // 
            this.txtUserID.Enabled = false;
            this.txtUserID.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserID.Location = new System.Drawing.Point(244, 32);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(404, 29);
            this.txtUserID.TabIndex = 96;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Enabled = false;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(515, 209);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 20);
            this.label16.TabIndex = 88;
            this.label16.Text = "TrayderID";
            this.label16.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Enabled = false;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(164, 262);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 20);
            this.label15.TabIndex = 87;
            this.label15.Text = "Stop Win";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Enabled = false;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(157, 212);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 20);
            this.label14.TabIndex = 86;
            this.label14.Text = "Stop Loss";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Enabled = false;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(131, 166);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 20);
            this.label13.TabIndex = 85;
            this.label13.Text = "Valor Entrada";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Enabled = false;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(112, 120);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 20);
            this.label12.TabIndex = 84;
            this.label12.Text = "Diretorio Bynaria";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Enabled = false;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(121, 72);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 20);
            this.label11.TabIndex = 83;
            this.label11.Text = "Diretorio Digital";
            // 
            // txtDiretorioDigital
            // 
            this.txtDiretorioDigital.Enabled = false;
            this.txtDiretorioDigital.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiretorioDigital.Location = new System.Drawing.Point(244, 67);
            this.txtDiretorioDigital.Name = "txtDiretorioDigital";
            this.txtDiretorioDigital.Size = new System.Drawing.Size(594, 29);
            this.txtDiretorioDigital.TabIndex = 79;
            // 
            // txtStopLoss
            // 
            this.txtStopLoss.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStopLoss.Location = new System.Drawing.Point(244, 206);
            this.txtStopLoss.Name = "txtStopLoss";
            this.txtStopLoss.Size = new System.Drawing.Size(225, 29);
            this.txtStopLoss.TabIndex = 2;
            // 
            // txtValorEntrada
            // 
            this.txtValorEntrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorEntrada.Location = new System.Drawing.Point(244, 160);
            this.txtValorEntrada.Name = "txtValorEntrada";
            this.txtValorEntrada.Size = new System.Drawing.Size(225, 29);
            this.txtValorEntrada.TabIndex = 1;
            // 
            // txtStopWin
            // 
            this.txtStopWin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStopWin.Location = new System.Drawing.Point(244, 256);
            this.txtStopWin.Name = "txtStopWin";
            this.txtStopWin.Size = new System.Drawing.Size(225, 29);
            this.txtStopWin.TabIndex = 3;
            // 
            // txtDiretorioBinaria
            // 
            this.txtDiretorioBinaria.Enabled = false;
            this.txtDiretorioBinaria.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiretorioBinaria.Location = new System.Drawing.Point(244, 114);
            this.txtDiretorioBinaria.Name = "txtDiretorioBinaria";
            this.txtDiretorioBinaria.Size = new System.Drawing.Size(594, 29);
            this.txtDiretorioBinaria.TabIndex = 80;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button3.Location = new System.Drawing.Point(807, 533);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(102, 47);
            this.button3.TabIndex = 4;
            this.button3.Text = "ALTERAR";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(46, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(84, 20);
            this.label20.TabIndex = 87;
            this.label20.Text = "USUARIO";
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnDelete.Location = new System.Drawing.Point(673, 533);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(102, 47);
            this.btnDelete.TabIndex = 88;
            this.btnDelete.Text = "DELETAR";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnNovo_Click_1);
            // 
            // cmUser
            // 
            this.cmUser.Enabled = false;
            this.cmUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmUser.FormattingEnabled = true;
            this.cmUser.Location = new System.Drawing.Point(136, 7);
            this.cmUser.Name = "cmUser";
            this.cmUser.Size = new System.Drawing.Size(639, 32);
            this.cmUser.TabIndex = 86;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1189, 609);
            this.tabControl1.TabIndex = 85;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tabPage1.Controls.Add(this.txtUserNome);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.cmOperacinal01);
            this.tabPage1.Controls.Add(this.txtDiaSemana01);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.btnAlterar);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1181, 583);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "INDIVIDUAL";
            // 
            // txtUserNome
            // 
            this.txtUserNome.Enabled = false;
            this.txtUserNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserNome.Location = new System.Drawing.Point(204, 7);
            this.txtUserNome.Name = "txtUserNome";
            this.txtUserNome.Size = new System.Drawing.Size(639, 32);
            this.txtUserNome.TabIndex = 122;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button4.Location = new System.Drawing.Point(934, 46);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(70, 31);
            this.button4.TabIndex = 121;
            this.button4.Text = "Buscar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Enabled = false;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(859, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 20);
            this.label6.TabIndex = 120;
            this.label6.Text = "Dia Semana";
            // 
            // cmOperacinal01
            // 
            this.cmOperacinal01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmOperacinal01.FormattingEnabled = true;
            this.cmOperacinal01.Location = new System.Drawing.Point(204, 46);
            this.cmOperacinal01.Name = "cmOperacinal01";
            this.cmOperacinal01.Size = new System.Drawing.Size(639, 32);
            this.cmOperacinal01.TabIndex = 116;
            this.cmOperacinal01.SelectedIndexChanged += new System.EventHandler(this.cmOperacinal01_SelectedIndexChanged);
            // 
            // txtDiaSemana01
            // 
            this.txtDiaSemana01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaSemana01.Location = new System.Drawing.Point(863, 48);
            this.txtDiaSemana01.Name = "txtDiaSemana01";
            this.txtDiaSemana01.Size = new System.Drawing.Size(65, 29);
            this.txtDiaSemana01.TabIndex = 119;
            this.txtDiaSemana01.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(76, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 20);
            this.label7.TabIndex = 117;
            this.label7.Text = "OPERACIONAL";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.button5);
            this.panel2.Controls.Add(this.chBynaria01);
            this.panel2.Controls.Add(this.chDigital01);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.txtOperacionalId01);
            this.panel2.Controls.Add(this.txtTrayderID01);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.txtDiaSemanaCad);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.txtUser01);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.txtDiretorioDigtla01);
            this.panel2.Controls.Add(this.txtStopLoss01);
            this.panel2.Controls.Add(this.txtValorEntarda01);
            this.panel2.Controls.Add(this.txtStopWin01);
            this.panel2.Controls.Add(this.txtDiretorioBynaria01);
            this.panel2.Location = new System.Drawing.Point(78, 84);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(926, 439);
            this.panel2.TabIndex = 118;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button5.Location = new System.Drawing.Point(0, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(118, 32);
            this.button5.TabIndex = 108;
            this.button5.Text = "<Voltar Menu";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // chBynaria01
            // 
            this.chBynaria01.AutoSize = true;
            this.chBynaria01.Enabled = false;
            this.chBynaria01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chBynaria01.Location = new System.Drawing.Point(799, 404);
            this.chBynaria01.Name = "chBynaria01";
            this.chBynaria01.Size = new System.Drawing.Size(91, 28);
            this.chBynaria01.TabIndex = 107;
            this.chBynaria01.Text = "Bynaria";
            this.chBynaria01.UseVisualStyleBackColor = true;
            // 
            // chDigital01
            // 
            this.chDigital01.AutoSize = true;
            this.chDigital01.Enabled = false;
            this.chDigital01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chDigital01.Location = new System.Drawing.Point(678, 404);
            this.chDigital01.Name = "chDigital01";
            this.chDigital01.Size = new System.Drawing.Size(79, 28);
            this.chDigital01.TabIndex = 106;
            this.chDigital01.Text = "Digital";
            this.chDigital01.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(115, 309);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 20);
            this.label8.TabIndex = 103;
            this.label8.Text = "OperacionalID";
            // 
            // txtOperacionalId01
            // 
            this.txtOperacionalId01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOperacionalId01.Location = new System.Drawing.Point(244, 303);
            this.txtOperacionalId01.Name = "txtOperacionalId01";
            this.txtOperacionalId01.Size = new System.Drawing.Size(594, 29);
            this.txtOperacionalId01.TabIndex = 101;
            // 
            // txtTrayderID01
            // 
            this.txtTrayderID01.Enabled = false;
            this.txtTrayderID01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrayderID01.Location = new System.Drawing.Point(600, 206);
            this.txtTrayderID01.Name = "txtTrayderID01";
            this.txtTrayderID01.Size = new System.Drawing.Size(238, 29);
            this.txtTrayderID01.TabIndex = 100;
            this.txtTrayderID01.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Enabled = false;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(670, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 20);
            this.label9.TabIndex = 99;
            this.label9.Text = "Dia Semana";
            // 
            // txtDiaSemanaCad
            // 
            this.txtDiaSemanaCad.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaSemanaCad.Location = new System.Drawing.Point(773, 29);
            this.txtDiaSemanaCad.Name = "txtDiaSemanaCad";
            this.txtDiaSemanaCad.Size = new System.Drawing.Size(65, 29);
            this.txtDiaSemanaCad.TabIndex = 98;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Enabled = false;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(144, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 20);
            this.label10.TabIndex = 97;
            this.label10.Text = "UsúarioID";
            // 
            // txtUser01
            // 
            this.txtUser01.Enabled = false;
            this.txtUser01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser01.Location = new System.Drawing.Point(244, 32);
            this.txtUser01.Name = "txtUser01";
            this.txtUser01.Size = new System.Drawing.Size(404, 29);
            this.txtUser01.TabIndex = 96;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Enabled = false;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(515, 209);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 20);
            this.label17.TabIndex = 88;
            this.label17.Text = "TrayderID";
            this.label17.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Enabled = false;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(164, 262);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 20);
            this.label18.TabIndex = 87;
            this.label18.Text = "Stop Win";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Enabled = false;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(157, 212);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(81, 20);
            this.label19.TabIndex = 86;
            this.label19.Text = "Stop Loss";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Enabled = false;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(131, 166);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(107, 20);
            this.label21.TabIndex = 85;
            this.label21.Text = "Valor Entrada";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Enabled = false;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(112, 120);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(126, 20);
            this.label22.TabIndex = 84;
            this.label22.Text = "Diretorio Bynaria";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Enabled = false;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(121, 72);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(117, 20);
            this.label23.TabIndex = 83;
            this.label23.Text = "Diretorio Digital";
            // 
            // txtDiretorioDigtla01
            // 
            this.txtDiretorioDigtla01.Enabled = false;
            this.txtDiretorioDigtla01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiretorioDigtla01.Location = new System.Drawing.Point(244, 67);
            this.txtDiretorioDigtla01.Name = "txtDiretorioDigtla01";
            this.txtDiretorioDigtla01.Size = new System.Drawing.Size(594, 29);
            this.txtDiretorioDigtla01.TabIndex = 79;
            // 
            // txtStopLoss01
            // 
            this.txtStopLoss01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStopLoss01.Location = new System.Drawing.Point(244, 206);
            this.txtStopLoss01.Name = "txtStopLoss01";
            this.txtStopLoss01.Size = new System.Drawing.Size(225, 29);
            this.txtStopLoss01.TabIndex = 2;
            // 
            // txtValorEntarda01
            // 
            this.txtValorEntarda01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorEntarda01.Location = new System.Drawing.Point(244, 160);
            this.txtValorEntarda01.Name = "txtValorEntarda01";
            this.txtValorEntarda01.Size = new System.Drawing.Size(225, 29);
            this.txtValorEntarda01.TabIndex = 1;
            // 
            // txtStopWin01
            // 
            this.txtStopWin01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStopWin01.Location = new System.Drawing.Point(244, 256);
            this.txtStopWin01.Name = "txtStopWin01";
            this.txtStopWin01.Size = new System.Drawing.Size(225, 29);
            this.txtStopWin01.TabIndex = 3;
            // 
            // txtDiretorioBynaria01
            // 
            this.txtDiretorioBynaria01.Enabled = false;
            this.txtDiretorioBynaria01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiretorioBynaria01.Location = new System.Drawing.Point(244, 114);
            this.txtDiretorioBynaria01.Name = "txtDiretorioBynaria01";
            this.txtDiretorioBynaria01.Size = new System.Drawing.Size(594, 29);
            this.txtDiretorioBynaria01.TabIndex = 80;
            // 
            // btnAlterar
            // 
            this.btnAlterar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnAlterar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnAlterar.Location = new System.Drawing.Point(875, 529);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(102, 47);
            this.btnAlterar.TabIndex = 112;
            this.btnAlterar.Text = "ALTERAR";
            this.btnAlterar.UseVisualStyleBackColor = false;
            this.btnAlterar.Click += new System.EventHandler(this.button6_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(76, 14);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(84, 20);
            this.label24.TabIndex = 114;
            this.label24.Text = "USUARIO";
            // 
            // AlteracaoOperacional02
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1188, 619);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.Name = "AlteracaoOperacional02";
            this.Text = "Alteração Operacional";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.DeleteOperacional_Load);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cmOperacional;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chBinaria;
        private System.Windows.Forms.CheckBox chDigital;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtOperacionalID;
        private System.Windows.Forms.TextBox txtTrayderID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSemana;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtDiretorioDigital;
        private System.Windows.Forms.TextBox txtStopLoss;
        private System.Windows.Forms.TextBox txtValorEntrada;
        private System.Windows.Forms.TextBox txtStopWin;
        private System.Windows.Forms.TextBox txtDiretorioBinaria;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ComboBox cmUser;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDiaSemana;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmOperacinal01;
        private System.Windows.Forms.TextBox txtDiaSemana01;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.CheckBox chBynaria01;
        private System.Windows.Forms.CheckBox chDigital01;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtOperacionalId01;
        private System.Windows.Forms.TextBox txtTrayderID01;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDiaSemanaCad;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtUser01;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtDiretorioDigtla01;
        private System.Windows.Forms.TextBox txtStopLoss01;
        private System.Windows.Forms.TextBox txtValorEntarda01;
        private System.Windows.Forms.TextBox txtStopWin01;
        private System.Windows.Forms.TextBox txtDiretorioBynaria01;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtUserNome;
    }
}