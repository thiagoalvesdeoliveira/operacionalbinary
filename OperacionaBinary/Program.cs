﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OperacionaBinary
{
    static class Program
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            //Application.Run(new CadastroUsuario());
            //Application.Run(new Operacional());
            //Application.Run(new Menu());
            //Application.Run(new ListOperacional());
            Application.Run(new Login());
            //Application.Run(new EscolherParidade());
            //Application.Run(new OperacionalSemanal());
            //Application.Run(new ExecultarRobo());
            //Application.Run(new AutUsuario());

        }
    }
}
