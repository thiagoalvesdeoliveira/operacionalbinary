﻿using MongoDB.Driver;


namespace MongoDbCRUD.Models.Respository
{
    public class MongoDBRespository
    {
        // MongoClient is used for connect to server 
        public MongoClient client;
        // IMongoDatabase interface is used for database transactions
        public IMongoDatabase db;

        public MongoDBRespository()
        {
            //here we are connected to server 
            this.client = new MongoClient("mongodb+srv://lilikarepilica27:lilikarepilica27@cluster0.sstxx.mongodb.net/<dbname>?retryWrites=true&w=majority"/*"mongodb://localhost:27017"*/);
            // if database is not exist we create new one with ExampleMongoDB name
            this.db = this.client.GetDatabase("RoboBynari");
        }

    }
}