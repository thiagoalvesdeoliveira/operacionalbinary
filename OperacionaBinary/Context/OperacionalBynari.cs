﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace OperacionaBinary.Context
{
    public class OperacionalBynari
    {

        [BsonId]
        public ObjectId Id { get; set; }

       
        [BsonElement("DiretorioDigital")]
        public string DiretorioDigital { get; set; }

        [Required]
        [BsonElement("DiretorioBinario")]
        public string DiretorioBinario { get; set; }

        [Required]
        [BsonElement("ValorEntrada")]
        public int ValorEntrada { get; set; }

        [Required]
        [BsonElement("StopLoss")]
        public string StopLoss { get; set; }

        [Required]
        [BsonElement("StopWin")]
        public string StopWin { get; set; }

       
        [Required]
        [BsonElement("DataCadastro")]
        public DateTime DataCadastro { get; set; }

        [Required]
        [BsonElement("TrayderId")]
        public int TrayderId { get; set; }

        [Required]
        [BsonElement("UsuarioId")]
        public string UsuarioId { get; set; }

        [Required]
        [BsonElement("DiaSemana")]
        public int DiaSemana { get; set; }

        
        [BsonElement("Conta")]
        public string Conta { get; set; }

        [BsonElement("TopTryder")]
        public string TopTryder { get; set; }


        [BsonElement("Banca")]
        public decimal Banca { get; set; }


        
        [BsonElement("ValorMinimo")]
        public int ValorMinimo { get; set; }

  
    }
}
