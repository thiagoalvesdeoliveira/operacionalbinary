﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperacionaBinary.Context.Repository
{
    public class Aut
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [Required]
        [BsonElement("Token")]
        public string Token { get; set; }
    }
}
