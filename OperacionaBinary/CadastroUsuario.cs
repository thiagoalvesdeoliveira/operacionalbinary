﻿using MongoDB.Driver.Core.WireProtocol.Messages.Encoders;
using MongoDbCRUD.Models;
using System;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Windows.Forms;

namespace OperacionaBinary
{
    public partial class CadastroUsuario : Form
    {
        public CadastroUsuario()
        {
            InitializeComponent();
        }

        private UsuarioCollection db = new UsuarioCollection();

        private void CadastroUsuario_Load(object sender, EventArgs e)
        {
            txtNome.Focus();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (txtNome.Text == "" && txtSenha.Text == "" && txtEmail.Text == "" && txtEmail_IQ.Text == "" && txtSenha_IQ.Text == "" && txtConfirme_IQ.Text == "")
            {
                MessageBox.Show("Contem Campos Que Não Foi Informado Os Valores", "Validador De Cadastro Usúario", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var dataUser = db.GetAllUsuario();

                var emailValidetion = db.GetAllUsuario();

                if (emailValidetion.Any(c => c.Email == txtEmail_IQ.Text))
                {
                    MessageBox.Show("Email Digitado Já Cadastrado", "Validador De Email", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEmail_IQ.Clear();
                    txtEmail_IQ.Focus();
                }
                else
                {
                    if (!txtEmail_IQ.Text.Contains("@") && txtEmail_IQ.Text != "")
                    {

                        MessageBox.Show("Email Digitado Está incorreto", "Validador De Email", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtEmail_IQ.Clear();
                        txtEmail_IQ.Focus();
                    }
                    else
                    {

                        var data = db.GetAllUsuario();
                        var existUsuario = data.Find(c => c.Email == txtEmail.Text);

                        var numeroAutenticate = "";

                        for (int i = 0; i <= 1; i++)
                        {
                            Random random = new Random();
                            numeroAutenticate = numeroAutenticate + random.Next(4, 1000).ToString();
                        }

                        if (existUsuario == null)
                        {
                            var usuarioAdd = new Usuario
                            {
                                Nome = txtNome.Text,
                                Email = txtEmail.Text,
                                Senha = txtSenha.Text,
                                Email_IQ = txtEmail_IQ.Text,
                                Senha_IQ = txtSenha_IQ.Text,
                                Conta = txtConta.Text,
                                DataCadastro = DateTime.Now,
                                Autenticacao = Convert.ToInt32(numeroAutenticate)
                            };

                            try
                            {
                                db.InsertContact(usuarioAdd);

                                MessageBox.Show("Registro salvo com sucesso!");
                               
                                MessageBox.Show($"Sejá Bem Vindo '{txtNome.Text}' AO Robô Binario Office", "Cadastro Usúario", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                                var user = db.GetAllUsuario();
                                var userID = user.Find(c => c.Email == txtEmail.Text);

                                Login login = new Login(userID.Id.ToString());
                                login.Show();
                                this.Hide();
                            }
                            catch (Exception)
                            {
                                MessageBox.Show("Não foi Possivel Efetuar O Cadastro", "Validador De Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Usúario Já Cadastrato Em Nossa Base De Dados", "Validador De Usúario", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }                   

                }                
            }         
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            var usuarioAPut = new Usuario
            {
                Nome = txtNome.Text,
                Conta = txtConta.Text,

            };

            var user = db.GetAllUsuario();
            var usuario = user.Find(c => c.Email == txtEmail_IQ.Text);

            if (usuario != null)
            {
                db.UpdateContact(usuario.Id.ToString(), usuarioAPut);
                var allProduct = db.GetAllUsuario();
            }
            else
            {
                MessageBox.Show("Não foi possivel localizar usuario");
            }
        }

        private void btnCalcelar_Click(object sender, EventArgs e)
        {
            Menu mainMenu = new Menu();
            mainMenu.Show();
            this.Hide();
        }


        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtEmail.Focus();
            }
        }

        private void txtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtSenha.Focus();

            }
        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtConfirmeSenha.Focus();

            }
        }

        private void txtConfirmeSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtEmail_IQ.Focus();
            }
        }

        private void txtEmail_IQ_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtSenha_IQ.Focus();
            }
        }

        private void txtSenha_IQ_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtConfirme_IQ.Focus();
            }
        }

        private void txtConfirme_IQ_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                btnSalvar.Focus();
            }
        }
    }
}
