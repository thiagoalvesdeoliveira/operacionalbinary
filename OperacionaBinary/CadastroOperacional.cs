﻿using MongoDbCRUD.Models;
using OperacionaBinary.Context;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace OperacionaBinary
{
    public partial class CadastroOperacional : Form
    {
        public CadastroOperacional()
        {
            InitializeComponent();
        }

        private UsuarioCollection dbUsuario = new UsuarioCollection();
        private OperacionalCollection dbOperacional = new OperacionalCollection();

        string usuarioId = "";
        bool cadastrarOp = false;
        bool cadastrarOperaciolaSema = false;
        int semanaMes = 0;
        int tecnicaTrayder = 0;
        bool masterUser = true;


        public CadastroOperacional(string userId, bool cadOperacional, bool master)
        {
            InitializeComponent();

            cadastrarOp = cadOperacional;
            var userAll = dbUsuario.GetUsuarioById(userId.ToString());
            txtUserId.Text = userAll.Id.ToString();
            usuarioId = userAll.Id.ToString();
            masterUser = master;

        }

        public CadastroOperacional(int semana, string usuariario_ID, bool cadOperacionalSema, int tecnica)
        {
            var userAll = dbUsuario.GetUsuarioById(usuariario_ID.ToString());
            usuarioId = userAll.Id.ToString();

            InitializeComponent();
            semanaMes = semana;
            cadastrarOperaciolaSema = cadOperacionalSema;
            tecnicaTrayder = tecnica;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var listaUsuarioDTO = dbUsuario.GetAllUsuario().ToList();

            foreach (var item in listaUsuarioDTO)
            {
                cmUser.Items.Add(item.Nome);
            }       

            btnNovo.Enabled = false;
            var semanaMes = 0;
            if (semanaMes == 1)
                semanaMes = 1;
            else if (semanaMes == 2)
                semanaMes = 2;
            else if (semanaMes == 3)
                semanaMes = 3;
            else if (semanaMes == 4)
                semanaMes = 4;

            var userOper = dbUsuario.GetAllUsuario();

            var userOp = userOper.Find(c => c.Id.ToString() == usuarioId);

            var op = dbOperacional.GetAllOperacional().ToList();

            var operacionalUser = op.Find(c => c.UsuarioId == userOp.Id.ToString());

            if (operacionalUser != null && cadastrarOp != false && cadastrarOperaciolaSema != false)
            {
                Menu mainMenu = new Menu();
                mainMenu.Show();
                this.Hide();
            }

            //Verificação se já tem no banco de dados operacional cadastrado
            if (semanaMes != 0)
            {
                var opSemana = op.Find(c => c.DiaSemana == semanaMes && c.TrayderId == Convert.ToInt32(cmTraiderId.Text));
                txtDiretorioDigital.Text = opSemana.DiretorioDigital;
                txtDiretorioBinaria.Text = opSemana.DiretorioBinario;
                txtConta.Text = opSemana.Conta;
                txtStopLoss.Text = opSemana.StopLoss;
                txtStopWin.Text = opSemana.StopWin;
                txtValorEntrada.Text = opSemana.ValorEntrada.ToString();
                txtConta.Text = opSemana.Conta;
                cmTraiderId.Text = opSemana.TrayderId.ToString();
                txtConta.Text = opSemana.Conta;
                txtValorMinimo.Text = opSemana.ValorMinimo.ToString();
                rdSegunda.Checked = true;
                rdTerca.Enabled = false;
                rdQuarta.Enabled = false;
                rdQuinta.Enabled = false;
                rdSexta.Enabled = false;
                this.Hide();
            }

        }

        List<string> listEndereco = new List<string>();

        public void Alterar(string diretorioBinaryOTC, string diretorDigitalOTC,string diretorioBinary, string diretorDigital, string conta, int valorEntrada, string stopLOss, string stopWin, int trayderID, int valorMinimo, string topTryder, bool digital, bool binary, bool otcBinary, bool otcDigital, string senha_IQ, string email_IQ)
        {
            var diretorioDigital = diretorDigital;
            var diretoDigitalOTC  = diretorDigitalOTC;

            var diretorioOpBynari = diretorioBinary;
            var diretoOpBynariOTC = diretorioBinaryOTC;


            if (otcDigital == true)
            {
                var diretorioOTCBynari = txtDiretorioBinaria.Text;

                listEndereco.Add($@"{diretorioDigital}\EUR-USD\config.txt");
                listEndereco.Add($@"{diretorioDigital}\GBP-USD\config.txt");
                listEndereco.Add($@"{diretorioDigital}\GBP-JPY\config.txt");
                listEndereco.Add($@"{diretorioDigital}\USD-JPY\config.txt");
            }

            if (otcBinary == true)
            {
                var diretorioOTCBynari = txtDiretorioBinaria.Text;

                listEndereco.Add($@"{diretoOpBynariOTC}\AUD-CAD\config.txt");
                listEndereco.Add($@"{diretoOpBynariOTC}\EUR-GBP\config.txt");
                listEndereco.Add($@"{diretoOpBynariOTC}\EUR-USD\config.txt");
                listEndereco.Add($@"{diretoOpBynariOTC}\GBP-USD\config.txt");
                listEndereco.Add($@"{diretoOpBynariOTC}\NZD-USD\config.txt");
                listEndereco.Add($@"{diretoOpBynariOTC}\USD-CHF\config.txt");
            }
           

            if (binary == true)
            {

                listEndereco.Add($@"{diretorioOpBynari}\EUR-USD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\EUR-AUD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\EUR-CAD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\EUR-GBP\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\EUR-JPY\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\EUR-CHF\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\EUR-NZD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\GBP-AUD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\GBP-CAD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\GBP-CHF\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\GBP-JPY\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\GBP-NZD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\GBP-USD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\AUD-JPY\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\AUD-USD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\AUD-NZD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\AUD-CHF\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\AUD-CAD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\USD-CAD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\USD-CHF\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\CAD-CHF\config.txt");
                listEndereco.Add($@"{diretorioOpBynari}\NZD-USD\config.txt");

            }

            if (digital == true)
            {
                listEndereco.Add($@"{diretorioDigital}\AUD-CAD\config.txt");
                listEndereco.Add($@"{diretorioDigital}\AUD-JPY\config.txt");
                listEndereco.Add($@"{diretorioDigital}\AUD-USD\config.txt");
                listEndereco.Add($@"{diretorioDigital}\EUR-AUD\config.txt");
                listEndereco.Add($@"{diretorioDigital}\EUR-USD\config.txt");
                listEndereco.Add($@"{diretorioDigital}\EUR-CAD\config.txt");
                listEndereco.Add($@"{diretorioDigital}\EUR-GBP\config.txt");
                listEndereco.Add($@"{diretorioDigital}\EUR-JPY\config.txt");
                listEndereco.Add($@"{diretorioDigital}\GBP-AUD\config.txt");
                listEndereco.Add($@"{diretorioDigital}\GBP-CAD\config.txt");
                listEndereco.Add($@"{diretorioDigital}\GBP-CHF\config.txt");
                listEndereco.Add($@"{diretorioDigital}\GBP-JPY\config.txt");
                listEndereco.Add($@"{diretorioDigital}\GBP-NZD\config.txt");
                listEndereco.Add($@"{diretorioDigital}\GBP-USD\config.txt");
                listEndereco.Add($@"{diretorioDigital}\USD-CAD\config.txt");
                listEndereco.Add($@"{diretorioDigital}\USD-CHF\config.txt");
                listEndereco.Add($@"{diretorioDigital}\USD-JPY\config.txt");
                listEndereco.Add($@"{diretorioDigital}\CAD-JPY\config.txt");

            }


            try
            {

                foreach (var item in listEndereco)
                {

                    //Verifico se o arquivo que desejo abrir existe e passo como parâmetro a variável respectiva

                    if (File.Exists(item))
                    {

                        //Instancio o FileStream passando como parâmetro a variável padrão, o FileMode que será

                        //o modo Open e o FileAccess, que será Read(somente leitura). Este método é diferente dos

                        //demais: primeiro irei abrir o arquivo, depois criar um FileStream temporário que irá

                        //armazenar os novos dados e depois criarei outro FileStream para fazer a junção dos dois

                        using (FileStream fs = new FileStream(item, FileMode.Open, FileAccess.Read))
                        {

                            //Aqui instancio o StreamReader passando como parâmetro o FileStream criado acima.

                            //Uso o StreamReader já que faço 1º a leitura do arquivo. Irei percorrer o arquivo e

                            //quando encontrar uma string qualquer farei a alteração por outra string qualquer

                            using (StreamReader sr = new StreamReader(fs))
                            {

                                //Crio o FileStream temporário onde irei gravar as informações

                                using (FileStream fsTmp = new FileStream(item + ".tmp",

                                FileMode.Create, FileAccess.Write))

                                {

                                    //Instancio o StreamWriter para escrever os dados no arquivo temporário,

                                    //passando como parâmetro a variável fsTmp, referente ao FileStream criado

                                    using (StreamWriter sw = new StreamWriter(fsTmp))
                                    {

                                        //Crio uma variável e a atribuo como nula. Faço um while que percorrerá

                                        //meu arquivo original e enquanto ele estiver diferente de nulo...

                                        string strLinha = null;
                                        string strLinha1 = null;
                                        string strLinha2 = null;
                                        string strLinha3 = null;
                                        string strLinha4 = null;
                                        string strLinha5 = null;
                                        string strLinha6 = null;
                                        string strLinha7 = null;
                                        string strLinha8 = null;
                                        string strLinha9 = null;
                                        string strLinha10 = null;
                                        int Count = 0;
                                        List<string> ListaTXT_Original = new List<string>();
                                        string IndiceTemporario = string.Empty;


                                        while (!sr.EndOfStream)
                                        {
                                            Count += 1;
                                            if (Count.ToString().Length == 1)
                                            {
                                                IndiceTemporario = string.Concat("0", Count);
                                            }
                                            else
                                            {
                                                IndiceTemporario = Count.ToString();
                                            }
                                            ListaTXT_Original.Add(string.Concat(IndiceTemporario, ";", sr.ReadLine()));
                                        }

                                        foreach (var item1 in ListaTXT_Original)
                                        {

                                            //GERAL
                                            if (item1.IndexOf("01;[GERAL]") > -1)
                                            {

                                                sw.Write("[GERAL]\n");

                                            }

                                            //PARIDADE
                                            if (item1.IndexOf($"02;paridade = ") > -1)
                                            {
                                                var paridade = item1.Replace("02;", "");
                                                sw.Write($"{paridade}\n");

                                            }

                                            //VALOReNTARDA
                                            if (item1.IndexOf("03;valor_entrada") > -1)
                                            {
                                                //uso o método Replace que espera o valor antigo e valor novo

                                                strLinha = item1.Replace($"{ item1}", $"valor_entrada = {valorEntrada}\n");
                                                sw.Write(strLinha);

                                            }

                                            //timeframe
                                            if (item1.IndexOf("04;timeframe") > -1)
                                            {

                                                sw.Write("timeframe = 1\n");

                                            }

                                            //martingale
                                            if (item1.IndexOf("05;martingale = N") > -1)
                                            {

                                                sw.Write("martingale = N\n");

                                            }

                                            //sorosgale
                                            if (item1.IndexOf("06;sorosgale = N") > -1)
                                            {

                                                sw.Write("sorosgale = N\n");

                                            }

                                            //niveis
                                            if (item1.IndexOf("07;niveis = 1") > -1)
                                            {

                                                sw.Write("niveis = 1\n");

                                            }

                                            //stop_loss
                                            if (item1.IndexOf("08;stop_loss =") > -1)
                                            {

                                                strLinha4 = "stop_loss = 1000\n";
                                                sw.Write(strLinha4);

                                            }

                                            //stop_win
                                            if (item1.IndexOf("09;stop_win =") > -1)
                                            {

                                                strLinha5 = "stop_win = 10000\n";
                                                sw.Write(strLinha5);

                                            }

                                            //valor_minimo
                                            if (item1.IndexOf("10;valor_minimo") > -1)
                                            {
                                                strLinha6 = item1.Replace($"{item1}", $"valor_minimo ={valorMinimo}\n");
                                                sw.Write(strLinha6);

                                            }

                                            //filtro_pais
                                            if (item1.IndexOf("11;filtro_pais = todos") > -1)
                                            {

                                                sw.Write("filtro_pais = todos\n");//

                                            }

                                            //filtro_top_traders
                                            if (item1.IndexOf("12;filtro_top_traders") > -1)
                                            {
                                                if (topTryder != "")
                                                {
                                                    strLinha7 = item1.Replace($"{item1}", $"filtro_top_traders = {topTryder}\n");
                                                    sw.Write(strLinha7);
                                                }
                                                else
                                                {
                                                    //filtro_top_traders
                                                    if (item1.IndexOf("12;filtro_top_traders = ") > -1)
                                                    {
                                                        sw.Write("filtro_top_traders = 0\n");
                                                    }
                                                }
                                            }

                                            //filtro_diferenca_sinal
                                            if (item1.IndexOf("13;filtro_diferenca_sinal") > -1)
                                            {

                                                sw.Write("filtro_diferenca_sinal = 2\n");

                                            }

                                            //seguir_ids
                                            if (item1.IndexOf("14;seguir_ids") > -1)
                                            {
                                                strLinha1 = item1.Replace($"{item1}", $"seguir_ids = {trayderID}\n");

                                                sw.Write(strLinha1);

                                            }

                                            //stopLoss
                                            if (item1.IndexOf("15;stopLoss") > -1)
                                            {

                                                //uso o método Replace que espera o valor antigo e valor novo

                                                strLinha2 = item1.Replace($"{item1}", $"stopLoss = {stopLOss}\n");
                                                sw.Write(strLinha2);

                                            }

                                            //StopWin
                                            if (item1.IndexOf("16;StopWin") > -1)
                                            {
                                                strLinha3 = item1.Replace($"{item1}", $"StopWin = {stopWin}\n");
                                                sw.Write(strLinha3);
                                            }

                                            //Email
                                            if (item1.IndexOf("17;Email") > -1)
                                            {
                                                strLinha8 = item1.Replace($"{item1}", $"Email = {email_IQ}\n");
                                                sw.Write(strLinha8);
                                            }

                                            //Senha
                                            if (item1.IndexOf("18;Senha") > -1)
                                            {
                                                strLinha9 = item1.Replace($"{item1}", $"Senha = {senha_IQ}\n");
                                                sw.Write(strLinha9);
                                            }

                                            //Tipo_Conta
                                            if (item1.IndexOf("19;Tipo_Conta") > -1)
                                            {
                                                strLinha10 = item1.Replace($"{item1}", $"Tipo_Conta = {conta}\n");
                                                sw.Write(strLinha10);
                                            }

                                        }

                                    }

                                }

                            }

                        }

                        //Ao final excluo o arquivo anterior e movo o temporário no lugar do original

                        //Dessa forma não perco os dados de modificação de meu arquivo

                        File.Delete(item);

                        //No método Move passo o arquivo de origem, o temporário, e o de destino, o original

                        File.Move(item + ".tmp", item);

                        //Exibo a mensagem ao usuário                       

                    }
                    else
                    {
                        //Se não existir exibo a mensagem

                        MessageBox.Show($"Arquivo não encontrado!{item}");
                    }

                }

                MessageBox.Show("Arquivo alterado com sucesso!");
                txtDiretorioDigital.Clear();
                txtDiretorioBinaria.Clear();
                txtStopLoss.Clear();
                txtStopWin.Clear();
                txtValorEntrada.Clear();
                chBinaria.Checked = false;
                chDigital.Checked = false;

            }

            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

            }
        }


        public void Cadastrar(string conta, string email_IQ, string Senha_IQ, string DiretorioBinary, string diretorioDigital, int valorEntrada, string stopLOss, string stopWin, int trayderID, int valorMinimo, string topTryder)
        {
            var diretorioDigital1 = diretorioDigital;

            var diretorioOpBynari01 = DiretorioBinary;

            if (chBynariOTC.Checked == true)
            {
                var diretorioOTCBynari = txtDiretorioBinaria.Text;

               
                listEndereco.Add($@"{diretorioOTCBynari}\EUR-GBP\config.txt");
                listEndereco.Add($@"{diretorioOTCBynari}\EUR-USD\config.txt");
                listEndereco.Add($@"{diretorioOTCBynari}\NZD-USD\config.txt");
                listEndereco.Add($@"{diretorioOTCBynari}\AUD-CAD\config.txt");
                listEndereco.Add($@"{diretorioOTCBynari}\USD-CHF\config.txt");
                listEndereco.Add($@"{diretorioOTCBynari}\GBP-USD\config.txt");           
              

              
            }

            if (chDigitalOTC.Checked == true)
            {
                var diretorioOTCBynari = txtDiretorioBinaria.Text;

                listEndereco.Add($@"{diretorioDigital1}\GBP-USD\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\EUR-USD\config.txt");               
                listEndereco.Add($@"{diretorioDigital1}\GBP-JPY\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\NZD-USD\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\USD-CHF\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\EUR-JPY\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\USD-JPY\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\AUD-CAD\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\EUR-GBP\config.txt");
            }

            if (chBinaria.Checked == true)
            {

                listEndereco.Add($@"{diretorioOpBynari01}\EUR-USD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\EUR-AUD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\EUR-CAD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\EUR-GBP\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\EUR-JPY\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\EUR-CHF\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\EUR-NZD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\GBP-AUD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\GBP-CAD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\GBP-CHF\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\GBP-JPY\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\GBP-NZD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\GBP-USD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\AUD-JPY\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\AUD-USD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\AUD-NZD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\AUD-CHF\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\AUD-CAD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\USD-CAD\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\USD-CHF\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\CAD-CHF\config.txt");
                listEndereco.Add($@"{diretorioOpBynari01}\NZD-USD\config.txt");

            }

            if (chDigital.Checked == true)
            {
                listEndereco.Add($@"{diretorioDigital1}\AUD-CAD\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\AUD-JPY\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\AUD-USD\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\EUR-AUD\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\EUR-USD\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\EUR-CAD\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\EUR-GBP\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\EUR-JPY\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\GBP-AUD\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\GBP-CAD\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\GBP-CHF\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\GBP-JPY\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\GBP-NZD\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\GBP-USD\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\USD-CAD\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\USD-CHF\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\USD-JPY\config.txt");
                listEndereco.Add($@"{diretorioDigital1}\CAD-JPY\config.txt");

            }


            try
            {

                foreach (var item in listEndereco)
                {

                    //Verifico se o arquivo que desejo abrir existe e passo como parâmetro a variável respectiva

                    if (File.Exists(item))
                    {

                        //Instancio o FileStream passando como parâmetro a variável padrão, o FileMode que será

                        //o modo Open e o FileAccess, que será Read(somente leitura). Este método é diferente dos

                        //demais: primeiro irei abrir o arquivo, depois criar um FileStream temporário que irá

                        //armazenar os novos dados e depois criarei outro FileStream para fazer a junção dos dois

                        using (FileStream fs = new FileStream(item, FileMode.Open, FileAccess.Read))
                        {

                            //Aqui instancio o StreamReader passando como parâmetro o FileStream criado acima.

                            //Uso o StreamReader já que faço 1º a leitura do arquivo. Irei percorrer o arquivo e

                            //quando encontrar uma string qualquer farei a alteração por outra string qualquer

                            using (StreamReader sr = new StreamReader(fs))
                            {

                                //Crio o FileStream temporário onde irei gravar as informações

                                using (FileStream fsTmp = new FileStream(item + ".tmp",

                                FileMode.Create, FileAccess.Write))

                                {

                                    //Instancio o StreamWriter para escrever os dados no arquivo temporário,

                                    //passando como parâmetro a variável fsTmp, referente ao FileStream criado

                                    using (StreamWriter sw = new StreamWriter(fsTmp))
                                    {

                                        //Crio uma variável e a atribuo como nula. Faço um while que percorrerá

                                        //meu arquivo original e enquanto ele estiver diferente de nulo...

                                        string strLinha = null;
                                        string strLinha1 = null;
                                        string strLinha2 = null;
                                        string strLinha3 = null;
                                        string strLinha4 = null;
                                        string strLinha5 = null;
                                        string strLinha6 = null;
                                        string strLinha7 = null;
                                        string strLinha8 = null;
                                        string strLinha9 = null;
                                        string strLinha10 = null;
                                        int Count = 0;
                                        List<string> ListaTXT_Original = new List<string>();
                                        string IndiceTemporario = string.Empty;


                                        while (!sr.EndOfStream)
                                        {
                                            Count += 1;
                                            if (Count.ToString().Length == 1)
                                            {
                                                IndiceTemporario = string.Concat("0", Count);
                                            }
                                            else
                                            {
                                                IndiceTemporario = Count.ToString();
                                            }
                                            ListaTXT_Original.Add(string.Concat(IndiceTemporario, ";", sr.ReadLine()));
                                        }

                                        foreach (var item1 in ListaTXT_Original)
                                        {

                                            //GERAL
                                            if (item1.IndexOf("01;[GERAL]") > -1)
                                            {

                                                sw.Write("[GERAL]\n");

                                            }

                                            //PARIDADE
                                            if (item1.IndexOf($"02;paridade = ") > -1)
                                            {
                                                var paridade = item1.Replace("02;", "");
                                                sw.Write($"{paridade}\n");

                                            }

                                            //VALOReNTARDA
                                            if (item1.IndexOf("03;valor_entrada") > -1)
                                            {
                                                //uso o método Replace que espera o valor antigo e valor novo

                                                strLinha = item1.Replace($"{ item1}", $"valor_entrada = {valorEntrada}\n");
                                                sw.Write(strLinha);

                                            }

                                            //timeframe
                                            if (item1.IndexOf("04;timeframe") > -1)
                                            {

                                                sw.Write("timeframe = 1\n");

                                            }

                                            //martingale
                                            if (item1.IndexOf("05;martingale = N") > -1)
                                            {

                                                sw.Write("martingale = N\n");

                                            }

                                            //sorosgale
                                            if (item1.IndexOf("06;sorosgale = N") > -1)
                                            {

                                                sw.Write("sorosgale = N\n");

                                            }

                                            //niveis
                                            if (item1.IndexOf("07;niveis = 1") > -1)
                                            {

                                                sw.Write("niveis = 1\n");

                                            }

                                            //stop_loss
                                            if (item1.IndexOf("08;stop_loss =") > -1)
                                            {

                                                strLinha4 = "stop_loss = 1000\n";
                                                sw.Write(strLinha4);

                                            }

                                            //stop_win
                                            if (item1.IndexOf("09;stop_win =") > -1)
                                            {

                                                strLinha5 = "stop_win = 10000\n";
                                                sw.Write(strLinha5);

                                            }

                                            //valor_minimo
                                            if (item1.IndexOf("10;valor_minimo") > -1)
                                            {
                                                strLinha6 = item1.Replace($"{item1}", $"valor_minimo ={valorMinimo}\n");
                                                sw.Write(strLinha6);

                                            }

                                            //filtro_pais
                                            if (item1.IndexOf("11;filtro_pais = todos") > -1)
                                            {

                                                sw.Write("filtro_pais = todos\n");//

                                            }

                                            //filtro_top_traders
                                            if (item1.IndexOf("12;filtro_top_traders") > -1)
                                            {
                                                if (topTryder != "")
                                                {
                                                    strLinha7 = item1.Replace($"{item1}", $"filtro_top_traders = {topTryder}\n");
                                                    sw.Write(strLinha7);
                                                }
                                                else
                                                {
                                                    //filtro_top_traders
                                                    if (item1.IndexOf("12;filtro_top_traders = ") > -1)
                                                    {
                                                        sw.Write("filtro_top_traders = 0\n");
                                                    }
                                                }
                                            }

                                            //filtro_diferenca_sinal
                                            if (item1.IndexOf("13;filtro_diferenca_sinal") > -1)
                                            {

                                                sw.Write("filtro_diferenca_sinal = 2\n");

                                            }

                                            //seguir_ids
                                            if (item1.IndexOf("14;seguir_ids") > -1)
                                            {
                                                strLinha1 = item1.Replace($"{item1}", $"seguir_ids = {trayderID}\n");

                                                sw.Write(strLinha1);

                                            }

                                            //stopLoss
                                            if (item1.IndexOf("15;stopLoss") > -1)
                                            {

                                                //uso o método Replace que espera o valor antigo e valor novo

                                                strLinha2 = item1.Replace($"{item1}", $"stopLoss = {stopLOss}\n");
                                                sw.Write(strLinha2);

                                            }

                                            //StopWin
                                            if (item1.IndexOf("16;StopWin") > -1)
                                            {
                                                strLinha3 = item1.Replace($"{item1}", $"StopWin = {stopWin}\n");
                                                sw.Write(strLinha3);
                                            }

                                            //Email
                                            if (item1.IndexOf("17;Email") > -1)
                                            {
                                                strLinha8 = item1.Replace($"{item1}", $"Email = {email_IQ}\n");
                                                sw.Write(strLinha8);
                                            }

                                            //Senha
                                            if (item1.IndexOf("18;Senha") > -1)
                                            {
                                                strLinha9 = item1.Replace($"{item1}", $"Senha = {Senha_IQ}\n");
                                                sw.Write(strLinha9);
                                            }

                                            //Tipo_Conta
                                            if (item1.IndexOf("19;Tipo_Conta") > -1)
                                            {
                                                strLinha10 = item1.Replace($"{item1}", $"Tipo_Conta = {conta}\n");
                                                sw.Write(strLinha10);
                                            }
                                        }

                                    }

                                }

                            }

                        }

                        //Ao final excluo o arquivo anterior e movo o temporário no lugar do original

                        //Dessa forma não perco os dados de modificação de meu arquivo

                        File.Delete(item);

                        //No método Move passo o arquivo de origem, o temporário, e o de destino, o original

                        File.Move(item + ".tmp", item);

                        //Exibo a mensagem ao usuário                       

                    }
                    else
                    {
                        //Se não existir exibo a mensagem

                        MessageBox.Show($"Arquivo não encontrado!{item}");
                    }

                }

                MessageBox.Show("Arquivo alterado com sucesso!");
                txtDiretorioDigital.Clear();
                txtDiretorioBinaria.Clear();
                txtStopLoss.Clear();
                txtStopWin.Clear();
                txtValorEntrada.Clear();
                chBinaria.Checked = false;
                chDigital.Checked = false;

            }

            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {


        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtStopLoss_Leave(object sender, EventArgs e)
        {
            if (!txtStopLoss.Text.Contains("."))
            {
                MessageBox.Show("Valor Do Stop Loss Falto Ponto!", "Validador  Stop", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtStopLoss.Clear();
                txtStopLoss.Show();
            }
        }

        private void txtStopWin_Leave(object sender, EventArgs e)
        {

            if (!txtStopWin.Text.Contains("."))
            {
                MessageBox.Show("Valor Do stop Win Falto Ponto!", "Validador  Stop", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtStopWin.Clear();
                txtStopWin.Show();
            }
        }


        private void btnSalvar_Click(object sender, EventArgs e)
        {


        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click_1(object sender, EventArgs e)
        {
            var userDTO = dbUsuario.GetAllUsuario().ToList();

            var usuarioDTO = userDTO.Find(c => c.Nome == cmUser.Text);
           

            if (rdSegunda01.Checked == false && rdTerca02.Checked == false && rdQuarta03.Checked == false &&
                rdQuinta04.Checked == false && rdSexta05.Checked == false && rdSabado06.Checked == false &&
                rdDomingo07.Checked == false)
            {
                MessageBox.Show("Necesserio Informar Dia Do Operacional!", "Validador Dia Operacional", MessageBoxButtons.OK, MessageBoxIcon.Error);

                txtStopWin.Show();
            }
            else
            {
                var treyderID = 0;

                var diaSemana = 0;
                if (rdSegunda01.Checked == true)
                {
                    diaSemana = 1;
                }
                else if (rdTerca02.Checked == true)
                {
                    diaSemana = 2;
                }
                else if (rdQuarta03.Checked == true)
                {
                    diaSemana = 3;
                }
                else if (rdQuinta04.Checked == true)
                {
                    diaSemana = 4;
                }
                else if (rdSexta05.Checked == true)
                {
                    diaSemana = 5;
                }
                else if (rdSabado06.Checked == true)
                {
                    diaSemana = 6;
                }
                else if (rdDomingo07.Checked == true)
                {
                    diaSemana = 7;
                }

                if (cmTraiderId.Items.Count > 0)
                {
                    if (cmTraiderId.Text.Contains("TECNICA01(Canga)"))
                    {
                        treyderID = 45316621;
                        //txtTraiderId.Items.Remove("TECNICA01");
                    }

                    else if (cmTraiderId.Text.Contains("TECNICA02(Rodrigo L.)"))
                    {
                        treyderID = 22756290;
                        //txtTraiderId.Items.Remove("TECNICA02");
                    }

                    else if (cmTraiderId.Text.Contains("TECNICA03(Fabio G.)"))
                    {
                        treyderID = 51292712;
                        //txtTraiderId.Items.Remove("TECNICA03");
                    }

                    else if (cmTraiderId.Text.Contains("TECNICA04(Elizabeth R."))
                    {
                        treyderID = 45844787;
                        //txtTraiderId.Items.Remove("TECNICA04");
                    }

                    else if (cmTraiderId.Text.Contains("TECNICA05(Thomas S.)"))
                    {
                        treyderID = 44707398;
                        //txtTraiderId.Items.Remove("TECNICA05");
                    }

                    else if (cmTraiderId.Text.Contains("TECNICA05(Sarwaka S.)"))
                    {
                        treyderID = 42624866;
                        //txtTraiderId.Items.Remove("TECNICA05");
                    }

                    else if(txtIdTrayder.Text != "")
                    {
                        treyderID = Convert.ToInt32( txtIdTrayder.Text);
                    }

                }

                var operacionalAdd = new OperacionalBynari
                {
                    DiretorioDigital = txtDiretorioDigital.Text,
                    DiretorioBinario = txtDiretorioBinaria.Text,
                    ValorEntrada = Convert.ToInt32(txtValorEntrada.Text.Replace(".00", "")),
                    StopLoss = txtStopLoss.Text,
                    StopWin = txtStopWin.Text,
                    DataCadastro = DateTime.Now,
                    UsuarioId = usuarioDTO.Id.ToString(),
                    DiaSemana = diaSemana,
                    Conta = txtConta.Text,
                    TopTryder = txtTop.Text,
                    ValorMinimo = Convert.ToInt32(txtValorMinimo.Text.Replace(".00", "")),
                    TrayderId = treyderID

                };

                var gerenciamento = dbOperacional.GetAllOperacional().ToList();

                if (gerenciamento.Any(x => x.TrayderId.ToString() == cmTraiderId.Text && x.UsuarioId == usuarioId && x.DiaSemana == semanaMes && x.DiretorioBinario == txtDiretorioBinaria.Text && x.DiretorioDigital == txtDiretorioDigital.Text))
                {
                    MessageBox.Show("Diretorio Expesificado Já cadastrado para Dia Da Semana!", "OperacionalDIario", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {                   

                    //Somente inserir no banco de dados se esse campo tiver como verdadeiro
                    if (chArquivarDiretorio.Checked)
                    {
                        Cadastrar(operacionalAdd.Conta, usuarioDTO.Email_IQ, usuarioDTO.Senha_IQ, operacionalAdd.DiretorioBinario, operacionalAdd.DiretorioDigital, operacionalAdd.ValorEntrada, operacionalAdd.StopLoss, operacionalAdd.StopWin, operacionalAdd.TrayderId, operacionalAdd.ValorMinimo, operacionalAdd.TopTryder);
                    }
                    if(chArquivarBanco.Checked)
                    {
                        dbOperacional.InsertOperacional(operacionalAdd);
                    }                   

                    MessageBox.Show("Operaciol Salvo com sucesso!", "Operacional", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                    btnNovo.Enabled = true;
                    btnSalvar.Enabled = false;
                }
            }
        }               

        private void btnNovo_Click(object sender, EventArgs e)
        {
            rdSegunda01.Checked = false;
            rdTerca02.Checked = false;
            rdQuarta03.Checked = false;
            rdQuinta04.Checked = false;
            rdSexta05.Checked = false;
            rdSabado06.Checked = false;
            rdDomingo07.Checked = false;
            txtConta.Text = "";
            txtValorMinimo.Clear();
            cmUser.Text = "";
            cmTraiderId.Text = "";
            btnSalvar.Enabled = true;
            btnNovo.Enabled = true;
            chBynariOTC.Checked = false;
            chDigitalOTC.Checked = false;
            chDigital.Checked = false;
            chBinaria.Checked = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            txtUserId.Text = usuarioId;
            Menu mainMenu = new Menu(usuarioId);
            mainMenu.Show();

        }       

        private void txtDiretorioDigital_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtDiretorioBinaria.Focus();
            }
        }

        private void txtDiretorioBinaria_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtValorEntrada.Focus();
            }
        }

        private void txtValorEntrada_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtStopLoss.Focus();
            }
        }

        private void txtStopLoss_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                cmTraiderId.Focus();
            }
        }

        private void cmTraiderId_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                cmUser.Focus();
            }
        }

        private void cmUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtTop.Focus();
            }
        }

        private void txtTop_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtValorMinimo.Focus();
            }
        }

        private void txtValorMinimo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtValorInicial.Focus();
            }
        }

        private void txtValorInicial_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = e.SuppressKeyPress = true;
                txtConta.Focus();
            }
        }
    }
}
