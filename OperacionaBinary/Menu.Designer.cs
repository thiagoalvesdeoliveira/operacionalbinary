﻿namespace OperacionaBinary
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.operacionalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.nemuCadastrar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.subNemuDeleOperacional = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.relatoriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.pesquisarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operacionalSemanalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operacionalDiarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operacionalMêsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCotacao = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.mOEDASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.execultarRobôToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.bynariaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.subNenuOTC = new System.Windows.Forms.ToolStripMenuItem();
            this.digitalToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bynariaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.munuPesquisa = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tENDÊNCIASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dIARIAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.hORASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.mINUTOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.mINUTOSToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.mINUTOSToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAutenticacao = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.adicionarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.txtEurUsd = new System.Windows.Forms.TextBox();
            this.lblParidadeB = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblParidadeD = new System.Windows.Forms.Label();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtStopWin = new System.Windows.Forms.TextBox();
            this.txtStopLoss = new System.Windows.Forms.TextBox();
            this.txtEntrada = new System.Windows.Forms.TextBox();
            this.txtValorBReal = new System.Windows.Forms.TextBox();
            this.txtValorBTeste = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNivel = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSemana = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSaldoStopWin = new System.Windows.Forms.TextBox();
            this.txtSaldoStopLoss = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.operacionalToolStripMenuItem,
            this.menuCotacao,
            this.execultarRobôToolStripMenuItem,
            this.munuPesquisa,
            this.menuAutenticacao});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1219, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // operacionalToolStripMenuItem
            // 
            this.operacionalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.nemuCadastrar,
            this.toolStripSeparator2,
            this.toolStripSeparator3,
            this.subNemuDeleOperacional,
            this.toolStripSeparator14,
            this.relatoriosToolStripMenuItem,
            this.toolStripSeparator4,
            this.pesquisarToolStripMenuItem});
            this.operacionalToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("operacionalToolStripMenuItem.Image")));
            this.operacionalToolStripMenuItem.Name = "operacionalToolStripMenuItem";
            this.operacionalToolStripMenuItem.Size = new System.Drawing.Size(115, 20);
            this.operacionalToolStripMenuItem.Text = "OPERACIONAL";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // nemuCadastrar
            // 
            this.nemuCadastrar.Image = ((System.Drawing.Image)(resources.GetObject("nemuCadastrar.Image")));
            this.nemuCadastrar.Name = "nemuCadastrar";
            this.nemuCadastrar.Size = new System.Drawing.Size(180, 22);
            this.nemuCadastrar.Text = "CADASTRO";
            this.nemuCadastrar.Visible = false;
            this.nemuCadastrar.Click += new System.EventHandler(this.cadastroToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(177, 6);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(177, 6);
            // 
            // subNemuDeleOperacional
            // 
            this.subNemuDeleOperacional.Checked = true;
            this.subNemuDeleOperacional.CheckState = System.Windows.Forms.CheckState.Checked;
            this.subNemuDeleOperacional.Image = ((System.Drawing.Image)(resources.GetObject("subNemuDeleOperacional.Image")));
            this.subNemuDeleOperacional.Name = "subNemuDeleOperacional";
            this.subNemuDeleOperacional.Size = new System.Drawing.Size(180, 22);
            this.subNemuDeleOperacional.Text = "ALTERAÇÃO";
            this.subNemuDeleOperacional.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(177, 6);
            // 
            // relatoriosToolStripMenuItem
            // 
            this.relatoriosToolStripMenuItem.Enabled = false;
            this.relatoriosToolStripMenuItem.Name = "relatoriosToolStripMenuItem";
            this.relatoriosToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.relatoriosToolStripMenuItem.Text = "RELATORIOS";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(177, 6);
            // 
            // pesquisarToolStripMenuItem
            // 
            this.pesquisarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.operacionalSemanalToolStripMenuItem,
            this.operacionalDiarioToolStripMenuItem,
            this.operacionalMêsToolStripMenuItem});
            this.pesquisarToolStripMenuItem.Enabled = false;
            this.pesquisarToolStripMenuItem.Name = "pesquisarToolStripMenuItem";
            this.pesquisarToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.pesquisarToolStripMenuItem.Text = "PESQUISA";
            // 
            // operacionalSemanalToolStripMenuItem
            // 
            this.operacionalSemanalToolStripMenuItem.Name = "operacionalSemanalToolStripMenuItem";
            this.operacionalSemanalToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.operacionalSemanalToolStripMenuItem.Text = "Operacional Semanal";
            // 
            // operacionalDiarioToolStripMenuItem
            // 
            this.operacionalDiarioToolStripMenuItem.Name = "operacionalDiarioToolStripMenuItem";
            this.operacionalDiarioToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.operacionalDiarioToolStripMenuItem.Text = "Operacional Diario";
            // 
            // operacionalMêsToolStripMenuItem
            // 
            this.operacionalMêsToolStripMenuItem.Name = "operacionalMêsToolStripMenuItem";
            this.operacionalMêsToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.operacionalMêsToolStripMenuItem.Text = "Operacional Mês";
            // 
            // menuCotacao
            // 
            this.menuCotacao.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator11,
            this.mOEDASToolStripMenuItem,
            this.toolStripSeparator5});
            this.menuCotacao.Enabled = false;
            this.menuCotacao.Image = ((System.Drawing.Image)(resources.GetObject("menuCotacao.Image")));
            this.menuCotacao.Name = "menuCotacao";
            this.menuCotacao.Size = new System.Drawing.Size(89, 20);
            this.menuCotacao.Text = "COTAÇÃO";
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(119, 6);
            // 
            // mOEDASToolStripMenuItem
            // 
            this.mOEDASToolStripMenuItem.Name = "mOEDASToolStripMenuItem";
            this.mOEDASToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.mOEDASToolStripMenuItem.Text = "MOEDAS";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(119, 6);
            // 
            // execultarRobôToolStripMenuItem
            // 
            this.execultarRobôToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator13,
            this.bynariaToolStripMenuItem,
            this.toolStripSeparator12,
            this.subNenuOTC});
            this.execultarRobôToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("execultarRobôToolStripMenuItem.Image")));
            this.execultarRobôToolStripMenuItem.Name = "execultarRobôToolStripMenuItem";
            this.execultarRobôToolStripMenuItem.Size = new System.Drawing.Size(130, 20);
            this.execultarRobôToolStripMenuItem.Text = "EXECULTAR ROBÔ";
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(177, 6);
            // 
            // bynariaToolStripMenuItem
            // 
            this.bynariaToolStripMenuItem.Name = "bynariaToolStripMenuItem";
            this.bynariaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.bynariaToolStripMenuItem.Text = "DIGITAL/BINARIA";
            this.bynariaToolStripMenuItem.Click += new System.EventHandler(this.bynariaToolStripMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(177, 6);
            // 
            // subNenuOTC
            // 
            this.subNenuOTC.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.digitalToolStripMenuItem1,
            this.bynariaToolStripMenuItem1});
            this.subNenuOTC.Enabled = false;
            this.subNenuOTC.Name = "subNenuOTC";
            this.subNenuOTC.Size = new System.Drawing.Size(180, 22);
            this.subNenuOTC.Text = "OTC";
            // 
            // digitalToolStripMenuItem1
            // 
            this.digitalToolStripMenuItem1.Name = "digitalToolStripMenuItem1";
            this.digitalToolStripMenuItem1.Size = new System.Drawing.Size(119, 22);
            this.digitalToolStripMenuItem1.Text = "DIGITAL";
            // 
            // bynariaToolStripMenuItem1
            // 
            this.bynariaToolStripMenuItem1.Name = "bynariaToolStripMenuItem1";
            this.bynariaToolStripMenuItem1.Size = new System.Drawing.Size(119, 22);
            this.bynariaToolStripMenuItem1.Text = "BINARIA";
            // 
            // munuPesquisa
            // 
            this.munuPesquisa.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator6,
            this.tENDÊNCIASToolStripMenuItem});
            this.munuPesquisa.Enabled = false;
            this.munuPesquisa.Image = ((System.Drawing.Image)(resources.GetObject("munuPesquisa.Image")));
            this.munuPesquisa.Name = "munuPesquisa";
            this.munuPesquisa.Size = new System.Drawing.Size(95, 20);
            this.munuPesquisa.Text = "PESQUISAR";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(140, 6);
            // 
            // tENDÊNCIASToolStripMenuItem
            // 
            this.tENDÊNCIASToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dIARIAToolStripMenuItem,
            this.toolStripSeparator7,
            this.hORASToolStripMenuItem,
            this.toolStripSeparator8,
            this.mINUTOSToolStripMenuItem,
            this.toolStripSeparator9,
            this.mINUTOSToolStripMenuItem1,
            this.toolStripSeparator10,
            this.mINUTOSToolStripMenuItem2});
            this.tENDÊNCIASToolStripMenuItem.Name = "tENDÊNCIASToolStripMenuItem";
            this.tENDÊNCIASToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.tENDÊNCIASToolStripMenuItem.Text = "TENDÊNCIAS";
            // 
            // dIARIAToolStripMenuItem
            // 
            this.dIARIAToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("dIARIAToolStripMenuItem.Image")));
            this.dIARIAToolStripMenuItem.Name = "dIARIAToolStripMenuItem";
            this.dIARIAToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.dIARIAToolStripMenuItem.Text = "DIARIA";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(137, 6);
            // 
            // hORASToolStripMenuItem
            // 
            this.hORASToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("hORASToolStripMenuItem.Image")));
            this.hORASToolStripMenuItem.Name = "hORASToolStripMenuItem";
            this.hORASToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.hORASToolStripMenuItem.Text = "HORAS";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(137, 6);
            // 
            // mINUTOSToolStripMenuItem
            // 
            this.mINUTOSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("mINUTOSToolStripMenuItem.Image")));
            this.mINUTOSToolStripMenuItem.Name = "mINUTOSToolStripMenuItem";
            this.mINUTOSToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.mINUTOSToolStripMenuItem.Text = "30 MINUTOS";
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(137, 6);
            // 
            // mINUTOSToolStripMenuItem1
            // 
            this.mINUTOSToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("mINUTOSToolStripMenuItem1.Image")));
            this.mINUTOSToolStripMenuItem1.Name = "mINUTOSToolStripMenuItem1";
            this.mINUTOSToolStripMenuItem1.Size = new System.Drawing.Size(140, 22);
            this.mINUTOSToolStripMenuItem1.Text = "15 MINUTOS";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(137, 6);
            // 
            // mINUTOSToolStripMenuItem2
            // 
            this.mINUTOSToolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("mINUTOSToolStripMenuItem2.Image")));
            this.mINUTOSToolStripMenuItem2.Name = "mINUTOSToolStripMenuItem2";
            this.mINUTOSToolStripMenuItem2.Size = new System.Drawing.Size(140, 22);
            this.mINUTOSToolStripMenuItem2.Text = "5 MINUTOS";
            // 
            // menuAutenticacao
            // 
            this.menuAutenticacao.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator15,
            this.adicionarToolStripMenuItem});
            this.menuAutenticacao.Image = ((System.Drawing.Image)(resources.GetObject("menuAutenticacao.Image")));
            this.menuAutenticacao.Name = "menuAutenticacao";
            this.menuAutenticacao.Size = new System.Drawing.Size(122, 20);
            this.menuAutenticacao.Text = "AUTENTICAÇÃO";
            this.menuAutenticacao.Click += new System.EventHandler(this.menuAutenticacao_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(134, 6);
            // 
            // adicionarToolStripMenuItem
            // 
            this.adicionarToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("adicionarToolStripMenuItem.Image")));
            this.adicionarToolStripMenuItem.Name = "adicionarToolStripMenuItem";
            this.adicionarToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.adicionarToolStripMenuItem.Text = "ADICIONAR";
            this.adicionarToolStripMenuItem.Click += new System.EventHandler(this.adicionarToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.textBox23);
            this.panel1.Controls.Add(this.textBox24);
            this.panel1.Controls.Add(this.textBox25);
            this.panel1.Controls.Add(this.textBox26);
            this.panel1.Controls.Add(this.textBox47);
            this.panel1.Controls.Add(this.textBox48);
            this.panel1.Controls.Add(this.textBox49);
            this.panel1.Controls.Add(this.textBox50);
            this.panel1.Controls.Add(this.textBox51);
            this.panel1.Controls.Add(this.textBox15);
            this.panel1.Controls.Add(this.textBox16);
            this.panel1.Controls.Add(this.textBox17);
            this.panel1.Controls.Add(this.textBox18);
            this.panel1.Controls.Add(this.textBox14);
            this.panel1.Controls.Add(this.textBox13);
            this.panel1.Controls.Add(this.textBox12);
            this.panel1.Controls.Add(this.textBox11);
            this.panel1.Controls.Add(this.textBox10);
            this.panel1.Controls.Add(this.textBox9);
            this.panel1.Controls.Add(this.textBox8);
            this.panel1.Controls.Add(this.textBox7);
            this.panel1.Controls.Add(this.txtEurUsd);
            this.panel1.Controls.Add(this.lblParidadeB);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(12, 141);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(343, 522);
            this.panel1.TabIndex = 1;
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.Color.Lime;
            this.textBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox23.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox23.Location = new System.Drawing.Point(183, 91);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(131, 29);
            this.textBox23.TabIndex = 22;
            this.textBox23.Text = "AUD-JPY";
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.Color.Lime;
            this.textBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox24.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox24.Location = new System.Drawing.Point(183, 126);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(131, 29);
            this.textBox24.TabIndex = 21;
            this.textBox24.Text = "AUD-NZD";
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.Color.Lime;
            this.textBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox25.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox25.Location = new System.Drawing.Point(183, 161);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(131, 29);
            this.textBox25.TabIndex = 20;
            this.textBox25.Text = "AUD-USD";
            // 
            // textBox26
            // 
            this.textBox26.BackColor = System.Drawing.Color.Lime;
            this.textBox26.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox26.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox26.Location = new System.Drawing.Point(183, 197);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(131, 29);
            this.textBox26.TabIndex = 19;
            this.textBox26.Text = "USD-CAD";
            // 
            // textBox47
            // 
            this.textBox47.BackColor = System.Drawing.Color.Lime;
            this.textBox47.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox47.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox47.Location = new System.Drawing.Point(183, 234);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(131, 29);
            this.textBox47.TabIndex = 18;
            this.textBox47.Text = "USD-CHF";
            // 
            // textBox48
            // 
            this.textBox48.BackColor = System.Drawing.Color.Lime;
            this.textBox48.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox48.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox48.Location = new System.Drawing.Point(183, 269);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(131, 29);
            this.textBox48.TabIndex = 17;
            this.textBox48.Text = "CAD-CHF";
            // 
            // textBox49
            // 
            this.textBox49.BackColor = System.Drawing.Color.Lime;
            this.textBox49.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox49.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox49.Location = new System.Drawing.Point(183, 304);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(131, 29);
            this.textBox49.TabIndex = 16;
            this.textBox49.Text = "NZD-USD";
            // 
            // textBox50
            // 
            this.textBox50.BackColor = System.Drawing.Color.Lime;
            this.textBox50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox50.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox50.Location = new System.Drawing.Point(183, 341);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(131, 29);
            this.textBox50.TabIndex = 15;
            this.textBox50.Text = "EUR-USD";
            // 
            // textBox51
            // 
            this.textBox51.BackColor = System.Drawing.Color.Lime;
            this.textBox51.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox51.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox51.Location = new System.Drawing.Point(183, 56);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(131, 29);
            this.textBox51.TabIndex = 14;
            this.textBox51.Text = "AUD-CHF";
            // 
            // textBox15
            // 
            this.textBox15.BackColor = System.Drawing.Color.Lime;
            this.textBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox15.Location = new System.Drawing.Point(7, 376);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(131, 29);
            this.textBox15.TabIndex = 13;
            this.textBox15.Text = "GBP-JPY";
            // 
            // textBox16
            // 
            this.textBox16.BackColor = System.Drawing.Color.Lime;
            this.textBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox16.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox16.Location = new System.Drawing.Point(7, 411);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(131, 29);
            this.textBox16.TabIndex = 12;
            this.textBox16.Text = "GBP-NZD";
            // 
            // textBox17
            // 
            this.textBox17.BackColor = System.Drawing.Color.Lime;
            this.textBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox17.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox17.Location = new System.Drawing.Point(7, 446);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(131, 29);
            this.textBox17.TabIndex = 11;
            this.textBox17.Text = "GBP-USD";
            // 
            // textBox18
            // 
            this.textBox18.BackColor = System.Drawing.Color.Lime;
            this.textBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox18.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox18.Location = new System.Drawing.Point(7, 483);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(131, 29);
            this.textBox18.TabIndex = 10;
            this.textBox18.Text = "AUD-CAD";
            // 
            // textBox14
            // 
            this.textBox14.BackColor = System.Drawing.Color.Lime;
            this.textBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox14.Location = new System.Drawing.Point(7, 91);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(131, 29);
            this.textBox14.TabIndex = 9;
            this.textBox14.Text = "EUR-CAD";
            // 
            // textBox13
            // 
            this.textBox13.BackColor = System.Drawing.Color.Lime;
            this.textBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox13.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox13.Location = new System.Drawing.Point(7, 126);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(131, 29);
            this.textBox13.TabIndex = 8;
            this.textBox13.Text = "EUR-CHF";
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.Color.Lime;
            this.textBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox12.Location = new System.Drawing.Point(7, 161);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(131, 29);
            this.textBox12.TabIndex = 7;
            this.textBox12.Text = "EUR-GBP";
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.Color.Lime;
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox11.Location = new System.Drawing.Point(7, 197);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(131, 29);
            this.textBox11.TabIndex = 6;
            this.textBox11.Text = "EUR-JPY";
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.Color.Lime;
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox10.Location = new System.Drawing.Point(7, 234);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(131, 29);
            this.textBox10.TabIndex = 5;
            this.textBox10.Text = "EUR-NZD";
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.Color.Lime;
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox9.Location = new System.Drawing.Point(7, 269);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(131, 29);
            this.textBox9.TabIndex = 4;
            this.textBox9.Text = "GBP-AUD";
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.Color.Lime;
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox8.Location = new System.Drawing.Point(7, 304);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(131, 29);
            this.textBox8.TabIndex = 3;
            this.textBox8.Text = "GBP-CAD";
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.Lime;
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox7.Location = new System.Drawing.Point(7, 341);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(131, 29);
            this.textBox7.TabIndex = 2;
            this.textBox7.Text = "GBP-CHF";
            // 
            // txtEurUsd
            // 
            this.txtEurUsd.BackColor = System.Drawing.Color.Lime;
            this.txtEurUsd.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEurUsd.ForeColor = System.Drawing.Color.DarkRed;
            this.txtEurUsd.Location = new System.Drawing.Point(7, 56);
            this.txtEurUsd.Name = "txtEurUsd";
            this.txtEurUsd.Size = new System.Drawing.Size(131, 29);
            this.txtEurUsd.TabIndex = 1;
            this.txtEurUsd.Text = "EUR-AUD";
            // 
            // lblParidadeB
            // 
            this.lblParidadeB.AutoSize = true;
            this.lblParidadeB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParidadeB.Location = new System.Drawing.Point(3, 16);
            this.lblParidadeB.Name = "lblParidadeB";
            this.lblParidadeB.Size = new System.Drawing.Size(320, 24);
            this.lblParidadeB.TabIndex = 0;
            this.lblParidadeB.Text = "PARIDADES ABERTAS BYNARIA";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.lblParidadeD);
            this.panel2.Controls.Add(this.textBox39);
            this.panel2.Controls.Add(this.textBox57);
            this.panel2.Controls.Add(this.textBox56);
            this.panel2.Controls.Add(this.textBox31);
            this.panel2.Controls.Add(this.textBox55);
            this.panel2.Controls.Add(this.textBox32);
            this.panel2.Controls.Add(this.textBox54);
            this.panel2.Controls.Add(this.textBox33);
            this.panel2.Controls.Add(this.textBox53);
            this.panel2.Controls.Add(this.textBox52);
            this.panel2.Controls.Add(this.textBox46);
            this.panel2.Controls.Add(this.textBox45);
            this.panel2.Controls.Add(this.textBox44);
            this.panel2.Controls.Add(this.textBox43);
            this.panel2.Controls.Add(this.textBox42);
            this.panel2.Controls.Add(this.textBox40);
            this.panel2.Controls.Add(this.textBox41);
            this.panel2.Enabled = false;
            this.panel2.Location = new System.Drawing.Point(385, 141);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(329, 522);
            this.panel2.TabIndex = 2;
            // 
            // lblParidadeD
            // 
            this.lblParidadeD.AutoSize = true;
            this.lblParidadeD.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParidadeD.Location = new System.Drawing.Point(3, 16);
            this.lblParidadeD.Name = "lblParidadeD";
            this.lblParidadeD.Size = new System.Drawing.Size(309, 24);
            this.lblParidadeD.TabIndex = 1;
            this.lblParidadeD.Text = "PARIDADES ABERTAS DIGITAL";
            // 
            // textBox39
            // 
            this.textBox39.BackColor = System.Drawing.Color.Aqua;
            this.textBox39.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox39.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox39.Location = new System.Drawing.Point(181, 56);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(131, 29);
            this.textBox39.TabIndex = 40;
            this.textBox39.Text = "GBPUSD";
            // 
            // textBox57
            // 
            this.textBox57.BackColor = System.Drawing.Color.Aqua;
            this.textBox57.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox57.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox57.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox57.Location = new System.Drawing.Point(5, 56);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(131, 29);
            this.textBox57.TabIndex = 27;
            this.textBox57.Text = "AUD-CAD";
            // 
            // textBox56
            // 
            this.textBox56.BackColor = System.Drawing.Color.Aqua;
            this.textBox56.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox56.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox56.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox56.Location = new System.Drawing.Point(5, 341);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(131, 29);
            this.textBox56.TabIndex = 28;
            this.textBox56.Text = "GBP-AUD";
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.Color.Aqua;
            this.textBox31.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox31.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox31.Location = new System.Drawing.Point(181, 91);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(131, 29);
            this.textBox31.TabIndex = 48;
            this.textBox31.Text = "USD-CAD";
            // 
            // textBox55
            // 
            this.textBox55.BackColor = System.Drawing.Color.Aqua;
            this.textBox55.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox55.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox55.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox55.Location = new System.Drawing.Point(5, 304);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(131, 29);
            this.textBox55.TabIndex = 29;
            this.textBox55.Text = "EUR-USD";
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.Color.Aqua;
            this.textBox32.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox32.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox32.Location = new System.Drawing.Point(181, 126);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(131, 29);
            this.textBox32.TabIndex = 47;
            this.textBox32.Text = "USD-CHF";
            // 
            // textBox54
            // 
            this.textBox54.BackColor = System.Drawing.Color.Aqua;
            this.textBox54.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox54.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox54.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox54.Location = new System.Drawing.Point(5, 269);
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(131, 29);
            this.textBox54.TabIndex = 30;
            this.textBox54.Text = "EUR-JPY";
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.Color.Aqua;
            this.textBox33.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox33.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox33.Location = new System.Drawing.Point(181, 161);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(131, 29);
            this.textBox33.TabIndex = 46;
            this.textBox33.Text = "USD-JPY";
            // 
            // textBox53
            // 
            this.textBox53.BackColor = System.Drawing.Color.Aqua;
            this.textBox53.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox53.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox53.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox53.Location = new System.Drawing.Point(5, 234);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(131, 29);
            this.textBox53.TabIndex = 31;
            this.textBox53.Text = "EUR-CAD";
            // 
            // textBox52
            // 
            this.textBox52.BackColor = System.Drawing.Color.Aqua;
            this.textBox52.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox52.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox52.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox52.Location = new System.Drawing.Point(5, 197);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(131, 29);
            this.textBox52.TabIndex = 32;
            this.textBox52.Text = "EUR-AUD";
            // 
            // textBox46
            // 
            this.textBox46.BackColor = System.Drawing.Color.Aqua;
            this.textBox46.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox46.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox46.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox46.Location = new System.Drawing.Point(5, 161);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(131, 29);
            this.textBox46.TabIndex = 33;
            this.textBox46.Text = "CAD-JPY";
            // 
            // textBox45
            // 
            this.textBox45.BackColor = System.Drawing.Color.Aqua;
            this.textBox45.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox45.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox45.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox45.Location = new System.Drawing.Point(5, 126);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(131, 29);
            this.textBox45.TabIndex = 34;
            this.textBox45.Text = "AUD-USD";
            // 
            // textBox44
            // 
            this.textBox44.BackColor = System.Drawing.Color.Aqua;
            this.textBox44.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox44.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox44.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox44.Location = new System.Drawing.Point(5, 91);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(131, 29);
            this.textBox44.TabIndex = 35;
            this.textBox44.Text = "AUD-JPY";
            // 
            // textBox43
            // 
            this.textBox43.BackColor = System.Drawing.Color.Aqua;
            this.textBox43.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox43.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox43.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox43.Location = new System.Drawing.Point(5, 483);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(131, 29);
            this.textBox43.TabIndex = 36;
            this.textBox43.Text = "GBP-NZD";
            // 
            // textBox42
            // 
            this.textBox42.BackColor = System.Drawing.Color.Aqua;
            this.textBox42.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox42.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox42.Location = new System.Drawing.Point(5, 446);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(131, 29);
            this.textBox42.TabIndex = 37;
            this.textBox42.Text = "GBP-JPY";
            // 
            // textBox40
            // 
            this.textBox40.BackColor = System.Drawing.Color.Aqua;
            this.textBox40.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox40.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox40.Location = new System.Drawing.Point(5, 376);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(131, 29);
            this.textBox40.TabIndex = 39;
            this.textBox40.Text = "GBP-CAD";
            // 
            // textBox41
            // 
            this.textBox41.BackColor = System.Drawing.Color.Aqua;
            this.textBox41.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox41.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox41.Location = new System.Drawing.Point(5, 411);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(131, 29);
            this.textBox41.TabIndex = 38;
            this.textBox41.Text = "GBP-CHF";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(227)))), ((int)(((byte)(205)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.txtStopWin);
            this.panel3.Controls.Add(this.txtStopLoss);
            this.panel3.Controls.Add(this.txtEntrada);
            this.panel3.Controls.Add(this.txtValorBReal);
            this.panel3.Controls.Add(this.txtValorBTeste);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(736, 141);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(471, 522);
            this.panel3.TabIndex = 3;
            // 
            // txtStopWin
            // 
            this.txtStopWin.Enabled = false;
            this.txtStopWin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStopWin.Location = new System.Drawing.Point(186, 192);
            this.txtStopWin.Name = "txtStopWin";
            this.txtStopWin.Size = new System.Drawing.Size(186, 29);
            this.txtStopWin.TabIndex = 23;
            // 
            // txtStopLoss
            // 
            this.txtStopLoss.Enabled = false;
            this.txtStopLoss.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStopLoss.Location = new System.Drawing.Point(186, 148);
            this.txtStopLoss.Name = "txtStopLoss";
            this.txtStopLoss.Size = new System.Drawing.Size(186, 29);
            this.txtStopLoss.TabIndex = 22;
            // 
            // txtEntrada
            // 
            this.txtEntrada.Enabled = false;
            this.txtEntrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEntrada.Location = new System.Drawing.Point(186, 104);
            this.txtEntrada.Name = "txtEntrada";
            this.txtEntrada.Size = new System.Drawing.Size(186, 29);
            this.txtEntrada.TabIndex = 21;
            // 
            // txtValorBReal
            // 
            this.txtValorBReal.Enabled = false;
            this.txtValorBReal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorBReal.Location = new System.Drawing.Point(186, 63);
            this.txtValorBReal.Name = "txtValorBReal";
            this.txtValorBReal.Size = new System.Drawing.Size(186, 29);
            this.txtValorBReal.TabIndex = 20;
            // 
            // txtValorBTeste
            // 
            this.txtValorBTeste.Enabled = false;
            this.txtValorBTeste.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorBTeste.Location = new System.Drawing.Point(186, 21);
            this.txtValorBTeste.Name = "txtValorBTeste";
            this.txtValorBTeste.Size = new System.Drawing.Size(186, 29);
            this.txtValorBTeste.TabIndex = 19;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(95)))), ((int)(((byte)(197)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.textBox6);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.textBox5);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.txtNivel);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.txtSemana);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.txtSaldoStopWin);
            this.panel4.Controls.Add(this.txtSaldoStopLoss);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Enabled = false;
            this.panel4.Location = new System.Drawing.Point(-2, 321);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(471, 199);
            this.panel4.TabIndex = 6;
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(234, 130);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(108, 29);
            this.textBox6.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Green;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(230, 103);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(127, 24);
            this.label12.TabIndex = 17;
            this.label12.Text = "Dias Stop Win";
            // 
            // textBox5
            // 
            this.textBox5.Enabled = false;
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(234, 50);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(97, 29);
            this.textBox5.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Red;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(230, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 24);
            this.label11.TabIndex = 15;
            this.label11.Text = "Dias Stop Loss";
            // 
            // txtNivel
            // 
            this.txtNivel.Enabled = false;
            this.txtNivel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNivel.Location = new System.Drawing.Point(380, 130);
            this.txtNivel.Name = "txtNivel";
            this.txtNivel.Size = new System.Drawing.Size(84, 29);
            this.txtNivel.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(381, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 24);
            this.label10.TabIndex = 13;
            this.label10.Text = "Nível";
            // 
            // txtSemana
            // 
            this.txtSemana.Enabled = false;
            this.txtSemana.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSemana.Location = new System.Drawing.Point(380, 71);
            this.txtSemana.Name = "txtSemana";
            this.txtSemana.Size = new System.Drawing.Size(84, 29);
            this.txtSemana.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(381, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 24);
            this.label9.TabIndex = 11;
            this.label9.Text = "Semana";
            // 
            // txtSaldoStopWin
            // 
            this.txtSaldoStopWin.Enabled = false;
            this.txtSaldoStopWin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSaldoStopWin.Location = new System.Drawing.Point(-2, 130);
            this.txtSaldoStopWin.Name = "txtSaldoStopWin";
            this.txtSaldoStopWin.Size = new System.Drawing.Size(186, 29);
            this.txtSaldoStopWin.TabIndex = 10;
            // 
            // txtSaldoStopLoss
            // 
            this.txtSaldoStopLoss.Enabled = false;
            this.txtSaldoStopLoss.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSaldoStopLoss.Location = new System.Drawing.Point(-1, 50);
            this.txtSaldoStopLoss.Name = "txtSaldoStopLoss";
            this.txtSaldoStopLoss.Size = new System.Drawing.Size(186, 29);
            this.txtSaldoStopLoss.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Green;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 103);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(200, 24);
            this.label8.TabIndex = 8;
            this.label8.Text = "Valor Fechar Stop Win";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Red;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(206, 24);
            this.label7.TabIndex = 7;
            this.label7.Text = "Valor Fechar Stop Loss";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(94, 195);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 24);
            this.label5.TabIndex = 4;
            this.label5.Text = "Stop Win";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(88, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 24);
            this.label4.TabIndex = 3;
            this.label4.Text = "Stop Loss";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "Valor Entrada Atual";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(74, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Banca Real";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(65, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Banca Teste";
            // 
            // txtUserId
            // 
            this.txtUserId.Location = new System.Drawing.Point(882, 27);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(100, 20);
            this.txtUserId.TabIndex = 6;
            this.txtUserId.Visible = false;
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1219, 700);
            this.Controls.Add(this.txtUserId);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Menu";
            this.Text = "Menu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Menu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem operacionalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nemuCadastrar;
        private System.Windows.Forms.ToolStripMenuItem relatoriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuCotacao;
        private System.Windows.Forms.ToolStripMenuItem mOEDASToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblParidadeB;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblParidadeD;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSaldoStopWin;
        private System.Windows.Forms.TextBox txtSaldoStopLoss;
        private System.Windows.Forms.Label label8;
        protected System.Windows.Forms.TextBox textBox5;
        protected System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNivel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtSemana;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox txtEurUsd;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtStopWin;
        private System.Windows.Forms.TextBox txtStopLoss;
        private System.Windows.Forms.TextBox txtEntrada;
        private System.Windows.Forms.TextBox txtValorBReal;
        private System.Windows.Forms.TextBox txtValorBTeste;
        private System.Windows.Forms.ToolStripMenuItem pesquisarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operacionalSemanalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operacionalDiarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operacionalMêsToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.ToolStripMenuItem execultarRobôToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bynariaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subNenuOTC;
        private System.Windows.Forms.ToolStripMenuItem digitalToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bynariaToolStripMenuItem1;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem munuPesquisa;
        private System.Windows.Forms.ToolStripMenuItem tENDÊNCIASToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dIARIAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hORASToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mINUTOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mINUTOSToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mINUTOSToolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem subNemuDeleOperacional;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem menuAutenticacao;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem adicionarToolStripMenuItem;
    }
}