﻿using MongoDB.Driver;
using MongoDB.Bson;
using MongoDbCRUD.Models.Respository;
using System.Collections.Generic;
using OperacionaBinary.Context.Repository;

namespace MongoDbCRUD.Models
{
    // this class is our Product Collection (Product Table)
    public class AutCollection
    {

        internal MongoDBRespository _repo = new MongoDBRespository();
        public IMongoCollection<Aut> Collection;

        public AutCollection()
        {
            // here we were created new Collection with Products name
            this.Collection = _repo.db.GetCollection<Aut>("Autenticacao");
        }

        public void InsertContact(Aut contact)
        {
            this.Collection.InsertOneAsync(contact);
        }

        public List<Aut> GetAllAut()
        {
            var query = this.Collection
                .Find(new BsonDocument())
                .ToListAsync();
            return query.Result;
        }

        public Aut GetAutById(string id)
        {
            var product = this.Collection.Find(
                    new BsonDocument { { "_id", new ObjectId(id) } })
                    .FirstAsync()
                    .Result;
            return product;
        }

        public void UpdateContact(string id, Aut contact)
        {
            contact.Id = new ObjectId(id);

            var filter = Builders<Aut>
                .Filter
                .Eq(s => s.Id, contact.Id);
            this.Collection.ReplaceOneAsync(filter, contact);

        }

        public void DeleteContact(string AutId)
        {
            Aut contact = new Aut();
            contact.Id = new ObjectId(AutId);
            var filter = Builders<Aut>.Filter.Eq(s => s.Id, contact.Id);
            this.Collection.DeleteOneAsync(filter);

        }
    }
}