﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MongoDbCRUD.Models
{
    public class Usuario
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [Required]
        [BsonElement("Nome")]
        public string Nome { get; set; }
        [Required]
        [BsonElement("EmailIq")]
        public string EmailIq { get; set; }
        [Required]
        [BsonElement("Senha")]
        public string Senha { get; set; }

    }
}