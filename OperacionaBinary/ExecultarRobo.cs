﻿using MongoDbCRUD.Models;
using OperacionaBinary.API;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OperacionaBinary
{
    public partial class ExecultarRobo : Form
    {
        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();

        private TraydersStatusCollection dbStatusTryder = new TraydersStatusCollection();
        private UsuarioCollection dbUsuario = new UsuarioCollection();

        private Thread _paridadeMoedas;


        public ExecultarRobo()
        {
            InitializeComponent();
        }

        string usuarioIdDb = "";

        public ExecultarRobo(string usuarioId)
        {
            usuarioIdDb = usuarioId;

            InitializeComponent();
        }

        private OperacionalCollection dbOperacional = new OperacionalCollection();

        private void ExecultarRobo_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            CheckForIllegalCrossThreadCalls = false;

            GetBloqueiaBynari();
            //GetBloqueiaDigital();
            ////criação da thread de verificação e sua execução
            _paridadeMoedas = new Thread(VerificarHorario);
            _paridadeMoedas.Start();
            CheckForIllegalCrossThreadCalls = false;
        }

        protected void VerificarHorario()
        {
            var tempoAtualizacao = 18000;
            while (true)
            {
                GetMoedas();

                Thread.Sleep(tempoAtualizacao);
            }
        }


        public void GetBloqueiaBynari()
        {
            btnStartB01.Visible = false;
            btnStartB02.Visible = false;
            btnStartB03.Visible = false;
            btnStartB04.Visible = false;
            btnStartB05.Visible = false;
            btnStartB06.Visible = false;
            btnStartB07.Visible = false;
            btnStartB08.Visible = false;
            btnStartB09.Visible = false;
            btnStartB10.Visible = false;
            btnStartB11.Visible = false;
            btnStartB12.Visible = false;
            btnStartB13.Visible = false;
            btnStartB14.Visible = false;
            btnStartB15.Visible = false;
            btnStartB16.Visible = false;
            btnStartB17.Visible = false;
            btnStartB18.Visible = false;
            btnStartB19.Visible = false;
            btnStartB20.Visible = false;
            btnStartB21.Visible = false;
            btnStartB22.Visible = false;

            btnStartB0101.Visible = false;
            btnStartB0102.Visible = false;
            btnStartB0103.Visible = false;
            btnStartB0104.Visible = false;
            btnStartB0105.Visible = false;
            btnStartB0106.Visible = false;
            btnStartB0107.Visible = false;
            btnStartB0108.Visible = false;
            btnStartB0109.Visible = false;
            btnStartB0110.Visible = false;
            btnStartB0111.Visible = false;
            btnStartB0112.Visible = false;
            btnStartB0113.Visible = false;
            btnStartB0114.Visible = false;
            btnStartB0115.Visible = false;
            btnStartB0116.Visible = false;
            btnStartB0117.Visible = false;
            btnStartB0118.Visible = false;
            btnStartB0119.Visible = false;
            btnStartB0120.Visible = false;
            btnStartB0121.Visible = false;
            btnStartB0122.Visible = false;


            btnStartPG01.Visible = false;
            btnStartPG02.Visible = false;
            btnStartPG03.Visible = false;
            btnStartPG04.Visible = false;
            btnStartPG05.Visible = false;
            btnStartPG06.Visible = false;
            btnStartPG07.Visible = false;
            btnStartPG08.Visible = false;
            btnStartPG09.Visible = false;
            btnStartPG10.Visible = false;
            btnStartPG11.Visible = false;
            btnStartPG12.Visible = false;
            btnStartPG13.Visible = false;
            btnStartPG14.Visible = false;
            btnStartPG15.Visible = false;
            btnStartPG16.Visible = false;
            btnStartPG17.Visible = false;
            btnStartPG18.Visible = false;
            btnStartPG19.Visible = false;
            btnStartPG20.Visible = false;
            btnStartPG21.Visible = false;
            btnStartPG22.Visible = false;


            btnStartBPG0101.Visible = false;
            btnStartBPG0102.Visible = false;
            btnStartBPG0103.Visible = false;
            btnStartBPG0104.Visible = false;
            btnStartBPG0105.Visible = false;
            btnStartBPG0106.Visible = false;
            btnStartBPG0107.Visible = false;
            btnStartBPG0108.Visible = false;
            btnStartBPG0109.Visible = false;
            btnStartBPG0110.Visible = false;
            btnStartBPG0111.Visible = false;
            btnStartBPG0112.Visible = false;
            btnStartBPG0113.Visible = false;
            btnStartBPG0114.Visible = false;
            btnStartBPG0115.Visible = false;
            btnStartBPG0116.Visible = false;
            btnStartBPG0117.Visible = false;
            btnStartBPG0118.Visible = false;
            btnStartBPG0119.Visible = false;
            btnStartBPG0120.Visible = false;
            btnStartBPG0121.Visible = false;
            btnStartBPG0122.Visible = false;

            btnStartGeral01.Visible = false;
            btnStartGeral02.Visible = false;
            btnStartGeral03.Visible = false;
            btnStartGeral04.Visible = false;
            btnStartGeral05.Visible = false;
            btnStartGeral06.Visible = false;
            btnStartGeral07.Visible = false;
            btnStartGeral08.Visible = false;
            btnStartGeral09.Visible = false;
            btnStartGeral10.Visible = false;
            btnStartGeral11.Visible = false;
            btnStartGeral12.Visible = false;
            btnStartGeral13.Visible = false;
            btnStartGeral14.Visible = false;
            btnStartGeral15.Visible = false;
            btnStartGeral16.Visible = false;
            btnStartGeral17.Visible = false;
            btnStartGeral18.Visible = false;
            btnStartGeral19.Visible = false;
            btnStartGeral20.Visible = false;
            btnStartGeral21.Visible = false;
            btnStartGeral22.Visible = false;


        }

        public void GetBloqueiaDigital()
        {
            btnStartD01.Visible = false;
            btnStartD02.Visible = false;
            btnStartD03.Visible = false;
            btnStartD04.Visible = false;
            btnStartD05.Visible = false;
            btnStartD06.Visible = false;
            btnStartD07.Visible = false;
            btnStartD08.Visible = false;
            btnStartD09.Visible = false;
            btnStartD10.Visible = false;
            btnStartD11.Visible = false;
            btnStartD12.Visible = false;
            btnStartD13.Visible = false;
            btnStartD14.Visible = false;
            btnStartD15.Visible = false;
            btnStartD16.Visible = false;
            btnStartD17.Visible = false;
            btnStartD18.Visible = false;


            btnStartD0101.Visible = false;
            btnStartD0102.Visible = false;
            btnStartD0103.Visible = false;
            btnStartD0104.Visible = false;
            btnStartD0105.Visible = false;
            btnStartD0106.Visible = false;
            btnStartD0107.Visible = false;
            btnStartD0108.Visible = false;
            btnStartD0109.Visible = false;
            btnStartD0110.Visible = false;
            btnStartD0111.Visible = false;
            btnStartD0112.Visible = false;
            btnStartD0113.Visible = false;
            btnStartD0114.Visible = false;
            btnStartD0115.Visible = false;
            btnStartD0116.Visible = false;
            btnStartD0117.Visible = false;
            btnStartD0118.Visible = false;


        }


        private void button1_Click(object sender, EventArgs e)
        {
            LigarBynaria();
        }


        public void LigarBynaria()
        {
            var tec01 = 100;
            var tec02 = 100;
            var tec03 = 100;
            var tec04 = 100;
            var tec05 = 100;
            var tec06 = 100;

            var diretorioBynari = "";
            var tecnica = dbOperacional.GetAllOperacional();
            tecnica = tecnica.FindAll(c => c.DiretorioBinario != "");

            if (chPullBachTecnico.Checked == false && chPropabilisticaTecnico.Checked == false && chPullBachGeral.Checked == false && chPropabilisticaGeral.Checked == false && chTecnicaGeral.Checked == false)
            {
                MessageBox.Show("É Necessario Adicionar  Uma Tecnica Antes De Ligar O Robô", "Verificação Tecnica", MessageBoxButtons.OK, MessageBoxIcon.Error);
                chPropabilisticaTecnico.Checked = true;
            }
            else
            {
                if (chPullBachTecnico.Checked == true)
                {
                    tec01 = 45316621;//Canga
                }

                if (chPullBachGeral.Checked == true)
                {
                    tec02 = 22756290;//Rodrigo L.
                }

                if (chPropabilisticaTecnico.Checked == true)
                {
                    tec03 = 51292712;//Fabio G.

                }

                if (chPropabilisticaGeral.Checked == true)
                {
                    tec04 = 45844787;//Elizabeth R.
                }

                if (chTecnicaGeral.Checked == true)
                {
                    tec05 = 44707398;//Tomais
                }

                if (chTecnicaSW.Checked == true)
                {
                    tec06 = 42624866;//SarwaKa S.
                }

                //Ligando Robô
                if (tec01 != 100)
                {
                    diretorioBynari = tecnica.Find(c => c.TrayderId == tec01).DiretorioBinario;
                    LigarRobosBynarioTec01(diretorioBynari);
                }

                if (tec02 != 100)
                {
                    diretorioBynari = tecnica.Find(c => c.TrayderId == tec02).DiretorioBinario;
                    LigarRobosBynarioTec02(diretorioBynari);

                }

                if (tec03 != 100)
                {
                    diretorioBynari = tecnica.Find(c => c.TrayderId == tec03).DiretorioBinario;
                    LigarRobosBynarioTec03(diretorioBynari);

                }

                if (tec04 != 100)
                {
                    diretorioBynari = tecnica.Find(c => c.TrayderId == tec04).DiretorioBinario;
                    LigarRobosBynarioTec04(diretorioBynari);

                }

                if (tec05 != 100)
                {
                    diretorioBynari = tecnica.Find(c => c.TrayderId == tec05).DiretorioBinario;
                    LigarRobosBynarioTec05(diretorioBynari);

                }

                if (tec06 != 100)
                {
                    diretorioBynari = tecnica.Find(c => c.TrayderId == tec05).DiretorioBinario;
                    LigarRobosBynarioTec06(diretorioBynari);

                }
            }
        }
        public void LigarDigital()
        {
            var tec01 = 100;
            var tec02 = 100;
            var tec03 = 100;
            var tec04 = 100;
            var tec05 = 100;
            var tec06 = 100;

            var diretorioDigital = "";

            var tecnica = dbOperacional.GetAllOperacional();
            tecnica = tecnica.FindAll(c => c.DiretorioDigital != "");

            if (chPullBachTecnicoD.Checked == false && chProbabilisticaD.Checked == false && chProbabilisticaGeralD.Checked == false && chProbabilisticaTecnicoD.Checked == false && chTecnicaV.Checked == false)
            {
                MessageBox.Show("É Necessario Adicionar  Uma Tecnica Antes De Ligar O Robô", "Verificação Tecnica", MessageBoxButtons.OK, MessageBoxIcon.Error);
                chPropabilisticaTecnico.Checked = true;
            }
            else
            {
                if (chPullBachTecnicoD.Checked == true)
                {
                    tec01 = 45316621;//Canga
                }

                if (chProbabilisticaD.Checked == true)
                {
                    tec02 = 22756290;//Rodrigo L.
                }

                if (chProbabilisticaTecnicoD.Checked == true)
                {
                    tec03 = 51292712;//Fabio G.

                }

                if (chProbabilisticaGeralD.Checked == true)
                {
                    tec04 = 6822849;//Vinicios
                }

                if (chTecnicaV.Checked == true)
                {
                    tec05 = 44707398;//Thomas S.
                }

                if (chTecnicaSW.Checked == true)
                {
                    tec06 = 42624866;//SarwaKa S.
                }

                //Ligando Robô
                if (tec01 != 100)
                {
                    diretorioDigital = tecnica.Find(c => c.TrayderId == tec01).DiretorioDigital;
                    LigarRobosDigitalTec01(diretorioDigital);
                }

                if (tec02 != 100)
                {
                    diretorioDigital = tecnica.Find(c => c.TrayderId == tec02).DiretorioDigital;
                    LigarRobosDigitalTec02(diretorioDigital);

                }

                if (tec03 != 100)
                {
                    diretorioDigital = tecnica.Find(c => c.TrayderId == tec03).DiretorioDigital;
                    LigarRobosDigitalTec03(diretorioDigital);

                }

                if (tec04 != 100)
                {
                    diretorioDigital = tecnica.Find(c => c.TrayderId == tec04).DiretorioDigital;
                    LigarRobosDigitalTec04(diretorioDigital);

                }

                if (tec05 != 100)
                {
                    diretorioDigital = tecnica.Find(c => c.TrayderId == tec05).DiretorioDigital;
                    LigarRobosDigitalTec05(diretorioDigital);

                }

                if (tec06 != 100)
                {
                    diretorioDigital = tecnica.Find(c => c.TrayderId == tec05).DiretorioDigital;
                    LigarRobosDigitalTec06(diretorioDigital);

                }
            }
        }


        public void LigarBynariaOTC()
        {
            var tec01 = 100;
            var tec02 = 100;
            var diretorioBynari = "";
            var tecnica = dbOperacional.GetAllOperacional();
            tecnica = tecnica.FindAll(c => c.DiretorioBinario != "");

            if (chPullBachTecnicoBOTC.Checked == false && chOTCGeral.Checked == false)
            {
                MessageBox.Show("É Necessario Adicionar  Uma Tecnica Antes De Ligar O Robô", "Verificação Tecnica", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                if (chPullBachTecnicoBOTC.Checked == true)
                {
                    tec01 = 45316621;//Canga
                }

                if (chOTCGeral.Checked == true)
                {
                    tec02 = 1000;//Top100
                }

                //Ligando Robô
                //if (tec01 != 100)
                //{
                //    diretorioBynari = tecnica.Find(c => c.TrayderId == tec01 && c.DiretorioBinario.Contains("OtcBynaria")).DiretorioBinario;
                //    LigarRobosBynarioTec01OTC(diretorioBynari);
                //}

                if (tec02 != 100)
                {
                    diretorioBynari = tecnica.Find(c => c.TrayderId == tec02).DiretorioBinario;
                    LigarRobosBynarioTec02(diretorioBynari);

                }
            }
        }
        public void LigarDigitalOTC()
        {
            var tec01 = 100;
            var tec02 = 100;
            var diretorioBynari = "";
            var tecnica = dbOperacional.GetAllOperacional();
            tecnica = tecnica.FindAll(c => c.DiretorioDigital != "");

            if (chPullBachTecnicoDOTC.Checked == false && chPullGeralD.Checked == false)
            {
                MessageBox.Show("É Necessario Adicionar  Uma Tecnica Antes De Ligar O Robô", "Verificação Tecnica", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                if (chPullBachTecnicoDOTC.Checked == true)
                {
                    tec01 = 45316621;//Canga
                }

                if (chPullGeralD.Checked == true)
                {
                    tec02 = 1000;//Top100
                }

                ////Ligando Robô
                //if (tec01 != 100)
                //{
                //    diretorioBynari = tecnica.Find(c => c.TrayderId == tec01 && c.DiretorioDigital.Contains("OtcDigital")).DiretorioDigital;
                //    LigarRobosBynarioTec01OTCDigital(diretorioBynari);
                //}

                //if (tec02 != 100)
                //{
                //    diretorioBynari = tecnica.Find(c => c.TrayderId == tec02).DiretorioDigital;
                //    LigarRobosBynarioTec01OTCDigital(diretorioBynari);

                //}
            }
        }

        private void LigarRobosBynarioTec01(string diretorioBynari)
        {
            var itemDBynari = "";

            if (chMoedaB01.Checked == true && btnStartB01.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                btnStartB01.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB01.Visible = true;
            }

            if (chMoedaB02.Checked == true && btnStartB02.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                btnStartB02.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB02.Visible = true;
            }

            if (chMoedaB03.Checked == true && btnStartB03.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                btnStartB03.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB03.Visible = true;
            }

            if (chMoedaB04.Checked == true && btnStartB04.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                btnStartB04.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB04.Visible = true;
            }

            if (chMoedaB05.Checked == true && btnStartB05.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                btnStartB05.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB05.Visible = true;
            }

            if (chMoedaB06.Checked == true && btnStartB06.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                btnStartB06.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB06.Visible = true;
            }

            if (chMoedaB07.Checked == true && btnStartB07.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                btnStartB07.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB07.Visible = true;
            }


            if (chMoedaB08.Checked == true && btnStartB08.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                btnStartB08.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB08.Visible = true;
            }



            if (chMoedaB09.Checked == true && btnStartB09.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                btnStartB09.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB09.Visible = true;
            }



            if (chMoedaB10.Checked == true && btnStartB10.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                btnStartB10.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB10.Visible = true;
            }



            if (chMoedaB11.Checked == true && btnStartB11.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                btnStartB11.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB11.Visible = true;
            }


            if (chMoedaB12.Checked == true && btnStartB12.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                btnStartB12.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB12.Visible = true;
            }




            if (chMoedaB13.Checked == true && btnStartB13.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                btnStartB13.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB13.Visible = true;
            }



            if (chMoedaB14.Checked == true && btnStartB14.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                btnStartB14.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB14.Visible = true;
            }

            if (chMoedaB15.Checked == true && btnStartB15.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                btnStartB15.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB15.Visible = true;
            }

            if (chMoedaB16.Checked == true && btnStartB16.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB16.Enabled = false;
                btnStartB16.Visible = true;
            }

            if (chMoedaB17.Checked == true && btnStartB17.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-NZD\config.txt");
                btnStartB17.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB17.Visible = true;
            }

            if (chMoedaB18.Checked == true && btnStartB18.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\CAD-JPY\config.txt");
                btnStartB18.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB18.Visible = true;
            }

            if (chMoedaB19.Checked == true && btnStartB19.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                btnStartB19.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB19.Visible = true;
            }



            if (chMoedaB20.Checked == true && btnStartB20.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                btnStartB20.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB20.Visible = true;
            }



            if (chMoedaB21.Checked == true && btnStartB21.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                btnStartB21.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB21.Visible = true;
            }

            if (chMoedaB22.Checked == true && btnStartB22.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                btnStartB22.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB22.Visible = true;
            }



            if (chMoedaB01.Checked == false && chMoedaB02.Checked == false && chMoedaB03.Checked == false && chMoedaB04.Checked == false &&
                chMoedaB05.Checked == false && chMoedaB06.Checked == false && chMoedaB07.Checked == false && chMoedaB08.Checked == false &&
                chMoedaB09.Checked == false && chMoedaB10.Checked == false && chMoedaB11.Checked == false && chMoedaB12.Checked == false &&
                chMoedaB13.Checked == false && chMoedaB14.Checked == false && chMoedaB15.Checked == false && chMoedaB16.Checked == false &&
                chMoedaB17.Checked == false && chMoedaB18.Checked == false && chMoedaB19.Checked == false && chMoedaB20.Checked == false &&
                chMoedaB21.Checked == false && chMoedaB22.Checked == false

                )
            {
                MessageBox.Show("Para ligar Robô e Necessario selecionar uma Paridade", "Verificação Paridade", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void LigarRobosBynarioTec02(string diretorioBynari)
        {
            var itemDBynari = "";

            if (chMoedaB01.Checked == true && btnStartB0101.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                btnStartB0101.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0101.Visible = true;
            }

            if (chMoedaB02.Checked == true && btnStartB0102.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                btnStartB0102.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0102.Visible = true;
            }

            if (chMoedaB03.Checked == true && btnStartB0103.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                btnStartB0103.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0103.Visible = true;
            }

            if (chMoedaB04.Checked == true && btnStartB0104.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                btnStartB0104.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0104.Visible = true;
            }

            if (chMoedaB05.Checked == true && btnStartB0105.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                btnStartB0105.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0105.Visible = true;
            }


            if (chMoedaB21.Checked == true && btnStartB0121.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                btnStartB0121.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0121.Visible = true;
            }

            if (chMoedaB22.Checked == true && btnStartB0122.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                btnStartB0122.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0122.Visible = true;
            }

            if (chMoedaB06.Checked == true && btnStartB0106.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                btnStartB0106.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0106.Visible = true;
            }

            if (chMoedaB07.Checked == true && btnStartB0107.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                btnStartB0107.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0107.Visible = true;
            }

            if (chMoedaB08.Checked == true && btnStartB0108.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                btnStartB0108.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0108.Visible = true;
            }

            if (chMoedaB09.Checked == true && btnStartB0109.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                btnStartB0109.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0109.Visible = true;
            }

            if (chMoedaB10.Checked == true && btnStartB0110.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                btnStartB0110.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0110.Visible = true;
            }

            if (chMoedaB11.Checked == true && btnStartB0111.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                btnStartB0111.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0111.Visible = true;
            }

            if (chMoedaB13.Checked == true && btnStartB0113.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                btnStartB0113.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0113.Visible = true;
            }

            if (chMoedaB14.Checked == true && btnStartB0114.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                btnStartB0114.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0114.Visible = true;
            }

            if (chMoedaB17.Checked == true && btnStartB0117.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-NZD\config.txt");
                btnStartB0117.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0117.Visible = true;
            }

            if (chMoedaB13.Checked == true && btnStartB0113.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                btnStartB0113.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0113.Visible = true;
            }

            if (chMoedaB12.Checked == true && btnStartB0112.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                btnStartB0112.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0112.Visible = true;
            }

            if (chMoedaB15.Checked == true && btnStartB0115.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                btnStartB0115.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0115.Visible = true;
            }

            if (chMoedaB16.Checked == true && btnStartB0116.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0116.Enabled = false;
                btnStartB0116.Visible = true;
            }

            if (chMoedaB19.Checked == true && btnStartB0119.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                btnStartB0119.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0119.Visible = true;
            }

            if (chMoedaB20.Checked == true && btnStartB0120.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                btnStartB0120.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartB0120.Visible = true;
            }

            if (chMoedaB01.Checked == false && chMoedaB02.Checked == false && chMoedaB03.Checked == false && chMoedaB04.Checked == false &&
                chMoedaB05.Checked == false && chMoedaB06.Checked == false && chMoedaB07.Checked == false && chMoedaB08.Checked == false &&
                chMoedaB09.Checked == false && chMoedaB10.Checked == false && chMoedaB11.Checked == false && chMoedaB12.Checked == false &&
                chMoedaB13.Checked == false && chMoedaB14.Checked == false && chMoedaB15.Checked == false && chMoedaB16.Checked == false &&
                chMoedaB17.Checked == false && chMoedaB18.Checked == false && chMoedaB19.Checked == false && chMoedaB20.Checked == false &&
                chMoedaB21.Checked == false && chMoedaB22.Checked == false

                )
            {
                MessageBox.Show("Para ligar Robô e Necessario selecionar uma Paridade", "Verificação Paridade", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void LigarRobosBynarioTec03(string diretorioBynari)
        {
            var itemDBynari = "";

            if (chMoedaB01.Checked == true && btnStartPG01.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                btnStartPG01.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG01.Visible = true;
            }

            if (chMoedaB02.Checked == true && btnStartPG02.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                btnStartPG02.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG02.Visible = true;
            }

            if (chMoedaB03.Checked == true && btnStartPG03.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                btnStartPG03.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG03.Visible = true;
            }

            if (chMoedaB04.Checked == true && btnStartPG04.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                btnStartPG04.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG04.Visible = true;
            }

            if (chMoedaB05.Checked == true && btnStartPG05.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                btnStartPG05.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG05.Visible = true;
            }


            if (chMoedaB21.Checked == true && btnStartPG21.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                btnStartPG21.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG21.Visible = true;
            }

            if (chMoedaB22.Checked == true && btnStartPG22.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                btnStartPG22.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG22.Visible = true;
            }

            if (chMoedaB06.Checked == true && btnStartPG06.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                btnStartPG06.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG06.Visible = true;
            }

            if (chMoedaB07.Checked == true && btnStartPG07.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                btnStartPG07.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG07.Visible = true;
            }

            if (chMoedaB08.Checked == true && btnStartPG08.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                btnStartPG08.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG08.Visible = true;
            }

            if (chMoedaB09.Checked == true && btnStartPG09.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                btnStartPG09.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG09.Visible = true;
            }

            if (chMoedaB10.Checked == true && btnStartPG10.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                btnStartPG10.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG10.Visible = true;
            }

            if (chMoedaB11.Checked == true && btnStartPG11.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                btnStartPG11.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG11.Visible = true;
            }

            if (chMoedaB13.Checked == true && btnStartPG13.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                btnStartPG13.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG13.Visible = true;
            }

            if (chMoedaB14.Checked == true && btnStartPG14.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                btnStartPG14.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG14.Visible = true;
            }

            if (chMoedaB17.Checked == true && btnStartPG17.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-NZD\config.txt");
                btnStartPG17.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG17.Visible = true;
            }

            if (chMoedaB13.Checked == true && btnStartPG13.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                btnStartPG13.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG13.Visible = true;
            }


            if (chMoedaB12.Checked == true && btnStartPG12.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                btnStartPG12.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG12.Visible = true;
            }

            if (chMoedaB15.Checked == true && btnStartPG15.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                btnStartPG15.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG15.Visible = true;
            }

            if (chMoedaB16.Checked == true && btnStartPG16.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG16.Enabled = false;
                btnStartPG16.Visible = true;
            }

            if (chMoedaB19.Checked == true && btnStartPG19.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                btnStartPG19.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG19.Visible = true;
            }

            if (chMoedaB20.Checked == true && btnStartPG20.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                btnStartPG20.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPG20.Visible = true;
            }

            if (chMoedaB01.Checked == false && chMoedaB02.Checked == false && chMoedaB03.Checked == false && chMoedaB04.Checked == false &&
                chMoedaB05.Checked == false && chMoedaB06.Checked == false && chMoedaB07.Checked == false && chMoedaB08.Checked == false &&
                chMoedaB09.Checked == false && chMoedaB10.Checked == false && chMoedaB11.Checked == false && chMoedaB12.Checked == false &&
                chMoedaB13.Checked == false && chMoedaB14.Checked == false && chMoedaB15.Checked == false && chMoedaB16.Checked == false &&
                chMoedaB17.Checked == false && chMoedaB18.Checked == false && chMoedaB19.Checked == false && chMoedaB20.Checked == false &&
                chMoedaB21.Checked == false && chMoedaB22.Checked == false

                )
            {
                MessageBox.Show("Para ligar Robô e Necessario selecionar uma Paridade", "Verificação Paridade", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void LigarRobosBynarioTec04(string diretorioBynari)
        {
            var itemDBynari = "";

            if (chMoedaB01.Checked == true && btnStartBPG0101.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                btnStartBPG0101.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0101.Visible = true;
            }

            if (chMoedaB02.Checked == true && btnStartBPG0102.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                btnStartBPG0102.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0102.Visible = true;
            }

            if (chMoedaB03.Checked == true && btnStartBPG0103.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                btnStartBPG0103.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0103.Visible = true;
            }

            if (chMoedaB04.Checked == true && btnStartBPG0104.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                btnStartBPG0104.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0104.Visible = true;
            }

            if (chMoedaB05.Checked == true && btnStartBPG0105.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                btnStartBPG0105.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0105.Visible = true;
            }


            if (chMoedaB21.Checked == true && btnStartBPG0121.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                btnStartBPG0121.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0121.Visible = true;
            }

            if (chMoedaB22.Checked == true && btnStartBPG0122.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                btnStartBPG0122.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0122.Visible = true;
            }

            if (chMoedaB06.Checked == true && btnStartBPG0106.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                btnStartBPG0106.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0106.Visible = true;
            }

            if (chMoedaB07.Checked == true && btnStartBPG0107.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                btnStartBPG0107.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0107.Visible = true;
            }

            if (chMoedaB08.Checked == true && btnStartBPG0108.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                btnStartBPG0108.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0108.Visible = true;
            }

            if (chMoedaB09.Checked == true && btnStartBPG0109.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                btnStartBPG0109.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0109.Visible = true;
            }

            if (chMoedaB10.Checked == true && btnStartBPG0110.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                btnStartBPG0110.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0110.Visible = true;
            }

            if (chMoedaB11.Checked == true && btnStartBPG0111.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                btnStartBPG0111.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0111.Visible = true;
            }

            if (chMoedaB13.Checked == true && btnStartBPG0113.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                btnStartBPG0113.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0113.Visible = true;
            }

            if (chMoedaB14.Checked == true && btnStartBPG0114.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                btnStartBPG0114.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0114.Visible = true;
            }

            if (chMoedaB17.Checked == true && btnStartBPG0117.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-NZD\config.txt");
                btnStartBPG0117.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0117.Visible = true;
            }

            if (chMoedaB13.Checked == true && btnStartBPG0113.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                btnStartBPG0113.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0113.Visible = true;
            }


            if (chMoedaB12.Checked == true && btnStartBPG0112.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                btnStartBPG0112.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0112.Visible = true;
            }

            if (chMoedaB15.Checked == true && btnStartBPG0115.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                btnStartBPG0115.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0115.Visible = true;
            }

            if (chMoedaB16.Checked == true && btnStartBPG0116.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0116.Enabled = false;
                btnStartBPG0116.Visible = true;
            }

            if (chMoedaB19.Checked == true && btnStartBPG0119.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                btnStartBPG0119.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0119.Visible = true;
            }

            if (chMoedaB20.Checked == true && btnStartBPG0120.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                btnStartBPG0120.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartBPG0120.Visible = true;
            }

            if (chMoedaB01.Checked == false && chMoedaB02.Checked == false && chMoedaB03.Checked == false && chMoedaB04.Checked == false &&
                chMoedaB05.Checked == false && chMoedaB06.Checked == false && chMoedaB07.Checked == false && chMoedaB08.Checked == false &&
                chMoedaB09.Checked == false && chMoedaB10.Checked == false && chMoedaB11.Checked == false && chMoedaB12.Checked == false &&
                chMoedaB13.Checked == false && chMoedaB14.Checked == false && chMoedaB15.Checked == false && chMoedaB16.Checked == false &&
                chMoedaB17.Checked == false && chMoedaB18.Checked == false && chMoedaB19.Checked == false && chMoedaB20.Checked == false &&
                chMoedaB21.Checked == false && chMoedaB22.Checked == false

                )
            {
                MessageBox.Show("Para ligar Robô e Necessario selecionar uma Paridade", "Verificação Paridade", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void LigarRobosBynarioTec05(string diretorioBynari)
        {
            var itemDBynari = "";

            if (chMoedaB01.Checked == true && btnStartGeral01.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                btnStartGeral01.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral01.Visible = true;
            }

            if (chMoedaB02.Checked == true && btnStartGeral02.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                btnStartGeral02.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral02.Visible = true;
            }

            if (chMoedaB03.Checked == true && btnStartGeral03.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                btnStartGeral03.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral03.Visible = true;
            }

            if (chMoedaB04.Checked == true && btnStartGeral04.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                btnStartGeral04.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral04.Visible = true;
            }

            if (chMoedaB05.Checked == true && btnStartGeral05.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                btnStartGeral05.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral05.Visible = true;
            }


            if (chMoedaB21.Checked == true && btnStartGeral21.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                btnStartGeral21.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral21.Visible = true;
            }

            if (chMoedaB22.Checked == true && btnStartGeral22.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                btnStartGeral22.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral22.Visible = true;
            }

            if (chMoedaB06.Checked == true && btnStartGeral06.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                btnStartGeral06.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral06.Visible = true;
            }

            if (chMoedaB07.Checked == true && btnStartGeral07.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                btnStartGeral07.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral07.Visible = true;
            }

            if (chMoedaB08.Checked == true && btnStartGeral08.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                btnStartGeral08.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral08.Visible = true;
            }

            if (chMoedaB09.Checked == true && btnStartGeral09.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                btnStartGeral08.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral08.Visible = true;
            }

            if (chMoedaB10.Checked == true && btnStartGeral10.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                btnStartGeral10.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral10.Visible = true;
            }

            if (chMoedaB11.Checked == true && btnStartGeral11.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                btnStartGeral11.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral11.Visible = true;
            }

            if (chMoedaB13.Checked == true && btnStartGeral13.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                btnStartGeral13.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral13.Visible = true;
            }

            if (chMoedaB14.Checked == true && btnStartGeral14.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                btnStartGeral14.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral14.Visible = true;
            }

            if (chMoedaB17.Checked == true && btnStartGeral17.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-NZD\config.txt");
                btnStartGeral17.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral17.Visible = true;
            }

            if (chMoedaB13.Checked == true && btnStartGeral13.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                btnStartGeral13.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral13.Visible = true;
            }


            if (chMoedaB12.Checked == true && btnStartGeral12.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                btnStartGeral12.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral12.Visible = true;
            }

            if (chMoedaB15.Checked == true && btnStartGeral15.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                btnStartGeral15.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral15.Visible = true;
            }

            if (chMoedaB16.Checked == true && btnStartGeral16.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral16.Enabled = false;
                btnStartGeral16.Visible = true;
            }

            if (chMoedaB19.Checked == true && btnStartGeral19.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                btnStartGeral19.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral19.Visible = true;
            }

            if (chMoedaB20.Checked == true && btnStartGeral20.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                btnStartGeral20.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral20.Visible = true;
            }

            if (chMoedaB01.Checked == false && chMoedaB02.Checked == false && chMoedaB03.Checked == false && chMoedaB04.Checked == false &&
                chMoedaB05.Checked == false && chMoedaB06.Checked == false && chMoedaB07.Checked == false && chMoedaB08.Checked == false &&
                chMoedaB09.Checked == false && chMoedaB10.Checked == false && chMoedaB11.Checked == false && chMoedaB12.Checked == false &&
                chMoedaB13.Checked == false && chMoedaB14.Checked == false && chMoedaB15.Checked == false && chMoedaB16.Checked == false &&
                chMoedaB17.Checked == false && chMoedaB18.Checked == false && chMoedaB19.Checked == false && chMoedaB20.Checked == false &&
                chMoedaB21.Checked == false && chMoedaB22.Checked == false

                )
            {
                MessageBox.Show("Para ligar Robô e Necessario selecionar uma Paridade", "Verificação Paridade", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void LigarRobosBynarioTec06(string diretorioBynari)
        {
            var itemDBynari = "";

            if (chMoedaB01.Checked == true && btnSt01.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                btnSt01.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt01.Visible = true;
            }

            if (chMoedaB02.Checked == true && btnSt02.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                btnSt02.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt02.Visible = true;
            }

            if (chMoedaB03.Checked == true && btnSt03.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                btnSt03.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt03.Visible = true;
            }

            if (chMoedaB04.Checked == true && btnSt04.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                btnSt04.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartGeral04.Visible = true;
            }

            if (chMoedaB05.Checked == true && btnSt05.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                btnSt05.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt05.Visible = true;
            }


            if (chMoedaB21.Checked == true && btnSt21.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                btnSt21.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt21.Visible = true;
            }

            if (chMoedaB22.Checked == true && btnSt22.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                btnSt22.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt22.Visible = true;
            }

            if (chMoedaB06.Checked == true && btnSt06.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                btnSt06.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt06.Visible = true;
            }

            if (chMoedaB07.Checked == true && btnSt07.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                btnSt07.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt07.Visible = true;
            }

            if (chMoedaB08.Checked == true && btnSt08.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                btnSt08.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt08.Visible = true;
            }

            if (chMoedaB09.Checked == true && btnSt09.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                btnSt09.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt09.Visible = true;
            }

            if (chMoedaB10.Checked == true && btnSt10.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                btnSt10.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt10.Visible = true;
            }

            if (chMoedaB11.Checked == true && btnSt11.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                btnSt11.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt11.Visible = true;
            }

            if (chMoedaB13.Checked == true && btnSt13.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                btnSt13.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt13.Visible = true;
            }

            if (chMoedaB14.Checked == true && btnSt14.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                btnSt14.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt14.Visible = true;
            }

            if (chMoedaB17.Checked == true && btnSt17.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-NZD\config.txt");
                btnSt17.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt17.Visible = true;
            }

            if (chMoedaB13.Checked == true && btnSt13.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                btnSt13.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt13.Visible = true;
            }


            if (chMoedaB12.Checked == true && btnSt12.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                btnSt12.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt12.Visible = true;
            }

            if (chMoedaB15.Checked == true && btnSt15.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                btnSt15.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt15.Visible = true;
            }

            if (chMoedaB16.Checked == true && btnSt16.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt16.Enabled = false;
                btnSt16.Visible = true;
            }

            if (chMoedaB19.Checked == true && btnSt19.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                btnSt19.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt19.Visible = true;
            }

            if (chMoedaB20.Checked == true && btnSt20.Enabled == true)
            {
                itemDBynari = diretorioBynari;
                itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                btnSt20.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnSt20.Visible = true;
            }

            if (chMoedaB01.Checked == false && chMoedaB02.Checked == false && chMoedaB03.Checked == false && chMoedaB04.Checked == false &&
                chMoedaB05.Checked == false && chMoedaB06.Checked == false && chMoedaB07.Checked == false && chMoedaB08.Checked == false &&
                chMoedaB09.Checked == false && chMoedaB10.Checked == false && chMoedaB11.Checked == false && chMoedaB12.Checked == false &&
                chMoedaB13.Checked == false && chMoedaB14.Checked == false && chMoedaB15.Checked == false && chMoedaB16.Checked == false &&
                chMoedaB17.Checked == false && chMoedaB18.Checked == false && chMoedaB19.Checked == false && chMoedaB20.Checked == false &&
                chMoedaB21.Checked == false && chMoedaB22.Checked == false

                )
            {
                MessageBox.Show("Para ligar Robô e Necessario selecionar uma Paridade", "Verificação Paridade", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void LigarRobosDigitalTec01(string diretorioDigital)
        {
            var itemDBynari = "";

            if (chMoedaD01.Checked == true && btnStartD01.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                btnStartD01.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD01.Visible = true;
            }

            if (chMoedaD02.Checked == true && btnStartD02.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD02.Enabled = false;
                btnStartD02.Visible = true;
            }

            if (chMoedaD03.Checked == true && btnStartD03.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                btnStartD03.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD03.Visible = true;
            }

            if (chMoedaD04.Checked == true && btnStartD04.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                btnStartD04.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD04.Visible = true;
            }

            if (chMoedaD05.Checked == true && btnStartD05.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                btnStartD05.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD05.Visible = true;
            }

            if (chMoedaD06.Checked == true && btnStartD06.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                btnStartD06.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD06.Visible = true;
            }

            if (chMoedaD07.Checked == true && btnStartD07.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                btnStartD07.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD07.Visible = true;
            }


            if (chMoedaD08.Checked == true && btnStartD08.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                btnStartD08.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD08.Visible = true;
            }

            if (chMoedaD09.Checked == true && btnStartD09.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                btnStartD09.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD09.Visible = true;
            }

            if (chMoedaD10.Checked == true && btnStartD10.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                btnStartD10.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD10.Visible = true;
            }

            if (chMoedaD11.Checked == true && btnStartD11.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                btnStartD11.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD11.Visible = true;
            }


            if (chMoedaD12.Checked == true && btnStartD12.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                btnStartD12.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD12.Visible = true;
            }


            if (chMoedaD13.Checked == true && btnStartD13.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                btnStartD13.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD13.Visible = true;
            }

            if (chMoedaD14.Checked == true && btnStartD14.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                btnStartD14.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD14.Visible = true;
            }


            if (chMoedaD15.Checked == true && btnStartD15.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                btnStartD15.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD15.Visible = true;
            }


            if (chMoedaD16.Checked == true && btnStartD16.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                btnStartD16.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD16.Visible = true;
            }


            if (chMoedaD17.Checked == true && btnStartD17.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                btnStartD17.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD17.Visible = true;
            }


            if (chMoedaD18.Checked == true && btnStartD18.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-JPY\config.txt");
                btnStartD18.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD18.Visible = true;
            }


            if (chMoedaD01.Checked == false && chMoedaD02.Checked == false && chMoedaD03.Checked == false && chMoedaD04.Checked == false &&
                chMoedaD05.Checked == false && chMoedaD06.Checked == false && chMoedaD07.Checked == false && chMoedaD08.Checked == false &&
                chMoedaD09.Checked == false && chMoedaD10.Checked == false && chMoedaD11.Checked == false && chMoedaD12.Checked == false &&
                chMoedaD13.Checked == false && chMoedaD14.Checked == false && chMoedaD15.Checked == false && chMoedaD16.Checked == false &&
                chMoedaD17.Checked == false && chMoedaD18.Checked == false


                )
            {
                MessageBox.Show("Para ligar Robô e Necessario selecionar uma Paridade", "Verificação Paridade", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void LigarRobosDigitalTec02(string diretorioDigital)
        {
            var itemDBynari = "";

            if (chMoedaD01.Checked == true && btnStartD0101.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                btnStartD0101.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0101.Visible = true;
            }

            if (chMoedaD02.Checked == true && btnStartD0102.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0102.Enabled = false;
                btnStartD0102.Visible = true;
            }

            if (chMoedaD03.Checked == true && btnStartD0103.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                btnStartD0103.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0103.Visible = true;
            }

            if (chMoedaD04.Checked == true && btnStartD0104.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                btnStartD0104.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0104.Visible = true;
            }

            if (chMoedaD05.Checked == true && btnStartD0105.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                btnStartD0105.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0105.Visible = true;
            }

            if (chMoedaD06.Checked == true && btnStartD0106.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                btnStartD0106.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0106.Visible = true;
            }

            if (chMoedaD07.Checked == true && btnStartD0107.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                btnStartD0107.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0107.Visible = true;
            }


            if (chMoedaD08.Checked == true && btnStartD0108.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                btnStartD0108.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0108.Visible = true;
            }

            if (chMoedaD09.Checked == true && btnStartD0109.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                btnStartD0109.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0109.Visible = true;
            }


            if (chMoedaD10.Checked == true && btnStartD0110.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                btnStartD0110.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0110.Visible = true;
            }

            if (chMoedaD11.Checked == true && btnStartD0111.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                btnStartD0111.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0111.Visible = true;
            }


            if (chMoedaD12.Checked == true && btnStartD0112.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                btnStartD0112.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0112.Visible = true;
            }


            if (chMoedaD13.Checked == true && btnStartD0113.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                btnStartD0113.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0113.Visible = true;
            }

            if (chMoedaD14.Checked == true && btnStartD0114.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                btnStartD0114.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0114.Visible = true;
            }


            if (chMoedaD15.Checked == true && btnStartD0115.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                btnStartD0115.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0115.Visible = true;
            }


            if (chMoedaD16.Checked == true && btnStartD0116.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                btnStartD0116.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0116.Visible = true;
            }


            if (chMoedaD17.Checked == true && btnStartD0117.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                btnStartD0117.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0117.Visible = true;
            }


            if (chMoedaD18.Checked == true && btnStartD0118.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-JPY\config.txt");
                btnStartD0118.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartD0118.Visible = true;
            }


            if (chMoedaD01.Checked == false && chMoedaD02.Checked == false && chMoedaD03.Checked == false && chMoedaD04.Checked == false &&
                chMoedaD05.Checked == false && chMoedaD06.Checked == false && chMoedaD07.Checked == false && chMoedaD08.Checked == false &&
                chMoedaD09.Checked == false && chMoedaD10.Checked == false && chMoedaD11.Checked == false && chMoedaD12.Checked == false &&
                chMoedaD13.Checked == false && chMoedaD14.Checked == false && chMoedaD15.Checked == false && chMoedaD16.Checked == false &&
                chMoedaD17.Checked == false && chMoedaD18.Checked == false


                )
            {
                MessageBox.Show("Para ligar Robô e Necessario selecionar uma Paridade", "Verificação Paridade", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void LigarRobosDigitalTec03(string diretorioDigital)
        {
            var itemDBynari = "";

            if (chMoedaD01.Checked == true && btnStartPGD01.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                btnStartPGD01.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD01.Visible = true;

            }

            if (chMoedaD02.Checked == true && btnStartPGD02.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD02.Enabled = false;
                btnStartPGD02.Visible = true;
            }

            if (chMoedaD03.Checked == true && btnStartPGD03.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                btnStartPGD03.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD03.Visible = true;
            }

            if (chMoedaD04.Checked == true && btnStartPGD04.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                btnStartPGD04.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD04.Visible = true;
            }

            if (chMoedaD05.Checked == true && btnStartPGD05.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                btnStartPGD05.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD05.Visible = true;
            }

            if (chMoedaD06.Checked == true && btnStartPGD06.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                btnStartPGD06.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD06.Visible = true;
            }

            if (chMoedaD07.Checked == true && btnStartPGD07.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                btnStartPGD07.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD07.Visible = true;
            }


            if (chMoedaD08.Checked == true && btnStartPGD08.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                btnStartPGD08.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD08.Visible = true;
            }

            if (chMoedaD09.Checked == true && btnStartPGD09.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                btnStartPGD09.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD09.Visible = true;
            }


            if (chMoedaD10.Checked == true && btnStartPGD10.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                btnStartPGD10.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD10.Visible = true;
            }

            if (chMoedaD11.Checked == true && btnStartPGD11.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                btnStartPGD11.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD11.Visible = true;
            }


            if (chMoedaD12.Checked == true && btnStartPGD12.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                btnStartPGD12.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD12.Visible = true;
            }


            if (chMoedaD13.Checked == true && btnStartPGD13.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                btnStartPGD13.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD13.Visible = true;
            }

            if (chMoedaD14.Checked == true && btnStartPGD14.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                btnStartPGD14.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD14.Visible = true;
            }


            if (chMoedaD15.Checked == true && btnStartPGD15.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                btnStartPGD15.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD15.Visible = true;
            }


            if (chMoedaD16.Checked == true && btnStartPGD16.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                btnStartPGD16.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD16.Visible = true;
            }


            if (chMoedaD17.Checked == true && btnStartPGD17.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                btnStartPGD17.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD17.Visible = true;
            }


            if (chMoedaD18.Checked == true && btnStartPGD18.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-JPY\config.txt");
                btnStartPGD18.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartPGD18.Visible = true;
            }


            if (chMoedaD01.Checked == false && chMoedaD02.Checked == false && chMoedaD03.Checked == false && chMoedaD04.Checked == false &&
                chMoedaD05.Checked == false && chMoedaD06.Checked == false && chMoedaD07.Checked == false && chMoedaD08.Checked == false &&
                chMoedaD09.Checked == false && chMoedaD10.Checked == false && chMoedaD11.Checked == false && chMoedaD12.Checked == false &&
                chMoedaD13.Checked == false && chMoedaD14.Checked == false && chMoedaD15.Checked == false && chMoedaD16.Checked == false &&
                chMoedaD17.Checked == false && chMoedaD18.Checked == false


                )
            {
                MessageBox.Show("Para ligar Robô e Necessario selecionar uma Paridade", "Verificação Paridade", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void LigarRobosDigitalTec04(string diretorioDigital)
        {
            var itemDBynari = "";

            if (chMoedaD01.Checked == true && btnStartDGeral01.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                btnStartDGeral01.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral01.Visible = true;
            }

            if (chMoedaD02.Checked == true && btnStartDGeral02.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral02.Enabled = false;
                btnStartDGeral02.Visible = true;
            }

            if (chMoedaD03.Checked == true && btnStartDGeral03.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                btnStartDGeral03.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral03.Visible = true;
            }

            if (chMoedaD04.Checked == true && btnStartDGeral04.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                btnStartDGeral04.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral04.Visible = true;
            }

            if (chMoedaD05.Checked == true && btnStartDGeral05.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                btnStartDGeral04.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral04.Visible = true;
            }

            if (chMoedaD06.Checked == true && btnStartDGeral06.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                btnStartDGeral06.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral06.Visible = true;
            }

            if (chMoedaD07.Checked == true && btnStartDGeral07.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                btnStartDGeral07.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral07.Visible = true;
            }


            if (chMoedaD08.Checked == true && btnStartDGeral08.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                btnStartDGeral08.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral08.Visible = true;
            }

            if (chMoedaD09.Checked == true && btnStartDGeral09.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                btnStartDGeral09.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral09.Visible = true;
            }


            if (chMoedaD10.Checked == true && btnStartDGeral10.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                btnStartDGeral10.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral10.Visible = true;
            }

            if (chMoedaD11.Checked == true && btnStartDGeral11.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                btnStartDGeral11.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral11.Visible = true;
            }


            if (chMoedaD12.Checked == true && btnStartDGeral12.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                btnStartDGeral12.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral12.Visible = true;
            }


            if (chMoedaD13.Checked == true && btnStartDGeral13.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                btnStartDGeral13.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral13.Visible = true;
            }

            if (chMoedaD14.Checked == true && btnStartDGeral14.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                btnStartDGeral14.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral14.Visible = true;
            }


            if (chMoedaD15.Checked == true && btnStartDGeral15.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                btnStartDGeral15.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral15.Visible = true;
            }


            if (chMoedaD16.Checked == true && btnStartDGeral16.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                btnStartDGeral16.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral16.Visible = true;
            }


            if (chMoedaD17.Checked == true && btnStartDGeral17.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                btnStartDGeral17.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral17.Visible = true;
            }


            if (chMoedaD18.Checked == true && btnStartDGeral18.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-JPY\config.txt");
                btnStartDGeral18.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                Process.Start($@"{diretorioT}ROBO.brs");
                System.Threading.Thread.Sleep(7000);
                itemDBynari = "";
                btnStartDGeral18.Visible = true;
            }


            if (chMoedaD01.Checked == false && chMoedaD02.Checked == false && chMoedaD03.Checked == false && chMoedaD04.Checked == false &&
                chMoedaD05.Checked == false && chMoedaD06.Checked == false && chMoedaD07.Checked == false && chMoedaD08.Checked == false &&
                chMoedaD09.Checked == false && chMoedaD10.Checked == false && chMoedaD11.Checked == false && chMoedaD12.Checked == false &&
                chMoedaD13.Checked == false && chMoedaD14.Checked == false && chMoedaD15.Checked == false && chMoedaD16.Checked == false &&
                chMoedaD17.Checked == false && chMoedaD18.Checked == false


                )
            {
                MessageBox.Show("Para ligar Robô e Necessario selecionar uma Paridade", "Verificação Paridade", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void LigarRobosDigitalTec05(string diretorioDigital)
        {
            var itemDBynari = "";

            if (chMoedaD01.Checked == true && btnStartDTop01.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                btnStartDTop01.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartDTop01.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD02.Checked == true && btnStartDTop02.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartDTop02.Enabled = true;
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop02.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD03.Checked == true && btnStartDTop03.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                btnStartDTop03.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop03.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD04.Checked == true && btnStartDTop04.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                btnStartDTop04.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop04.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD05.Checked == true && btnStartDTop05.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                btnStartDTop05.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop05.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD06.Checked == true && btnStartDTop06.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                btnStartDTop06.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop06.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD07.Checked == true && btnStartDTop07.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                btnStartDTop07.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop07.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD08.Checked == true && btnStartDTop08.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                btnStartDTop08.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop08.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD09.Checked == true && btnStartDTop09.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                btnStartDTop09.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop09.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD10.Checked == true && btnStartDTop10.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                btnStartDTop10.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop10.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD11.Checked == true && btnStartDTop11.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                btnStartDTop11.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop11.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD12.Checked == true && btnStartDTop12.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                btnStartDTop12.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop12.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD13.Checked == true && btnStartDTop13.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                btnStartDTop13.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop13.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD14.Checked == true && btnStartDTop14.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                btnStartDTop14.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop14.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD15.Checked == true && btnStartDTop15.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                btnStartDTop15.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop15.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD16.Checked == true && btnStartDTop16.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                btnStartDTop16.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop16.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD17.Checked == true && btnStartDTop17.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                btnStartDTop17.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop17.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD18.Checked == true && btnStartDTop18.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-JPY\config.txt");
                btnStartDTop18.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //Process.Start($@"{diretorioT}ROBO.brs");
                //System.Threading.Thread.Sleep(7000);
                //itemDBynari = "";
                btnStartDTop18.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD01.Checked == false && chMoedaD02.Checked == false && chMoedaD03.Checked == false && chMoedaD04.Checked == false &&
                chMoedaD05.Checked == false && chMoedaD06.Checked == false && chMoedaD07.Checked == false && chMoedaD08.Checked == false &&
                chMoedaD09.Checked == false && chMoedaD10.Checked == false && chMoedaD11.Checked == false && chMoedaD12.Checked == false &&
                chMoedaD13.Checked == false && chMoedaD14.Checked == false && chMoedaD15.Checked == false && chMoedaD16.Checked == false &&
                chMoedaD17.Checked == false && chMoedaD18.Checked == false


                )
            {
                MessageBox.Show("Para ligar Robô e Necessario selecionar uma Paridade", "Verificação Paridade", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void LigarRobosDigitalTec06(string diretorioDigital)
        {
            var itemDBynari = "";

            if (chMoedaD01.Checked == true && btnStartSWD01.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                btnStartSWD01.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD01.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD02.Checked == true && btnStartSWD02.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD02.Enabled = false;
                btnStartSWD02.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD03.Checked == true && btnStartSWD02.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                btnStartSWD02.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD02.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD04.Checked == true && btnStartSWD04.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                btnStartSWD04.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD04.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD05.Checked == true && btnStartSWD05.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                btnStartSWD05.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD05.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD06.Checked == true && btnStartSWD06.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                btnStartSWD06.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD06.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD07.Checked == true && btnStartSWD07.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                btnStartSWD07.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD07.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD08.Checked == true && btnStartSWD08.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                btnStartSWD08.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD08.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD09.Checked == true && btnStartSWD09.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                btnStartSWD09.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD09.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD10.Checked == true && btnStartSWD10.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                btnStartSWD10.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD10.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD11.Checked == true && btnStartSWD11.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                btnStartSWD11.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD11.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD12.Checked == true && btnStartSWD12.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                btnStartSWD12.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD12.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD13.Checked == true && btnStartSWD13.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                btnStartSWD13.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD13.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }

            if (chMoedaD14.Checked == true && btnStartSWD14.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                btnStartSWD14.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD14.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD15.Checked == true && btnStartSWD15.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                btnStartSWD15.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD15.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD16.Checked == true && btnStartSWD16.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                btnStartSWD15.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD16.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD17.Checked == true && btnStartSWD17.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                btnStartSWD17.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD17.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD18.Checked == true && btnStartSWD18.Enabled == true)
            {
                itemDBynari = diretorioDigital;
                itemDBynari = ($@"{itemDBynari}\USD-JPY\config.txt");
                btnStartSWD18.Enabled = false;
                var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                btnStartSWD18.Visible = true;
                var startInfo = new ProcessStartInfo($@"{diretorioT}RODO.brs");
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                Process.Start(startInfo);
                System.Threading.Thread.Sleep(7000);
            }


            if (chMoedaD01.Checked == false && chMoedaD02.Checked == false && chMoedaD03.Checked == false && chMoedaD04.Checked == false &&
                chMoedaD05.Checked == false && chMoedaD06.Checked == false && chMoedaD07.Checked == false && chMoedaD08.Checked == false &&
                chMoedaD09.Checked == false && chMoedaD10.Checked == false && chMoedaD11.Checked == false && chMoedaD12.Checked == false &&
                chMoedaD13.Checked == false && chMoedaD14.Checked == false && chMoedaD15.Checked == false && chMoedaD16.Checked == false &&
                chMoedaD17.Checked == false && chMoedaD18.Checked == false


                )
            {
                MessageBox.Show("Para ligar Robô e Necessario selecionar uma Paridade", "Verificação Paridade", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }


        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Menu mainMenu = new Menu(usuarioIdDb);
            mainMenu.Show();

        }
      

        private void btnLigar_Click(object sender, EventArgs e)
        {
            LigarDigital();
        }

        private void btnVoltarD_Click(object sender, EventArgs e)
        {
            Menu mainMenu = new Menu();
            mainMenu.Show();
        }

        private void chPullBachTecnico_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnLigarOTCB_Click(object sender, EventArgs e)
        {
            LigarBynariaOTC();
        }

        private void btnLigarOTCD_Click(object sender, EventArgs e)
        {
            LigarDigitalOTC();
        }

        /// <summary>
        /// /LIGAR TODAS AS TECNIDAS DOS ROBÔ AUTOMATIZADO.
        /// </summary>
        public void GetMoedas()
        {

            if (chAutomatizado.Checked)
            {
                var listMoedas = dbStatusTryder.GetAllStatusTrayder().ToList();
                var qtd = 0;
                txtqtd.Text = qtd.ToString();

                foreach (var item in listMoedas)
                {
                    var listaOperacional = dbOperacional.GetAllOperacional();

                    if (item.TipoMoeda == "turbo" && !item.ParidadeOperando.Contains("(OTC)"))
                    {
                        var operacionalBynariDTO = listaOperacional.Find(c => c.TrayderId == item.TrayderID && c.UsuarioId == usuarioIdDb && c.DiretorioBinario != "" && c.DiretorioDigital == "");
                        if (operacionalBynariDTO != null)
                            LigarRoboAutomatizadoBinari(item.TrayderID, item.ParidadeOperando, operacionalBynariDTO);
                        //dbStatusTryder.DeleteContact(item.Id.ToString());
                    }
                    else if (item.TipoMoeda == "digital-option" && !item.ParidadeOperando.Contains("(OTC)"))
                    {
                        var operacionaDigitallDTO = listaOperacional.Find(c => c.TrayderId == item.TrayderID && c.UsuarioId == usuarioIdDb && c.DiretorioDigital != "" && c.DiretorioBinario == "");
                        if (operacionaDigitallDTO != null)
                            LigarRoboAutormatizadoDigital(item.TrayderID, item.ParidadeOperando, operacionaDigitallDTO);
                        //dbStatusTryder.DeleteContact(item.Id.ToString());

                    }

                    //if (item.TipoMoeda == "turbo" && item.ParidadeOperando.Contains("(OTC)"))
                    //{
                    //    var operacionaDigitallDTOOTC = listaOperacional.Find(c => c.TrayderId == item.TrayderID && c.UsuarioId == usuarioIdDb && c.DiretorioDigital != "" && c.DiretorioBinario == "" && c.DiretorioDigital.Contains("OtcDigital"));
                    //    if (operacionaDigitallDTOOTC != null)
                    //    {
                    //        LigarRobosBynarioTec01OTCDigital(item.ParidadeOperando, operacionaDigitallDTOOTC);
                    //    }
                    //}
                    //else if (item.TipoMoeda == "digital-option" && item.ParidadeOperando.Contains("(OTC)"))
                    //{
                    //    var operacionalBynariDTOOTC = listaOperacional.Find(c => c.TrayderId == item.TrayderID && c.UsuarioId == usuarioIdDb && c.DiretorioBinario != "" && c.DiretorioDigital == "" && c.DiretorioBinario.Contains("OtcBynaria"));
                    //    if (operacionalBynariDTOOTC != null)
                    //    {
                    //        LigarRobosBynarioTec01OTC(item.ParidadeOperando, operacionalBynariDTOOTC);
                    //    }
                    //}

                    if (Convert.ToInt32(txtqtd.Text) > 30)
                    {
                        dbStatusTryder.DeleteContact(item.Id.ToString());
                    }
                    else
                    {
                        txtqtd.Clear();
                        qtd = qtd++;
                        txtqtd.Text = qtd.ToString();
                    }
                }
            }
        }

        //OPERAÇÃO DIGITAL
        private void LigarRoboAutormatizadoDigital(int trayderID, string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            if (trayderID == 45316621)
                TecnicaCangaDigital(trayderID, paridade, operacionaDigitallDTO);

            else if (trayderID == 51292712)
                TecnicaFabioDigital(trayderID, paridade, operacionaDigitallDTO);

            else if (trayderID == 45844787)
                TecnicaElizabetDigital(trayderID, paridade, operacionaDigitallDTO);

            else if (trayderID == 44707398)
                TecnicaThomasSDigital(trayderID, paridade, operacionaDigitallDTO);

            else if (trayderID == 42624866)
                TecnicaSawaKaSDigital(trayderID, paridade, operacionaDigitallDTO);

            else if (trayderID == 42624866)
                TecnicaSawaKaSDigital(trayderID, paridade, operacionaDigitallDTO);

            else if (trayderID == 71988162)
                TecnicaAdamTDigital(trayderID, paridade, operacionaDigitallDTO);

            else if (trayderID == 52744315)
                TecnicaAestonDigital(trayderID, paridade, operacionaDigitallDTO);

            else if (trayderID == 61571520)
                TecnicaArifiTDigital(trayderID, paridade, operacionaDigitallDTO);

            else if (trayderID == 52744315)
                TecnicaEmilyTDigital(trayderID, paridade, operacionaDigitallDTO);

            else if (trayderID == 52971483)
                TecnicaAlexanderDigital(trayderID, paridade, operacionaDigitallDTO);

            else if (trayderID == 47315587)
                TecnicaAlissonBDigital(trayderID, paridade, operacionaDigitallDTO);

            else if (trayderID == 14574142)
                TecnicaParkerDigital(trayderID, paridade, operacionaDigitallDTO);
        }

        private void LigarRoboAutomatizadoBinari(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            if (trayderID == 45316621)
                TecnicaCangaBynaria(trayderID, paridade, operacionalBynariDTO);

            else if (trayderID == 51292712)
                TecnicaFabioBinaria(trayderID, paridade, operacionalBynariDTO);

            else if (trayderID == 45844787)
                TecnicaElizabetBinaria(trayderID, paridade, operacionalBynariDTO);

            else if (trayderID == 44707398)
                TecnicaThomasSBinaria(trayderID, paridade, operacionalBynariDTO);

            else if (trayderID == 42624866)
                TecnicaSawaKaSBinaria(trayderID, paridade, operacionalBynariDTO);

            else if (trayderID == 61571520)
                TecnicaArifiBinaria(trayderID, paridade, operacionalBynariDTO);

            else if (trayderID == 52744315)
                TecnicaEastonBinaria(trayderID, paridade, operacionalBynariDTO);

            else if (trayderID == 71988162)
                TecnicaAdamTBinaria(trayderID, paridade, operacionalBynariDTO);

            else if (trayderID == 13600742)
                TecnicaShihabudeenBinaria(trayderID, paridade, operacionalBynariDTO);

            else if (trayderID == 52971483)
                TecnicaAlexanderBinaria(trayderID, paridade, operacionalBynariDTO);

            else if (trayderID == 56149067)
                TecnicaEmilyBinaria(trayderID, paridade, operacionalBynariDTO);

            else if (trayderID == 56609579)
                TecnicaJaceCBinaria(trayderID, paridade, operacionalBynariDTO);

            else if (trayderID == 47315587)
                TecnicaAlissonBBinaria(trayderID, paridade, operacionalBynariDTO);

            else if (trayderID == 14574142)
                TecnicaPakerBinaria(trayderID, paridade, operacionalBynariDTO);
        }

        private void TecnicaCangaDigital(int trayderID, string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            if (trayderID == 45316621)
            {
                if (paridade.Contains("AUDJPY"))
                {
                    if (btnStartD01.Enabled == true)
                    {
                        chMoedaD01.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDCHF"))
                {
                    if (btnStartD02.Enabled == true)
                    {
                        chMoedaD02.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPNZD"))
                {
                    if (btnStartD03.Enabled == true)
                    {
                        chMoedaD03.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPJPY"))
                {
                    if (btnStartD04.Enabled == true)
                    {
                        chMoedaD04.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPCHF"))
                {
                    if (btnStartD05.Enabled == true)
                    {
                        chMoedaD05.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPCAD"))
                {
                    if (btnStartD06.Enabled == true)
                    {
                        chMoedaD06.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPAUD"))
                {
                    if (btnStartD07.Enabled == true)
                    {
                        chMoedaD07.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURUSD"))
                {
                    if (btnStartD08.Enabled == true)
                    {
                        chMoedaD08.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("AUDUSD"))
                {
                    if (btnStartD09.Enabled == true)
                    {
                        chMoedaD09.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("AUDCAD"))
                {
                    if (btnStartD10.Enabled == true)
                    {
                        chMoedaD10.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDCAD"))
                {
                    if (btnStartD11.Enabled == true)
                    {
                        chMoedaD11.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPUSD"))
                {
                    if (btnStartD12.Enabled == true)
                    {
                        chMoedaD12.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURJPY"))
                {
                    if (btnStartD13.Enabled == true)
                    {
                        chMoedaD13.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURGBP"))
                {
                    if (btnStartD14.Enabled == true)
                    {
                        chMoedaD14.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURCAD"))
                {
                    if (btnStartD15.Enabled == true)
                    {
                        chMoedaD15.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURAUD"))
                {
                    if (btnStartD16.Enabled == true)
                    {
                        chMoedaD16.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("CADJPY"))
                {
                    if (btnStartD17.Enabled == true)
                    {
                        chMoedaD17.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDJPY"))
                {
                    if (btnStartD18.Enabled == true)
                    {
                        chMoedaD18.Checked = true;
                        chPullBachTecnicoD.Checked = true;
                        LigarRobosDigitalTec01(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
            }
        }

        private void TecnicaFabioDigital(int trayderID, string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            if (trayderID == 51292712)
            {
                if (paridade.Contains("AUDJPY"))
                {
                    if (btnStartPGD01.Enabled == true)
                    {
                        chMoedaD01.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDCHF"))
                {
                    if (btnStartPGD02.Enabled == true)
                    {
                        chMoedaD02.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPNZD"))
                {
                    if (btnStartPGD03.Enabled == true)
                    {
                        chMoedaD03.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPJPY"))
                {
                    if (btnStartPGD04.Enabled == true)
                    {
                        chMoedaD04.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPCHF"))
                {
                    if (btnStartPGD05.Enabled == true)
                    {
                        chMoedaD05.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPCAD"))
                {
                    if (btnStartPGD06.Enabled == true)
                    {
                        chMoedaD06.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPAUD"))
                {
                    if (btnStartPGD07.Enabled == true)
                    {
                        chMoedaD07.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURUSD"))
                {
                    if (btnStartPGD08.Enabled == true)
                    {
                        chMoedaD08.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("AUDUSD"))
                {
                    if (btnStartPGD09.Enabled == true)
                    {
                        chMoedaD09.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("AUDCAD"))
                {
                    if (btnStartPGD10.Enabled == true)
                    {
                        chMoedaD10.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDCAD"))
                {
                    if (btnStartPGD11.Enabled == true)
                    {
                        chMoedaD11.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPUSD"))
                {
                    if (btnStartPGD12.Enabled == true)
                    {
                        chMoedaD12.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURJPY"))
                {
                    if (btnStartPGD13.Enabled == true)
                    {
                        chMoedaD13.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURGBP"))
                {
                    if (btnStartPGD14.Enabled == true)
                    {
                        chMoedaD14.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURCAD"))
                {
                    if (btnStartPGD15.Enabled == true)
                    {
                        chMoedaD15.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURAUD"))
                {
                    if (btnStartPGD16.Enabled == true)
                    {
                        chMoedaD16.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("CADJPY"))
                {
                    if (btnStartPGD17.Enabled == true)
                    {
                        chMoedaD17.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDJPY"))
                {
                    if (btnStartPGD18.Enabled == true)
                    {
                        chMoedaD18.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec03(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
            }
        }

        private void TecnicaElizabetDigital(int trayderID, string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            if (trayderID == 6822849)
            {
                if (paridade.Contains("AUDJPY"))
                {
                    if (btnStartDGeral01.Enabled == true)
                    {
                        chMoedaD01.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDCHF"))
                {
                    if (btnStartDGeral02.Enabled == true)
                    {
                        chMoedaD02.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPNZD"))
                {
                    if (btnStartDGeral03.Enabled == true)
                    {
                        chMoedaD03.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPJPY"))
                {
                    if (btnStartDGeral04.Enabled == true)
                    {
                        chMoedaD04.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPCHF"))
                {
                    if (btnStartDGeral05.Enabled == true)
                    {
                        chMoedaD05.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPCAD"))
                {
                    if (btnStartDGeral06.Enabled == true)
                    {
                        chMoedaD06.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPAUD"))
                {
                    if (btnStartDGeral07.Enabled == true)
                    {
                        chMoedaD07.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURUSD"))
                {
                    if (btnStartDGeral08.Enabled == true)
                    {
                        chMoedaD08.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("AUDUSD"))
                {
                    if (btnStartDGeral09.Enabled == true)
                    {
                        chMoedaD09.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("AUDCAD"))
                {
                    if (btnStartDGeral10.Enabled == true)
                    {
                        chMoedaD10.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDCAD"))
                {
                    if (btnStartDGeral11.Enabled == true)
                    {
                        chMoedaD11.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPUSD"))
                {
                    if (btnStartDGeral12.Enabled == true)
                    {
                        chMoedaD12.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURJPY"))
                {
                    if (btnStartDGeral13.Enabled == true)
                    {
                        chMoedaD13.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURGBP"))
                {
                    if (btnStartDGeral14.Enabled == true)
                    {
                        chMoedaD14.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURCAD"))
                {
                    if (btnStartDGeral15.Enabled == true)
                    {
                        chMoedaD15.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURAUD"))
                {
                    if (btnStartDGeral16.Enabled == true)
                    {
                        chMoedaD16.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("CADJPY"))
                {
                    if (btnStartDGeral17.Enabled == true)
                    {
                        chMoedaD17.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDJPY"))
                {
                    if (btnStartDGeral18.Enabled == true)
                    {
                        chMoedaD18.Checked = true;
                        chProbabilisticaTecnicoD.Checked = true;
                        LigarRobosDigitalTec04(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
            }
        }

        private void TecnicaAdamTDigital(int trayderID, string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("AUDJPY"))
            {
                if (btnStarttD01.Enabled == true)
                {
                    chMoedaD01.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                    btnStarttD01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD01.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStarttD02.Enabled == true)
                {
                    chMoedaD02.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD02.Enabled = false;
                    btnStarttD02.Visible = true;
                }

            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnStarttD03.Enabled == true)
                {
                    chMoedaD03.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnStarttD03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD03.Visible = true;
                }

            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnStarttD04.Enabled == true)
                {
                    chMoedaD04.Checked = true;
                    btnStarttD04.Visible = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnStarttD04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                }


            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnStarttD05.Enabled == true)
                {
                    chMoedaD05.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnStarttD04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD04.Visible = true;
                }

            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnStarttD06.Enabled == true)
                {
                    chMoedaD06.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnStarttD06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD06.Visible = true;
                }

            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnStarttD07.Enabled == true)
                {
                    chMoedaD07.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnStarttD07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD07.Visible = true;

                }

            }
            else if (paridade.Contains("EURUSD"))
            {
                if (btnStarttD08.Enabled == true)
                {
                    chMoedaD08.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnStarttD08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD08.Visible = true;
                }

            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnStarttD09.Enabled == true)
                {
                    chMoedaD09.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnStarttD09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD09.Visible = true;
                }

            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnStarttD10.Enabled == true)
                {
                    chMoedaD10.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnStarttD10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD10.Visible = true;
                }

            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnStarttD11.Enabled == true)
                {
                    chMoedaD11.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnStarttD11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD11.Visible = true;
                }

            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnStarttD12.Enabled == true)
                {
                    chMoedaD12.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnStarttD12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD12.Visible = true;
                }

            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnStarttD13.Enabled == true)
                {
                    chMoedaD13.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnStarttD13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD13.Visible = true;
                }

            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnStarttD14.Enabled == true)
                {
                    chMoedaD14.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnStarttD14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD14.Visible = true;
                }

            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnStarttD15.Enabled == true)
                {
                    chMoedaD15.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnStarttD15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD15.Visible = true;
                }

            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnStarttD16.Enabled == true)
                {
                    chMoedaD16.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnStarttD16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD16.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStarttD17.Enabled == true)
                {
                    chMoedaD17.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnStarttD17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD17.Visible = true;
                }

            }
            else if (paridade.Contains("AUDJPY"))
            {
                if (btnStarttD18.Enabled == true)
                {
                    chMoedaD18.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                    btnStarttD18.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD18.Visible = true;
                }
            }
        }

        private void TecnicaArifiTDigital(int trayderID, string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("AUDJPY"))
            {
                if (btnStartDGeral01.Enabled == true)
                {
                    chMoedaD01.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                    btnStartDGeral01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral01.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStartDGeral02.Enabled == true)
                {
                    chMoedaD02.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral02.Enabled = false;
                    btnStartDGeral02.Visible = true;
                }

            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnStartDGeral03.Enabled == true)
                {
                    chMoedaD03.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnStartDGeral03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral03.Visible = true;
                }

            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnStartDGeral04.Enabled == true)
                {
                    chMoedaD04.Checked = true;
                    btnStartDGeral04.Visible = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnStartDGeral04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                }


            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnStartDGeral05.Enabled == true)
                {
                    chMoedaD05.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnStartDGeral04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral04.Visible = true;
                }

            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnStartDGeral06.Enabled == true)
                {
                    chMoedaD06.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnStartDGeral06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral06.Visible = true;
                }

            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnStartDGeral07.Enabled == true)
                {
                    chMoedaD07.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnStartDGeral07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral07.Visible = true;

                }

            }
            else if (paridade.Contains("EURUSD"))
            {
                if (btnStartDGeral08.Enabled == true)
                {
                    chMoedaD08.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnStartDGeral08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral08.Visible = true;
                }

            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnStartDGeral09.Enabled == true)
                {
                    chMoedaD09.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnStartDGeral09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral09.Visible = true;
                }

            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnStartDGeral10.Enabled == true)
                {
                    chMoedaD10.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnStartDGeral10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral10.Visible = true;
                }

            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnStartDGeral11.Enabled == true)
                {
                    chMoedaD11.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnStartDGeral11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral11.Visible = true;
                }

            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnStartDGeral12.Enabled == true)
                {
                    chMoedaD12.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnStartDGeral12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral12.Visible = true;
                }

            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnStartDGeral13.Enabled == true)
                {
                    chMoedaD13.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnStartDGeral13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral13.Visible = true;
                }

            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnStartDGeral14.Enabled == true)
                {
                    chMoedaD14.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnStartDGeral14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral14.Visible = true;
                }

            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnStartDGeral15.Enabled == true)
                {
                    chMoedaD15.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnStartDGeral15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral15.Visible = true;
                }

            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnStartDGeral16.Enabled == true)
                {
                    chMoedaD16.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnStartDGeral16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral16.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStartDGeral17.Enabled == true)
                {
                    chMoedaD17.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnStartDGeral17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral17.Visible = true;
                }

            }
            else if (paridade.Contains("AUDJPY"))
            {
                if (btnStartDGeral18.Enabled == true)
                {
                    chMoedaD18.Checked = true;
                    chTecArifi.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                    btnStartDGeral18.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDGeral18.Visible = true;
                }
            }
        }

        private void TecnicaEmilyTDigital(int trayderID, string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("AUDJPY"))
            {
                if (btnStarst01.Enabled == true)
                {
                    chMoedaD01.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                    btnStarst01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst01.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStarst02.Enabled == true)
                {
                    chMoedaD02.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst02.Enabled = false;
                    btnStarst02.Visible = true;
                }

            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnStarst03.Enabled == true)
                {
                    chMoedaD03.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnStarst03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst03.Visible = true;
                }

            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnStarst04.Enabled == true)
                {
                    chMoedaD04.Checked = true;
                    btnStarst04.Visible = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnStarst04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                }


            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnStarst05.Enabled == true)
                {
                    chMoedaD05.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnStarst04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst04.Visible = true;
                }

            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnStarst06.Enabled == true)
                {
                    chMoedaD06.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnStarst06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst06.Visible = true;
                }

            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnStarst07.Enabled == true)
                {
                    chMoedaD07.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnStarst07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst07.Visible = true;

                }

            }
            else if (paridade.Contains("EURUSD"))
            {
                if (btnStarst08.Enabled == true)
                {
                    chMoedaD08.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnStarst08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst08.Visible = true;
                }

            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnStarst09.Enabled == true)
                {
                    chMoedaD09.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnStarst09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst09.Visible = true;
                }

            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnStarst10.Enabled == true)
                {
                    chMoedaD10.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnStarst10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst10.Visible = true;
                }

            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnStarst11.Enabled == true)
                {
                    chMoedaD11.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnStarst11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst11.Visible = true;
                }

            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnStarst12.Enabled == true)
                {
                    chMoedaD12.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnStarst12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst12.Visible = true;
                }

            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnStarst13.Enabled == true)
                {
                    chMoedaD13.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnStarst13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst13.Visible = true;
                }

            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnStarst14.Enabled == true)
                {
                    chMoedaD14.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnStarst14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst14.Visible = true;
                }

            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnStarst15.Enabled == true)
                {
                    chMoedaD15.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnStarst15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst15.Visible = true;
                }

            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnStarst16.Enabled == true)
                {
                    chMoedaD16.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnStarst16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst16.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStarst17.Enabled == true)
                {
                    chMoedaD17.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnStarst17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst17.Visible = true;
                }

            }
            else if (paridade.Contains("AUDJPY"))
            {
                if (btnStarst18.Enabled == true)
                {
                    chMoedaD18.Checked = true;
                    chTecnicaEmily.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                    btnStarst18.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarst18.Visible = true;
                }
            }
        }

        private void TecnicaAlexanderDigital(int trayderID, string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("AUDJPY"))
            {
                if (btnStarts01.Enabled == true)
                {
                    chMoedaD01.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                    btnStarts01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts01.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStarts02.Enabled == true)
                {
                    chMoedaD02.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts02.Enabled = false;
                    btnStarts02.Visible = true;
                }

            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnStarts03.Enabled == true)
                {
                    chMoedaD03.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnStarts03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts03.Visible = true;
                }

            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnStarts04.Enabled == true)
                {
                    chMoedaD04.Checked = true;
                    btnStarts04.Visible = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnStarts04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                }


            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnStarts05.Enabled == true)
                {
                    chMoedaD05.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnStarts04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts04.Visible = true;
                }

            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnStarts06.Enabled == true)
                {
                    chMoedaD06.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnStarts06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts06.Visible = true;
                }

            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnStarts07.Enabled == true)
                {
                    chMoedaD07.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnStarts07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts07.Visible = true;

                }

            }
            else if (paridade.Contains("EURUSD"))
            {
                if (btnStarts08.Enabled == true)
                {
                    chMoedaD08.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnStarts08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts08.Visible = true;
                }

            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnStarts09.Enabled == true)
                {
                    chMoedaD09.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnStarts09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts09.Visible = true;
                }

            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnStarts10.Enabled == true)
                {
                    chMoedaD10.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnStarts10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts10.Visible = true;
                }

            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnStarts11.Enabled == true)
                {
                    chMoedaD11.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnStarts11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts11.Visible = true;
                }

            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnStarts12.Enabled == true)
                {
                    chMoedaD12.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnStarts12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts12.Visible = true;
                }

            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnStarts13.Enabled == true)
                {
                    chMoedaD13.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnStarts13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts13.Visible = true;
                }

            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnStarts14.Enabled == true)
                {
                    chMoedaD14.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnStarts14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts14.Visible = true;
                }

            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnStarts15.Enabled == true)
                {
                    chMoedaD15.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnStarts15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts15.Visible = true;
                }

            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnStarts16.Enabled == true)
                {
                    chMoedaD16.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnStarts16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts16.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStarts17.Enabled == true)
                {
                    chMoedaD17.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnStarts17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts17.Visible = true;
                }

            }
            else if (paridade.Contains("AUDJPY"))
            {
                if (btnStarts18.Enabled == true)
                {
                    chMoedaD18.Checked = true;
                   chTecAlexander.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                    btnStarts18.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarts18.Visible = true;
                }
            }
        }

        private void TecnicaAlissonBDigital(int trayderID, string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("AUDJPY"))
            {
                if (btnSttD01.Enabled == true)
                {
                    chMoedaD01.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                    btnSttD01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD01.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnSttD02.Enabled == true)
                {
                    chMoedaD02.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD02.Enabled = false;
                    btnSttD02.Visible = true;
                }

            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnSttD03.Enabled == true)
                {
                    chMoedaD03.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnSttD03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD03.Visible = true;
                }

            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnSttD04.Enabled == true)
                {
                    chMoedaD04.Checked = true;
                    btnSttD04.Visible = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnSttD04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                }


            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnSttD05.Enabled == true)
                {
                    chMoedaD05.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnSttD04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD04.Visible = true;
                }

            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnSttD06.Enabled == true)
                {
                    chMoedaD06.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnSttD06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD06.Visible = true;
                }

            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnSttD07.Enabled == true)
                {
                    chMoedaD07.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnSttD07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD07.Visible = true;

                }

            }
            else if (paridade.Contains("EURUSD"))
            {
                if (btnSttD08.Enabled == true)
                {
                    chMoedaD08.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnSttD08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD08.Visible = true;
                }

            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnSttD09.Enabled == true)
                {
                    chMoedaD09.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnSttD09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD09.Visible = true;
                }

            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnSttD10.Enabled == true)
                {
                    chMoedaD10.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnSttD10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD10.Visible = true;
                }

            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnSttD11.Enabled == true)
                {
                    chMoedaD11.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnSttD11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD11.Visible = true;
                }

            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnSttD12.Enabled == true)
                {
                    chMoedaD12.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnSttD12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD12.Visible = true;
                }

            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnSttD13.Enabled == true)
                {
                    chMoedaD13.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnSttD13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD13.Visible = true;
                }

            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnSttD14.Enabled == true)
                {
                    chMoedaD14.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnSttD14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD14.Visible = true;
                }

            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnSttD15.Enabled == true)
                {
                    chMoedaD15.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnSttD15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD15.Visible = true;
                }

            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnSttD16.Enabled == true)
                {
                    chMoedaD16.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnSttD16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD16.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnSttD17.Enabled == true)
                {
                    chMoedaD17.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnSttD17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD17.Visible = true;
                }

            }
            else if (paridade.Contains("AUDJPY"))
            {
                if (btnSttD18.Enabled == true)
                {
                    chMoedaD18.Checked = true;
                    chTecAlissonD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                    btnSttD18.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSttD18.Visible = true;
                }
            }
        }

        private void TecnicaParkerDigital(int trayderID, string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("AUDJPY"))
            {
                if (btnStarrD01.Enabled == true)
                {
                    chMoedaD01.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                    btnStarrD01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD01.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStarrD02.Enabled == true)
                {
                    chMoedaD02.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD02.Enabled = false;
                    btnStarrD02.Visible = true;
                }

            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnStarrD03.Enabled == true)
                {
                    chMoedaD03.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnStarrD03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD03.Visible = true;
                }

            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnStarrD04.Enabled == true)
                {
                    chMoedaD04.Checked = true;
                    btnStarrD04.Visible = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnStarrD04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                }


            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnStarrD05.Enabled == true)
                {
                    chMoedaD05.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnStarrD04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD04.Visible = true;
                }

            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnStarrD06.Enabled == true)
                {
                    chMoedaD06.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnStarrD06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD06.Visible = true;
                }

            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnStarrD07.Enabled == true)
                {
                    chMoedaD07.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnStarrD07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD07.Visible = true;

                }

            }
            else if (paridade.Contains("EURUSD"))
            {
                if (btnStarrD08.Enabled == true)
                {
                    chMoedaD08.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnStarrD08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD08.Visible = true;
                }

            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnStarrD09.Enabled == true)
                {
                    chMoedaD09.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnStarrD09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD09.Visible = true;
                }

            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnStarrD10.Enabled == true)
                {
                    chMoedaD10.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnStarrD10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD10.Visible = true;
                }

            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnStarrD11.Enabled == true)
                {
                    chMoedaD11.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnStarrD11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD11.Visible = true;
                }

            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnStarrD12.Enabled == true)
                {
                    chMoedaD12.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnStarrD12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD12.Visible = true;
                }

            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnStarrD13.Enabled == true)
                {
                    chMoedaD13.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnStarrD13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD13.Visible = true;
                }

            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnStarrD14.Enabled == true)
                {
                    chMoedaD14.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnStarrD14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD14.Visible = true;
                }

            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnStarrD15.Enabled == true)
                {
                    chMoedaD15.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnStarrD15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD15.Visible = true;
                }

            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnStarrD16.Enabled == true)
                {
                    chMoedaD16.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnStarrD16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD16.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStarrD17.Enabled == true)
                {
                    chMoedaD17.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnStarrD17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD17.Visible = true;
                }

            }
            else if (paridade.Contains("AUDJPY"))
            {
                if (btnStarrD18.Enabled == true)
                {
                    chMoedaD18.Checked = true;
                    chTecPakerD.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                    btnStarrD18.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarrD18.Visible = true;
                }
            }
        }

        private void TecnicaAestonDigital(int trayderID, string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("AUDJPY"))
            {
                if (btnStarttD01.Enabled == true)
                {
                    chMoedaD01.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-JPY\config.txt");
                    btnStarttD01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD01.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStarttD02.Enabled == true)
                {
                    chMoedaD02.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD02.Enabled = false;
                    btnStarttD02.Visible = true;
                }

            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnStarttD03.Enabled == true)
                {
                    chMoedaD03.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnStarttD03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD03.Visible = true;
                }

            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnStarttD04.Enabled == true)
                {
                    chMoedaD04.Checked = true;
                    btnStarttD04.Visible = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnStarttD04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                }


            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnStarttD05.Enabled == true)
                {
                    chMoedaD05.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnStarttD05.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD05.Visible = true;
                }

            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnStarttD06.Enabled == true)
                {
                    chMoedaD06.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnStarttD06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD06.Visible = true;
                }

            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnStarttD07.Enabled == true)
                {
                    chMoedaD07.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnStarttD07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD07.Visible = true;

                }

            }
            else if (paridade.Contains("EURUSD"))
            {
                if (btnStarttD08.Enabled == true)
                {
                    chMoedaD08.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnStarttD08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD08.Visible = true;
                }

            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnStarttD09.Enabled == true)
                {
                    chMoedaD09.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnStarttD09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD09.Visible = true;
                }

            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnStarttD10.Enabled == true)
                {
                    chMoedaD10.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnStarttD10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD10.Visible = true;
                }

            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnStarttD11.Enabled == true)
                {
                    chMoedaD11.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnStarttD11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD11.Visible = true;
                }

            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnStarttD12.Enabled == true)
                {
                    chMoedaD12.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnStarttD12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD12.Visible = true;
                }

            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnStarttD13.Enabled == true)
                {
                    chMoedaD13.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnStarttD13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD13.Visible = true;
                }

            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnStarttD14.Enabled == true)
                {
                    chMoedaD14.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnStarttD14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD14.Visible = true;
                }

            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnStarttD15.Enabled == true)
                {
                    chMoedaD15.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnStarttD15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD15.Visible = true;
                }

            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnStarttD16.Enabled == true)
                {
                    chMoedaD16.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnStarttD16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD16.Visible = true;
                }

            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStarttD17.Enabled == true)
                {
                    chMoedaD17.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnStarttD17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD17.Visible = true;
                }

            }
            else if (paridade.Contains("USDJPY"))
            {
                if (btnStarttD18.Enabled == true)
                {
                    chMoedaD18.Checked = true;
                    chTecnidaAdam.Checked = true;
                    itemDBynari = operacionaDigitallDTO.DiretorioDigital;
                    itemDBynari = ($@"{itemDBynari}\USD-JPY\config.txt");
                    btnStarttD18.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStarttD18.Visible = true;
                }
            }
        }

        private void TecnicaThomasSDigital(int trayderID, string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            if (trayderID == 6822849)
            {
                if (paridade.Contains("AUDJPY"))
                {
                    if (btnStartDTop01.Enabled == true)
                    {
                        chMoedaD01.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDCHF"))
                {
                    if (btnStartDTop02.Enabled == true)
                    {
                        chMoedaD02.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPNZD"))
                {
                    if (btnStartDTop03.Enabled == true)
                    {
                        chMoedaD03.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPJPY"))
                {
                    if (btnStartDTop04.Enabled == true)
                    {
                        chMoedaD04.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPCHF"))
                {
                    if (btnStartDTop06.Enabled == true)
                    {
                        chMoedaD05.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPCAD"))
                {
                    if (btnStartDTop06.Enabled == true)
                    {
                        chMoedaD06.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPAUD"))
                {
                    if (btnStartDTop07.Enabled == true)
                    {
                        chMoedaD07.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURUSD"))
                {
                    if (btnStartDTop08.Enabled == true)
                    {
                        chMoedaD08.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("AUDUSD"))
                {
                    if (btnStartDTop09.Enabled == true)
                    {
                        chMoedaD09.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("AUDCAD"))
                {
                    if (btnStartDTop10.Enabled == true)
                    {
                        chMoedaD10.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDCAD"))
                {
                    if (btnStartDTop10.Enabled == true)
                    {
                        chMoedaD11.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPUSD"))
                {
                    if (btnStartDTop11.Enabled == true)
                    {
                        chMoedaD12.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURJPY"))
                {
                    if (btnStartDTop13.Enabled == true)
                    {
                        chMoedaD13.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURGBP"))
                {
                    if (btnStartDGeral14.Enabled == true)
                    {
                        chMoedaD14.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURCAD"))
                {
                    if (btnStartDTop15.Enabled == true)
                    {
                        chMoedaD15.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURAUD"))
                {
                    if (btnStartDTop16.Enabled == true)
                    {
                        chMoedaD16.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("CADJPY"))
                {
                    if (btnStartDTop17.Enabled == true)
                    {
                        chMoedaD17.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDJPY"))
                {
                    if (btnStartDTop18.Enabled == true)
                    {
                        chMoedaD18.Checked = true;
                        chProbabilisticaGeralD.Checked = true;
                        LigarRobosDigitalTec05(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
            }
        }

        private void TecnicaSawaKaSDigital(int trayderID, string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            if (trayderID == 42624866)
            {
                if (paridade.Contains("AUDJPY"))
                {
                    if (btnStartSWD01.Enabled == true)
                    {
                        chMoedaD01.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDCHF"))
                {
                    if (btnStartDTop02.Enabled == true)
                    {
                        chMoedaD02.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPNZD"))
                {
                    if (btnStartDTop03.Enabled == true)
                    {
                        chMoedaD03.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPJPY"))
                {
                    if (btnStartDTop04.Enabled == true)
                    {
                        chMoedaD04.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPCHF"))
                {
                    if (btnStartDTop06.Enabled == true)
                    {
                        chMoedaD05.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPCAD"))
                {
                    if (btnStartDTop06.Enabled == true)
                    {
                        chMoedaD06.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPAUD"))
                {
                    if (btnStartDTop07.Enabled == true)
                    {
                        chMoedaD07.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURUSD"))
                {
                    if (btnStartDTop08.Enabled == true)
                    {
                        chMoedaD08.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("AUDUSD"))
                {
                    if (btnStartDTop09.Enabled == true)
                    {
                        chMoedaD09.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("AUDCAD"))
                {
                    if (btnStartDTop10.Enabled == true)
                    {
                        chMoedaD10.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDCAD"))
                {
                    if (btnStartDTop10.Enabled == true)
                    {
                        chMoedaD11.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("GBPUSD"))
                {
                    if (btnStartDTop11.Enabled == true)
                    {
                        chMoedaD12.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURJPY"))
                {
                    if (btnStartDTop13.Enabled == true)
                    {
                        chMoedaD13.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURGBP"))
                {
                    if (btnStartDGeral14.Enabled == true)
                    {
                        chMoedaD14.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURCAD"))
                {
                    if (btnStartDTop15.Enabled == true)
                    {
                        chMoedaD15.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("EURAUD"))
                {
                    if (btnStartDTop16.Enabled == true)
                    {
                        chMoedaD16.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("CADJPY"))
                {
                    if (btnStartDTop17.Enabled == true)
                    {
                        chMoedaD17.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
                else if (paridade.Contains("USDJPY"))
                {
                    if (btnStartDTop18.Enabled == true)
                    {
                        chMoedaD18.Checked = true;
                        chTecnicaSWKD.Checked = true;
                        LigarRobosDigitalTec06(operacionaDigitallDTO.DiretorioDigital);
                    }
                }
            }
        }      


        //OPERAÇÃO BINARYA
        private void TecnicaCangaBynaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            if (trayderID == 45316621)
            {
                if (paridade.Contains("EURUSD"))
                {
                    if (btnStartB01.Enabled == true)
                    {
                        chMoedaB01.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURAUD"))
                {
                    if (btnStartB02.Enabled == true)
                    {
                        chMoedaB02.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURCAD"))
                {
                    if (btnStartB03.Enabled == true)
                    {
                        chMoedaB03.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURGBP"))
                {
                    if (btnStartB04.Enabled == true)
                    {
                        chMoedaB04.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURJPY"))
                {
                    if (btnStartB05.Enabled == true)
                    {
                        chMoedaB05.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPAUD"))
                {
                    if (btnStartB06.Enabled == true)
                    {
                        chMoedaB06.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPCAD"))
                {
                    if (btnStartB07.Enabled == true)
                    {
                        chMoedaB07.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPCHF"))
                {
                    if (btnStartB08.Enabled == true)
                    {
                        chMoedaB08.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPJPY"))
                {
                    if (btnStartB09.Enabled == true)
                    {
                        chMoedaB09.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPNZD"))
                {
                    if (btnStartB10.Enabled == true)
                    {
                        chMoedaB10.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPUSD"))
                {
                    if (btnStartB11.Enabled == true)
                    {
                        chMoedaB11.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDCAD"))
                {
                    if (btnStartB12.Enabled == true)
                    {
                        chMoedaB12.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDCHF"))
                {
                    if (btnStartB13.Enabled == true)
                    {
                        chMoedaB13.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDUSD"))
                {
                    if (btnStartB14.Enabled == true)
                    {
                        chMoedaB14.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("USDCAD"))
                {
                    if (btnStartB15.Enabled == true)
                    {
                        chMoedaB15.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("USDCHF"))
                {
                    if (btnStartB16.Enabled == true)
                    {
                        chMoedaB16.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDNZD"))
                {
                    if (btnStartB17.Enabled == true)
                    {
                        chMoedaB17.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("CADJPY"))
                {
                    if (btnStartB11.Enabled == true)
                    {
                        chMoedaB18.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("CADCHF"))
                {
                    if (btnStartB19.Enabled == true)
                    {
                        chMoedaB19.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("NZDUSD"))
                {
                    if (btnStartB20.Enabled == true)
                    {
                        chMoedaB20.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURCHF"))
                {
                    if (btnStartB21.Enabled == true)
                    {
                        chMoedaB21.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURNZD"))
                {
                    if (btnStartB22.Enabled == true)
                    {
                        chMoedaB22.Checked = true;
                        chPullBachTecnico.Checked = true;
                        LigarRobosBynarioTec01(operacionalBynariDTO.DiretorioBinario);
                    }
                }
            }
        }

        private void TecnicaElizabetBinaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            if (trayderID == 6822849)
            {
                if (paridade.Contains("EURUSD"))
                {
                    if (btnStartBPG0101.Enabled == true)
                    {
                        chMoedaB01.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURAUD"))
                {
                    if (btnStartBPG0102.Enabled == true)
                    {
                        chMoedaB02.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURCAD"))
                {
                    if (btnStartBPG0103.Enabled == true)
                    {
                        chMoedaB03.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURGBP"))
                {
                    if (btnStartBPG0104.Enabled == true)
                    {
                        chMoedaB04.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURJPY"))
                {
                    if (btnStartBPG0105.Enabled == true)
                    {
                        chMoedaB05.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPAUD"))
                {
                    if (btnStartBPG0106.Enabled == true)
                    {
                        chMoedaB06.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPCAD"))
                {
                    if (btnStartBPG0107.Enabled == true)
                    {
                        chMoedaB07.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPCHF"))
                {
                    if (btnStartBPG0108.Enabled == true)
                    {
                        chMoedaB08.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPJPY"))
                {
                    if (btnStartBPG0109.Enabled == true)
                    {
                        chMoedaB09.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPNZD"))
                {
                    if (btnStartBPG0110.Enabled == true)
                    {
                        chMoedaB10.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPUSD"))
                {
                    if (btnStartBPG0111.Enabled == true)
                    {
                        chMoedaB11.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDCAD"))
                {
                    if (btnStartBPG0112.Enabled == true)
                    {
                        chMoedaB12.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDCHF"))
                {
                    if (btnStartBPG0113.Enabled == true)
                    {
                        chMoedaB13.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDUSD"))
                {
                    if (btnStartBPG0114.Enabled == true)
                    {
                        chMoedaB14.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("USDCAD"))
                {
                    if (btnStartBPG0115.Enabled == true)
                    {
                        chMoedaB15.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("USDCHF"))
                {
                    if (btnStartBPG0116.Enabled == true)
                    {
                        chMoedaB16.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDNZD"))
                {
                    if (btnStartBPG0117.Enabled == true)
                    {
                        chMoedaB17.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("CADJPY"))
                {
                    if (btnStartBPG0118.Enabled == true)
                    {
                        chMoedaB18.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("CADCHF"))
                {
                    if (btnStartBPG0119.Enabled == true)
                    {
                        chMoedaB19.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("NZDUSD"))
                {
                    if (btnStartBPG0120.Enabled == true)
                    {
                        chMoedaB20.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURCHF"))
                {
                    if (btnStartBPG0121.Enabled == true)
                    {
                        chMoedaB21.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURNZD"))
                {
                    if (btnStartBPG0122.Enabled == true)
                    {
                        chMoedaB22.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec04(operacionalBynariDTO.DiretorioBinario);
                    }
                }
            }
        }

        private void TecnicaThomasSBinaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            if (trayderID == 44707398)
            {
                if (paridade.Contains("EURUSD"))
                {
                    if (btnStartGeral01.Enabled == true)
                    {
                        chMoedaB01.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURAUD"))
                {
                    if (btnStartGeral02.Enabled == true)
                    {
                        chMoedaB02.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURCAD"))
                {
                    if (btnStartGeral03.Enabled == true)
                    {
                        chMoedaB03.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURGBP"))
                {
                    if (btnStartGeral04.Enabled == true)
                    {
                        chMoedaB04.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURJPY"))
                {
                    if (btnStartGeral05.Enabled == true)
                    {
                        chMoedaB05.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPAUD"))
                {
                    if (btnStartGeral06.Enabled == true)
                    {
                        chMoedaB06.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPCAD"))
                {
                    if (btnStartGeral07.Enabled == true)
                    {
                        chMoedaB07.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPCHF"))
                {
                    if (btnStartGeral08.Enabled == true)
                    {
                        chMoedaB08.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPJPY"))
                {
                    if (btnStartGeral09.Enabled == true)
                    {
                        chMoedaB09.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPNZD"))
                {
                    if (btnStartGeral10.Enabled == true)
                    {
                        chMoedaB10.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPUSD"))
                {
                    if (btnStartGeral11.Enabled == true)
                    {
                        chMoedaB11.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDCAD"))
                {
                    if (btnStartGeral12.Enabled == true)
                    {
                        chMoedaB12.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDCHF"))
                {
                    if (btnStartGeral13.Enabled == true)
                    {
                        chMoedaB13.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDUSD"))
                {
                    if (btnStartGeral14.Enabled == true)
                    {
                        chMoedaB14.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("USDCAD"))
                {
                    if (btnStartGeral15.Enabled == true)
                    {
                        chMoedaB15.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("USDCHF"))
                {
                    if (btnStartGeral16.Enabled == true)
                    {
                        chMoedaB16.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDNZD"))
                {
                    if (btnStartGeral17.Enabled == true)
                    {
                        chMoedaB17.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("CADJPY"))
                {
                    if (btnStartGeral18.Enabled == true)
                    {
                        chMoedaB18.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("CADCHF"))
                {
                    if (btnStartBPG0119.Enabled == true)
                    {
                        chMoedaB19.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("NZDUSD"))
                {
                    if (btnStartGeral20.Enabled == true)
                    {
                        chMoedaB20.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURCHF"))
                {
                    if (btnStartGeral21.Enabled == true)
                    {
                        chMoedaB21.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURNZD"))
                {
                    if (btnStartGeral22.Enabled == true)
                    {
                        chMoedaB22.Checked = true;
                        chTecnicaGeral.Checked = true;
                        LigarRobosBynarioTec05(operacionalBynariDTO.DiretorioBinario);
                    }
                }
            }
        }

        private void TecnicaSawaKaSBinaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            if (trayderID == 42624866)
            {
                if (paridade.Contains("EURUSD"))
                {
                    if (btnStartSW01.Enabled == true)
                    {
                        chMoedaB01.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURAUD"))
                {
                    if (btnStartSW02.Enabled == true)
                    {
                        chMoedaB02.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURCAD"))
                {
                    if (btnStartSW03.Enabled == true)
                    {
                        chMoedaB03.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURGBP"))
                {
                    if (btnStartSW04.Enabled == true)
                    {
                        chMoedaB04.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURJPY"))
                {
                    if (btnStartSW05.Enabled == true)
                    {
                        chMoedaB05.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPAUD"))
                {
                    if (btnStartSW06.Enabled == true)
                    {
                        chMoedaB06.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPCAD"))
                {
                    if (btnStartSW07.Enabled == true)
                    {
                        chMoedaB07.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPCHF"))
                {
                    if (btnStartSW08.Enabled == true)
                    {
                        chMoedaB08.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPJPY"))
                {
                    if (btnStartSW09.Enabled == true)
                    {
                        chMoedaB09.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPNZD"))
                {
                    if (btnStartSW10.Enabled == true)
                    {
                        chMoedaB10.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPUSD"))
                {
                    if (btnStartSW11.Enabled == true)
                    {
                        chMoedaB11.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDCAD"))
                {
                    if (btnStartSW12.Enabled == true)
                    {
                        chMoedaB12.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDCHF"))
                {
                    if (btnStartSW13.Enabled == true)
                    {
                        chMoedaB13.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDUSD"))
                {
                    if (btnStartSW14.Enabled == true)
                    {
                        chMoedaB14.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("USDCAD"))
                {
                    if (btnStartSW15.Enabled == true)
                    {
                        chMoedaB15.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("USDCHF"))
                {
                    if (btnStartSW16.Enabled == true)
                    {
                        chMoedaB16.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDNZD"))
                {
                    if (btnStartSW17.Enabled == true)
                    {
                        chMoedaB17.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("CADJPY"))
                {
                    if (btnStartSW18.Enabled == true)
                    {
                        chMoedaB18.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("CADCHF"))
                {
                    if (btnStartSW19.Enabled == true)
                    {
                        chMoedaB19.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("NZDUSD"))
                {
                    if (btnStartSW20.Enabled == true)
                    {
                        chMoedaB20.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURCHF"))
                {
                    if (btnStartSW21.Enabled == true)
                    {
                        chMoedaB21.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURNZD"))
                {
                    if (btnStartSW22.Enabled == true)
                    {
                        chMoedaB22.Checked = true;
                        chTecnicaSW.Checked = true;
                        LigarRobosBynarioTec06(operacionalBynariDTO.DiretorioBinario);
                    }
                }
            }
        }

        private void TecnicaArifiBinaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("EURUSD"))
            {
                if (btnSt01.Enabled == true)
                {
                    chMoedaB01.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnSt01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt01.Visible = true;
                }
            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnSt02.Enabled == true)
                {
                    chMoedaB02.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnSt02.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt02.Visible = true;
                }
            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnSt03.Enabled == true)
                {
                    chMoedaB03.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnSt03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt03.Visible = true;
                }
            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnSt04.Enabled == true)
                {
                    chMoedaB04.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnSt04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt04.Visible = true;
                }
            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnSt05.Enabled == true)
                {
                    chMoedaB05.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnSt05.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt05.Visible = true;
                }
            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnSt06.Enabled == true)
                {
                    chMoedaB06.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnSt06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt06.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnSt07.Enabled == true)
                {
                    chMoedaB07.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnSt07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt07.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnSt08.Enabled == true)
                {
                    chMoedaB08.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnSt08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt08.Visible = true;
                }
            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnSt09.Enabled == true)
                {
                    chMoedaB09.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnSt09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt09.Visible = true;
                }
            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnSt10.Enabled == true)
                {
                    chMoedaB10.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnSt10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt10.Visible = true;
                }
            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnSt11.Enabled == true)
                {
                    chMoedaB11.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnSt11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt11.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnSt12.Enabled == true)
                {
                    chMoedaB12.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnSt12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt12.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCHF"))
            {
                if (btnSt13.Enabled == true)
                {
                    chMoedaB13.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                    btnSt13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt13.Visible = true;
                }
            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnSt14.Enabled == true)
                {
                    chMoedaB14.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnSt14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt14.Visible = true;
                }
            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnSt15.Enabled == true)
                {
                    chMoedaB15.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnSt15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt15.Visible = true;
                }
            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnSt16.Enabled == true)
                {
                    chMoedaB16.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnSt16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt16.Visible = true;
                }
            }
            else if (paridade.Contains("AUDNZD"))
            {
                if (btnSt17.Enabled == true)
                {
                    chMoedaB17.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-NZD\config.txt");
                    btnSt17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt17.Visible = true;
                }
            }
            else if (paridade.Contains("CADJPY"))
            {
                if (btnSt18.Enabled == true)
                {
                    chMoedaB18.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-JPY\config.txt");
                    btnSt18.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt18.Visible = true;
                }
            }
            else if (paridade.Contains("CADCHF"))
            {
                if (btnSt19.Enabled == true)
                {
                    chMoedaB19.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                    btnSt19.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt19.Visible = true;
                }
            }
            else if (paridade.Contains("NZDUSD"))
            {
                if (btnSt20.Enabled == true)
                {
                    chMoedaB20.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                    btnSt20.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt20.Visible = true;
                }
            }
            if (paridade.Contains("EURCHF"))
            {
                if (btnSt21.Enabled == true)
                {
                    chMoedaB21.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                    btnSt21.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt21.Visible = true;
                }
            }
            if (paridade.Contains("EURNZD"))
            {
                if (btnSt22.Enabled == true)
                {
                    chMoedaB22.Checked = true;
                    chArifi.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                    btnSt22.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSt22.Visible = true;
                }
            }
        }

        private void TecnicaEastonBinaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("EURUSD"))
            {
                if (btnSta01.Enabled == true)
                {
                    chMoedaB01.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnSta01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta01.Visible = true;
                }
            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnSta02.Enabled == true)
                {
                    chMoedaB02.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnSta02.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta02.Visible = true;
                }
            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnSta03.Enabled == true)
                {
                    chMoedaB03.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnSta03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta03.Visible = true;
                }
            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnSta04.Enabled == true)
                {
                    chMoedaB04.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnSta04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta04.Visible = true;
                }
            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnSta05.Enabled == true)
                {
                    chMoedaB05.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnSta05.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta05.Visible = true;
                }
            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnSta06.Enabled == true)
                {
                    chMoedaB06.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnSta06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta06.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnSta07.Enabled == true)
                {
                    chMoedaB07.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnSta07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta07.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnSta08.Enabled == true)
                {
                    chMoedaB08.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnSta08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta08.Visible = true;
                }
            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnSta09.Enabled == true)
                {
                    chMoedaB09.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnSta09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta09.Visible = true;
                }
            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnSta10.Enabled == true)
                {
                    chMoedaB10.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnSta10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta10.Visible = true;
                }
            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnSta11.Enabled == true)
                {
                    chMoedaB11.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnSta11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta11.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnSta12.Enabled == true)
                {
                    chMoedaB12.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnSta12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta12.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCHF"))
            {
                if (btnSta13.Enabled == true)
                {
                    chMoedaB13.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                    btnSta13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta13.Visible = true;
                }
            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnSta14.Enabled == true)
                {
                    chMoedaB14.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnSta14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta14.Visible = true;
                }
            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnSta15.Enabled == true)
                {
                    chMoedaB15.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnSta15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta15.Visible = true;
                }
            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnSta16.Enabled == true)
                {
                    chMoedaB16.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnSta16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta16.Visible = true;
                }
            }
            else if (paridade.Contains("AUDNZD"))
            {
                if (btnSta17.Enabled == true)
                {
                    chMoedaB17.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-NZD\config.txt");
                    btnSta17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta17.Visible = true;
                }
            }
            else if (paridade.Contains("CADJPY"))
            {
                if (btnSta18.Enabled == true)
                {
                    chMoedaB18.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-JPY\config.txt");
                    btnSta18.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta18.Visible = true;
                }
            }
            else if (paridade.Contains("CADCHF"))
            {
                if (btnSta19.Enabled == true)
                {
                    chMoedaB19.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                    btnSta19.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta19.Visible = true;
                }
            }
            else if (paridade.Contains("NZDUSD"))
            {
                if (btnSta20.Enabled == true)
                {
                    chMoedaB20.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                    btnSta20.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta20.Visible = true;
                }
            }
            if (paridade.Contains("EURCHF"))
            {
                if (btnSta21.Enabled == true)
                {
                    chMoedaB21.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                    btnSta21.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta21.Visible = true;
                }
            }
            if (paridade.Contains("EURNZD"))
            {
                if (btnSta22.Enabled == true)
                {
                    chMoedaB22.Checked = true;
                    chTecEaston.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                    btnSta22.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSta22.Visible = true;
                }
            }
        }

        private void TecnicaAdamTBinaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("EURUSD"))
            {
                if (btnStar01.Enabled == true)
                {
                    chMoedaB01.Checked = true;
                    chAdam.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnStar01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar01.Visible = true;
                }
            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnStar02.Enabled == true)
                {
                    chMoedaB02.Checked = true;
                    chAdam.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnStar02.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar02.Visible = true;
                }
            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnStar03.Enabled == true)
                {
                    chMoedaB03.Checked = true;
                    chAdam.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnStar03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar03.Visible = true;
                }
            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnStar04.Enabled == true)
                {
                    chMoedaB04.Checked = true;
                    chAdam.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnStar04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar04.Visible = true;
                }
            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnStar05.Enabled == true)
                {
                    chMoedaB05.Checked = true;
                    chAdam.Checked = true;

                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnStar05.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar05.Visible = true;
                }
            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnStar06.Enabled == true)
                {
                    chMoedaB06.Checked = true;
                    chAdam.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnStar06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar06.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnStar07.Enabled == true)
                {
                    chMoedaB07.Checked = true;
                    chAdam.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnStar07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar07.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnStar08.Enabled == true)
                {
                    chMoedaB08.Checked = true;
                    chAdam.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnStar08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar08.Visible = true;
                }
            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnStar09.Enabled == true)
                {
                    chMoedaB09.Checked = true;
                    chAdam.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnStar09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar09.Visible = true;
                }
            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnStar10.Enabled == true)
                {
                    chMoedaB10.Checked = true;
                    chAdam.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnStar10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar10.Visible = true;
                }
            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnStar11.Enabled == true)
                {
                    chMoedaB11.Checked = true;
                    chAdam.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnStar11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar11.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnStar12.Enabled == true)
                {
                    chMoedaB12.Checked = true;
                    chAdam.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnStar12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar12.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCHF"))
            {
                if (btnStar13.Enabled == true)
                {
                    chMoedaB13.Checked = true;
                    chAdam.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                    btnStar13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar13.Visible = true;
                }
            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnStar14.Enabled == true)
                {
                    chMoedaB14.Checked = true;
                    chAdam.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnStar14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar14.Visible = true;
                }
            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnStar15.Enabled == true)
                {
                    chMoedaB15.Checked = true;
                    chAdam.Checked = true;

                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnStar15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar15.Visible = true;
                }
            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStar16.Enabled == true)
                {
                    chMoedaB16.Checked = true;
                    chAdam.Checked = true;

                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnStar16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar16.Visible = true;
                }
            }
            else if (paridade.Contains("AUDNZD"))
            {
                if (btnStar17.Enabled == true)
                {
                    chMoedaB17.Checked = true;
                    chAdam.Checked = true;

                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-NZD\config.txt");
                    btnStar17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar17.Visible = true;
                }
            }
            else if (paridade.Contains("CADJPY"))
            {
                if (btnStar18.Enabled == true)
                {
                    chMoedaB18.Checked = true;
                    chAdam.Checked = true;

                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-JPY\config.txt");
                    btnStar18.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar18.Visible = true;
                }
            }
            else if (paridade.Contains("CADCHF"))
            {
                if (btnStar19.Enabled == true)
                {
                    chMoedaB19.Checked = true;
                    chAdam.Checked = true;

                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                    btnStar19.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar19.Visible = true;
                }
            }
            else if (paridade.Contains("NZDUSD"))
            {
                if (btnStar20.Enabled == true)
                {
                    chMoedaB20.Checked = true;
                    chAdam.Checked = true;

                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                    btnStar20.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar20.Visible = true;
                }
            }
            if (paridade.Contains("EURCHF"))
            {
                if (btnStar21.Enabled == true)
                {
                    chMoedaB21.Checked = true;
                    chAdam.Checked = true;

                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                    btnStar21.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar21.Visible = true;
                }
            }
            if (paridade.Contains("EURNZD"))
            {
                if (btnStar22.Enabled == true)
                {
                    chMoedaB22.Checked = true;
                    chTecEaston.Checked = true;

                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                    btnStar22.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStar22.Visible = true;
                }
            }

        }

        private void TecnicaShihabudeenBinaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("EURUSD"))
            {
                if (btnStartt01.Enabled == true)
                {
                    chMoedaB01.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnStartt01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt01.Visible = true;
                }
            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnStartt02.Enabled == true)
                {
                    chMoedaB02.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnStartt02.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt02.Visible = true;
                }
            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnStartt03.Enabled == true)
                {
                    chMoedaB03.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnStartt03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt03.Visible = true;
                }
            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnStartt04.Enabled == true)
                {
                    chMoedaB04.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnStartt04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt04.Visible = true;
                }
            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnStartt05.Enabled == true)
                {
                    chMoedaB05.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnStartt05.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt05.Visible = true;
                }
            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnStartt06.Enabled == true)
                {
                    chMoedaB06.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnStartt06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt06.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnStartt07.Enabled == true)
                {
                    chMoedaB07.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnStartt07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt07.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnStartt08.Enabled == true)
                {
                    chMoedaB08.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnStartt08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt08.Visible = true;
                }
            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnStartt09.Enabled == true)
                {
                    chMoedaB09.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnStartt09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt09.Visible = true;
                }
            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnStartt10.Enabled == true)
                {
                    chMoedaB10.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnStartt10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt10.Visible = true;
                }
            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnStartt11.Enabled == true)
                {
                    chMoedaB11.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnStartt11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt11.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnStartt12.Enabled == true)
                {
                    chMoedaB12.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnStartt12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt12.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCHF"))
            {
                if (btnStartt13.Enabled == true)
                {
                    chMoedaB13.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                    btnStartt13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt13.Visible = true;
                }
            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnStartt14.Enabled == true)
                {
                    chMoedaB14.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnStartt14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt14.Visible = true;
                }
            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnStartt15.Enabled == true)
                {
                    chMoedaB15.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnStartt15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt15.Visible = true;
                }
            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStartt16.Enabled == true)
                {
                    chMoedaB16.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnStartt16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt16.Visible = true;
                }
            }
            else if (paridade.Contains("AUDNZD"))
            {
                if (btnStartt17.Enabled == true)
                {
                    chMoedaB17.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-NZD\config.txt");
                    btnStartt17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt17.Visible = true;
                }
            }
            else if (paridade.Contains("CADJPY"))
            {
                if (btnStartt18.Enabled == true)
                {
                    chMoedaB18.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-JPY\config.txt");
                    btnStartt18.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt18.Visible = true;
                }
            }
            else if (paridade.Contains("CADCHF"))
            {
                if (btnStartt19.Enabled == true)
                {
                    chMoedaB19.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                    btnStartt19.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt19.Visible = true;
                }
            }
            else if (paridade.Contains("NZDUSD"))
            {
                if (btnStartt20.Enabled == true)
                {
                    chMoedaB20.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                    btnStartt20.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt20.Visible = true;
                }
            }
            if (paridade.Contains("EURCHF"))
            {
                if (btnStartt21.Enabled == true)
                {
                    chMoedaB21.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                    btnStartt21.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt21.Visible = true;
                }
            }
            if (paridade.Contains("EURNZD"))
            {
                if (btnStartt22.Enabled == true)
                {
                    chMoedaB22.Checked = true;
                    chTecSh.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                    btnStartt22.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStartt22.Visible = true;
                }
            }

        }

        private void TecnicaAlexanderBinaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("EURUSD"))
            {
                if (btnStarttt01.Enabled == true)
                {
                    chMoedaB01.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnStarttt01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt01.Visible = true;
                }
            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnStarttt02.Enabled == true)
                {
                    chMoedaB02.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnStarttt02.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt02.Visible = true;
                }
            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnStarttt03.Enabled == true)
                {
                    chMoedaB03.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnStarttt03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt03.Visible = true;
                }
            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnStarttt04.Enabled == true)
                {
                    chMoedaB04.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnStarttt04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt04.Visible = true;
                }
            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnStarttt05.Enabled == true)
                {
                    chMoedaB05.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnStarttt05.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt05.Visible = true;
                }
            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnStarttt06.Enabled == true)
                {
                    chMoedaB06.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnStarttt06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt06.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnStarttt07.Enabled == true)
                {
                    chMoedaB07.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnStarttt07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt07.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnStarttt08.Enabled == true)
                {
                    chMoedaB08.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnStarttt08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt08.Visible = true;
                }
            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnStarttt09.Enabled == true)
                {
                    chMoedaB09.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnStarttt09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt09.Visible = true;
                }
            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnStarttt10.Enabled == true)
                {
                    chMoedaB10.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnStarttt10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt10.Visible = true;
                }
            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnStarttt11.Enabled == true)
                {
                    chMoedaB11.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnStarttt11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt11.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnStarttt12.Enabled == true)
                {
                    chMoedaB12.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnStarttt12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt12.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCHF"))
            {
                if (btnStarttt13.Enabled == true)
                {
                    chMoedaB13.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                    btnStarttt13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt13.Visible = true;
                }
            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnStarttt14.Enabled == true)
                {
                    chMoedaB14.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnStarttt14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt14.Visible = true;
                }
            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnStarttt15.Enabled == true)
                {
                    chMoedaB15.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnStarttt15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt15.Visible = true;
                }
            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStarttt16.Enabled == true)
                {
                    chMoedaB16.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnStarttt16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt16.Visible = true;
                }
            }
            else if (paridade.Contains("AUDNZD"))
            {
                if (btnStarttt17.Enabled == true)
                {
                    chMoedaB17.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-NZD\config.txt");
                    btnStarttt17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt17.Visible = true;
                }
            }
            else if (paridade.Contains("CADJPY"))
            {
                if (btnStarttt18.Enabled == true)
                {
                    chMoedaB18.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-JPY\config.txt");
                    btnStarttt18.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt18.Visible = true;
                }
            }
            else if (paridade.Contains("CADCHF"))
            {
                if (btnStarttt19.Enabled == true)
                {
                    chMoedaB19.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                    btnStarttt19.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt19.Visible = true;
                }
            }
            else if (paridade.Contains("NZDUSD"))
            {
                if (btnStarttt20.Enabled == true)
                {
                    chMoedaB20.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                    btnStarttt20.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt20.Visible = true;
                }
            }
            if (paridade.Contains("EURCHF"))
            {
                if (btnStarttt21.Enabled == true)
                {
                    chMoedaB21.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                    btnStarttt21.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt21.Visible = true;
                }
            }
            if (paridade.Contains("EURNZD"))
            {
                if (btnStarttt22.Enabled == true)
                {
                    chMoedaB22.Checked = true;
                    chTecAlex.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                    btnStarttt22.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarttt22.Visible = true;
                }
            }
        }

        private void TecnicaEmilyBinaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("EURUSD"))
            {
                if (btnStarta01.Enabled == true)
                {
                    chMoedaB01.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnStarta01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta01.Visible = true;
                }
            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnStarta02.Enabled == true)
                {
                    chMoedaB02.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnStarta02.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta02.Visible = true;
                }
            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnStarta03.Enabled == true)
                {
                    chMoedaB03.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnStarta03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta03.Visible = true;
                }
            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnStarta04.Enabled == true)
                {
                    chMoedaB04.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnStarta04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta04.Visible = true;
                }
            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnStarta05.Enabled == true)
                {
                    chMoedaB05.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnStarta05.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta05.Visible = true;
                }
            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnStarta06.Enabled == true)
                {
                    chMoedaB06.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnStarta06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta06.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnStarta07.Enabled == true)
                {
                    chMoedaB07.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnStarta07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta07.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnStarta08.Enabled == true)
                {
                    chMoedaB08.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnStarta08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta08.Visible = true;
                }
            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnStarta09.Enabled == true)
                {
                    chMoedaB09.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnStarta09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta09.Visible = true;
                }
            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnStarta10.Enabled == true)
                {
                    chMoedaB10.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnStarta10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta10.Visible = true;
                }
            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnStarta11.Enabled == true)
                {
                    chMoedaB11.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnStarta11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta11.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnStarta12.Enabled == true)
                {
                    chMoedaB12.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnStarta12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta12.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCHF"))
            {
                if (btnStarta13.Enabled == true)
                {
                    chMoedaB13.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                    btnStarta13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta13.Visible = true;
                }
            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnStarta14.Enabled == true)
                {
                    chMoedaB14.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnStarta14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta14.Visible = true;
                }
            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnStarta15.Enabled == true)
                {
                    chMoedaB15.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnStarta15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta15.Visible = true;
                }
            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnStarta16.Enabled == true)
                {
                    chMoedaB16.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnStarta16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta16.Visible = true;
                }
            }
            else if (paridade.Contains("AUDNZD"))
            {
                if (btnStarta17.Enabled == true)
                {
                    chMoedaB17.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-NZD\config.txt");
                    btnStarta17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta17.Visible = true;
                }
            }
            else if (paridade.Contains("CADJPY"))
            {
                if (btnStarta18.Enabled == true)
                {
                    chMoedaB18.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-JPY\config.txt");
                    btnStarta18.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta18.Visible = true;
                }
            }
            else if (paridade.Contains("CADCHF"))
            {
                if (btnStarta19.Enabled == true)
                {
                    chMoedaB19.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                    btnStarta19.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta19.Visible = true;
                }
            }
            else if (paridade.Contains("NZDUSD"))
            {
                if (btnStarta20.Enabled == true)
                {
                    chMoedaB20.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                    btnStarta20.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta20.Visible = true;
                }
            }
            if (paridade.Contains("EURCHF"))
            {
                if (btnStarta21.Enabled == true)
                {
                    chMoedaB21.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                    btnStarta21.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta21.Visible = true;
                }
            }
            if (paridade.Contains("EURNZD"))
            {
                if (btnStarta22.Enabled == true)
                {
                    chMoedaB22.Checked = true;
                    chtecEmily.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                    btnStarta22.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnStarta22.Visible = true;
                }
            }

        }

        private void TecnicaFabioBinaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            if (trayderID == 51292712)
            {
                if (paridade.Contains("EURUSD"))
                {
                    if (btnStartPG02.Enabled == true)
                    {
                        chMoedaB01.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURAUD"))
                {
                    if (btnStartPG02.Enabled == true)
                    {
                        chMoedaB02.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURCAD"))
                {
                    if (btnStartPG03.Enabled == true)
                    {
                        chMoedaB03.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURGBP"))
                {
                    if (btnStartPG04.Enabled == true)
                    {
                        chMoedaB04.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURJPY"))
                {
                    if (btnStartPG05.Enabled == true)
                    {
                        chMoedaB05.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPAUD"))
                {
                    if (btnStartPG06.Enabled == true)
                    {
                        chMoedaB06.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPCAD"))
                {
                    if (btnStartPG07.Enabled == true)
                    {
                        chMoedaB07.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPCHF"))
                {
                    if (btnStartPG08.Enabled == true)
                    {
                        chMoedaB08.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPJPY"))
                {
                    if (btnStartPG08.Enabled == true)
                    {
                        chMoedaB09.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPNZD"))
                {
                    if (btnStartPG10.Enabled == true)
                    {
                        chMoedaB10.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("GBPUSD"))
                {
                    if (btnStartPG11.Enabled == true)
                    {
                        chMoedaB11.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDCAD"))
                {
                    if (btnStartPG12.Enabled == true)
                    {
                        chMoedaB12.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDCHF"))
                {
                    if (btnStartPG13.Enabled == true)
                    {
                        chMoedaB13.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDUSD"))
                {
                    if (btnStartPG14.Enabled == true)
                    {
                        chMoedaB14.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("USDCAD"))
                {
                    if (btnStartPG15.Enabled == true)
                    {
                        chMoedaB15.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("USDCHF"))
                {
                    if (btnStartPG16.Enabled == true)
                    {
                        chMoedaB16.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("AUDNZD"))
                {
                    if (btnStartPG17.Enabled == true)
                    {
                        chMoedaB17.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("CADJPY"))
                {
                    if (btnStartPG18.Enabled == true)
                    {
                        chMoedaB18.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("CADCHF"))
                {
                    if (btnStartPG19.Enabled == true)
                    {
                        chMoedaB19.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("NZDUSD"))
                {
                    if (btnStartPG20.Enabled == true)
                    {
                        chMoedaB20.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURCHF"))
                {
                    if (btnStartPG21.Enabled == true)
                    {
                        chMoedaB21.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
                if (paridade.Contains("EURNZD"))
                {
                    if (btnStartPG22.Enabled == true)
                    {
                        chMoedaB22.Checked = true;
                        chPropabilisticaTecnico.Checked = true;
                        LigarRobosBynarioTec03(operacionalBynariDTO.DiretorioBinario);
                    }
                }
            }
        }

        private void TecnicaJaceCBinaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("EURUSD"))
            {
                if (btnSttart01.Enabled == true)
                {
                    chMoedaB01.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnSttart01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart01.Visible = true;
                }
            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnSttart02.Enabled == true)
                {
                    chMoedaB02.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnSttart02.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart02.Visible = true;
                }
            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnSttart03.Enabled == true)
                {
                    chMoedaB03.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnSttart03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart03.Visible = true;
                }
            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnSttart04.Enabled == true)
                {
                    chMoedaB04.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnSttart04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart04.Visible = true;
                }
            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnSttart05.Enabled == true)
                {
                    chMoedaB05.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnSttart05.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart05.Visible = true;
                }
            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnSttart06.Enabled == true)
                {
                    chMoedaB06.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnSttart06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart06.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnSttart07.Enabled == true)
                {
                    chMoedaB07.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnSttart07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart07.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnSttart08.Enabled == true)
                {
                    chMoedaB08.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnSttart08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart08.Visible = true;
                }
            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnSttart09.Enabled == true)
                {
                    chMoedaB09.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnSttart09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart09.Visible = true;
                }
            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnSttart10.Enabled == true)
                {
                    chMoedaB10.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnSttart10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart10.Visible = true;
                }
            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnSttart11.Enabled == true)
                {
                    chMoedaB11.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnSttart11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart11.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnSttart12.Enabled == true)
                {
                    chMoedaB12.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnSttart12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart12.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCHF"))
            {
                if (btnSttart13.Enabled == true)
                {
                    chMoedaB13.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                    btnSttart13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart13.Visible = true;
                }
            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnSttart14.Enabled == true)
                {
                    chMoedaB14.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnSttart14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart14.Visible = true;
                }
            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnSttart15.Enabled == true)
                {
                    chMoedaB15.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnSttart15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart15.Visible = true;
                }
            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnSttart16.Enabled == true)
                {
                    chMoedaB16.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnSttart16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart16.Visible = true;
                }
            }
            else if (paridade.Contains("AUDNZD"))
            {
                if (btnSttart17.Enabled == true)
                {
                    chMoedaB17.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-NZDm\config.txt");
                    btnSttart17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart17.Visible = true;
                }
            }
            else if (paridade.Contains("CADJPY"))
            {
                //if (btnSttart18.Enabled == true)
                //{
                //    chMoedaB18.Checked = true;
                //    chTecJose.Checked = true;
                //    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                //    itemDBynari = ($@"{itemDBynari}\CAD-JPY\config.txt");
                //    btnSttart18.Enabled = false;
                //    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //    Process.Start($@"{diretorioT}ROBO.brs");
                //    System.Threading.Thread.Sleep(7000);
                //    btnSttart18.Visible = true;
                //}
            }
            else if (paridade.Contains("CADCHF"))
            {
                if (btnSttart19.Enabled == true)
                {
                    chMoedaB19.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                    btnSttart19.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart19.Visible = true;
                }
            }
            else if (paridade.Contains("NZDUSD"))
            {
                if (btnSttart20.Enabled == true)
                {
                    chMoedaB20.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                    btnSttart20.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart20.Visible = true;
                }
            }
            if (paridade.Contains("EURCHF"))
            {
                if (btnSttart21.Enabled == true)
                {
                    chMoedaB21.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                    btnSttart21.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart21.Visible = true;
                }
            }
            if (paridade.Contains("EURNZD"))
            {
                if (btnSttart22.Enabled == true)
                {
                    chMoedaB22.Checked = true;
                    chTecJose.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                    btnSttart22.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart22.Visible = true;
                }
            }
        }

        private void TecnicaAlissonBBinaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("EURUSD"))
            {
                if (btnSst01.Enabled == true)
                {
                    chMoedaB01.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnSst01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst01.Visible = true;
                }
            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnSst02.Enabled == true)
                {
                    chMoedaB02.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnSst02.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst02.Visible = true;
                }
            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnSst03.Enabled == true)
                {
                    chMoedaB03.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnSst03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst03.Visible = true;
                }
            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnSst04.Enabled == true)
                {
                    chMoedaB04.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnSst04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst04.Visible = true;
                }
            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnSst05.Enabled == true)
                {
                    chMoedaB05.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnSst05.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst05.Visible = true;
                }
            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnSst06.Enabled == true)
                {
                    chMoedaB06.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnSst06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst06.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnSst07.Enabled == true)
                {
                    chMoedaB07.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnSst07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst07.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnSst08.Enabled == true)
                {
                    chMoedaB08.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnSst08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst08.Visible = true;
                }
            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnSst09.Enabled == true)
                {
                    chMoedaB09.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnSst09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst09.Visible = true;
                }
            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnSst10.Enabled == true)
                {
                    chMoedaB10.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnSst10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst10.Visible = true;
                }
            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnSst11.Enabled == true)
                {
                    chMoedaB11.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnSst11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst11.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnSst12.Enabled == true)
                {
                    chMoedaB12.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnSst12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst12.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCHF"))
            {
                if (btnSst13.Enabled == true)
                {
                    chMoedaB13.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                    btnSst13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst13.Visible = true;
                }
            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnSst14.Enabled == true)
                {
                    chMoedaB14.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnSst14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst14.Visible = true;
                }
            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnSst15.Enabled == true)
                {
                    chMoedaB15.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnSst15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst15.Visible = true;
                }
            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnSst16.Enabled == true)
                {
                    chMoedaB16.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnSst16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst16.Visible = true;
                }
            }
            else if (paridade.Contains("AUDNZD"))
            {
                if (btnSst17.Enabled == true)
                {
                    chMoedaB17.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-NZDm\config.txt");
                    btnSst17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst17.Visible = true;
                }
            }
            else if (paridade.Contains("CADJPY"))
            {
                //if (btnSst18.Enabled == true)
                //{
                //    chMoedaB18.Checked = true;
                //    chTecAlisson.Checked = true;
                //    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                //    itemDBynari = ($@"{itemDBynari}\CAD-JPY\config.txt");
                //    btnSst18.Enabled = false;
                //    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //    Process.Start($@"{diretorioT}ROBO.brs");
                //    System.Threading.Thread.Sleep(7000);
                //    btnSst18.Visible = true;
                //}
            }
            else if (paridade.Contains("CADCHF"))
            {
                if (btnSst19.Enabled == true)
                {
                    chMoedaB19.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                    btnSst19.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst19.Visible = true;
                }
            }
            else if (paridade.Contains("NZDUSD"))
            {
                if (btnSst20.Enabled == true)
                {
                    chMoedaB20.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                    btnSst20.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst20.Visible = true;
                }
            }
            if (paridade.Contains("EURCHF"))
            {
                if (btnSst21.Enabled == true)
                {
                    chMoedaB21.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                    btnSst21.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSst21.Visible = true;
                }
            }
            if (paridade.Contains("EURNZD"))
            {
                if (btnSst22.Enabled == true)
                {
                    chMoedaB22.Checked = true;
                    chTecAlisson.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                    btnSst22.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart22.Visible = true;
                }
            }
        }

        private void TecnicaPakerBinaria(int trayderID, string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            var itemDBynari = "";

            if (paridade.Contains("EURUSD"))
            {
                if (btnSstart01.Enabled == true)
                {
                    chMoedaB01.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnSstart01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart01.Visible = true;
                }
            }
            else if (paridade.Contains("EURAUD"))
            {
                if (btnSstart02.Enabled == true)
                {
                    chMoedaB02.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-AUD\config.txt");
                    btnSstart02.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart02.Visible = true;
                }
            }
            else if (paridade.Contains("EURCAD"))
            {
                if (btnSstart03.Enabled == true)
                {
                    chMoedaB03.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CAD\config.txt");
                    btnSstart03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart03.Visible = true;
                }
            }
            else if (paridade.Contains("EURGBP"))
            {
                if (btnSstart04.Enabled == true)
                {
                    chMoedaB04.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnSstart04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart04.Visible = true;
                }
            }
            else if (paridade.Contains("EURJPY"))
            {
                if (btnSstart05.Enabled == true)
                {
                    chMoedaB05.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnSstart05.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart05.Visible = true;
                }
            }
            else if (paridade.Contains("GBPAUD"))
            {
                if (btnSstart06.Enabled == true)
                {
                    chMoedaB06.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-AUD\config.txt");
                    btnSstart06.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart06.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCAD"))
            {
                if (btnSstart07.Enabled == true)
                {
                    chMoedaB07.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CAD\config.txt");
                    btnSstart07.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart07.Visible = true;
                }
            }
            else if (paridade.Contains("GBPCHF"))
            {
                if (btnSstart08.Enabled == true)
                {
                    chMoedaB08.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-CHF\config.txt");
                    btnSstart08.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart08.Visible = true;
                }
            }
            else if (paridade.Contains("GBPJPY"))
            {
                if (btnSstart09.Enabled == true)
                {
                    chMoedaB09.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnSstart09.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart09.Visible = true;
                }
            }
            else if (paridade.Contains("GBPNZD"))
            {
                if (btnSstart10.Enabled == true)
                {
                    chMoedaB10.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-NZD\config.txt");
                    btnSstart10.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart10.Visible = true;
                }
            }
            else if (paridade.Contains("GBPUSD"))
            {
                if (btnSstart11.Enabled == true)
                {
                    chMoedaB11.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnSstart11.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart11.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCAD"))
            {
                if (btnSstart12.Enabled == true)
                {
                    chMoedaB12.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnSstart12.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart12.Visible = true;
                }
            }
            else if (paridade.Contains("AUDCHF"))
            {
                if (btnSstart13.Enabled == true)
                {
                    chMoedaB13.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-CHF\config.txt");
                    btnSstart13.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart13.Visible = true;
                }
            }
            else if (paridade.Contains("AUDUSD"))
            {
                if (btnSstart14.Enabled == true)
                {
                    chMoedaB14.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-USD\config.txt");
                    btnSstart14.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart14.Visible = true;
                }
            }
            else if (paridade.Contains("USDCAD"))
            {
                if (btnSstart15.Enabled == true)
                {
                    chMoedaB15.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CAD\config.txt");
                    btnSstart15.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart15.Visible = true;
                }
            }
            else if (paridade.Contains("USDCHF"))
            {
                if (btnSstart16.Enabled == true)
                {
                    chMoedaB16.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnSstart16.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart16.Visible = true;
                }
            }
            else if (paridade.Contains("AUDNZD"))
            {
                if (btnSstart17.Enabled == true)
                {
                    chMoedaB17.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\AUD-NZDm\config.txt");
                    btnSstart17.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart17.Visible = true;
                }
            }
            else if (paridade.Contains("CADJPY"))
            {
                //if (btnSstart18.Enabled == true)
                //{
                //    chMoedaB18.Checked = true;
                //    chTecParker.Checked = true;
                //    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                //    itemDBynari = ($@"{itemDBynari}\CAD-JPY\config.txt");
                //    btnSstart18.Enabled = false;
                //    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                //    Process.Start($@"{diretorioT}ROBO.brs");
                //    System.Threading.Thread.Sleep(7000);
                //    btnSstart18.Visible = true;
                //}
            }
            else if (paridade.Contains("CADCHF"))
            {
                if (btnSstart19.Enabled == true)
                {
                    chMoedaB19.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\CAD-CHF\config.txt");
                    btnSstart19.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart19.Visible = true;
                }
            }
            else if (paridade.Contains("NZDUSD"))
            {
                if (btnSstart20.Enabled == true)
                {
                    chMoedaB20.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                    btnSstart20.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart20.Visible = true;
                }
            }
            if (paridade.Contains("EURCHF"))
            {
                if (btnSstart21.Enabled == true)
                {
                    chMoedaB21.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-CHF\config.txt");
                    btnSstart21.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSstart21.Visible = true;
                }
            }
            if (paridade.Contains("EURNZD"))
            {
                if (btnSstart22.Enabled == true)
                {
                    chMoedaB22.Checked = true;
                    chTecParker.Checked = true;
                    itemDBynari = operacionalBynariDTO.DiretorioBinario;
                    itemDBynari = ($@"{itemDBynari}\EUR-NZD\config.txt");
                    btnSstart22.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    btnSttart22.Visible = true;
                }
            }
        }

        //Operação OTC
        private void LigarRobosBynarioTec01OTC(string paridade, Context.OperacionalBynari operacionalBynariDTO)
        {
            var itemDBynari = "";
            var diretorioBynari = operacionalBynariDTO.DiretorioBinario;

            if (paridade.Contains("EURUSD (OTC)"))
            {
                if (btnStartBO02.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnStartBO02.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartBO02.Visible = true;
                }
            }

            if (paridade.Contains("EURGBP (OTC)"))
            {
                if (btnStartBO03.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnStartBO03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartBO03.Visible = true;
                }
            }

            if (paridade.Contains("GBPUSD (OTC)"))
            {
                if (btnStartBO04.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnStartBO04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartBO04.Visible = true;
                }
            }

            if (paridade.Contains("AUDCAD (OTC)"))
            {
                if (btnStartBO01.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnStartBO01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}robo.exe");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartBO01.Visible = true;
                }
            }

            if (paridade.Contains("USDCHF (OTC)"))
            {
                if (btnStartBO06.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartBO06.Enabled = false;
                    btnStartBO06.Visible = true;
                }
            }

            if (paridade.Contains("NZDUSD (OTC)"))
            {
                if (btnStartBO05.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                    btnStartBO05.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartBO05.Visible = true;
                }
            }

            if (paridade.Contains("EURJPY (OTC)"))
            {
                if (btnSOTC02.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnSOTC02.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSOTC02.Visible = true;
                }
            }

            if (paridade.Contains("USDCHF (OTC)"))
            {
                if (btnSOTC03.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnSOTC03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSOTC03.Visible = true;
                }
            }

            if (paridade.Contains("USDJPY (OTC)"))
            {
                if (btnSOTC01.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\USD-JPY\config.txt");
                    btnSOTC01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnSOTC01.Visible = true;
                }
            }
        }

        private void LigarRobosBynarioTec01OTCDigital(string paridade, Context.OperacionalBynari operacionaDigitallDTO)
        {
            var itemDBynari = "";
            var diretorioBynari = operacionaDigitallDTO.DiretorioDigital;

            if (paridade.Contains("EURUSD (OTC)"))
            {
                if (btnStartDO03.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\EUR-USD\config.txt");
                    btnStartDO03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDO03.Visible = true;
                }
            }

            else if (paridade.Contains("GBPUSD (OTC)"))
            {
                if (btnStartDO04.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\GBP-USD\config.txt");
                    btnStartDO04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDO04.Visible = true;
                }

            }
             
            else if (paridade.Contains("GBPJPY (OTC)"))
            {
                if (btnStartDO02.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\GBP-JPY\config.txt");
                    btnStartDO02.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDO02.Visible = true;
                }
            }

            else if (paridade.Contains("USDJPY (OTC)"))
            {
                if (btnStartDO01.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\USD-JPY\config.txt");
                    btnStartDO01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartDO01.Visible = true;
                }
            }

            else if (paridade.Contains("NZDUSD (OTC)"))
            {
                if (btnStartD01.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\NZD-USD\config.txt");
                    btnStartD01.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartD01.Visible = true;
                }
            }

            else if (paridade.Contains("USDCHF (OTC)"))
            {
                if (btnStartD02.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\USD-CHF\config.txt");
                    btnStartD02.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartD02.Visible = true;
                }
            }

            else if (paridade.Contains("EURJPY (OTC)"))
            {
                if (btnStartD03.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\EUR-JPY\config.txt");
                    btnStartD03.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartD03.Visible = true;
                }
            }


            else if (paridade.Contains("AUDCAD (OTC)"))
            {
                if (btnStartD04.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\AUD-CAD\config.txt");
                    btnStartD04.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartD04.Visible = true;
                }
            }

            else if (paridade.Contains("EURGBP (OTC)"))
            {
                if (btnStartD05.Enabled == true)
                {
                    itemDBynari = diretorioBynari;
                    itemDBynari = ($@"{itemDBynari}\EUR-GBP\config.txt");
                    btnStartD05.Enabled = false;
                    var diretorioT = $"{itemDBynari.Replace("config.txt", "")}";
                    Process.Start($@"{diretorioT}ROBO.brs");
                    System.Threading.Thread.Sleep(7000);
                    itemDBynari = "";
                    btnStartD05.Visible = true;
                }
            }
        }
    }
}
