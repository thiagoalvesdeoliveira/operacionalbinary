﻿namespace OperacionaBinary
{
    partial class AlteracaoOperacional
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtTraiderId = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnNovo = new System.Windows.Forms.Button();
            this.chTecPull = new System.Windows.Forms.CheckBox();
            this.chTecPro = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtValorMinimo = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.chDigital = new System.Windows.Forms.CheckBox();
            this.chBinaria = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.chBynariOTC = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTop = new System.Windows.Forms.Label();
            this.rdDomingo07 = new System.Windows.Forms.RadioButton();
            this.txtTop = new System.Windows.Forms.TextBox();
            this.rdSabado06 = new System.Windows.Forms.RadioButton();
            this.rdSexta05 = new System.Windows.Forms.RadioButton();
            this.rdQuinta04 = new System.Windows.Forms.RadioButton();
            this.rdQuarta03 = new System.Windows.Forms.RadioButton();
            this.rdTerca02 = new System.Windows.Forms.RadioButton();
            this.rdSegunda01 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtConta = new System.Windows.Forms.ComboBox();
            this.txtDiretorioDigital = new System.Windows.Forms.TextBox();
            this.txtStopLoss = new System.Windows.Forms.TextBox();
            this.txtValorInicial = new System.Windows.Forms.TextBox();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.txtValorEntrada = new System.Windows.Forms.TextBox();
            this.txtStopWin = new System.Windows.Forms.TextBox();
            this.txtDiretorioBinaria = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnNovoO = new System.Windows.Forms.Button();
            this.lblValorMInimo = new System.Windows.Forms.Label();
            this.txtValorMinimos = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rd07 = new System.Windows.Forms.RadioButton();
            this.rd06 = new System.Windows.Forms.RadioButton();
            this.rd05 = new System.Windows.Forms.RadioButton();
            this.rd04 = new System.Windows.Forms.RadioButton();
            this.rd03 = new System.Windows.Forms.RadioButton();
            this.rd02 = new System.Windows.Forms.RadioButton();
            this.rd01 = new System.Windows.Forms.RadioButton();
            this.lblTopTrayder = new System.Windows.Forms.Label();
            this.txtTopTrayders = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtContta = new System.Windows.Forms.ComboBox();
            this.txtLoss = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.txtEntrada = new System.Windows.Forms.TextBox();
            this.txtWin = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rdotcBynary = new System.Windows.Forms.RadioButton();
            this.rdotcDigital = new System.Windows.Forms.RadioButton();
            this.rdDigital = new System.Windows.Forms.RadioButton();
            this.rdBynary = new System.Windows.Forms.RadioButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtTraiderId
            // 
            this.txtTraiderId.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTraiderId.FormattingEnabled = true;
            this.txtTraiderId.Items.AddRange(new object[] {
            "TECNICA01",
            "TECNICA02",
            "TECNICA03",
            "TECNICA04",
            "TECNICA05"});
            this.txtTraiderId.Location = new System.Drawing.Point(588, 854);
            this.txtTraiderId.Name = "txtTraiderId";
            this.txtTraiderId.Size = new System.Drawing.Size(138, 32);
            this.txtTraiderId.TabIndex = 86;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.button2.Location = new System.Drawing.Point(384, 581);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 23);
            this.button2.TabIndex = 85;
            this.button2.Text = "<Voltar Menu";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // btnNovo
            // 
            this.btnNovo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnNovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnNovo.Location = new System.Drawing.Point(913, 1038);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(102, 47);
            this.btnNovo.TabIndex = 84;
            this.btnNovo.Text = "Novo";
            this.btnNovo.UseVisualStyleBackColor = false;
            // 
            // chTecPull
            // 
            this.chTecPull.AutoSize = true;
            this.chTecPull.Enabled = false;
            this.chTecPull.Location = new System.Drawing.Point(415, 1054);
            this.chTecPull.Name = "chTecPull";
            this.chTecPull.Size = new System.Drawing.Size(80, 17);
            this.chTecPull.TabIndex = 83;
            this.chTecPull.Text = "checkBox2";
            this.chTecPull.UseVisualStyleBackColor = true;
            this.chTecPull.Visible = false;
            // 
            // chTecPro
            // 
            this.chTecPro.AutoSize = true;
            this.chTecPro.Location = new System.Drawing.Point(512, 1054);
            this.chTecPro.Name = "chTecPro";
            this.chTecPro.Size = new System.Drawing.Size(80, 17);
            this.chTecPro.TabIndex = 82;
            this.chTecPro.Text = "checkBox1";
            this.chTecPro.UseVisualStyleBackColor = true;
            this.chTecPro.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(480, 1000);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 20);
            this.label19.TabIndex = 81;
            this.label19.Text = "Valor Minimo";
            // 
            // txtValorMinimo
            // 
            this.txtValorMinimo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorMinimo.Location = new System.Drawing.Point(588, 994);
            this.txtValorMinimo.Name = "txtValorMinimo";
            this.txtValorMinimo.Size = new System.Drawing.Size(138, 29);
            this.txtValorMinimo.TabIndex = 80;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(525, 959);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 20);
            this.label18.TabIndex = 79;
            this.label18.Text = "Banca";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(528, 913);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 20);
            this.label17.TabIndex = 78;
            this.label17.Text = "Conta";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(501, 860);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 20);
            this.label16.TabIndex = 77;
            this.label16.Text = "TrayderID";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(508, 802);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 20);
            this.label15.TabIndex = 76;
            this.label15.Text = "Stop Win";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(501, 748);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 20);
            this.label14.TabIndex = 75;
            this.label14.Text = "Stop Loss";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(475, 702);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 20);
            this.label13.TabIndex = 74;
            this.label13.Text = "Valor Entrada";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(456, 656);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 20);
            this.label12.TabIndex = 73;
            this.label12.Text = "Diretorio Bynaria";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(465, 608);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 20);
            this.label11.TabIndex = 72;
            this.label11.Text = "Diretorio Digital";
            // 
            // btnSalvar
            // 
            this.btnSalvar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSalvar.Location = new System.Drawing.Point(1021, 1038);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(102, 47);
            this.btnSalvar.TabIndex = 71;
            this.btnSalvar.Text = "Cadastrar";
            this.btnSalvar.UseVisualStyleBackColor = false;
            // 
            // chDigital
            // 
            this.chDigital.AutoSize = true;
            this.chDigital.Location = new System.Drawing.Point(825, 1054);
            this.chDigital.Name = "chDigital";
            this.chDigital.Size = new System.Drawing.Size(68, 17);
            this.chDigital.TabIndex = 61;
            this.chDigital.Text = "DIGITAL";
            this.chDigital.UseVisualStyleBackColor = true;
            // 
            // chBinaria
            // 
            this.chBinaria.AutoSize = true;
            this.chBinaria.Location = new System.Drawing.Point(727, 1054);
            this.chBinaria.Name = "chBinaria";
            this.chBinaria.Size = new System.Drawing.Size(69, 17);
            this.chBinaria.TabIndex = 60;
            this.chBinaria.Text = "BINARIA";
            this.chBinaria.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(397, 1014);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 24);
            this.label10.TabIndex = 68;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(397, 963);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 24);
            this.label8.TabIndex = 65;
            // 
            // chBynariOTC
            // 
            this.chBynariOTC.AutoSize = true;
            this.chBynariOTC.Location = new System.Drawing.Point(610, 1054);
            this.chBynariOTC.Name = "chBynariOTC";
            this.chBynariOTC.Size = new System.Drawing.Size(94, 17);
            this.chBynariOTC.TabIndex = 69;
            this.chBynariOTC.Text = "BINARIA-OTC";
            this.chBynariOTC.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(397, 693);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 24);
            this.label6.TabIndex = 63;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(397, 633);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 24);
            this.label5.TabIndex = 59;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(397, 854);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 24);
            this.label4.TabIndex = 56;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(397, 805);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 24);
            this.label3.TabIndex = 55;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(397, 757);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 24);
            this.label2.TabIndex = 54;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblTop);
            this.panel1.Controls.Add(this.rdDomingo07);
            this.panel1.Controls.Add(this.txtTop);
            this.panel1.Controls.Add(this.rdSabado06);
            this.panel1.Controls.Add(this.rdSexta05);
            this.panel1.Controls.Add(this.rdQuinta04);
            this.panel1.Controls.Add(this.rdQuarta03);
            this.panel1.Controls.Add(this.rdTerca02);
            this.panel1.Controls.Add(this.rdSegunda01);
            this.panel1.Location = new System.Drawing.Point(742, 715);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(353, 267);
            this.panel1.TabIndex = 67;
            // 
            // lblTop
            // 
            this.lblTop.AutoSize = true;
            this.lblTop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTop.Location = new System.Drawing.Point(150, 162);
            this.lblTop.Name = "lblTop";
            this.lblTop.Size = new System.Drawing.Size(40, 20);
            this.lblTop.TabIndex = 49;
            this.lblTop.Text = "TOP";
            // 
            // rdDomingo07
            // 
            this.rdDomingo07.AutoSize = true;
            this.rdDomingo07.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdDomingo07.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdDomingo07.Location = new System.Drawing.Point(235, 79);
            this.rdDomingo07.Name = "rdDomingo07";
            this.rdDomingo07.Size = new System.Drawing.Size(111, 29);
            this.rdDomingo07.TabIndex = 6;
            this.rdDomingo07.TabStop = true;
            this.rdDomingo07.Text = "Domingo";
            this.rdDomingo07.UseVisualStyleBackColor = true;
            // 
            // txtTop
            // 
            this.txtTop.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTop.Location = new System.Drawing.Point(196, 156);
            this.txtTop.Name = "txtTop";
            this.txtTop.Size = new System.Drawing.Size(138, 29);
            this.txtTop.TabIndex = 48;
            // 
            // rdSabado06
            // 
            this.rdSabado06.AutoSize = true;
            this.rdSabado06.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdSabado06.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdSabado06.Location = new System.Drawing.Point(235, 27);
            this.rdSabado06.Name = "rdSabado06";
            this.rdSabado06.Size = new System.Drawing.Size(99, 29);
            this.rdSabado06.TabIndex = 5;
            this.rdSabado06.TabStop = true;
            this.rdSabado06.Text = "Sábado";
            this.rdSabado06.UseVisualStyleBackColor = true;
            // 
            // rdSexta05
            // 
            this.rdSexta05.AutoSize = true;
            this.rdSexta05.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdSexta05.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdSexta05.Location = new System.Drawing.Point(19, 216);
            this.rdSexta05.Name = "rdSexta05";
            this.rdSexta05.Size = new System.Drawing.Size(81, 29);
            this.rdSexta05.TabIndex = 4;
            this.rdSexta05.TabStop = true;
            this.rdSexta05.Text = "Sexta";
            this.rdSexta05.UseVisualStyleBackColor = true;
            // 
            // rdQuinta04
            // 
            this.rdQuinta04.AutoSize = true;
            this.rdQuinta04.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdQuinta04.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdQuinta04.Location = new System.Drawing.Point(19, 172);
            this.rdQuinta04.Name = "rdQuinta04";
            this.rdQuinta04.Size = new System.Drawing.Size(89, 29);
            this.rdQuinta04.TabIndex = 3;
            this.rdQuinta04.TabStop = true;
            this.rdQuinta04.Text = "Quinta";
            this.rdQuinta04.UseVisualStyleBackColor = true;
            // 
            // rdQuarta03
            // 
            this.rdQuarta03.AutoSize = true;
            this.rdQuarta03.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdQuarta03.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdQuarta03.Location = new System.Drawing.Point(19, 122);
            this.rdQuarta03.Name = "rdQuarta03";
            this.rdQuarta03.Size = new System.Drawing.Size(90, 29);
            this.rdQuarta03.TabIndex = 2;
            this.rdQuarta03.TabStop = true;
            this.rdQuarta03.Text = "Quarta";
            this.rdQuarta03.UseVisualStyleBackColor = true;
            // 
            // rdTerca02
            // 
            this.rdTerca02.AutoSize = true;
            this.rdTerca02.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdTerca02.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTerca02.Location = new System.Drawing.Point(19, 76);
            this.rdTerca02.Name = "rdTerca02";
            this.rdTerca02.Size = new System.Drawing.Size(83, 29);
            this.rdTerca02.TabIndex = 1;
            this.rdTerca02.TabStop = true;
            this.rdTerca02.Text = "Terça";
            this.rdTerca02.UseVisualStyleBackColor = true;
            // 
            // rdSegunda01
            // 
            this.rdSegunda01.AutoSize = true;
            this.rdSegunda01.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdSegunda01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdSegunda01.Location = new System.Drawing.Point(19, 27);
            this.rdSegunda01.Name = "rdSegunda01";
            this.rdSegunda01.Size = new System.Drawing.Size(111, 29);
            this.rdSegunda01.TabIndex = 0;
            this.rdSegunda01.TabStop = true;
            this.rdSegunda01.Text = "Segunda";
            this.rdSegunda01.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(397, 915);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 24);
            this.label1.TabIndex = 53;
            // 
            // txtConta
            // 
            this.txtConta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConta.FormattingEnabled = true;
            this.txtConta.Items.AddRange(new object[] {
            "PRACTICE ",
            "REAL"});
            this.txtConta.Location = new System.Drawing.Point(588, 907);
            this.txtConta.Name = "txtConta";
            this.txtConta.Size = new System.Drawing.Size(138, 32);
            this.txtConta.TabIndex = 70;
            // 
            // txtDiretorioDigital
            // 
            this.txtDiretorioDigital.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiretorioDigital.Location = new System.Drawing.Point(588, 603);
            this.txtDiretorioDigital.Name = "txtDiretorioDigital";
            this.txtDiretorioDigital.Size = new System.Drawing.Size(507, 29);
            this.txtDiretorioDigital.TabIndex = 58;
            // 
            // txtStopLoss
            // 
            this.txtStopLoss.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStopLoss.Location = new System.Drawing.Point(588, 742);
            this.txtStopLoss.Name = "txtStopLoss";
            this.txtStopLoss.Size = new System.Drawing.Size(138, 29);
            this.txtStopLoss.TabIndex = 52;
            // 
            // txtValorInicial
            // 
            this.txtValorInicial.Enabled = false;
            this.txtValorInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorInicial.Location = new System.Drawing.Point(588, 953);
            this.txtValorInicial.Name = "txtValorInicial";
            this.txtValorInicial.Size = new System.Drawing.Size(138, 29);
            this.txtValorInicial.TabIndex = 66;
            // 
            // txtUserId
            // 
            this.txtUserId.Enabled = false;
            this.txtUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserId.Location = new System.Drawing.Point(395, 1009);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(47, 29);
            this.txtUserId.TabIndex = 64;
            this.txtUserId.Visible = false;
            // 
            // txtValorEntrada
            // 
            this.txtValorEntrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorEntrada.Location = new System.Drawing.Point(588, 696);
            this.txtValorEntrada.Name = "txtValorEntrada";
            this.txtValorEntrada.Size = new System.Drawing.Size(138, 29);
            this.txtValorEntrada.TabIndex = 51;
            // 
            // txtStopWin
            // 
            this.txtStopWin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStopWin.Location = new System.Drawing.Point(588, 796);
            this.txtStopWin.Name = "txtStopWin";
            this.txtStopWin.Size = new System.Drawing.Size(138, 29);
            this.txtStopWin.TabIndex = 57;
            // 
            // txtDiretorioBinaria
            // 
            this.txtDiretorioBinaria.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiretorioBinaria.Location = new System.Drawing.Point(588, 650);
            this.txtDiretorioBinaria.Name = "txtDiretorioBinaria";
            this.txtDiretorioBinaria.Size = new System.Drawing.Size(507, 29);
            this.txtDiretorioBinaria.TabIndex = 62;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.button1.Location = new System.Drawing.Point(3, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 121;
            this.button1.Text = "<Voltar Menu";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnNovoO
            // 
            this.btnNovoO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnNovoO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovoO.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnNovoO.Location = new System.Drawing.Point(559, 418);
            this.btnNovoO.Name = "btnNovoO";
            this.btnNovoO.Size = new System.Drawing.Size(102, 47);
            this.btnNovoO.TabIndex = 120;
            this.btnNovoO.Text = "Novo";
            this.btnNovoO.UseVisualStyleBackColor = false;
            this.btnNovoO.Click += new System.EventHandler(this.btnNovoO_Click);
            // 
            // lblValorMInimo
            // 
            this.lblValorMInimo.AutoSize = true;
            this.lblValorMInimo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorMInimo.Location = new System.Drawing.Point(66, 261);
            this.lblValorMInimo.Name = "lblValorMInimo";
            this.lblValorMInimo.Size = new System.Drawing.Size(100, 20);
            this.lblValorMInimo.TabIndex = 117;
            this.lblValorMInimo.Text = "Valor Minimo";
            // 
            // txtValorMinimos
            // 
            this.txtValorMinimos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorMinimos.Location = new System.Drawing.Point(174, 255);
            this.txtValorMinimos.Name = "txtValorMinimos";
            this.txtValorMinimos.Size = new System.Drawing.Size(138, 29);
            this.txtValorMinimos.TabIndex = 116;
            this.txtValorMinimos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtValorMinimos_KeyDown);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(114, 208);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 20);
            this.label20.TabIndex = 114;
            this.label20.Text = "Conta";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(94, 158);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(74, 20);
            this.label22.TabIndex = 112;
            this.label22.Text = "Stop Win";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(87, 104);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(81, 20);
            this.label23.TabIndex = 111;
            this.label23.Text = "Stop Loss";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(61, 58);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(107, 20);
            this.label24.TabIndex = 110;
            this.label24.Text = "Valor Entrada";
            // 
            // btnAlterar
            // 
            this.btnAlterar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnAlterar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnAlterar.Location = new System.Drawing.Point(679, 418);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(102, 47);
            this.btnAlterar.TabIndex = 107;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = false;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(26, 441);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(0, 24);
            this.label27.TabIndex = 104;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(26, 390);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(0, 24);
            this.label28.TabIndex = 101;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(26, 120);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(0, 24);
            this.label29.TabIndex = 99;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(26, 60);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(0, 24);
            this.label30.TabIndex = 95;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(26, 281);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(0, 24);
            this.label31.TabIndex = 92;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(26, 232);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(0, 24);
            this.label32.TabIndex = 91;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(26, 184);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(0, 24);
            this.label33.TabIndex = 90;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.rd07);
            this.panel2.Controls.Add(this.rd06);
            this.panel2.Controls.Add(this.rd05);
            this.panel2.Controls.Add(this.rd04);
            this.panel2.Controls.Add(this.rd03);
            this.panel2.Controls.Add(this.rd02);
            this.panel2.Controls.Add(this.rd01);
            this.panel2.Location = new System.Drawing.Point(377, 52);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(238, 336);
            this.panel2.TabIndex = 103;
            // 
            // rd07
            // 
            this.rd07.AutoSize = true;
            this.rd07.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rd07.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rd07.Location = new System.Drawing.Point(19, 299);
            this.rd07.Name = "rd07";
            this.rd07.Size = new System.Drawing.Size(111, 29);
            this.rd07.TabIndex = 6;
            this.rd07.TabStop = true;
            this.rd07.Text = "Domingo";
            this.rd07.UseVisualStyleBackColor = true;
            // 
            // rd06
            // 
            this.rd06.AutoSize = true;
            this.rd06.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rd06.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rd06.Location = new System.Drawing.Point(19, 264);
            this.rd06.Name = "rd06";
            this.rd06.Size = new System.Drawing.Size(99, 29);
            this.rd06.TabIndex = 5;
            this.rd06.TabStop = true;
            this.rd06.Text = "Sábado";
            this.rd06.UseVisualStyleBackColor = true;
            // 
            // rd05
            // 
            this.rd05.AutoSize = true;
            this.rd05.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rd05.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rd05.Location = new System.Drawing.Point(19, 216);
            this.rd05.Name = "rd05";
            this.rd05.Size = new System.Drawing.Size(81, 29);
            this.rd05.TabIndex = 4;
            this.rd05.TabStop = true;
            this.rd05.Text = "Sexta";
            this.rd05.UseVisualStyleBackColor = true;
            // 
            // rd04
            // 
            this.rd04.AutoSize = true;
            this.rd04.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rd04.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rd04.Location = new System.Drawing.Point(19, 172);
            this.rd04.Name = "rd04";
            this.rd04.Size = new System.Drawing.Size(89, 29);
            this.rd04.TabIndex = 3;
            this.rd04.TabStop = true;
            this.rd04.Text = "Quinta";
            this.rd04.UseVisualStyleBackColor = true;
            // 
            // rd03
            // 
            this.rd03.AutoSize = true;
            this.rd03.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rd03.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rd03.Location = new System.Drawing.Point(19, 122);
            this.rd03.Name = "rd03";
            this.rd03.Size = new System.Drawing.Size(90, 29);
            this.rd03.TabIndex = 2;
            this.rd03.TabStop = true;
            this.rd03.Text = "Quarta";
            this.rd03.UseVisualStyleBackColor = true;
            // 
            // rd02
            // 
            this.rd02.AutoSize = true;
            this.rd02.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rd02.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rd02.Location = new System.Drawing.Point(19, 76);
            this.rd02.Name = "rd02";
            this.rd02.Size = new System.Drawing.Size(83, 29);
            this.rd02.TabIndex = 1;
            this.rd02.TabStop = true;
            this.rd02.Text = "Terça";
            this.rd02.UseVisualStyleBackColor = true;
            // 
            // rd01
            // 
            this.rd01.AutoSize = true;
            this.rd01.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rd01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rd01.Location = new System.Drawing.Point(19, 27);
            this.rd01.Name = "rd01";
            this.rd01.Size = new System.Drawing.Size(111, 29);
            this.rd01.TabIndex = 0;
            this.rd01.TabStop = true;
            this.rd01.Text = "Segunda";
            this.rd01.UseVisualStyleBackColor = true;
            // 
            // lblTopTrayder
            // 
            this.lblTopTrayder.AutoSize = true;
            this.lblTopTrayder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTopTrayder.Location = new System.Drawing.Point(46, 307);
            this.lblTopTrayder.Name = "lblTopTrayder";
            this.lblTopTrayder.Size = new System.Drawing.Size(122, 20);
            this.lblTopTrayder.TabIndex = 49;
            this.lblTopTrayder.Text = "TOP TRAYDER";
            // 
            // txtTopTrayders
            // 
            this.txtTopTrayders.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTopTrayders.Location = new System.Drawing.Point(174, 301);
            this.txtTopTrayders.Name = "txtTopTrayders";
            this.txtTopTrayders.Size = new System.Drawing.Size(138, 29);
            this.txtTopTrayders.TabIndex = 48;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(26, 342);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(0, 24);
            this.label35.TabIndex = 89;
            // 
            // txtContta
            // 
            this.txtContta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContta.FormattingEnabled = true;
            this.txtContta.Items.AddRange(new object[] {
            "PRACTICE ",
            "REAL"});
            this.txtContta.Location = new System.Drawing.Point(174, 202);
            this.txtContta.Name = "txtContta";
            this.txtContta.Size = new System.Drawing.Size(138, 32);
            this.txtContta.TabIndex = 106;
            this.txtContta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtContta_KeyDown);
            // 
            // txtLoss
            // 
            this.txtLoss.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoss.Location = new System.Drawing.Point(174, 98);
            this.txtLoss.Name = "txtLoss";
            this.txtLoss.Size = new System.Drawing.Size(197, 29);
            this.txtLoss.TabIndex = 88;
            this.txtLoss.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLoss_KeyDown);
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(12, 52);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(47, 29);
            this.textBox6.TabIndex = 100;
            this.textBox6.Visible = false;
            // 
            // txtEntrada
            // 
            this.txtEntrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEntrada.Location = new System.Drawing.Point(174, 52);
            this.txtEntrada.Name = "txtEntrada";
            this.txtEntrada.Size = new System.Drawing.Size(197, 29);
            this.txtEntrada.TabIndex = 1;
            this.txtEntrada.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEntrada_KeyDown);
            // 
            // txtWin
            // 
            this.txtWin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWin.Location = new System.Drawing.Point(174, 152);
            this.txtWin.Name = "txtWin";
            this.txtWin.Size = new System.Drawing.Size(197, 29);
            this.txtWin.TabIndex = 93;
            this.txtWin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWin_KeyDown);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.rdotcBynary);
            this.panel3.Controls.Add(this.rdotcDigital);
            this.panel3.Controls.Add(this.rdDigital);
            this.panel3.Controls.Add(this.rdBynary);
            this.panel3.Location = new System.Drawing.Point(621, 52);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(160, 336);
            this.panel3.TabIndex = 122;
            // 
            // rdotcBynary
            // 
            this.rdotcBynary.AutoSize = true;
            this.rdotcBynary.Enabled = false;
            this.rdotcBynary.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdotcBynary.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdotcBynary.Location = new System.Drawing.Point(15, 124);
            this.rdotcBynary.Name = "rdotcBynary";
            this.rdotcBynary.Size = new System.Drawing.Size(137, 29);
            this.rdotcBynary.TabIndex = 10;
            this.rdotcBynary.TabStop = true;
            this.rdotcBynary.Text = "OTC-Bynary";
            this.rdotcBynary.UseVisualStyleBackColor = true;
            // 
            // rdotcDigital
            // 
            this.rdotcDigital.AutoSize = true;
            this.rdotcDigital.Enabled = false;
            this.rdotcDigital.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdotcDigital.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdotcDigital.Location = new System.Drawing.Point(15, 175);
            this.rdotcDigital.Name = "rdotcDigital";
            this.rdotcDigital.Size = new System.Drawing.Size(130, 29);
            this.rdotcDigital.TabIndex = 9;
            this.rdotcDigital.TabStop = true;
            this.rdotcDigital.Text = "OTC-Digital";
            this.rdotcDigital.UseVisualStyleBackColor = true;
            // 
            // rdDigital
            // 
            this.rdDigital.AutoSize = true;
            this.rdDigital.Enabled = false;
            this.rdDigital.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdDigital.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdDigital.Location = new System.Drawing.Point(15, 69);
            this.rdDigital.Name = "rdDigital";
            this.rdDigital.Size = new System.Drawing.Size(84, 29);
            this.rdDigital.TabIndex = 8;
            this.rdDigital.TabStop = true;
            this.rdDigital.Text = "Digital";
            this.rdDigital.UseVisualStyleBackColor = true;
            // 
            // rdBynary
            // 
            this.rdBynary.AutoSize = true;
            this.rdBynary.Enabled = false;
            this.rdBynary.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdBynary.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdBynary.Location = new System.Drawing.Point(15, 27);
            this.rdBynary.Name = "rdBynary";
            this.rdBynary.Size = new System.Drawing.Size(86, 29);
            this.rdBynary.TabIndex = 7;
            this.rdBynary.TabStop = true;
            this.rdBynary.Text = "Bynari";
            this.rdBynary.UseVisualStyleBackColor = true;
            // 
            // AlteracaoOperacional
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 470);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblTopTrayder);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtTopTrayders);
            this.Controls.Add(this.btnNovoO);
            this.Controls.Add(this.lblValorMInimo);
            this.Controls.Add(this.txtValorMinimos);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.btnAlterar);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.txtContta);
            this.Controls.Add(this.txtLoss);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.txtEntrada);
            this.Controls.Add(this.txtWin);
            this.Controls.Add(this.txtTraiderId);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnNovo);
            this.Controls.Add(this.chTecPull);
            this.Controls.Add(this.chTecPro);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtValorMinimo);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.chDigital);
            this.Controls.Add(this.chBinaria);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.chBynariOTC);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtConta);
            this.Controls.Add(this.txtDiretorioDigital);
            this.Controls.Add(this.txtStopLoss);
            this.Controls.Add(this.txtValorInicial);
            this.Controls.Add(this.txtUserId);
            this.Controls.Add(this.txtValorEntrada);
            this.Controls.Add(this.txtStopWin);
            this.Controls.Add(this.txtDiretorioBinaria);
            this.MaximizeBox = false;
            this.Name = "AlteracaoOperacional";
            this.Text = "AlteracaoOperacional";
            this.Load += new System.EventHandler(this.AlteracaoOperacional_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox txtTraiderId;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.CheckBox chTecPull;
        private System.Windows.Forms.CheckBox chTecPro;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtValorMinimo;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.CheckBox chDigital;
        private System.Windows.Forms.CheckBox chBinaria;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chBynariOTC;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTop;
        private System.Windows.Forms.RadioButton rdDomingo07;
        private System.Windows.Forms.TextBox txtTop;
        private System.Windows.Forms.RadioButton rdSabado06;
        private System.Windows.Forms.RadioButton rdSexta05;
        private System.Windows.Forms.RadioButton rdQuinta04;
        private System.Windows.Forms.RadioButton rdQuarta03;
        private System.Windows.Forms.RadioButton rdTerca02;
        private System.Windows.Forms.RadioButton rdSegunda01;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox txtConta;
        private System.Windows.Forms.TextBox txtDiretorioDigital;
        private System.Windows.Forms.TextBox txtStopLoss;
        private System.Windows.Forms.TextBox txtValorInicial;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.TextBox txtValorEntrada;
        private System.Windows.Forms.TextBox txtStopWin;
        private System.Windows.Forms.TextBox txtDiretorioBinaria;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnNovoO;
        private System.Windows.Forms.Label lblValorMInimo;
        private System.Windows.Forms.TextBox txtValorMinimos;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rd07;
        private System.Windows.Forms.RadioButton rd06;
        private System.Windows.Forms.RadioButton rd05;
        private System.Windows.Forms.RadioButton rd04;
        private System.Windows.Forms.RadioButton rd03;
        private System.Windows.Forms.RadioButton rd02;
        private System.Windows.Forms.RadioButton rd01;
        private System.Windows.Forms.Label lblTopTrayder;
        private System.Windows.Forms.TextBox txtTopTrayders;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox txtContta;
        private System.Windows.Forms.TextBox txtLoss;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox txtEntrada;
        private System.Windows.Forms.TextBox txtWin;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rdotcBynary;
        private System.Windows.Forms.RadioButton rdotcDigital;
        private System.Windows.Forms.RadioButton rdDigital;
        private System.Windows.Forms.RadioButton rdBynary;
    }
}