﻿namespace OperacionaBinary
{
    partial class CadastroOperacional
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.chDigital = new System.Windows.Forms.CheckBox();
            this.chBinaria = new System.Windows.Forms.CheckBox();
            this.txtStopLoss = new System.Windows.Forms.TextBox();
            this.txtValorEntrada = new System.Windows.Forms.TextBox();
            this.txtStopWin = new System.Windows.Forms.TextBox();
            this.txtDiretorioBinaria = new System.Windows.Forms.TextBox();
            this.txtDiretorioDigital = new System.Windows.Forms.TextBox();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.txtValorInicial = new System.Windows.Forms.TextBox();
            this.chBynariOTC = new System.Windows.Forms.CheckBox();
            this.txtConta = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdDomingo07 = new System.Windows.Forms.RadioButton();
            this.rdSabado06 = new System.Windows.Forms.RadioButton();
            this.rdSexta05 = new System.Windows.Forms.RadioButton();
            this.rdQuinta04 = new System.Windows.Forms.RadioButton();
            this.rdQuarta03 = new System.Windows.Forms.RadioButton();
            this.rdTerca02 = new System.Windows.Forms.RadioButton();
            this.rdSegunda01 = new System.Windows.Forms.RadioButton();
            this.lblTop = new System.Windows.Forms.Label();
            this.txtTop = new System.Windows.Forms.TextBox();
            this.rdSegunda = new System.Windows.Forms.RadioButton();
            this.rdTerca = new System.Windows.Forms.RadioButton();
            this.rdQuarta = new System.Windows.Forms.RadioButton();
            this.rdQuinta = new System.Windows.Forms.RadioButton();
            this.rdSexta = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.chMoedaB02 = new System.Windows.Forms.CheckBox();
            this.chMoedaB03 = new System.Windows.Forms.CheckBox();
            this.chMoedaB04 = new System.Windows.Forms.CheckBox();
            this.chMoedaB05 = new System.Windows.Forms.CheckBox();
            this.chMoedaB06 = new System.Windows.Forms.CheckBox();
            this.chMoedaB07 = new System.Windows.Forms.CheckBox();
            this.chMoedaB08 = new System.Windows.Forms.CheckBox();
            this.chMoedaB09 = new System.Windows.Forms.CheckBox();
            this.chMoedaB10 = new System.Windows.Forms.CheckBox();
            this.chMoedaB11 = new System.Windows.Forms.CheckBox();
            this.chMoedaB12 = new System.Windows.Forms.CheckBox();
            this.chMoedaB13 = new System.Windows.Forms.CheckBox();
            this.chMoedaB14 = new System.Windows.Forms.CheckBox();
            this.chMoedaB15 = new System.Windows.Forms.CheckBox();
            this.chMoedaB16 = new System.Windows.Forms.CheckBox();
            this.chMoedaB17 = new System.Windows.Forms.CheckBox();
            this.chMoedaB18 = new System.Windows.Forms.CheckBox();
            this.chMoedaB19 = new System.Windows.Forms.CheckBox();
            this.chMoedaB20 = new System.Windows.Forms.CheckBox();
            this.chMoedaB21 = new System.Windows.Forms.CheckBox();
            this.chMoedaB22 = new System.Windows.Forms.CheckBox();
            this.chMoedaB01 = new System.Windows.Forms.CheckBox();
            this.btnLigarB01 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnStartB01 = new System.Windows.Forms.Button();
            this.btnStartB02 = new System.Windows.Forms.Button();
            this.btnStartB04 = new System.Windows.Forms.Button();
            this.btnStartB05 = new System.Windows.Forms.Button();
            this.btnStartB06 = new System.Windows.Forms.Button();
            this.btnStartB07 = new System.Windows.Forms.Button();
            this.btnStartB08 = new System.Windows.Forms.Button();
            this.btnStartB09 = new System.Windows.Forms.Button();
            this.btnStartB10 = new System.Windows.Forms.Button();
            this.btnStartB11 = new System.Windows.Forms.Button();
            this.btnStartB03 = new System.Windows.Forms.Button();
            this.btnStartB12 = new System.Windows.Forms.Button();
            this.btnStartB13 = new System.Windows.Forms.Button();
            this.btnStartB15 = new System.Windows.Forms.Button();
            this.btnStartB16 = new System.Windows.Forms.Button();
            this.btnStartB17 = new System.Windows.Forms.Button();
            this.btnStartB18 = new System.Windows.Forms.Button();
            this.btnStartB19 = new System.Windows.Forms.Button();
            this.btnStartB20 = new System.Windows.Forms.Button();
            this.btnStartB21 = new System.Windows.Forms.Button();
            this.btnStartB22 = new System.Windows.Forms.Button();
            this.btnStartB14 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtValorMinimo = new System.Windows.Forms.TextBox();
            this.chTecPro = new System.Windows.Forms.CheckBox();
            this.chTecPull = new System.Windows.Forms.CheckBox();
            this.btnNovo = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.cmTraiderId = new System.Windows.Forms.ComboBox();
            this.chDigitalOTC = new System.Windows.Forms.CheckBox();
            this.cmUser = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.chArquivarDiretorio = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtIdTrayder = new System.Windows.Forms.TextBox();
            this.chArquivarBanco = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 24);
            this.label5.TabIndex = 10;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 24);
            this.label6.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 275);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 24);
            this.label4.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 24);
            this.label7.TabIndex = 15;
            this.label7.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 226);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 24);
            this.label3.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 24);
            this.label2.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(11, 384);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 24);
            this.label8.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 336);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 24);
            this.label1.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(11, 435);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 24);
            this.label10.TabIndex = 21;
            // 
            // chDigital
            // 
            this.chDigital.AutoSize = true;
            this.chDigital.Location = new System.Drawing.Point(431, 476);
            this.chDigital.Name = "chDigital";
            this.chDigital.Size = new System.Drawing.Size(68, 17);
            this.chDigital.TabIndex = 12;
            this.chDigital.Text = "DIGITAL";
            this.chDigital.UseVisualStyleBackColor = true;
            // 
            // chBinaria
            // 
            this.chBinaria.AutoSize = true;
            this.chBinaria.Location = new System.Drawing.Point(333, 476);
            this.chBinaria.Name = "chBinaria";
            this.chBinaria.Size = new System.Drawing.Size(69, 17);
            this.chBinaria.TabIndex = 11;
            this.chBinaria.Text = "BINARIA";
            this.chBinaria.UseVisualStyleBackColor = true;
            // 
            // txtStopLoss
            // 
            this.txtStopLoss.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStopLoss.Location = new System.Drawing.Point(199, 193);
            this.txtStopLoss.Name = "txtStopLoss";
            this.txtStopLoss.Size = new System.Drawing.Size(138, 29);
            this.txtStopLoss.TabIndex = 4;
            this.txtStopLoss.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStopLoss_KeyDown);
            this.txtStopLoss.Leave += new System.EventHandler(this.txtStopLoss_Leave);
            // 
            // txtValorEntrada
            // 
            this.txtValorEntrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorEntrada.Location = new System.Drawing.Point(199, 147);
            this.txtValorEntrada.Name = "txtValorEntrada";
            this.txtValorEntrada.Size = new System.Drawing.Size(138, 29);
            this.txtValorEntrada.TabIndex = 3;
            this.txtValorEntrada.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtValorEntrada_KeyDown);
            // 
            // txtStopWin
            // 
            this.txtStopWin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStopWin.Location = new System.Drawing.Point(199, 247);
            this.txtStopWin.Name = "txtStopWin";
            this.txtStopWin.Size = new System.Drawing.Size(138, 29);
            this.txtStopWin.TabIndex = 5;
            this.txtStopWin.Leave += new System.EventHandler(this.txtStopWin_Leave);
            // 
            // txtDiretorioBinaria
            // 
            this.txtDiretorioBinaria.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiretorioBinaria.Location = new System.Drawing.Point(199, 101);
            this.txtDiretorioBinaria.Name = "txtDiretorioBinaria";
            this.txtDiretorioBinaria.Size = new System.Drawing.Size(594, 29);
            this.txtDiretorioBinaria.TabIndex = 2;
            this.txtDiretorioBinaria.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDiretorioBinaria_KeyDown);
            // 
            // txtDiretorioDigital
            // 
            this.txtDiretorioDigital.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiretorioDigital.Location = new System.Drawing.Point(199, 54);
            this.txtDiretorioDigital.Name = "txtDiretorioDigital";
            this.txtDiretorioDigital.Size = new System.Drawing.Size(594, 29);
            this.txtDiretorioDigital.TabIndex = 1;
            this.txtDiretorioDigital.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtDiretorioDigital.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDiretorioDigital_KeyDown);
            // 
            // txtUserId
            // 
            this.txtUserId.Enabled = false;
            this.txtUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserId.Location = new System.Drawing.Point(9, 430);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(47, 29);
            this.txtUserId.TabIndex = 16;
            this.txtUserId.Visible = false;
            // 
            // txtValorInicial
            // 
            this.txtValorInicial.Enabled = false;
            this.txtValorInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorInicial.Location = new System.Drawing.Point(510, 244);
            this.txtValorInicial.Name = "txtValorInicial";
            this.txtValorInicial.Size = new System.Drawing.Size(138, 29);
            this.txtValorInicial.TabIndex = 8;
            this.txtValorInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtValorInicial_KeyDown);
            // 
            // chBynariOTC
            // 
            this.chBynariOTC.AutoSize = true;
            this.chBynariOTC.Location = new System.Drawing.Point(216, 476);
            this.chBynariOTC.Name = "chBynariOTC";
            this.chBynariOTC.Size = new System.Drawing.Size(94, 17);
            this.chBynariOTC.TabIndex = 22;
            this.chBynariOTC.Text = "BINARIA-OTC";
            this.chBynariOTC.UseVisualStyleBackColor = true;
            // 
            // txtConta
            // 
            this.txtConta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConta.FormattingEnabled = true;
            this.txtConta.Items.AddRange(new object[] {
            "PRACTICE ",
            "REAL"});
            this.txtConta.Location = new System.Drawing.Point(510, 302);
            this.txtConta.Name = "txtConta";
            this.txtConta.Size = new System.Drawing.Size(138, 32);
            this.txtConta.TabIndex = 23;
            this.txtConta.Text = "REAL";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.rdDomingo07);
            this.panel1.Controls.Add(this.rdSabado06);
            this.panel1.Controls.Add(this.rdSexta05);
            this.panel1.Controls.Add(this.rdQuinta04);
            this.panel1.Controls.Add(this.rdQuarta03);
            this.panel1.Controls.Add(this.rdTerca02);
            this.panel1.Controls.Add(this.rdSegunda01);
            this.panel1.Location = new System.Drawing.Point(664, 136);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(132, 267);
            this.panel1.TabIndex = 20;
            // 
            // rdDomingo07
            // 
            this.rdDomingo07.AutoSize = true;
            this.rdDomingo07.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdDomingo07.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdDomingo07.Location = new System.Drawing.Point(136, 82);
            this.rdDomingo07.Name = "rdDomingo07";
            this.rdDomingo07.Size = new System.Drawing.Size(111, 29);
            this.rdDomingo07.TabIndex = 6;
            this.rdDomingo07.TabStop = true;
            this.rdDomingo07.Text = "Domingo";
            this.rdDomingo07.UseVisualStyleBackColor = true;
            // 
            // rdSabado06
            // 
            this.rdSabado06.AutoSize = true;
            this.rdSabado06.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdSabado06.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdSabado06.Location = new System.Drawing.Point(136, 30);
            this.rdSabado06.Name = "rdSabado06";
            this.rdSabado06.Size = new System.Drawing.Size(99, 29);
            this.rdSabado06.TabIndex = 5;
            this.rdSabado06.TabStop = true;
            this.rdSabado06.Text = "Sábado";
            this.rdSabado06.UseVisualStyleBackColor = true;
            // 
            // rdSexta05
            // 
            this.rdSexta05.AutoSize = true;
            this.rdSexta05.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdSexta05.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdSexta05.Location = new System.Drawing.Point(19, 216);
            this.rdSexta05.Name = "rdSexta05";
            this.rdSexta05.Size = new System.Drawing.Size(81, 29);
            this.rdSexta05.TabIndex = 4;
            this.rdSexta05.TabStop = true;
            this.rdSexta05.Text = "Sexta";
            this.rdSexta05.UseVisualStyleBackColor = true;
            // 
            // rdQuinta04
            // 
            this.rdQuinta04.AutoSize = true;
            this.rdQuinta04.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdQuinta04.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdQuinta04.Location = new System.Drawing.Point(19, 172);
            this.rdQuinta04.Name = "rdQuinta04";
            this.rdQuinta04.Size = new System.Drawing.Size(89, 29);
            this.rdQuinta04.TabIndex = 3;
            this.rdQuinta04.TabStop = true;
            this.rdQuinta04.Text = "Quinta";
            this.rdQuinta04.UseVisualStyleBackColor = true;
            // 
            // rdQuarta03
            // 
            this.rdQuarta03.AutoSize = true;
            this.rdQuarta03.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdQuarta03.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdQuarta03.Location = new System.Drawing.Point(19, 122);
            this.rdQuarta03.Name = "rdQuarta03";
            this.rdQuarta03.Size = new System.Drawing.Size(90, 29);
            this.rdQuarta03.TabIndex = 2;
            this.rdQuarta03.TabStop = true;
            this.rdQuarta03.Text = "Quarta";
            this.rdQuarta03.UseVisualStyleBackColor = true;
            // 
            // rdTerca02
            // 
            this.rdTerca02.AutoSize = true;
            this.rdTerca02.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdTerca02.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTerca02.Location = new System.Drawing.Point(19, 76);
            this.rdTerca02.Name = "rdTerca02";
            this.rdTerca02.Size = new System.Drawing.Size(83, 29);
            this.rdTerca02.TabIndex = 1;
            this.rdTerca02.TabStop = true;
            this.rdTerca02.Text = "Terça";
            this.rdTerca02.UseVisualStyleBackColor = true;
            // 
            // rdSegunda01
            // 
            this.rdSegunda01.AutoSize = true;
            this.rdSegunda01.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdSegunda01.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdSegunda01.Location = new System.Drawing.Point(19, 27);
            this.rdSegunda01.Name = "rdSegunda01";
            this.rdSegunda01.Size = new System.Drawing.Size(111, 29);
            this.rdSegunda01.TabIndex = 0;
            this.rdSegunda01.TabStop = true;
            this.rdSegunda01.Text = "Segunda";
            this.rdSegunda01.UseVisualStyleBackColor = true;
            // 
            // lblTop
            // 
            this.lblTop.AutoSize = true;
            this.lblTop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTop.Location = new System.Drawing.Point(684, 7);
            this.lblTop.Name = "lblTop";
            this.lblTop.Size = new System.Drawing.Size(40, 20);
            this.lblTop.TabIndex = 49;
            this.lblTop.Text = "TOP";
            // 
            // txtTop
            // 
            this.txtTop.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTop.Location = new System.Drawing.Point(739, 2);
            this.txtTop.Name = "txtTop";
            this.txtTop.Size = new System.Drawing.Size(138, 29);
            this.txtTop.TabIndex = 6;
            this.txtTop.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTop_KeyDown);
            // 
            // rdSegunda
            // 
            this.rdSegunda.AutoSize = true;
            this.rdSegunda.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdSegunda.Location = new System.Drawing.Point(3, 8);
            this.rdSegunda.Name = "rdSegunda";
            this.rdSegunda.Size = new System.Drawing.Size(105, 28);
            this.rdSegunda.TabIndex = 0;
            this.rdSegunda.TabStop = true;
            this.rdSegunda.Text = "Segunda";
            this.rdSegunda.UseVisualStyleBackColor = true;
            // 
            // rdTerca
            // 
            this.rdTerca.AutoSize = true;
            this.rdTerca.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTerca.Location = new System.Drawing.Point(3, 42);
            this.rdTerca.Name = "rdTerca";
            this.rdTerca.Size = new System.Drawing.Size(77, 28);
            this.rdTerca.TabIndex = 1;
            this.rdTerca.TabStop = true;
            this.rdTerca.Text = "Terça";
            this.rdTerca.UseVisualStyleBackColor = true;
            // 
            // rdQuarta
            // 
            this.rdQuarta.AutoSize = true;
            this.rdQuarta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdQuarta.Location = new System.Drawing.Point(3, 80);
            this.rdQuarta.Name = "rdQuarta";
            this.rdQuarta.Size = new System.Drawing.Size(84, 28);
            this.rdQuarta.TabIndex = 2;
            this.rdQuarta.TabStop = true;
            this.rdQuarta.Text = "Quarta";
            this.rdQuarta.UseVisualStyleBackColor = true;
            // 
            // rdQuinta
            // 
            this.rdQuinta.AutoSize = true;
            this.rdQuinta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdQuinta.Location = new System.Drawing.Point(4, 115);
            this.rdQuinta.Name = "rdQuinta";
            this.rdQuinta.Size = new System.Drawing.Size(83, 28);
            this.rdQuinta.TabIndex = 3;
            this.rdQuinta.TabStop = true;
            this.rdQuinta.Text = "Quinta";
            this.rdQuinta.UseVisualStyleBackColor = true;
            // 
            // rdSexta
            // 
            this.rdSexta.AutoSize = true;
            this.rdSexta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdSexta.Location = new System.Drawing.Point(4, 149);
            this.rdSexta.Name = "rdSexta";
            this.rdSexta.Size = new System.Drawing.Size(75, 28);
            this.rdSexta.TabIndex = 4;
            this.rdSexta.TabStop = true;
            this.rdSexta.Text = "Sexta";
            this.rdSexta.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Wide Latin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 19);
            this.label9.TabIndex = 5;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(3, 190);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(93, 28);
            this.radioButton1.TabIndex = 6;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Sábado";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.Location = new System.Drawing.Point(3, 224);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(105, 28);
            this.radioButton2.TabIndex = 7;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Domingo";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // btnSalvar
            // 
            this.btnSalvar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSalvar.Location = new System.Drawing.Point(719, 459);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(102, 47);
            this.btnSalvar.TabIndex = 35;
            this.btnSalvar.Text = "Cadastrar";
            this.btnSalvar.UseVisualStyleBackColor = false;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click_1);
            // 
            // chMoedaB02
            // 
            this.chMoedaB02.AutoSize = true;
            this.chMoedaB02.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB02.Location = new System.Drawing.Point(3, 45);
            this.chMoedaB02.Name = "chMoedaB02";
            this.chMoedaB02.Size = new System.Drawing.Size(161, 22);
            this.chMoedaB02.TabIndex = 1;
            this.chMoedaB02.TabStop = false;
            this.chMoedaB02.Text = "EUR-AUD";
            this.chMoedaB02.UseVisualStyleBackColor = true;
            // 
            // chMoedaB03
            // 
            this.chMoedaB03.AutoSize = true;
            this.chMoedaB03.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB03.Location = new System.Drawing.Point(5, 76);
            this.chMoedaB03.Name = "chMoedaB03";
            this.chMoedaB03.Size = new System.Drawing.Size(159, 22);
            this.chMoedaB03.TabIndex = 2;
            this.chMoedaB03.TabStop = false;
            this.chMoedaB03.Text = "EUR-CAD";
            this.chMoedaB03.UseVisualStyleBackColor = true;
            // 
            // chMoedaB04
            // 
            this.chMoedaB04.AutoSize = true;
            this.chMoedaB04.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB04.Location = new System.Drawing.Point(5, 109);
            this.chMoedaB04.Name = "chMoedaB04";
            this.chMoedaB04.Size = new System.Drawing.Size(160, 22);
            this.chMoedaB04.TabIndex = 3;
            this.chMoedaB04.TabStop = false;
            this.chMoedaB04.Text = "EUR-GBP";
            this.chMoedaB04.UseVisualStyleBackColor = true;
            // 
            // chMoedaB05
            // 
            this.chMoedaB05.AutoSize = true;
            this.chMoedaB05.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB05.Location = new System.Drawing.Point(6, 143);
            this.chMoedaB05.Name = "chMoedaB05";
            this.chMoedaB05.Size = new System.Drawing.Size(153, 22);
            this.chMoedaB05.TabIndex = 4;
            this.chMoedaB05.TabStop = false;
            this.chMoedaB05.Text = "EUR-JPY";
            this.chMoedaB05.UseVisualStyleBackColor = true;
            // 
            // chMoedaB06
            // 
            this.chMoedaB06.AutoSize = true;
            this.chMoedaB06.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB06.Location = new System.Drawing.Point(5, 171);
            this.chMoedaB06.Name = "chMoedaB06";
            this.chMoedaB06.Size = new System.Drawing.Size(159, 22);
            this.chMoedaB06.TabIndex = 5;
            this.chMoedaB06.TabStop = false;
            this.chMoedaB06.Text = "GBP-AUD";
            this.chMoedaB06.UseVisualStyleBackColor = true;
            // 
            // chMoedaB07
            // 
            this.chMoedaB07.AutoSize = true;
            this.chMoedaB07.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB07.Location = new System.Drawing.Point(5, 199);
            this.chMoedaB07.Name = "chMoedaB07";
            this.chMoedaB07.Size = new System.Drawing.Size(157, 22);
            this.chMoedaB07.TabIndex = 6;
            this.chMoedaB07.TabStop = false;
            this.chMoedaB07.Text = "GBP-CAD";
            this.chMoedaB07.UseVisualStyleBackColor = true;
            // 
            // chMoedaB08
            // 
            this.chMoedaB08.AutoSize = true;
            this.chMoedaB08.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB08.Location = new System.Drawing.Point(4, 229);
            this.chMoedaB08.Name = "chMoedaB08";
            this.chMoedaB08.Size = new System.Drawing.Size(158, 22);
            this.chMoedaB08.TabIndex = 7;
            this.chMoedaB08.TabStop = false;
            this.chMoedaB08.Text = "GBP-CHF";
            this.chMoedaB08.UseVisualStyleBackColor = true;
            // 
            // chMoedaB09
            // 
            this.chMoedaB09.AutoSize = true;
            this.chMoedaB09.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB09.Location = new System.Drawing.Point(3, 257);
            this.chMoedaB09.Name = "chMoedaB09";
            this.chMoedaB09.Size = new System.Drawing.Size(151, 22);
            this.chMoedaB09.TabIndex = 8;
            this.chMoedaB09.TabStop = false;
            this.chMoedaB09.Text = "GBP-JPY";
            this.chMoedaB09.UseVisualStyleBackColor = true;
            // 
            // chMoedaB10
            // 
            this.chMoedaB10.AutoSize = true;
            this.chMoedaB10.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB10.Location = new System.Drawing.Point(3, 291);
            this.chMoedaB10.Name = "chMoedaB10";
            this.chMoedaB10.Size = new System.Drawing.Size(157, 22);
            this.chMoedaB10.TabIndex = 9;
            this.chMoedaB10.TabStop = false;
            this.chMoedaB10.Text = "GBP-NZD";
            this.chMoedaB10.UseVisualStyleBackColor = true;
            // 
            // chMoedaB11
            // 
            this.chMoedaB11.AutoSize = true;
            this.chMoedaB11.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB11.Location = new System.Drawing.Point(5, 320);
            this.chMoedaB11.Name = "chMoedaB11";
            this.chMoedaB11.Size = new System.Drawing.Size(157, 22);
            this.chMoedaB11.TabIndex = 10;
            this.chMoedaB11.TabStop = false;
            this.chMoedaB11.Text = "GBP-USD";
            this.chMoedaB11.UseVisualStyleBackColor = true;
            // 
            // chMoedaB12
            // 
            this.chMoedaB12.AutoSize = true;
            this.chMoedaB12.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB12.Location = new System.Drawing.Point(385, 17);
            this.chMoedaB12.Name = "chMoedaB12";
            this.chMoedaB12.Size = new System.Drawing.Size(158, 22);
            this.chMoedaB12.TabIndex = 11;
            this.chMoedaB12.TabStop = false;
            this.chMoedaB12.Text = "AUD-CAD";
            this.chMoedaB12.UseVisualStyleBackColor = true;
            // 
            // chMoedaB13
            // 
            this.chMoedaB13.AutoSize = true;
            this.chMoedaB13.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB13.Location = new System.Drawing.Point(385, 52);
            this.chMoedaB13.Name = "chMoedaB13";
            this.chMoedaB13.Size = new System.Drawing.Size(152, 22);
            this.chMoedaB13.TabIndex = 12;
            this.chMoedaB13.TabStop = false;
            this.chMoedaB13.Text = "AUD-JPY";
            this.chMoedaB13.UseVisualStyleBackColor = true;
            // 
            // chMoedaB14
            // 
            this.chMoedaB14.AutoSize = true;
            this.chMoedaB14.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB14.Location = new System.Drawing.Point(385, 83);
            this.chMoedaB14.Name = "chMoedaB14";
            this.chMoedaB14.Size = new System.Drawing.Size(158, 22);
            this.chMoedaB14.TabIndex = 13;
            this.chMoedaB14.TabStop = false;
            this.chMoedaB14.Text = "AUD-USD";
            this.chMoedaB14.UseVisualStyleBackColor = true;
            // 
            // chMoedaB15
            // 
            this.chMoedaB15.AutoSize = true;
            this.chMoedaB15.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB15.Location = new System.Drawing.Point(385, 115);
            this.chMoedaB15.Name = "chMoedaB15";
            this.chMoedaB15.Size = new System.Drawing.Size(156, 22);
            this.chMoedaB15.TabIndex = 14;
            this.chMoedaB15.TabStop = false;
            this.chMoedaB15.Text = "USD-CAD";
            this.chMoedaB15.UseVisualStyleBackColor = true;
            // 
            // chMoedaB16
            // 
            this.chMoedaB16.AutoSize = true;
            this.chMoedaB16.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB16.Location = new System.Drawing.Point(385, 145);
            this.chMoedaB16.Name = "chMoedaB16";
            this.chMoedaB16.Size = new System.Drawing.Size(157, 22);
            this.chMoedaB16.TabIndex = 15;
            this.chMoedaB16.TabStop = false;
            this.chMoedaB16.Text = "USD-CHF";
            this.chMoedaB16.UseVisualStyleBackColor = true;
            // 
            // chMoedaB17
            // 
            this.chMoedaB17.AutoSize = true;
            this.chMoedaB17.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB17.Location = new System.Drawing.Point(385, 173);
            this.chMoedaB17.Name = "chMoedaB17";
            this.chMoedaB17.Size = new System.Drawing.Size(150, 22);
            this.chMoedaB17.TabIndex = 16;
            this.chMoedaB17.TabStop = false;
            this.chMoedaB17.Text = "USD-JPY";
            this.chMoedaB17.UseVisualStyleBackColor = true;
            // 
            // chMoedaB18
            // 
            this.chMoedaB18.AutoSize = true;
            this.chMoedaB18.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB18.Location = new System.Drawing.Point(385, 201);
            this.chMoedaB18.Name = "chMoedaB18";
            this.chMoedaB18.Size = new System.Drawing.Size(150, 22);
            this.chMoedaB18.TabIndex = 17;
            this.chMoedaB18.TabStop = false;
            this.chMoedaB18.Text = "CAD-JPY";
            this.chMoedaB18.UseVisualStyleBackColor = true;
            // 
            // chMoedaB19
            // 
            this.chMoedaB19.AutoSize = true;
            this.chMoedaB19.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB19.Location = new System.Drawing.Point(385, 229);
            this.chMoedaB19.Name = "chMoedaB19";
            this.chMoedaB19.Size = new System.Drawing.Size(157, 22);
            this.chMoedaB19.TabIndex = 18;
            this.chMoedaB19.TabStop = false;
            this.chMoedaB19.Text = "CAD-CHF";
            this.chMoedaB19.UseVisualStyleBackColor = true;
            // 
            // chMoedaB20
            // 
            this.chMoedaB20.AutoSize = true;
            this.chMoedaB20.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB20.Location = new System.Drawing.Point(385, 263);
            this.chMoedaB20.Name = "chMoedaB20";
            this.chMoedaB20.Size = new System.Drawing.Size(156, 22);
            this.chMoedaB20.TabIndex = 19;
            this.chMoedaB20.TabStop = false;
            this.chMoedaB20.Text = "NZD-USD";
            this.chMoedaB20.UseVisualStyleBackColor = true;
            // 
            // chMoedaB21
            // 
            this.chMoedaB21.AutoSize = true;
            this.chMoedaB21.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB21.Location = new System.Drawing.Point(385, 294);
            this.chMoedaB21.Name = "chMoedaB21";
            this.chMoedaB21.Size = new System.Drawing.Size(160, 22);
            this.chMoedaB21.TabIndex = 20;
            this.chMoedaB21.TabStop = false;
            this.chMoedaB21.Text = "EUR-CHF";
            this.chMoedaB21.UseVisualStyleBackColor = true;
            // 
            // chMoedaB22
            // 
            this.chMoedaB22.AutoSize = true;
            this.chMoedaB22.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB22.Location = new System.Drawing.Point(385, 326);
            this.chMoedaB22.Name = "chMoedaB22";
            this.chMoedaB22.Size = new System.Drawing.Size(159, 22);
            this.chMoedaB22.TabIndex = 21;
            this.chMoedaB22.TabStop = false;
            this.chMoedaB22.Text = "EUR-NZD";
            this.chMoedaB22.UseVisualStyleBackColor = true;
            // 
            // chMoedaB01
            // 
            this.chMoedaB01.AutoSize = true;
            this.chMoedaB01.Font = new System.Drawing.Font("Wide Latin", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chMoedaB01.Location = new System.Drawing.Point(3, 11);
            this.chMoedaB01.Name = "chMoedaB01";
            this.chMoedaB01.Size = new System.Drawing.Size(159, 22);
            this.chMoedaB01.TabIndex = 22;
            this.chMoedaB01.TabStop = false;
            this.chMoedaB01.Text = "EUR-USD";
            this.chMoedaB01.UseVisualStyleBackColor = true;
            // 
            // btnLigarB01
            // 
            this.btnLigarB01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnLigarB01.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLigarB01.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLigarB01.Location = new System.Drawing.Point(172, 372);
            this.btnLigarB01.Name = "btnLigarB01";
            this.btnLigarB01.Size = new System.Drawing.Size(75, 23);
            this.btnLigarB01.TabIndex = 23;
            this.btnLigarB01.Text = "Ligar Robô";
            this.btnLigarB01.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(550, 372);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "Ligar Robô";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // btnStartB01
            // 
            this.btnStartB01.BackColor = System.Drawing.Color.Green;
            this.btnStartB01.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB01.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB01.Location = new System.Drawing.Point(172, 10);
            this.btnStartB01.Name = "btnStartB01";
            this.btnStartB01.Size = new System.Drawing.Size(109, 23);
            this.btnStartB01.TabIndex = 25;
            this.btnStartB01.Text = "Robô Ligado";
            this.btnStartB01.UseVisualStyleBackColor = false;
            // 
            // btnStartB02
            // 
            this.btnStartB02.BackColor = System.Drawing.Color.Green;
            this.btnStartB02.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB02.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB02.Location = new System.Drawing.Point(172, 45);
            this.btnStartB02.Name = "btnStartB02";
            this.btnStartB02.Size = new System.Drawing.Size(109, 23);
            this.btnStartB02.TabIndex = 26;
            this.btnStartB02.Text = "Robô Ligado";
            this.btnStartB02.UseVisualStyleBackColor = false;
            // 
            // btnStartB04
            // 
            this.btnStartB04.BackColor = System.Drawing.Color.Green;
            this.btnStartB04.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB04.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB04.Location = new System.Drawing.Point(172, 108);
            this.btnStartB04.Name = "btnStartB04";
            this.btnStartB04.Size = new System.Drawing.Size(109, 23);
            this.btnStartB04.TabIndex = 27;
            this.btnStartB04.Text = "Robô Ligado";
            this.btnStartB04.UseVisualStyleBackColor = false;
            // 
            // btnStartB05
            // 
            this.btnStartB05.BackColor = System.Drawing.Color.Green;
            this.btnStartB05.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB05.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB05.Location = new System.Drawing.Point(172, 140);
            this.btnStartB05.Name = "btnStartB05";
            this.btnStartB05.Size = new System.Drawing.Size(109, 23);
            this.btnStartB05.TabIndex = 28;
            this.btnStartB05.Text = "Robô Ligado";
            this.btnStartB05.UseVisualStyleBackColor = false;
            // 
            // btnStartB06
            // 
            this.btnStartB06.BackColor = System.Drawing.Color.Green;
            this.btnStartB06.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB06.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB06.Location = new System.Drawing.Point(172, 169);
            this.btnStartB06.Name = "btnStartB06";
            this.btnStartB06.Size = new System.Drawing.Size(109, 23);
            this.btnStartB06.TabIndex = 29;
            this.btnStartB06.Text = "Robô Ligado";
            this.btnStartB06.UseVisualStyleBackColor = false;
            // 
            // btnStartB07
            // 
            this.btnStartB07.BackColor = System.Drawing.Color.Green;
            this.btnStartB07.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB07.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB07.Location = new System.Drawing.Point(172, 198);
            this.btnStartB07.Name = "btnStartB07";
            this.btnStartB07.Size = new System.Drawing.Size(109, 23);
            this.btnStartB07.TabIndex = 30;
            this.btnStartB07.Text = "Robô Ligado";
            this.btnStartB07.UseVisualStyleBackColor = false;
            // 
            // btnStartB08
            // 
            this.btnStartB08.BackColor = System.Drawing.Color.Green;
            this.btnStartB08.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB08.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB08.Location = new System.Drawing.Point(172, 227);
            this.btnStartB08.Name = "btnStartB08";
            this.btnStartB08.Size = new System.Drawing.Size(109, 23);
            this.btnStartB08.TabIndex = 31;
            this.btnStartB08.Text = "Robô Ligado";
            this.btnStartB08.UseVisualStyleBackColor = false;
            // 
            // btnStartB09
            // 
            this.btnStartB09.BackColor = System.Drawing.Color.Green;
            this.btnStartB09.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB09.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB09.Location = new System.Drawing.Point(172, 256);
            this.btnStartB09.Name = "btnStartB09";
            this.btnStartB09.Size = new System.Drawing.Size(109, 23);
            this.btnStartB09.TabIndex = 32;
            this.btnStartB09.Text = "Robô Ligado";
            this.btnStartB09.UseVisualStyleBackColor = false;
            // 
            // btnStartB10
            // 
            this.btnStartB10.BackColor = System.Drawing.Color.Green;
            this.btnStartB10.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB10.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB10.Location = new System.Drawing.Point(172, 290);
            this.btnStartB10.Name = "btnStartB10";
            this.btnStartB10.Size = new System.Drawing.Size(109, 23);
            this.btnStartB10.TabIndex = 33;
            this.btnStartB10.Text = "Robô Ligado";
            this.btnStartB10.UseVisualStyleBackColor = false;
            // 
            // btnStartB11
            // 
            this.btnStartB11.BackColor = System.Drawing.Color.Green;
            this.btnStartB11.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB11.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB11.Location = new System.Drawing.Point(172, 319);
            this.btnStartB11.Name = "btnStartB11";
            this.btnStartB11.Size = new System.Drawing.Size(109, 23);
            this.btnStartB11.TabIndex = 34;
            this.btnStartB11.Text = "Robô Ligado";
            this.btnStartB11.UseVisualStyleBackColor = false;
            // 
            // btnStartB03
            // 
            this.btnStartB03.BackColor = System.Drawing.Color.Green;
            this.btnStartB03.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB03.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB03.Location = new System.Drawing.Point(172, 75);
            this.btnStartB03.Name = "btnStartB03";
            this.btnStartB03.Size = new System.Drawing.Size(109, 23);
            this.btnStartB03.TabIndex = 35;
            this.btnStartB03.Text = "Robô Ligado";
            this.btnStartB03.UseVisualStyleBackColor = false;
            // 
            // btnStartB12
            // 
            this.btnStartB12.BackColor = System.Drawing.Color.Green;
            this.btnStartB12.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB12.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB12.Location = new System.Drawing.Point(550, 17);
            this.btnStartB12.Name = "btnStartB12";
            this.btnStartB12.Size = new System.Drawing.Size(109, 23);
            this.btnStartB12.TabIndex = 36;
            this.btnStartB12.Text = "Robô Ligado";
            this.btnStartB12.UseVisualStyleBackColor = false;
            this.btnStartB12.Click += new System.EventHandler(this.button12_Click);
            // 
            // btnStartB13
            // 
            this.btnStartB13.BackColor = System.Drawing.Color.Green;
            this.btnStartB13.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB13.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB13.Location = new System.Drawing.Point(550, 52);
            this.btnStartB13.Name = "btnStartB13";
            this.btnStartB13.Size = new System.Drawing.Size(109, 23);
            this.btnStartB13.TabIndex = 37;
            this.btnStartB13.Text = "Robô Ligado";
            this.btnStartB13.UseVisualStyleBackColor = false;
            // 
            // btnStartB15
            // 
            this.btnStartB15.BackColor = System.Drawing.Color.Green;
            this.btnStartB15.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB15.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB15.Location = new System.Drawing.Point(550, 115);
            this.btnStartB15.Name = "btnStartB15";
            this.btnStartB15.Size = new System.Drawing.Size(109, 23);
            this.btnStartB15.TabIndex = 38;
            this.btnStartB15.Text = "Robô Ligado";
            this.btnStartB15.UseVisualStyleBackColor = false;
            // 
            // btnStartB16
            // 
            this.btnStartB16.BackColor = System.Drawing.Color.Green;
            this.btnStartB16.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB16.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB16.Location = new System.Drawing.Point(550, 147);
            this.btnStartB16.Name = "btnStartB16";
            this.btnStartB16.Size = new System.Drawing.Size(109, 23);
            this.btnStartB16.TabIndex = 39;
            this.btnStartB16.Text = "Robô Ligado";
            this.btnStartB16.UseVisualStyleBackColor = false;
            // 
            // btnStartB17
            // 
            this.btnStartB17.BackColor = System.Drawing.Color.Green;
            this.btnStartB17.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB17.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB17.Location = new System.Drawing.Point(550, 176);
            this.btnStartB17.Name = "btnStartB17";
            this.btnStartB17.Size = new System.Drawing.Size(109, 23);
            this.btnStartB17.TabIndex = 40;
            this.btnStartB17.Text = "Robô Ligado";
            this.btnStartB17.UseVisualStyleBackColor = false;
            // 
            // btnStartB18
            // 
            this.btnStartB18.BackColor = System.Drawing.Color.Green;
            this.btnStartB18.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB18.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB18.Location = new System.Drawing.Point(550, 205);
            this.btnStartB18.Name = "btnStartB18";
            this.btnStartB18.Size = new System.Drawing.Size(109, 23);
            this.btnStartB18.TabIndex = 41;
            this.btnStartB18.Text = "Robô Ligado";
            this.btnStartB18.UseVisualStyleBackColor = false;
            // 
            // btnStartB19
            // 
            this.btnStartB19.BackColor = System.Drawing.Color.Green;
            this.btnStartB19.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB19.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB19.Location = new System.Drawing.Point(550, 234);
            this.btnStartB19.Name = "btnStartB19";
            this.btnStartB19.Size = new System.Drawing.Size(109, 23);
            this.btnStartB19.TabIndex = 42;
            this.btnStartB19.Text = "Robô Ligado";
            this.btnStartB19.UseVisualStyleBackColor = false;
            // 
            // btnStartB20
            // 
            this.btnStartB20.BackColor = System.Drawing.Color.Green;
            this.btnStartB20.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB20.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB20.Location = new System.Drawing.Point(550, 263);
            this.btnStartB20.Name = "btnStartB20";
            this.btnStartB20.Size = new System.Drawing.Size(109, 23);
            this.btnStartB20.TabIndex = 43;
            this.btnStartB20.Text = "Robô Ligado";
            this.btnStartB20.UseVisualStyleBackColor = false;
            // 
            // btnStartB21
            // 
            this.btnStartB21.BackColor = System.Drawing.Color.Green;
            this.btnStartB21.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB21.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB21.Location = new System.Drawing.Point(550, 297);
            this.btnStartB21.Name = "btnStartB21";
            this.btnStartB21.Size = new System.Drawing.Size(109, 23);
            this.btnStartB21.TabIndex = 44;
            this.btnStartB21.Text = "Robô Ligado";
            this.btnStartB21.UseVisualStyleBackColor = false;
            // 
            // btnStartB22
            // 
            this.btnStartB22.BackColor = System.Drawing.Color.Green;
            this.btnStartB22.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB22.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB22.Location = new System.Drawing.Point(550, 326);
            this.btnStartB22.Name = "btnStartB22";
            this.btnStartB22.Size = new System.Drawing.Size(109, 23);
            this.btnStartB22.TabIndex = 45;
            this.btnStartB22.Text = "Robô Ligado";
            this.btnStartB22.UseVisualStyleBackColor = false;
            // 
            // btnStartB14
            // 
            this.btnStartB14.BackColor = System.Drawing.Color.Green;
            this.btnStartB14.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStartB14.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartB14.Location = new System.Drawing.Point(550, 82);
            this.btnStartB14.Name = "btnStartB14";
            this.btnStartB14.Size = new System.Drawing.Size(109, 23);
            this.btnStartB14.TabIndex = 46;
            this.btnStartB14.Text = "Robô Ligado";
            this.btnStartB14.UseVisualStyleBackColor = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(76, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 20);
            this.label11.TabIndex = 36;
            this.label11.Text = "Diretorio Digital";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(67, 107);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 20);
            this.label12.TabIndex = 37;
            this.label12.Text = "Diretorio Bynaria";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(86, 153);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 20);
            this.label13.TabIndex = 38;
            this.label13.Text = "Valor Entrada";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(112, 199);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 20);
            this.label14.TabIndex = 39;
            this.label14.Text = "Stop Loss";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(119, 253);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 20);
            this.label15.TabIndex = 40;
            this.label15.Text = "Stop Win";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(112, 311);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 20);
            this.label16.TabIndex = 41;
            this.label16.Text = "TrayderID";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(450, 308);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 20);
            this.label17.TabIndex = 42;
            this.label17.Text = "Conta";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(396, 250);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(99, 20);
            this.label18.TabIndex = 43;
            this.label18.Text = "Banca Inicial";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(395, 199);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 20);
            this.label19.TabIndex = 45;
            this.label19.Text = "Valor Minimo";
            // 
            // txtValorMinimo
            // 
            this.txtValorMinimo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorMinimo.Location = new System.Drawing.Point(510, 193);
            this.txtValorMinimo.Name = "txtValorMinimo";
            this.txtValorMinimo.Size = new System.Drawing.Size(138, 29);
            this.txtValorMinimo.TabIndex = 7;
            this.txtValorMinimo.Text = "0";
            this.txtValorMinimo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtValorMinimo_KeyDown);
            // 
            // chTecPro
            // 
            this.chTecPro.AutoSize = true;
            this.chTecPro.Location = new System.Drawing.Point(9, 383);
            this.chTecPro.Name = "chTecPro";
            this.chTecPro.Size = new System.Drawing.Size(80, 17);
            this.chTecPro.TabIndex = 46;
            this.chTecPro.Text = "checkBox1";
            this.chTecPro.UseVisualStyleBackColor = true;
            this.chTecPro.Visible = false;
            // 
            // chTecPull
            // 
            this.chTecPull.AutoSize = true;
            this.chTecPull.Enabled = false;
            this.chTecPull.Location = new System.Drawing.Point(9, 476);
            this.chTecPull.Name = "chTecPull";
            this.chTecPull.Size = new System.Drawing.Size(80, 17);
            this.chTecPull.TabIndex = 47;
            this.chTecPull.Text = "checkBox2";
            this.chTecPull.UseVisualStyleBackColor = true;
            this.chTecPull.Visible = false;
            // 
            // btnNovo
            // 
            this.btnNovo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnNovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnNovo.Location = new System.Drawing.Point(609, 459);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(102, 47);
            this.btnNovo.TabIndex = 48;
            this.btnNovo.Text = "Novo";
            this.btnNovo.UseVisualStyleBackColor = false;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.button2.Location = new System.Drawing.Point(-2, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 23);
            this.button2.TabIndex = 49;
            this.button2.Text = "<Voltar Menu";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cmTraiderId
            // 
            this.cmTraiderId.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmTraiderId.FormattingEnabled = true;
            this.cmTraiderId.ItemHeight = 24;
            this.cmTraiderId.Items.AddRange(new object[] {
            "TECNICA01(Canga)",
            "TECNICA02(Rodrigo L.)",
            "TECNICA03(Fabio G.)\t",
            "TECNICA04(Elizabeth R.",
            "TECNICA05(Thomas S.)",
            "TECNICA05(Sarwaka S.)"});
            this.cmTraiderId.Location = new System.Drawing.Point(199, 305);
            this.cmTraiderId.Name = "cmTraiderId";
            this.cmTraiderId.Size = new System.Drawing.Size(245, 32);
            this.cmTraiderId.TabIndex = 50;
            this.cmTraiderId.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmTraiderId_KeyDown);
            // 
            // chDigitalOTC
            // 
            this.chDigitalOTC.AutoSize = true;
            this.chDigitalOTC.Location = new System.Drawing.Point(505, 476);
            this.chDigitalOTC.Name = "chDigitalOTC";
            this.chDigitalOTC.Size = new System.Drawing.Size(93, 17);
            this.chDigitalOTC.TabIndex = 51;
            this.chDigitalOTC.Text = "DIGITAL-OTC";
            this.chDigitalOTC.UseVisualStyleBackColor = true;
            // 
            // cmUser
            // 
            this.cmUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmUser.FormattingEnabled = true;
            this.cmUser.ItemHeight = 24;
            this.cmUser.Location = new System.Drawing.Point(199, 366);
            this.cmUser.Name = "cmUser";
            this.cmUser.Size = new System.Drawing.Size(449, 32);
            this.cmUser.TabIndex = 52;
            this.cmUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmUser_KeyDown);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(109, 372);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(84, 20);
            this.label20.TabIndex = 53;
            this.label20.Text = "USUARIO";
            // 
            // chArquivarDiretorio
            // 
            this.chArquivarDiretorio.AutoSize = true;
            this.chArquivarDiretorio.Location = new System.Drawing.Point(199, 31);
            this.chArquivarDiretorio.Name = "chArquivarDiretorio";
            this.chArquivarDiretorio.Size = new System.Drawing.Size(129, 17);
            this.chArquivarDiretorio.TabIndex = 54;
            this.chArquivarDiretorio.Text = "INSERT DIRETORIO";
            this.chArquivarDiretorio.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(427, 147);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 20);
            this.label21.TabIndex = 56;
            this.label21.Text = "IDTrayder";
            // 
            // txtIdTrayder
            // 
            this.txtIdTrayder.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdTrayder.Location = new System.Drawing.Point(510, 144);
            this.txtIdTrayder.Name = "txtIdTrayder";
            this.txtIdTrayder.Size = new System.Drawing.Size(138, 29);
            this.txtIdTrayder.TabIndex = 55;
            // 
            // chArquivarBanco
            // 
            this.chArquivarBanco.AutoSize = true;
            this.chArquivarBanco.Location = new System.Drawing.Point(338, 31);
            this.chArquivarBanco.Name = "chArquivarBanco";
            this.chArquivarBanco.Size = new System.Drawing.Size(106, 17);
            this.chArquivarBanco.TabIndex = 57;
            this.chArquivarBanco.Text = "INSERT BANCO";
            this.chArquivarBanco.UseVisualStyleBackColor = true;
            // 
            // CadastroOperacional
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 513);
            this.Controls.Add(this.chArquivarBanco);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtIdTrayder);
            this.Controls.Add(this.chArquivarDiretorio);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.cmUser);
            this.Controls.Add(this.lblTop);
            this.Controls.Add(this.chDigitalOTC);
            this.Controls.Add(this.cmTraiderId);
            this.Controls.Add(this.txtTop);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnNovo);
            this.Controls.Add(this.chTecPull);
            this.Controls.Add(this.chTecPro);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtValorMinimo);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.chDigital);
            this.Controls.Add(this.chBinaria);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.chBynariOTC);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtConta);
            this.Controls.Add(this.txtDiretorioDigital);
            this.Controls.Add(this.txtStopLoss);
            this.Controls.Add(this.txtValorInicial);
            this.Controls.Add(this.txtUserId);
            this.Controls.Add(this.txtValorEntrada);
            this.Controls.Add(this.txtStopWin);
            this.Controls.Add(this.txtDiretorioBinaria);
            this.MaximizeBox = false;
            this.Name = "CadastroOperacional";
            this.Text = "Cadastro Do Operacional Robô Bynari";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox txtConta;
        private System.Windows.Forms.CheckBox chBynariOTC;
        private System.Windows.Forms.TextBox txtValorInicial;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.TextBox txtDiretorioDigital;
        private System.Windows.Forms.TextBox txtDiretorioBinaria;
        private System.Windows.Forms.TextBox txtStopWin;
        private System.Windows.Forms.TextBox txtValorEntrada;
        private System.Windows.Forms.TextBox txtStopLoss;
        private System.Windows.Forms.CheckBox chBinaria;
        private System.Windows.Forms.CheckBox chDigital;
        private System.Windows.Forms.RadioButton rdSegunda;
        private System.Windows.Forms.RadioButton rdTerca;
        private System.Windows.Forms.RadioButton rdQuarta;
        private System.Windows.Forms.RadioButton rdQuinta;
        private System.Windows.Forms.RadioButton rdSexta;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.CheckBox chMoedaB02;
        private System.Windows.Forms.CheckBox chMoedaB03;
        private System.Windows.Forms.CheckBox chMoedaB04;
        private System.Windows.Forms.CheckBox chMoedaB05;
        private System.Windows.Forms.CheckBox chMoedaB06;
        private System.Windows.Forms.CheckBox chMoedaB07;
        private System.Windows.Forms.CheckBox chMoedaB08;
        private System.Windows.Forms.CheckBox chMoedaB09;
        private System.Windows.Forms.CheckBox chMoedaB10;
        private System.Windows.Forms.CheckBox chMoedaB11;
        private System.Windows.Forms.CheckBox chMoedaB12;
        private System.Windows.Forms.CheckBox chMoedaB13;
        private System.Windows.Forms.CheckBox chMoedaB14;
        private System.Windows.Forms.CheckBox chMoedaB15;
        private System.Windows.Forms.CheckBox chMoedaB16;
        private System.Windows.Forms.CheckBox chMoedaB17;
        private System.Windows.Forms.CheckBox chMoedaB18;
        private System.Windows.Forms.CheckBox chMoedaB19;
        private System.Windows.Forms.CheckBox chMoedaB20;
        private System.Windows.Forms.CheckBox chMoedaB21;
        private System.Windows.Forms.CheckBox chMoedaB22;
        private System.Windows.Forms.CheckBox chMoedaB01;
        private System.Windows.Forms.Button btnLigarB01;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnStartB01;
        private System.Windows.Forms.Button btnStartB02;
        private System.Windows.Forms.Button btnStartB04;
        private System.Windows.Forms.Button btnStartB05;
        private System.Windows.Forms.Button btnStartB06;
        private System.Windows.Forms.Button btnStartB07;
        private System.Windows.Forms.Button btnStartB08;
        private System.Windows.Forms.Button btnStartB09;
        private System.Windows.Forms.Button btnStartB10;
        private System.Windows.Forms.Button btnStartB11;
        private System.Windows.Forms.Button btnStartB03;
        private System.Windows.Forms.Button btnStartB12;
        private System.Windows.Forms.Button btnStartB13;
        private System.Windows.Forms.Button btnStartB15;
        private System.Windows.Forms.Button btnStartB16;
        private System.Windows.Forms.Button btnStartB17;
        private System.Windows.Forms.Button btnStartB18;
        private System.Windows.Forms.Button btnStartB19;
        private System.Windows.Forms.Button btnStartB20;
        private System.Windows.Forms.Button btnStartB21;
        private System.Windows.Forms.Button btnStartB22;
        private System.Windows.Forms.Button btnStartB14;
        private System.Windows.Forms.RadioButton rdDomingo07;
        private System.Windows.Forms.RadioButton rdSabado06;
        private System.Windows.Forms.RadioButton rdSexta05;
        private System.Windows.Forms.RadioButton rdQuinta04;
        private System.Windows.Forms.RadioButton rdQuarta03;
        private System.Windows.Forms.RadioButton rdTerca02;
        private System.Windows.Forms.RadioButton rdSegunda01;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtValorMinimo;
        private System.Windows.Forms.CheckBox chTecPro;
        private System.Windows.Forms.CheckBox chTecPull;
        private System.Windows.Forms.Label lblTop;
        private System.Windows.Forms.TextBox txtTop;
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox cmTraiderId;
        private System.Windows.Forms.CheckBox chDigitalOTC;
        private System.Windows.Forms.ComboBox cmUser;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox chArquivarDiretorio;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtIdTrayder;
        private System.Windows.Forms.CheckBox chArquivarBanco;
    }
}

