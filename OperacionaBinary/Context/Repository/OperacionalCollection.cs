﻿using MongoDB.Driver;
using MongoDB.Bson;
using MongoDbCRUD.Models.Respository;
using System.Collections.Generic;
using OperacionaBinary.Context;

namespace MongoDbCRUD.Models
{
    // this class is our Product Collection (Product Table)
    public class OperacionalCollection
    {

        internal MongoDBRespository _repo = new MongoDBRespository();
        public IMongoCollection<OperacionalBynari> Collection;

        public OperacionalCollection()
        {
            // here we were created new Collection with Products name
            this.Collection = _repo.db.GetCollection<OperacionalBynari>("Operacional");
        }

        public void InsertOperacional(OperacionalBynari contact)
        {
            this.Collection.InsertOneAsync(contact);
        }

        public List<OperacionalBynari> GetAllOperacional()
        {
            var query = this.Collection
                .Find(new BsonDocument())
                .ToListAsync();
            return query.Result;
        }

        public OperacionalBynari GetOperacionalById(string id)
        {
            var product = this.Collection.Find(
                    new BsonDocument { { "_id", new ObjectId(id) } })
                    .FirstAsync()
                    .Result;
            return product;
        }

        public void UpdateContact(string id, OperacionalBynari contact)
        {
            contact.Id = new ObjectId(id);

            var filter = Builders<OperacionalBynari>
                .Filter
                .Eq(s => s.Id, contact.Id);
            this.Collection.ReplaceOneAsync(filter, contact);
        }

        public void DeleteContact(string OperacionalId)
        {
            OperacionalBynari contact = new OperacionalBynari();
            contact.Id = new ObjectId(OperacionalId);
            var filter = Builders<OperacionalBynari>.Filter.Eq(s => s.Id, contact.Id);
            this.Collection.DeleteOneAsync(filter);

        }
    }
}