﻿using MongoDB.Bson;
using MongoDB.Driver.Core.WireProtocol.Messages.Encoders;
using MongoDbCRUD.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OperacionaBinary
{
    public partial class AutUsuario : Form
    {
        private UsuarioCollection dbUsuario = new UsuarioCollection();

        
        public AutUsuario()
        {
            InitializeComponent();
        }

        string usuarioID = "";
        private void AutUsuario_Load(object sender, EventArgs e)
        {
            grdDadosUser.Visible = false;
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var consulta = dbUsuario.GetAllUsuario().ToList();
            var userAut = consulta.Find(c => c.Email == txtEmail.Text);

            if (userAut != null)
            {
                grdDadosUser.DataSource = userAut;
                //grdDadosUser.Columns["Nome"].HeaderText = "Nome Do Usuario"; // altera cabeçalho da coluna

                grdDadosUser.Visible = true;
                txtAut.Text = userAut.Autenticacao.ToString();
                cbConta.Text = userAut.Conta;
            }
            else
            {
                MessageBox.Show("Não foi possivel localizar usuario");
            }     
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var numeroAutenticate = "";

            for (int i = 0; i < 1; i++)
            {
                Random random = new Random();
                numeroAutenticate = numeroAutenticate + random.Next(4, 1000).ToString();
            }              
            

            if (!chaAut.Checked)
                numeroAutenticate = txtAut.Text;

            var consulta = dbUsuario.GetAllUsuario().ToList();
            var userAut = consulta.Find(c => c.Email == txtEmail.Text);

            var usuarrioDTO = new Usuario
            {
                Id = userAut.Id,
                Nome = userAut.Nome,
                Cpf = userAut.Cpf,
                Email = userAut.Email,
                Senha = userAut.Senha,
                Email_IQ = userAut.Email_IQ,
                Senha_IQ = userAut.Senha_IQ,
                DataCadastro = userAut.DataCadastro,
                Autenticacao = Convert.ToInt32(numeroAutenticate),
                Conta = cbConta.Text

            };

            try
            {
                dbUsuario.UpdateContact(userAut.Id.ToString(), usuarrioDTO);
                MessageBox.Show("Token Gravado Com Sucesso", "Autenticação", MessageBoxButtons.OK);
            }
            catch (Exception)
            {
                MessageBox.Show("Senha não confere,Tente novamente", "Validador De Email", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }

            txtEmail.Clear();
            grdDadosUser.Enabled = false;
            txtAut.Clear();
            chaAut.Checked = false;         

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Menu mainMenu = new Menu(usuarioID);
            mainMenu.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            dbUsuario.DeleteContact(usuarioID);
        }
    }
}
