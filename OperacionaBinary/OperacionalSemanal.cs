﻿using MongoDbCRUD.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OperacionaBinary
{
    public partial class OperacionalSemanal : Form
    {
        string usuarioId = "";
        bool cadOperacionalSemana = false;
        private OperacionalCollection db = new OperacionalCollection();

        public OperacionalSemanal(string usuarioID)
        {
            InitializeComponent();

            usuarioId = usuarioID;
        }

        public OperacionalSemanal(string userId, bool cadOperacional)
        {
            cadOperacionalSemana = cadOperacional;
            usuarioId = userId;
            InitializeComponent();
        }

        private void OperacionalSemanal_Load(object sender, EventArgs e)
        {

            var diaAtual = GetDataEx();
            // BloqueiroRd(diaAtual);
            this.Hide();
            CadastroOperacional operacional = new CadastroOperacional(usuarioId, true, false);


        }

        public string GetDataEx()
        {
            //Mês INT
            int mes = 12;
            string mesExtenso;
            string diaExtenso;

            ////Mês em português por extenso
            //mesExtenso = new DateTime(1900, mes, 1).ToString("MMMM", new CultureInfo("pt-BR"));
            ////Mês abreviado em português também.
            //mesExtenso = new CultureInfo("pt-BR").DateTimeFormat.GetAbbreviatedMonthName(mes);
            ////Mês (int) por extenso com primeira letra maiúscula.
            //string month = new CultureInfo("pt-BR").DateTimeFormat.GetMonthName(mes);
            //mesExtenso = char.ToUpper(month[0]) + month.Substring(1);

            ////Dia da semana (int) por extenso em português (segunda-feira)
            // diaExtenso = new CultureInfo("pt-BR").DateTimeFormat.GetDayName((DayOfWeek)1);
            ////Dia da semana abreviado (seg)
            //diaExtenso = new CultureInfo("pt-BR").DateTimeFormat.GetAbbreviatedDayName(DayOfWeek.Monday);
            ////Dia atual por extenso
            //diaExtenso = DateTime.Now.ToString("dddd", new CultureInfo("pt-BR"));
            //Dia por extenso com primeira letra maiúscula.
            string day = new CultureInfo("pt-BR").DateTimeFormat.GetDayName(DateTime.Now.DayOfWeek);
            diaExtenso = char.ToUpper(day[0]) + day.Substring(1);

            return diaExtenso;
        }

        public void BloqueiroRd(string diaSemana, int dia)
        {

            //Semana01
            if (diaSemana == "Segunda-feira" || dia == 1)
            {
                rdTeca01.Enabled = true;
                rdTeca05.Enabled = true;
                rdTeca02.Enabled = true;
                rdTeca03.Enabled = true;
                rdTeca04.Enabled = true;

                pnDiaSemana01.Enabled = true;

                rdTeca0201.Enabled = false;
                rdTeca0205.Enabled = false;
                rdTeca0202.Enabled = false;
                rdTeca0203.Enabled = false;
                rdTeca0204.Enabled = false;

                pnDiaSemana02.Enabled = false;

                rdTeca0401.Enabled = false;
                rdTeca0405.Enabled = false;
                rdTeca0402.Enabled = false;
                rdTeca0403.Enabled = false;
                rdTeca0404.Enabled = false;

                pnDiaSemana04.Enabled = false;

                rdTeca0301.Enabled = false;
                rdTeca0305.Enabled = false;
                rdTeca0302.Enabled = false;
                rdTeca0303.Enabled = false;
                rdTeca0304.Enabled = false;

                pnDiaSemana03.Enabled = false;


            }

            //Semana02
            if (diaSemana == "Terça-feira" || dia == 2)
            {


                rdTeca01.Enabled = false;
                rdTeca05.Enabled = false;
                rdTeca02.Enabled = false;
                rdTeca03.Enabled = false;
                rdTeca04.Enabled = false;

                pnDiaSemana01.Enabled = false;

                rdTeca0201.Enabled = true;
                rdTeca0205.Enabled = true;
                rdTeca0202.Enabled = true;
                rdTeca0203.Enabled = true;
                rdTeca0204.Enabled = true;

                pnDiaSemana02.Enabled = true;

                rdTeca0401.Enabled = false;
                rdTeca0405.Enabled = false;
                rdTeca0402.Enabled = false;
                rdTeca0403.Enabled = false;
                rdTeca0404.Enabled = false;

                pnDiaSemana04.Enabled = false;

                rdTeca0301.Enabled = false;
                rdTeca0305.Enabled = false;
                rdTeca0302.Enabled = false;
                rdTeca0303.Enabled = false;
                rdTeca0304.Enabled = false;

                pnDiaSemana03.Enabled = false;



            }

            //Semana03
            if (diaSemana == "Quarta-feira" || dia == 3)
            {
                rdTeca01.Enabled = false;
                rdTeca05.Enabled = false;
                rdTeca02.Enabled = false;
                rdTeca03.Enabled = false;
                rdTeca04.Enabled = false;

                pnDiaSemana01.Enabled = false;

                rdTeca0201.Enabled = false;
                rdTeca0205.Enabled = false;
                rdTeca0202.Enabled = false;
                rdTeca0203.Enabled = false;
                rdTeca0204.Enabled = false;

                pnDiaSemana02.Enabled = false;

                rdTeca0401.Enabled = true;
                rdTeca0405.Enabled = true;
                rdTeca0402.Enabled = true;
                rdTeca0403.Enabled = true;
                rdTeca0404.Enabled = true;

                pnDiaSemana04.Enabled = true;

                rdTeca0301.Enabled = false;
                rdTeca0305.Enabled = false;
                rdTeca0302.Enabled = false;
                rdTeca0303.Enabled = false;
                rdTeca0304.Enabled = false;

                pnDiaSemana03.Enabled = false;


            }

            //Semana04
            if (diaSemana == "Quinta-feira" || dia == 4)
            {
                rdTeca01.Enabled = false;
                rdTeca05.Enabled = false;
                rdTeca02.Enabled = false;
                rdTeca03.Enabled = false;
                rdTeca04.Enabled = false;

                pnDiaSemana01.Enabled = false;

                rdTeca0201.Enabled = false;
                rdTeca0205.Enabled = false;
                rdTeca0202.Enabled = false;
                rdTeca0203.Enabled = false;
                rdTeca0204.Enabled = false;

                pnDiaSemana02.Enabled = false;



                rdTeca0401.Enabled = false;
                rdTeca0405.Enabled = false;
                rdTeca0402.Enabled = false;
                rdTeca0403.Enabled = false;
                rdTeca0404.Enabled = false;

                pnDiaSemana04.Enabled = false;

                rdTeca0301.Enabled = true;
                rdTeca0305.Enabled = true;
                rdTeca0302.Enabled = true;
                rdTeca0303.Enabled = true;
                rdTeca0304.Enabled = true;

                pnDiaSemana03.Enabled = true;


            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!chDigital.Checked && !chBinaria.Checked && !chOTCBynari.Checked && !chOTCDigital.Checked)
            {
                MessageBox.Show("Necesserio Informar Qual Paridade Deseja Alterar!", "Validador Dia Operacional", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                var segunda = pnDiaSemana01.Enabled;
                var terca   = pnDiaSemana02.Enabled;
                var quarta  = pnDiaSemana03.Enabled;
                var quinta  = pnDiaSemana04.Enabled;

                var tecnicaSemana01 = 0;
                var tecnica02 = 0;
                var tecnica03 = 0;
                var tecnica04 = 0;
                var tecnica05 = 0;
                var moeda = "";

                if (chDigital.Checked)
                    moeda = "Digital";
                if (chBinaria.Checked)
                    moeda = "Binaria";
                if (chOTCBynari.Checked)
                    moeda = "OTC-Binaria";
                if (chOTCDigital.Checked)
                    moeda = "OTC-Digital";

                tecnicaSemana01 = GetTecnicaSemanal(tecnicaSemana01);

                this.Hide();

                try
                {
                    if (segunda == true)
                    {

                        this.Hide();
                        AlteracaoOperacional alteracaoOperacional = new AlteracaoOperacional(usuarioId, 1, tecnicaSemana01, moeda, chDigital.Checked, chBinaria.Checked, chOTCBynari.Checked, chOTCDigital.Checked);
                        alteracaoOperacional.Show();


                    }
                    else if (terca == true)
                    {

                        this.Hide();
                        AlteracaoOperacional alteracaoOperacional = new AlteracaoOperacional(usuarioId, 2, tecnicaSemana01, moeda, chDigital.Checked, chBinaria.Checked, chOTCBynari.Checked, chOTCDigital.Checked);
                        alteracaoOperacional.Show();



                    }
                    else if (quarta == true)
                    {

                        this.Hide();
                        AlteracaoOperacional alteracaoOperacional = new AlteracaoOperacional(usuarioId, 3, tecnicaSemana01, moeda, chDigital.Checked, chBinaria.Checked, chOTCBynari.Checked, chOTCDigital.Checked);
                        alteracaoOperacional.Show();



                    }
                    else if (quinta == true)
                    {

                        this.Hide();
                        AlteracaoOperacional alteracaoOperacional = new AlteracaoOperacional(usuarioId, 4, tecnicaSemana01, moeda, chDigital.Checked, chBinaria.Checked, chOTCBynari.Checked, chOTCDigital.Checked);
                        alteracaoOperacional.Show();


                    }
                }
                catch (Exception)
                {

                    MessageBox.Show("Verifica os campos selecionados e tente novamente!", "Operacional", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
            }
        }

        public int GetTecnicaSemanal(int tecnicaSemana)
        {

            //Semana01
            if (pnDiaSemana01.Enabled)
            {
                var tec01 = rdTeca01.Checked;
                var tec02 = rdTeca02.Checked;
                var tec03 = rdTeca03.Checked;
                var tec04 = rdTeca04.Checked;
                var tec05 = rdTeca05.Checked;
                var tec06 = rdTeca06.Checked;

                if (!rdTeca01.Enabled && !rdTeca0201.Enabled && !rdTeca0401.Enabled && !rdTeca0301.Enabled)
                {
                    MessageBox.Show("Necesserio Informar Dia Da Semana!", "Validador Dia Operacional", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (tec01)
                        tecnicaSemana = 45316621;
                    else if (tec02)
                        tecnicaSemana = 22756290;
                    else if (tec03)
                        tecnicaSemana = 51292712;
                    else if (tec04)
                        tecnicaSemana = 45844787;
                    else if (tec05)
                        tecnicaSemana = 44707398;
                    else if (tec06)
                        tecnicaSemana = 42624866;

                    if (tec01 == false && tec02 == false && tec03 == false && tec04 == false && tec05 == false && tec06 == false)
                    {
                        MessageBox.Show("É Necessario Escolher Umas Das Tecnicas");
                    }
                }

            }

            //Semana02         
            if (pnDiaSemana02.Enabled)
            {
                var tec01 = rdTeca0201.Checked;
                var tec02 = rdTeca0202.Checked; 
                var tec03 = rdTeca0203.Checked; 
                var tec04 = rdTeca0204.Checked; 
                var tec05 = rdTeca0205.Checked;
                var tec06 = rdTeca0206.Checked;

                if (!rdTeca01.Enabled && !rdTeca0201.Enabled && !rdTeca0401.Enabled && !rdTeca0301.Enabled)
                {
                    MessageBox.Show("Necesserio Informar Dia Da Semana!", "Validador Dia Operacional", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (tec01)
                        tecnicaSemana = 45316621;
                    else if (tec02)
                        tecnicaSemana = 22756290;
                    else if (tec03)
                        tecnicaSemana = 51292712;
                    else if (tec04)
                        tecnicaSemana = 45844787;
                    else if (tec05)
                        tecnicaSemana = 44707398;
                    else if (tec06)
                        tecnicaSemana = 42624866;

                    if (tec01 == false && tec02 == false && tec03 == true && tec04 == true && tec05 == true)
                    {
                        MessageBox.Show("É Necessario Escolher Umas Das Tecnicas");
                    }
                }


            }

            //Semana03           
            if (pnDiaSemana03.Enabled)
            {
                var tec01 = rdTeca0401.Checked;
                var tec02 = rdTeca0402.Checked;
                var tec03 = rdTeca0403.Checked;
                var tec04 = rdTeca0405.Checked;
                var tec05 = rdTeca0404.Checked; 
                var tec06 = rdTeca0306.Checked;

                if (!rdTeca01.Enabled && !rdTeca0201.Enabled && !rdTeca0401.Enabled && !rdTeca0301.Enabled)
                {
                    MessageBox.Show("Necesserio Informar Dia Da Semana!", "Validador Dia Operacional", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (tec01)
                        tecnicaSemana = 45316621;
                    else if (tec02)
                        tecnicaSemana = 22756290;
                    else if (tec03)
                        tecnicaSemana = 51292712;
                    else if (tec04)
                        tecnicaSemana = 45844787;
                    else if (tec05)
                        tecnicaSemana = 44707398;
                    else if (tec06)
                        tecnicaSemana = 42624866;

                    if (tec01 == false && tec02 == false && tec03 == true && tec04 == true && tec05 == true)
                    {
                        MessageBox.Show("É Necessario Escolher Umas Das Tecnicas");

                    }
                }
            }

            //Semana04           
            if (pnDiaSemana04.Enabled)
            {
                var tec01 = rdTeca0301.Checked;
                var tec02 = rdTeca0302.Checked;
                var tec03 = rdTeca0303.Checked;
                var tec04 = rdTeca0304.Checked;
                var tec05 = rdTeca0305.Checked;
                var tec06 = rdTeca0406.Checked;

                if (!rdTeca01.Enabled && !rdTeca0201.Enabled && !rdTeca0401.Enabled && !rdTeca0301.Enabled)
                {
                    MessageBox.Show("Necesserio Informar Dia Da Semana!", "Validador Dia Operacional", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (tec01)
                        tecnicaSemana = 45316621;
                    else if (tec02)
                        tecnicaSemana = 22756290;
                    else if (tec03)
                        tecnicaSemana = 51292712;
                    else if (tec04)
                        tecnicaSemana = 45844787;
                    else if (tec05)
                        tecnicaSemana = 44707398;
                    else if (tec06)
                        tecnicaSemana = 42624866;

                    if (tec01 == false && tec02 == false && tec03 == true && tec04 == true && tec05 == true)
                    {
                        MessageBox.Show("É Necessario Escolher Umas Das Tecnicas");
                    }
                }
            }

            return tecnicaSemana;
        }

        private void btnAtualizar01_Click(object sender, EventArgs e)
        {
            BloqueiroRd("", 1);

        }
             

        private void btnAtualizar02_Click(object sender, EventArgs e)
        {
            BloqueiroRd("", 2);
        }

        private void btnAtualizar04_Click(object sender, EventArgs e)
        {
            BloqueiroRd("", 4);
        }

        private void btnAtualizar03_Click(object sender, EventArgs e)
        {
            BloqueiroRd("", 3);
        }

        private void chDigital_CheckedChanged(object sender, EventArgs e)
        {
            chBinaria.Enabled = true;
        }

        private void chBinaria_CheckedChanged(object sender, EventArgs e)
        {
            chDigital.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Menu mainMenu = new Menu(usuarioId);
            this.Hide();
            mainMenu.Show();
        }
    }
}
