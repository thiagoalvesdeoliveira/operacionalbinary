﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperacionaBinary.Context
{
    public class LoginUser
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [Required]
        [BsonElement("Senha")]
        public string Senha { get; set; }

        [Required]
        [BsonElement("Email")]
        public string Email { get; set; }
    }
}
