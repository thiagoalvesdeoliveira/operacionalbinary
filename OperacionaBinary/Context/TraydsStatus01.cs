﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MongoDbCRUD.Models
{
    public class TraydersStatus01
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [Required]
        [BsonElement("nome")]
        public string Nome { get; set; }

        [Required]
        [BsonElement("traiderId")]
        public int TrayderID { get; set; }

        [Required]
        [BsonElement("statusTrayder")]
        public string StatusTrayderAtual { get; set; }

        [Required]
        [BsonElement("contaTeste")]
        public bool ContaTeste { get; set; }

       
        [BsonElement("Tipo")]
        public string TipoMoeda { get; set; }

        [Required]
        [BsonElement("Paridade")]
        public string ParidadeOperando { get; set; }

    }
}