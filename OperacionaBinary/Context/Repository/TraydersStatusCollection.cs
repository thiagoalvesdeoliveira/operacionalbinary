﻿using MongoDB.Driver;
using MongoDB.Bson;
using MongoDbCRUD.Models.Respository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MongoDbCRUD.Models
{
    // this class is our Product Collection (Product Table)
    public class TraydersStatusCollection
    {

        internal MongoDBRespository _repo = new MongoDBRespository();
        public IMongoCollection<TraydersStatus01> Collection;

        public TraydersStatusCollection()
        {
            // here we were created new Collection with Products name
            this.Collection = _repo.db.GetCollection<TraydersStatus01>("TraydersStatus01");
        }

        public void InsertContact(TraydersStatus01 contact)
        {
            this.Collection.InsertOneAsync(contact);
        }

        public List<TraydersStatus01> GetAllStatusTrayder()
        {
            var query = this.Collection
                .Find(new BsonDocument())
                .ToListAsync();
            return query.Result;
        }

        public TraydersStatus01 GetStatusTrayderById(string id)
        {
            var StatusTrayder = this.Collection.Find(
                    new BsonDocument { { "_id", new ObjectId(id) } })
                    .FirstAsync()
                    .Result;
            return StatusTrayder;
        }

        //public void UpdateContact(string id, TraydersStatus contact)
        //{
        //    contact.Id = new ObjectId(id);

        //    var filter = Builders<TraydersStatus>
        //        .Filter
        //        .Eq(s => s.Id, contact.Id);
        //    this.Collection.ReplaceOneAsync(filter, contact);

        //}

        public void DeleteContact(string StatusTrayderId)
        {
            TraydersStatus01 contact = new TraydersStatus01();
            contact.Id = new ObjectId(StatusTrayderId);
            var filter = Builders<TraydersStatus01>.Filter.Eq(s => s.Id, contact.Id);
            this.Collection.DeleteOneAsync(filter);

        }
    }
}