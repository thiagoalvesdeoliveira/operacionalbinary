﻿using MongoDB.Driver;
using MongoDB.Bson;
using MongoDbCRUD.Models.Respository;
using System.Collections.Generic;
using OperacionaBinary;
using OperacionaBinary.Context;

namespace MongoDbCRUD.Models
{
    // this class is our Product Collection (Product Table)
    public class LoginUserCollection
    {

        internal MongoDBRespository _repo = new MongoDBRespository();
        public IMongoCollection<LoginUser> Collection;

        public LoginUserCollection()
        {
            // here we were created new Collection with Products name
            this.Collection = _repo.db.GetCollection<LoginUser>("LoginUser");
        }

        public void InsertContact(LoginUser contact)
        {
            this.Collection.InsertOneAsync(contact);
        }

        public List<LoginUser> GetAllLoginUser()
        {
            var query = this.Collection
                .Find(new BsonDocument())
                .ToListAsync();
            return query.Result;
        }

        public LoginUser GetLoginUserById(string id)
        {
            var product = this.Collection.Find(
                    new BsonDocument { { "_id", new ObjectId(id) } })
                    .FirstAsync()
                    .Result;
            return product;
        }

        //public void UpdateContact(string id, LoginUser contact)
        //{
        //    contact.Id = new ObjectId(id);

        //    var filter = Builders<LoginUser>
        //        .Filter
        //        .Eq(s => s.Id, contact.Id);
        //    this.Collection.ReplaceOneAsync(filter, contact);

        //}

        //public void DeleteContact(string LoginUserId)
        //{
        //    LoginUser contact = new LoginUser();
        //    contact.Id = new ObjectId(LoginUserId);
        //    var filter = Builders<LoginUser>.Filter.Eq(s => s.Id, contact.Id);
        //    this.Collection.DeleteOneAsync(filter);

        //}
    }
}