﻿namespace OperacionaBinary
{
    partial class OperacionalSemanal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.pnDiaSemana01 = new System.Windows.Forms.Panel();
            this.rdTeca06 = new System.Windows.Forms.RadioButton();
            this.rdTeca04 = new System.Windows.Forms.RadioButton();
            this.rdTeca03 = new System.Windows.Forms.RadioButton();
            this.rdTeca02 = new System.Windows.Forms.RadioButton();
            this.rdTeca05 = new System.Windows.Forms.RadioButton();
            this.rdTeca01 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pnDiaSemana02 = new System.Windows.Forms.Panel();
            this.rdTeca0206 = new System.Windows.Forms.RadioButton();
            this.rdTeca0204 = new System.Windows.Forms.RadioButton();
            this.rdTeca0203 = new System.Windows.Forms.RadioButton();
            this.rdTeca0202 = new System.Windows.Forms.RadioButton();
            this.rdTeca0205 = new System.Windows.Forms.RadioButton();
            this.rdTeca0201 = new System.Windows.Forms.RadioButton();
            this.pnDiaSemana04 = new System.Windows.Forms.Panel();
            this.rdTeca0406 = new System.Windows.Forms.RadioButton();
            this.rdTeca0404 = new System.Windows.Forms.RadioButton();
            this.rdTeca0403 = new System.Windows.Forms.RadioButton();
            this.rdTeca0402 = new System.Windows.Forms.RadioButton();
            this.rdTeca0405 = new System.Windows.Forms.RadioButton();
            this.rdTeca0401 = new System.Windows.Forms.RadioButton();
            this.pnDiaSemana03 = new System.Windows.Forms.Panel();
            this.rdTeca0306 = new System.Windows.Forms.RadioButton();
            this.rdTeca0304 = new System.Windows.Forms.RadioButton();
            this.rdTeca0303 = new System.Windows.Forms.RadioButton();
            this.rdTeca0302 = new System.Windows.Forms.RadioButton();
            this.rdTeca0305 = new System.Windows.Forms.RadioButton();
            this.rdTeca0301 = new System.Windows.Forms.RadioButton();
            this.btnAtualizar01 = new System.Windows.Forms.Button();
            this.btnAtualizar02 = new System.Windows.Forms.Button();
            this.btnAtualizar04 = new System.Windows.Forms.Button();
            this.btnAtualizar03 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chOTCBynari = new System.Windows.Forms.RadioButton();
            this.chOTCDigital = new System.Windows.Forms.RadioButton();
            this.chDigital = new System.Windows.Forms.RadioButton();
            this.chBinaria = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.pnDiaSemana01.SuspendLayout();
            this.pnDiaSemana02.SuspendLayout();
            this.pnDiaSemana04.SuspendLayout();
            this.pnDiaSemana03.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Font = new System.Drawing.Font("Wide Latin", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl.ForeColor = System.Drawing.Color.Navy;
            this.lbl.Location = new System.Drawing.Point(134, 9);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(766, 29);
            this.lbl.TabIndex = 1;
            this.lbl.Text = "Qual Operacional Deseja Modificar?";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Blue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(846, 465);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 38);
            this.button1.TabIndex = 3;
            this.button1.Text = "Pesquisar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pnDiaSemana01
            // 
            this.pnDiaSemana01.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnDiaSemana01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pnDiaSemana01.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnDiaSemana01.Controls.Add(this.rdTeca06);
            this.pnDiaSemana01.Controls.Add(this.rdTeca04);
            this.pnDiaSemana01.Controls.Add(this.rdTeca03);
            this.pnDiaSemana01.Controls.Add(this.rdTeca02);
            this.pnDiaSemana01.Controls.Add(this.rdTeca05);
            this.pnDiaSemana01.Controls.Add(this.rdTeca01);
            this.pnDiaSemana01.Enabled = false;
            this.pnDiaSemana01.Location = new System.Drawing.Point(12, 73);
            this.pnDiaSemana01.Name = "pnDiaSemana01";
            this.pnDiaSemana01.Size = new System.Drawing.Size(278, 284);
            this.pnDiaSemana01.TabIndex = 4;
            // 
            // rdTeca06
            // 
            this.rdTeca06.AutoSize = true;
            this.rdTeca06.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca06.Location = new System.Drawing.Point(4, 196);
            this.rdTeca06.Name = "rdTeca06";
            this.rdTeca06.Size = new System.Drawing.Size(118, 24);
            this.rdTeca06.TabIndex = 5;
            this.rdTeca06.TabStop = true;
            this.rdTeca06.Text = "6°TecnicaSw";
            this.rdTeca06.UseVisualStyleBackColor = true;
            // 
            // rdTeca04
            // 
            this.rdTeca04.AutoSize = true;
            this.rdTeca04.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca04.Location = new System.Drawing.Point(4, 125);
            this.rdTeca04.Name = "rdTeca04";
            this.rdTeca04.Size = new System.Drawing.Size(193, 24);
            this.rdTeca04.TabIndex = 4;
            this.rdTeca04.TabStop = true;
            this.rdTeca04.Text = "4°Tecnica(E:Moderada)";
            this.rdTeca04.UseVisualStyleBackColor = true;
            // 
            // rdTeca03
            // 
            this.rdTeca03.AutoSize = true;
            this.rdTeca03.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca03.Location = new System.Drawing.Point(3, 85);
            this.rdTeca03.Name = "rdTeca03";
            this.rdTeca03.Size = new System.Drawing.Size(260, 24);
            this.rdTeca03.TabIndex = 3;
            this.rdTeca03.TabStop = true;
            this.rdTeca03.Text = "3°Tenica(F:Moderada/Moderada)";
            this.rdTeca03.UseVisualStyleBackColor = true;
            // 
            // rdTeca02
            // 
            this.rdTeca02.AutoSize = true;
            this.rdTeca02.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca02.Location = new System.Drawing.Point(3, 44);
            this.rdTeca02.Name = "rdTeca02";
            this.rdTeca02.Size = new System.Drawing.Size(194, 24);
            this.rdTeca02.TabIndex = 2;
            this.rdTeca02.TabStop = true;
            this.rdTeca02.Text = "2°Tecnica(R:Moderada)";
            this.rdTeca02.UseVisualStyleBackColor = true;
            // 
            // rdTeca05
            // 
            this.rdTeca05.AutoSize = true;
            this.rdTeca05.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca05.Location = new System.Drawing.Point(4, 155);
            this.rdTeca05.Name = "rdTeca05";
            this.rdTeca05.Size = new System.Drawing.Size(267, 24);
            this.rdTeca05.TabIndex = 1;
            this.rdTeca05.TabStop = true;
            this.rdTeca05.Text = "5°Tecnica(T:Moderada/Moderada)";
            this.rdTeca05.UseVisualStyleBackColor = true;
            // 
            // rdTeca01
            // 
            this.rdTeca01.AutoSize = true;
            this.rdTeca01.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca01.Location = new System.Drawing.Point(3, 12);
            this.rdTeca01.Name = "rdTeca01";
            this.rdTeca01.Size = new System.Drawing.Size(193, 24);
            this.rdTeca01.TabIndex = 0;
            this.rdTeca01.TabStop = true;
            this.rdTeca01.Text = "1°Tecnica(C:Moderada)";
            this.rdTeca01.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label1.Font = new System.Drawing.Font("Wide Latin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 19);
            this.label1.TabIndex = 6;
            this.label1.Text = "01° Segunda";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label2.Font = new System.Drawing.Font("Wide Latin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(307, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 19);
            this.label2.TabIndex = 7;
            this.label2.Text = "02° Terça";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label3.Font = new System.Drawing.Font("Wide Latin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(603, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(166, 19);
            this.label3.TabIndex = 8;
            this.label3.Text = "03° Quarta";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label4.Font = new System.Drawing.Font("Wide Latin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(893, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 19);
            this.label4.TabIndex = 9;
            this.label4.Text = "04°Quinta";
            // 
            // pnDiaSemana02
            // 
            this.pnDiaSemana02.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnDiaSemana02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pnDiaSemana02.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnDiaSemana02.Controls.Add(this.rdTeca0206);
            this.pnDiaSemana02.Controls.Add(this.rdTeca0204);
            this.pnDiaSemana02.Controls.Add(this.rdTeca0203);
            this.pnDiaSemana02.Controls.Add(this.rdTeca0202);
            this.pnDiaSemana02.Controls.Add(this.rdTeca0205);
            this.pnDiaSemana02.Controls.Add(this.rdTeca0201);
            this.pnDiaSemana02.Enabled = false;
            this.pnDiaSemana02.Location = new System.Drawing.Point(306, 75);
            this.pnDiaSemana02.Name = "pnDiaSemana02";
            this.pnDiaSemana02.Size = new System.Drawing.Size(279, 282);
            this.pnDiaSemana02.TabIndex = 5;
            // 
            // rdTeca0206
            // 
            this.rdTeca0206.AutoSize = true;
            this.rdTeca0206.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0206.Location = new System.Drawing.Point(4, 217);
            this.rdTeca0206.Name = "rdTeca0206";
            this.rdTeca0206.Size = new System.Drawing.Size(118, 24);
            this.rdTeca0206.TabIndex = 6;
            this.rdTeca0206.TabStop = true;
            this.rdTeca0206.Text = "6°TecnicaSw";
            this.rdTeca0206.UseVisualStyleBackColor = true;
            // 
            // rdTeca0204
            // 
            this.rdTeca0204.AutoSize = true;
            this.rdTeca0204.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0204.Location = new System.Drawing.Point(4, 132);
            this.rdTeca0204.Name = "rdTeca0204";
            this.rdTeca0204.Size = new System.Drawing.Size(193, 24);
            this.rdTeca0204.TabIndex = 4;
            this.rdTeca0204.TabStop = true;
            this.rdTeca0204.Text = "4°Tecnica(E:Moderada)";
            this.rdTeca0204.UseVisualStyleBackColor = true;
            // 
            // rdTeca0203
            // 
            this.rdTeca0203.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdTeca0203.AutoSize = true;
            this.rdTeca0203.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0203.Location = new System.Drawing.Point(3, 100);
            this.rdTeca0203.Name = "rdTeca0203";
            this.rdTeca0203.Size = new System.Drawing.Size(260, 24);
            this.rdTeca0203.TabIndex = 3;
            this.rdTeca0203.TabStop = true;
            this.rdTeca0203.Text = "3°Tenica(F:Moderada/Moderada)";
            this.rdTeca0203.UseVisualStyleBackColor = true;
            // 
            // rdTeca0202
            // 
            this.rdTeca0202.AutoSize = true;
            this.rdTeca0202.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0202.Location = new System.Drawing.Point(3, 53);
            this.rdTeca0202.Name = "rdTeca0202";
            this.rdTeca0202.Size = new System.Drawing.Size(194, 24);
            this.rdTeca0202.TabIndex = 2;
            this.rdTeca0202.TabStop = true;
            this.rdTeca0202.Text = "2°Tecnica(R:Moderada)";
            this.rdTeca0202.UseVisualStyleBackColor = true;
            // 
            // rdTeca0205
            // 
            this.rdTeca0205.AutoSize = true;
            this.rdTeca0205.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0205.Location = new System.Drawing.Point(3, 174);
            this.rdTeca0205.Name = "rdTeca0205";
            this.rdTeca0205.Size = new System.Drawing.Size(267, 24);
            this.rdTeca0205.TabIndex = 1;
            this.rdTeca0205.TabStop = true;
            this.rdTeca0205.Text = "5°Tecnica(T:Moderada/Moderada)";
            this.rdTeca0205.UseVisualStyleBackColor = true;
            // 
            // rdTeca0201
            // 
            this.rdTeca0201.AutoSize = true;
            this.rdTeca0201.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0201.Location = new System.Drawing.Point(3, 12);
            this.rdTeca0201.Name = "rdTeca0201";
            this.rdTeca0201.Size = new System.Drawing.Size(193, 24);
            this.rdTeca0201.TabIndex = 0;
            this.rdTeca0201.TabStop = true;
            this.rdTeca0201.Text = "1°Tecnica(C:Moderada)";
            this.rdTeca0201.UseVisualStyleBackColor = true;
            // 
            // pnDiaSemana04
            // 
            this.pnDiaSemana04.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnDiaSemana04.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pnDiaSemana04.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnDiaSemana04.Controls.Add(this.rdTeca0406);
            this.pnDiaSemana04.Controls.Add(this.rdTeca0404);
            this.pnDiaSemana04.Controls.Add(this.rdTeca0403);
            this.pnDiaSemana04.Controls.Add(this.rdTeca0402);
            this.pnDiaSemana04.Controls.Add(this.rdTeca0405);
            this.pnDiaSemana04.Controls.Add(this.rdTeca0401);
            this.pnDiaSemana04.Enabled = false;
            this.pnDiaSemana04.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnDiaSemana04.Location = new System.Drawing.Point(892, 75);
            this.pnDiaSemana04.Name = "pnDiaSemana04";
            this.pnDiaSemana04.Size = new System.Drawing.Size(266, 282);
            this.pnDiaSemana04.TabIndex = 5;
            // 
            // rdTeca0406
            // 
            this.rdTeca0406.AutoSize = true;
            this.rdTeca0406.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0406.Location = new System.Drawing.Point(3, 194);
            this.rdTeca0406.Name = "rdTeca0406";
            this.rdTeca0406.Size = new System.Drawing.Size(118, 24);
            this.rdTeca0406.TabIndex = 8;
            this.rdTeca0406.TabStop = true;
            this.rdTeca0406.Text = "6°TecnicaSw";
            this.rdTeca0406.UseVisualStyleBackColor = true;
            // 
            // rdTeca0404
            // 
            this.rdTeca0404.AutoSize = true;
            this.rdTeca0404.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0404.Location = new System.Drawing.Point(3, 102);
            this.rdTeca0404.Name = "rdTeca0404";
            this.rdTeca0404.Size = new System.Drawing.Size(193, 24);
            this.rdTeca0404.TabIndex = 4;
            this.rdTeca0404.TabStop = true;
            this.rdTeca0404.Text = "4°Tecnica(E:Moderada)";
            this.rdTeca0404.UseVisualStyleBackColor = true;
            // 
            // rdTeca0403
            // 
            this.rdTeca0403.AutoSize = true;
            this.rdTeca0403.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0403.Location = new System.Drawing.Point(3, 72);
            this.rdTeca0403.Name = "rdTeca0403";
            this.rdTeca0403.Size = new System.Drawing.Size(260, 24);
            this.rdTeca0403.TabIndex = 3;
            this.rdTeca0403.TabStop = true;
            this.rdTeca0403.Text = "3°Tenica(F:Moderada/Moderada)";
            this.rdTeca0403.UseVisualStyleBackColor = true;
            // 
            // rdTeca0402
            // 
            this.rdTeca0402.AutoSize = true;
            this.rdTeca0402.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0402.Location = new System.Drawing.Point(3, 42);
            this.rdTeca0402.Name = "rdTeca0402";
            this.rdTeca0402.Size = new System.Drawing.Size(194, 24);
            this.rdTeca0402.TabIndex = 2;
            this.rdTeca0402.TabStop = true;
            this.rdTeca0402.Text = "2°Tecnica(R:Moderada)";
            this.rdTeca0402.UseVisualStyleBackColor = true;
            // 
            // rdTeca0405
            // 
            this.rdTeca0405.AutoSize = true;
            this.rdTeca0405.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0405.Location = new System.Drawing.Point(3, 144);
            this.rdTeca0405.Name = "rdTeca0405";
            this.rdTeca0405.Size = new System.Drawing.Size(267, 24);
            this.rdTeca0405.TabIndex = 1;
            this.rdTeca0405.TabStop = true;
            this.rdTeca0405.Text = "5°Tecnica(T:Moderada/Moderada)";
            this.rdTeca0405.UseVisualStyleBackColor = true;
            // 
            // rdTeca0401
            // 
            this.rdTeca0401.AutoSize = true;
            this.rdTeca0401.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0401.Location = new System.Drawing.Point(3, 12);
            this.rdTeca0401.Name = "rdTeca0401";
            this.rdTeca0401.Size = new System.Drawing.Size(193, 24);
            this.rdTeca0401.TabIndex = 0;
            this.rdTeca0401.TabStop = true;
            this.rdTeca0401.Text = "1°Tecnica(C:Moderada)";
            this.rdTeca0401.UseVisualStyleBackColor = true;
            // 
            // pnDiaSemana03
            // 
            this.pnDiaSemana03.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnDiaSemana03.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pnDiaSemana03.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnDiaSemana03.Controls.Add(this.rdTeca0306);
            this.pnDiaSemana03.Controls.Add(this.rdTeca0304);
            this.pnDiaSemana03.Controls.Add(this.rdTeca0303);
            this.pnDiaSemana03.Controls.Add(this.rdTeca0302);
            this.pnDiaSemana03.Controls.Add(this.rdTeca0305);
            this.pnDiaSemana03.Controls.Add(this.rdTeca0301);
            this.pnDiaSemana03.Enabled = false;
            this.pnDiaSemana03.Location = new System.Drawing.Point(602, 75);
            this.pnDiaSemana03.Name = "pnDiaSemana03";
            this.pnDiaSemana03.Size = new System.Drawing.Size(266, 282);
            this.pnDiaSemana03.TabIndex = 5;
            // 
            // rdTeca0306
            // 
            this.rdTeca0306.AutoSize = true;
            this.rdTeca0306.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0306.Location = new System.Drawing.Point(3, 229);
            this.rdTeca0306.Name = "rdTeca0306";
            this.rdTeca0306.Size = new System.Drawing.Size(118, 24);
            this.rdTeca0306.TabIndex = 7;
            this.rdTeca0306.TabStop = true;
            this.rdTeca0306.Text = "6°TecnicaSw";
            this.rdTeca0306.UseVisualStyleBackColor = true;
            // 
            // rdTeca0304
            // 
            this.rdTeca0304.AutoSize = true;
            this.rdTeca0304.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0304.Location = new System.Drawing.Point(4, 132);
            this.rdTeca0304.Name = "rdTeca0304";
            this.rdTeca0304.Size = new System.Drawing.Size(193, 24);
            this.rdTeca0304.TabIndex = 4;
            this.rdTeca0304.TabStop = true;
            this.rdTeca0304.Text = "4°Tecnica(E:Moderada)";
            this.rdTeca0304.UseVisualStyleBackColor = true;
            // 
            // rdTeca0303
            // 
            this.rdTeca0303.AutoSize = true;
            this.rdTeca0303.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0303.Location = new System.Drawing.Point(3, 83);
            this.rdTeca0303.Name = "rdTeca0303";
            this.rdTeca0303.Size = new System.Drawing.Size(260, 24);
            this.rdTeca0303.TabIndex = 3;
            this.rdTeca0303.TabStop = true;
            this.rdTeca0303.Text = "3°Tenica(F:Moderada/Moderada)";
            this.rdTeca0303.UseVisualStyleBackColor = true;
            // 
            // rdTeca0302
            // 
            this.rdTeca0302.AutoSize = true;
            this.rdTeca0302.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0302.Location = new System.Drawing.Point(3, 53);
            this.rdTeca0302.Name = "rdTeca0302";
            this.rdTeca0302.Size = new System.Drawing.Size(194, 24);
            this.rdTeca0302.TabIndex = 2;
            this.rdTeca0302.TabStop = true;
            this.rdTeca0302.Text = "2°Tecnica(R:Moderada)";
            this.rdTeca0302.UseVisualStyleBackColor = true;
            // 
            // rdTeca0305
            // 
            this.rdTeca0305.AutoSize = true;
            this.rdTeca0305.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0305.Location = new System.Drawing.Point(-2, 174);
            this.rdTeca0305.Name = "rdTeca0305";
            this.rdTeca0305.Size = new System.Drawing.Size(267, 24);
            this.rdTeca0305.TabIndex = 1;
            this.rdTeca0305.TabStop = true;
            this.rdTeca0305.Text = "5°Tecnica(T:Moderada/Moderada)";
            this.rdTeca0305.UseVisualStyleBackColor = true;
            // 
            // rdTeca0301
            // 
            this.rdTeca0301.AutoSize = true;
            this.rdTeca0301.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdTeca0301.Location = new System.Drawing.Point(3, 12);
            this.rdTeca0301.Name = "rdTeca0301";
            this.rdTeca0301.Size = new System.Drawing.Size(193, 24);
            this.rdTeca0301.TabIndex = 0;
            this.rdTeca0301.TabStop = true;
            this.rdTeca0301.Text = "1°Tecnica(C:Moderada)";
            this.rdTeca0301.UseVisualStyleBackColor = true;
            // 
            // btnAtualizar01
            // 
            this.btnAtualizar01.BackColor = System.Drawing.Color.Silver;
            this.btnAtualizar01.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAtualizar01.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAtualizar01.Location = new System.Drawing.Point(12, 363);
            this.btnAtualizar01.Name = "btnAtualizar01";
            this.btnAtualizar01.Size = new System.Drawing.Size(65, 32);
            this.btnAtualizar01.TabIndex = 10;
            this.btnAtualizar01.Text = "Atualizar";
            this.btnAtualizar01.UseVisualStyleBackColor = false;
            this.btnAtualizar01.Click += new System.EventHandler(this.btnAtualizar01_Click);
            // 
            // btnAtualizar02
            // 
            this.btnAtualizar02.BackColor = System.Drawing.Color.Silver;
            this.btnAtualizar02.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAtualizar02.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAtualizar02.Location = new System.Drawing.Point(306, 363);
            this.btnAtualizar02.Name = "btnAtualizar02";
            this.btnAtualizar02.Size = new System.Drawing.Size(65, 32);
            this.btnAtualizar02.TabIndex = 11;
            this.btnAtualizar02.Text = "Atualizar";
            this.btnAtualizar02.UseVisualStyleBackColor = false;
            this.btnAtualizar02.Click += new System.EventHandler(this.btnAtualizar02_Click);
            // 
            // btnAtualizar04
            // 
            this.btnAtualizar04.BackColor = System.Drawing.Color.Silver;
            this.btnAtualizar04.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAtualizar04.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAtualizar04.Location = new System.Drawing.Point(602, 363);
            this.btnAtualizar04.Name = "btnAtualizar04";
            this.btnAtualizar04.Size = new System.Drawing.Size(65, 32);
            this.btnAtualizar04.TabIndex = 12;
            this.btnAtualizar04.Text = "Atualizar";
            this.btnAtualizar04.UseVisualStyleBackColor = false;
            this.btnAtualizar04.Click += new System.EventHandler(this.btnAtualizar04_Click);
            // 
            // btnAtualizar03
            // 
            this.btnAtualizar03.BackColor = System.Drawing.Color.Silver;
            this.btnAtualizar03.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAtualizar03.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAtualizar03.Location = new System.Drawing.Point(892, 363);
            this.btnAtualizar03.Name = "btnAtualizar03";
            this.btnAtualizar03.Size = new System.Drawing.Size(65, 32);
            this.btnAtualizar03.TabIndex = 12;
            this.btnAtualizar03.Text = "Atualizar";
            this.btnAtualizar03.UseVisualStyleBackColor = false;
            this.btnAtualizar03.Click += new System.EventHandler(this.btnAtualizar03_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.chOTCBynari);
            this.panel1.Controls.Add(this.chOTCDigital);
            this.panel1.Controls.Add(this.chDigital);
            this.panel1.Controls.Add(this.chBinaria);
            this.panel1.Location = new System.Drawing.Point(371, 413);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(316, 100);
            this.panel1.TabIndex = 14;
            // 
            // chOTCBynari
            // 
            this.chOTCBynari.AutoSize = true;
            this.chOTCBynari.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chOTCBynari.Location = new System.Drawing.Point(161, 64);
            this.chOTCBynari.Name = "chOTCBynari";
            this.chOTCBynari.Size = new System.Drawing.Size(136, 24);
            this.chOTCBynari.TabIndex = 3;
            this.chOTCBynari.TabStop = true;
            this.chOTCBynari.Text = "OTC-BINARYA";
            this.chOTCBynari.UseVisualStyleBackColor = true;
            // 
            // chOTCDigital
            // 
            this.chOTCDigital.AutoSize = true;
            this.chOTCDigital.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chOTCDigital.Location = new System.Drawing.Point(3, 64);
            this.chOTCDigital.Name = "chOTCDigital";
            this.chOTCDigital.Size = new System.Drawing.Size(128, 24);
            this.chOTCDigital.TabIndex = 2;
            this.chOTCDigital.TabStop = true;
            this.chOTCDigital.Text = "OTC-DIGITAL";
            this.chOTCDigital.UseVisualStyleBackColor = true;
            // 
            // chDigital
            // 
            this.chDigital.AutoSize = true;
            this.chDigital.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chDigital.Location = new System.Drawing.Point(161, 17);
            this.chDigital.Name = "chDigital";
            this.chDigital.Size = new System.Drawing.Size(91, 24);
            this.chDigital.TabIndex = 1;
            this.chDigital.TabStop = true;
            this.chDigital.Text = "DIGITAL";
            this.chDigital.UseVisualStyleBackColor = true;
            // 
            // chBinaria
            // 
            this.chBinaria.AutoSize = true;
            this.chBinaria.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chBinaria.Location = new System.Drawing.Point(3, 15);
            this.chBinaria.Name = "chBinaria";
            this.chBinaria.Size = new System.Drawing.Size(93, 24);
            this.chBinaria.TabIndex = 0;
            this.chBinaria.TabStop = true;
            this.chBinaria.Text = "BINARIA";
            this.chBinaria.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(2, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(107, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "< Voltar Menu";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // OperacionalSemanal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1194, 577);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnAtualizar03);
            this.Controls.Add(this.btnAtualizar04);
            this.Controls.Add(this.btnAtualizar01);
            this.Controls.Add(this.btnAtualizar02);
            this.Controls.Add(this.pnDiaSemana03);
            this.Controls.Add(this.pnDiaSemana04);
            this.Controls.Add(this.pnDiaSemana02);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnDiaSemana01);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbl);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OperacionalSemanal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ALTERAÇÃO OPERACIONAL";
            this.Load += new System.EventHandler(this.OperacionalSemanal_Load);
            this.pnDiaSemana01.ResumeLayout(false);
            this.pnDiaSemana01.PerformLayout();
            this.pnDiaSemana02.ResumeLayout(false);
            this.pnDiaSemana02.PerformLayout();
            this.pnDiaSemana04.ResumeLayout(false);
            this.pnDiaSemana04.PerformLayout();
            this.pnDiaSemana03.ResumeLayout(false);
            this.pnDiaSemana03.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pnDiaSemana01;
        private System.Windows.Forms.RadioButton rdTeca01;
        private System.Windows.Forms.RadioButton rdTeca04;
        private System.Windows.Forms.RadioButton rdTeca03;
        private System.Windows.Forms.RadioButton rdTeca02;
        private System.Windows.Forms.RadioButton rdTeca05;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pnDiaSemana02;
        private System.Windows.Forms.RadioButton rdTeca0204;
        private System.Windows.Forms.RadioButton rdTeca0203;
        private System.Windows.Forms.RadioButton rdTeca0202;
        private System.Windows.Forms.RadioButton rdTeca0205;
        private System.Windows.Forms.RadioButton rdTeca0201;
        private System.Windows.Forms.Panel pnDiaSemana04;
        private System.Windows.Forms.RadioButton rdTeca0404;
        private System.Windows.Forms.RadioButton rdTeca0403;
        private System.Windows.Forms.RadioButton rdTeca0402;
        private System.Windows.Forms.RadioButton rdTeca0405;
        private System.Windows.Forms.RadioButton rdTeca0401;
        private System.Windows.Forms.Panel pnDiaSemana03;
        private System.Windows.Forms.RadioButton rdTeca0304;
        private System.Windows.Forms.RadioButton rdTeca0303;
        private System.Windows.Forms.RadioButton rdTeca0302;
        private System.Windows.Forms.RadioButton rdTeca0305;
        private System.Windows.Forms.RadioButton rdTeca0301;
        private System.Windows.Forms.Button btnAtualizar01;
        private System.Windows.Forms.Button btnAtualizar02;
        private System.Windows.Forms.Button btnAtualizar04;
        private System.Windows.Forms.Button btnAtualizar03;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton chOTCBynari;
        private System.Windows.Forms.RadioButton chOTCDigital;
        private System.Windows.Forms.RadioButton chDigital;
        private System.Windows.Forms.RadioButton chBinaria;
        private System.Windows.Forms.RadioButton rdTeca06;
        private System.Windows.Forms.RadioButton rdTeca0206;
        private System.Windows.Forms.RadioButton rdTeca0406;
        private System.Windows.Forms.RadioButton rdTeca0306;
        private System.Windows.Forms.Button button2;
    }
}