﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OperacionaBinary.Extensions;
using RestSharp;

namespace OperacionaBinary.API
{
    public class ConectAPI : BaseApi
    {
        private readonly string _transactionUrl = "http://127.0.0.1:5000";

        public async Task  GetParidade()
        {
            //await Task.Delay(60000);
            var request = CreateRequest("/start", Method.GET);

           

            var client = CreateClient(_transactionUrl);
           
            var response =  client.ExecuteDynamic(request);

            VerifyResponse(response);
          
        }       
    }
}
